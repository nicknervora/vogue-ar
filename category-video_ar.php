<?php

  get_header();

  $video_posts = getQuery__VideoPosts();

?>

  <div class="page page--videos">

    <div class="container container--mid">

<?php $post = FEATURED_VIDEO; setup_postdata($post); ?>
      <div class="post--item post--full video--featured">

        <?php getVideoPlayer( $post ); ?>

        <?php getFeedItem( $i = 0, $post, 'video' ); ?>

      </div>
<?php wp_reset_postdata(); ?>

    </div>

<?php getBreak_TrendingVideos(); ?>

<?php

  if ( !isset( $_GET['sort'] ) || ( isset( $_GET['sort'] ) && $_GET['sort'] == 'recent' ) ) : $sort = 'recent'; else : $sort = 'popular'; endif;

  //if ( CLEAR_QUERY_CACHE || CLEAR_QUERY_CACHE !== false ) : delete_transient( 'video_postlist-'.$sort.'-'.$paged ); endif; if ( false === ( $video_postlist = get_transient( 'video_postlist-'.$sort.'-'.$paged ) ) ) {  
	$video_postlist = get_posts($video_posts); //set_transient( 'video_postlist-'.$sort.'-'.$paged, $video_postlist, 12 * 60 * 60 ); } ?>

<?php if ( $video_postlist ) : ?>

  <section class="video-post-feed">

    <div class="container container--mid--reduced">

      <h3 class="section--header"> <?php echo __( 'Recent Videos' ); ?> </h3>

      <div class="video-posts">

        <div class="video-posts__filter">
          <a href="<?php get_term_link( 'video_ar', 'category' ); ?>?sort=recent" class="toggle-filter toggle-filter--recent toggle-filter<?php if ( !isset( $_GET['sort'] ) || ( isset( $_GET['sort'] ) && $_GET['sort'] == 'recent' ) ) : echo ' is-active'; endif; ?>"><?php echo __( 'Most Recent', 'vogue.me' ); ?></a>
          <a href="<?php get_term_link( 'video_ar', 'category' ); ?>?sort=popular" class="toggle-filter toggle-filter--popular<?php if ( isset( $_GET['sort'] ) && $_GET['sort'] == 'popular' ) : echo ' is-active'; endif; ?>"><?php echo __( 'Most Popular', 'vogue.me' ); ?></a>
        </div>

        <div class="scroll" data-ui="jscroll-default" data-continue="false">

          <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
          <div class="get--content loaded" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('single','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('single','pageid'); ?>" <?php if (count($video_postlist) < 9) { echo "data-end-reached"; } ?>>


              <?php $i = 1; foreach ( $video_postlist as $post ) :

                  if ($i < 9)
                  {
                      setup_postdata( $post );

                      if ( $i % 4 == 1 ) : ?>
                          <div class="row">
                      <?php endif;

                      $thumbnail = getThumbnail()[0];

                      ?>
                      <article class="video-post js-video-post">
                          <a href="<?php echo get_permalink(); ?>">
                          <div class="video-post__thumb">
                              <img class="auto-fade-in" src="<?php echo imageProvider( get_field( 'poster_image' )['url'], 255 ); ?>" alt="">
                          </div>
                          <div class="video-post__content">
                              <h2><?php the_title(); ?></h2>
                              <?php getKeyTopic(); ?>
                          </div>
                          </a> 
                      </article>

                      <?php 
                      if ( $i % 4 == 0 ) : ?>
                          </div>
                      <?php endif; 

                      $i++;
                  }

              endforeach;

              wp_reset_postdata(); 

              if ($i % 4 != 1): ?>
                  </div>
              <?php endif;

              $next_link = get_next_posts_link();

              if ($next_link) : ?>

                  <button type="button" data-url="<?php echo get_next_posts_page_link(); ?>" class="button black js-load-more"><?php echo __( 'Load more videos', 'vogue.me' ); ?> <?php inline_svg( 'va-gallery-next-white' ); ?> </button>

              <?php endif; ?>

          </div>
          <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

        </div>

      </div>

    </div>

  </section>

<?php endif; ?>

  </div>

<?php get_footer(); ?>