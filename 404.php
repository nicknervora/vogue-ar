<?php get_header(); ?>

<?php $image = get_field( 'image_404', 'option' ); ?>

<div class="screen-404" style="background-image: url( '<?php echo $image; ?>' ); ">

  <div class="inner-404">

    <h3 class="inner__title"><?php echo __( 'Sincerely, not found', 'vogue.me' ); ?></h3>

    <a href="<?php bloginfo('url'); ?>" class="button inline border white"><?php echo __( 'Go to homepage', 'vogue.me' ); ?></a>

  </div>

</div>

<?php get_footer(); ?>

