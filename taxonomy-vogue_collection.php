<?php get_header();

  $tag       = get_queried_object();
  $tag_title = $tag->name; // Same as single_tag_title()
  $tag_slug  = $tag->slug;
  $tag_id    = $tag->term_id;
  $tag_object = $tag->taxonomy;

  $colour = 'white';

  $get_taxonomy_term = $tag_object . '_' . $tag_id;

  $header_bg = getArchiveHeaderBG( $get_taxonomy_term );

  $header_color = getArchiveHeaderCOLOR( $get_taxonomy_term );

  $get_user = false;

  $video_cat_id = get_category_by_slug( 'video' )->term_id;

  $ignore = array();

  $ignore[] = $video_cat_id;
  foreach( getRunwayCats() as $cat )
  {
    $ignore[] = $cat;
  }

?>




    <div class="hero hero--sb">

      <div class="hero__bg"<?php if ( $header_bg ) : echo ' style="background-image: url(' . $header_bg . ');"'; endif; ?>></div>

    </div>

    <section class="pc pc--sb sb">

      <div class="pc__header pc-header">

        <div class="container container--mid--reduced">

          <div class="pc-header__inner">

            <div class="pc-header__top">

              <h1 class="pc-header__title"><?php echo $tag_title; ?></h1>
              <?php if ( get_field( 'social_profiles', $get_taxonomy_term ) ) : $social = get_field( 'social_profiles', $get_taxonomy_term ); ?>
              <ul class="pc-social">
            <?php foreach ( $social as $profile ) : $network = $profile['network']; $account = $profile['tag']; ?>
            <?php if ( $network == 'instagram' ) : $get_user = $account; endif; ?>
                <li><a class="fa fa-<?php echo $network; ?>" href="<?php echo get_social_url( $network, $account ); ?>" target="_blank"></a></li>
            <?php endforeach; ?>
              </ul>
            <?php endif; ?>
            </div>

            <div class="pc-header__search">
              <?php VogueCollectionSearch(); ?>
            </div>

          </div>

        </div>

      </div>


    <?php if ( get_field( 'born', $get_taxonomy_term ) || get_field( 'nationality', $get_taxonomy_term ) || get_field( 'location', $get_taxonomy_term ) || get_field( 'biography', $get_taxonomy_term ) ) : ?>

      <section class="sb-info">
      
        <div class="container container--mid">
        
          <div class="sb-info__inner">
          
            <div class="sb-info__left">
            
              <h2 class="sb-info__title"><?php echo __( 'Personal Information','vogue.me' ); ?></h2>
          <?php if ( get_field( 'born', $get_taxonomy_term ) ) : ?> <p class="sb-info__born"> <span><?php echo __( 'Born','vogue.me' ); ?></span> <?php echo get_field( 'born', $get_taxonomy_term ); ?> </p> <?php endif; ?>
          <?php if ( get_field( 'nationality', $get_taxonomy_term ) ) : ?> <p class="sb-info__nationality"> <span><?php echo __( 'Nationality','vogue.me' ); ?></span> <?php echo get_field( 'nationality', $get_taxonomy_term ); ?> </p> <?php endif; ?>
          <?php if ( get_field( 'location', $get_taxonomy_term ) ) : $location_id = get_field( 'location', $get_taxonomy_term ); $location = get_term( $location_id, 'countries' ); ?> <p class="sb-info__location"> <span><?php echo __( 'Location','vogue.me' ); ?></span> <a href="<?php echo get_term_link( $location->slug, 'countries' ); ?>"><?php echo $location->name; ?></a> </p> <?php endif; ?>
            
            </div>
          <?php if ( get_field( 'biography', $get_taxonomy_term ) ) : ?>
            <div class="sb-info__content">
              <h2 class="sb-info__title"><?php echo __( 'Biography','vogue.me' ); ?></h2>

              <div class="sb-info__intro"> <?php echo get_field( 'biography', $get_taxonomy_term ); ?> </div>
            <?php if ( get_field( 'biography_extended', $get_taxonomy_term ) ) : ?>
              <div class="sb-info__text js-sb-info__text" style="padding-top:24px;"> <?php echo get_field( 'biography_extended', $get_taxonomy_term ); ?> </div>

              <button type="button" class="sb-info__more js-sb-info__more">
                <?php echo __( 'Show','vogue.me' ); ?> <span class="js-more"><?php echo __( 'more','vogue.me' ); ?></span><span class="js-less"><?php echo __( 'less','vogue.me' ); ?></span>
                <i class="fa fa-chevron-down js-icon"></i>
              </button>
            <?php endif; ?>
            </div>
          <?php endif; ?>
          </div>
        </div>

      </section>
    <?php endif; ?>

<?php

  $args = array( 'post_status' => 'inherit', 'fields' => 'ids', 'posts_per_page' => 24, 'post_type' => 'attachment', );

  $args['tax_query'] = array( array( 'taxonomy' => 'vogue_collection', 'terms' => array( $tag_slug ), 'field' => 'slug', ), );

  if ( CLEAR_QUERY_CACHE || CLEAR_QUERY_CACHE !== false ) : delete_transient( 'collection_photos-'.$tag_slug ); endif; if ( false === ( $collection_photos = get_transient( 'collection_photos-'.$tag_slug ) ) ) { $collection_photos = get_posts($args); set_transient( 'collection_photos-'.$tag_slug, $collection_photos, 12 * 60 * 60 ); }

?>

  <?php if ( $collection_photos ) : ?>

      <section class="pc-slider pc-slider--auto-width">

        <div class="container container--mid">
          <header class="pc-slider__head">
            <a href="<?php echo bloginfo('url'); ?>/images?profile=<?php echo $tag_slug; ?>" class="pc-slider__link">View all</a>
            <h2 class="pc-slider__title"><?php echo __( 'Gallery','vogue.me' ); ?></h2>
          </header>
        </div>

        <div class="container container--mid">
          <div class="pc-slider__inner">
            <div class="swiper-container" data-columns="auto" data-columns-mobile="1"<?php if ( check_site( false, 'ar' ) ) : echo ' dir="rtl"'; endif; ?>>
              <div class="swiper-wrapper">

<?php foreach( $collection_photos as $post ) : setup_postdata($post); ?>

              <?php $attachment_id = $post->ID; ?>

              <?php $imageUrl = wp_get_attachment_image_src( $attachment_id, 'full' ); ?>
              <?php $full = imageProvider( $imageUrl[0], 0, 2500 ); ?>
              <?php $large = imageProvider( $imageUrl[0], 0, 2500 ); ?>
              <?php $medium = imageProvider( $imageUrl[0], 0, 500 ); ?>

              <?php if ( $full ) : $zoom = $full; ?>
              <?php elseif ( $large ) : $zoom = $large; ?>
              <?php else : $zoom = $medium; ?>
              <?php endif; ?>
              <article class="swiper-slide pc-slider__slide pc-slider-slide">
                  <img src="<?php echo $medium; ?>" data-src="<?php echo $zoom; ?>" class="pc-slider-slide__img js-has-zoom">
              </article>
        
<?php endforeach; wp_reset_postdata(); wp_reset_query(); ?>


            </div>
          </div>
          <div class="swiper-button-wrapper swiper-button-wrapper--left"><div class="swiper-button-prev swiper-button swiper-button--dark"></div></div>
          <div class="swiper-button-wrapper swiper-button-wrapper--right"><div class="swiper-button-next swiper-button swiper-button--dark"></div></div>
        </div>
      </div>
      
      <div class="container container--mid">
        <footer class="pc-slider__foot">
          <a href="<?php echo bloginfo('url'); ?>/images?profile=<?php echo $tag_slug; ?>" class="btn btn--center"><?php echo __( 'View all photos','vogue.me' ); ?></a>
        </footer>
      </div>

      </section>

  <?php endif; ?>

  <?php

    $collection_video_args = array( 'post_type' => array( 'legacy', 'post' ), 'fields' => 'ids', 'category__in' => array( $video_cat_id ), 'posts_per_page' => 20, 'tax_query' => array( array( 'taxonomy' => 'vogue_collection', 'field' => 'slug', 'terms' => $tag_slug ) ) );

    if ( CLEAR_QUERY_CACHE || CLEAR_QUERY_CACHE !== false ) : delete_transient( 'collection_videos-'.$tag_slug ); endif; if ( false === ( $collection_videos = get_transient( 'collection_videos-'.$tag_slug ) ) ) { $collection_videos = get_posts($collection_video_args); set_transient( 'collection_videos-'.$tag_slug, $collection_videos, 12 * 60 * 60 ); }

  ?>

  <?php if ( $collection_videos ) : ?>
      <section class="pc-slider">
        
        <div class="container container--mid">
          <header class="pc-slider__head">
            <a href="<?php echo bloginfo('url'); ?>/videos?profile=<?php echo $tag_slug; ?>" class="pc-slider__link"><?php echo __( 'View all','vogue.me' ); ?></a>
            <h2 class="pc-slider__title"><?php echo __( 'Videos','vogue.me' ); ?></h2>
          </header>
        </div>

        <div class="container container--mid">
          <div class="pc-slider__inner">
            <div class="swiper-container" data-columns="2" data-columns-mobile="2"<?php if ( check_site( false, 'ar' ) ) : echo ' dir="rtl"'; endif; ?>>
              
              <div class="swiper-wrapper">

              <?php $i = 1; foreach ( $collection_videos as $post ) : setup_postdata( $post ); ?>
                <?php if ( get_field( 'media_type' ) == 'video' ) : ?>
                <?php // TO DO: INVESTIGATE MISSING POSTER IMAGE ?>
                  <article class="swiper-slide pc-slider__slide pc-slider-slide"> <?php getVideoPlayer( '', '', $post ); ?> </article>
                <?php endif; ?>
              <?php $i++; endforeach; wp_reset_postdata(); wp_reset_query(); ?>

              </div>
    
            </div>
            <div class="swiper-button-wrapper swiper-button-wrapper--left"><div class="swiper-button-prev swiper-button swiper-button--dark"></div></div>
            <div class="swiper-button-wrapper swiper-button-wrapper--right"><div class="swiper-button-next swiper-button swiper-button--dark"></div></div>

          </div>
        </div>

        <div class="container container--mid">
          <footer class="pc-slider__foot">
            <a href="<?php echo bloginfo('url'); ?>/videos?profile=<?php echo $tag_slug; ?>" class="btn btn--center"><?php echo __( 'View all videos','vogue.me' ); ?></a>
          </footer>
        </div>
      
      </section>
<?php endif; ?>



<?php 

  $display_count = 5;

  $collection_args = array( 'post_type' => array( 'legacy', 'post' ), 'fields' => 'ids', 'posts_per_page' => -1, 'category__not_in' => $ignore, 'tax_query' => array( array( 'taxonomy' => 'vogue_collection', 'field' => 'slug', 'terms' => $tag_slug ) ) );

  $collection = get_posts( $collection_args );

  if ( CLEAR_QUERY_CACHE || CLEAR_QUERY_CACHE !== false ) : delete_transient( 'collection_posts-'.$tag_slug.'-'.$paged ); endif; if ( false === ( $collection = get_transient( 'collection_posts-'.$tag_slug.'-'.$paged ) ) ) { $collection = get_posts( $collection_args ); set_transient( 'collection_posts-'.$tag_slug.'-'.$paged, $collection, 12 * 60 * 60 ); }

?>

<!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
<div class="get--content">
  <?php $stored = (int) get_field( 'vogue_view_count', $get_taxonomy_term ); ?>

  <form class="form" method="post" class="update_page_view_count" style="display: none !important;">
    <?php $args = array( 'post_id' => $get_taxonomy_term, 'form' => false, 'fields' => array('field_57f290e2cf707'), 'submit_value' => '' ); acf_form( $args );  ?>
  </form>

</div>
<!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

<?php if ( $collection ) :

  $count = count( $collection );

  if ( isset( $_GET[ 'is-paged' ] ) )
  {
    $paged = $_GET[ 'is-paged' ];
    $offset = (int) $_GET[ 'is-paged' ] * $display_count;
  }
  else
  {
    $offset = 0;
  }

  $collection = array_slice( $collection, $offset, $display_count, true ); ?>

    <div class="container container--mid padding--top">

      <div class="scroll">

        <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
        <div class="get--content loaded" data-title="" data-url="<?php echo get_term_link( $tag_slug, COLLECTION ); ?>" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

          <ul class="list post--list post--latest">

          <?php $i = 1; foreach( $collection as $post ) : setup_postdata( $post ); ?>

            <?php getFeedItem( $i, $post, 'simple' ); ?>

          <?php $i++; endforeach; wp_reset_postdata(); wp_reset_query(); ?>

          </ul>

          <?php echo getAdvert('vert'); ?>

        </div>
        <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

      <?php /* if ( $collection % $offset === 5 ) : ?>

        <div class="next jscroll-next-parent"> <a href="<?php echo get_term_link( $tag_slug, COLLECTION ); ?>?is-paged=<?php echo $paged; ?>"></a> </div>

      <?php endif; */ ?>

      </div>

    </div>

    <?php if ( $get_user != false ) : getBreak_Instagram( $get_user ); endif; ?>

<?php endif; ?>


  

<?php get_footer(); ?>
