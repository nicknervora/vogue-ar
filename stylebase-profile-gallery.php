<?php 

// Template name: Stylebase profile gallery

	get_header();

	if ( isset( $_GET[ 'profile' ] ) ) :

	$term = $_GET[ 'profile' ];

	$term = get_term_by( 'slug', $term, COLLECTION );

	$term_id = $term->term_id;
	$term_slug = $term->slug;
	$term_name = $term->name;

	$tag_object = $term->taxonomy;

	$get_taxonomy_term = $tag_object . '_' . $term_id;

	$header_bg = getArchiveHeaderBG( $get_taxonomy_term );

?>

	<div class="hero hero--sb">
		<div class="hero__bg" style="background-image: url('<?php echo $header_bg; ?>');"></div>
	</div>

	<section class="pc pc--sb sb">

		<div class="pc__header pc-header">
			<div class="container container--mid--reduced">
				<div class="pc-header__inner">
					<div class="pc-header__top">
						<h1 class="pc-header__title"><?php echo $term_name; ?></h1>
					<?php if ( get_field( 'social_profiles', $get_taxonomy_term ) ) : $social = get_field( 'social_profiles', $get_taxonomy_term ); ?>
            <ul class="pc-social">
          <?php foreach ( $social as $profile ) : $network = $profile['network']; $account = $profile['tag']; ?>
              <li><a class="fa fa-<?php echo $network; ?>" href="<?php echo get_social_url( $network, $account ); ?>" target="_blank"></a></li>
          <?php endforeach; ?>
            </ul>
          <?php endif; ?>
					</div>
				
          <div class="pc-header__search">
            <?php VogueCollectionSearch(); ?>
          </div>

				</div>
			</div>
		</div>

<?php

	$args = array( 'post_status' => 'inherit', 'posts_per_page' => -1, 'post_type' => 'attachment', );
	$args['tax_query'] = array( array( 'taxonomy' => COLLECTION, 'terms' => array( $term_slug ), 'field' => 'slug' ) );

	$style_query = new WP_Query( $args );

?>

<?php if ( $style_query->have_posts() ) : ?>
		<section class="sb-gallery sb-gallery--profile-gallery">
			<div class="container container--mid">
				<header class="sb-gallery__head">
					<a href="<?php echo get_term_link( $term_slug, COLLECTION ); ?>" class="sb-gallery__link"><?php echo __( 'Go Back','vogue.me' ); ?></a>
					<h1 class="sb-gallery__title"><?php echo __( 'Gallery','vogue.me' ); ?></h1>
				</header>
				<div class="sb-gallery__inner">

<?php $i = 1; $row = 1; while ( $style_query->have_posts() ) : $style_query->the_post(); ?>

				<?php if ( $i % 4 == 1 ) : ?> <div class="row"> <?php endif; ?>
					<?php $attachment_id = $post->ID; ?>

          <?php $imageUrl = wp_get_attachment_image_src( $attachment_id, 'full' ); ?>
          <?php $full = imageProvider( $imageUrl[0], 0, 2500 ); ?>
          <?php $large = imageProvider( $imageUrl[0], 0, 2500 ); ?>

          <?php if ( $row % 2 == 0 ) : ?>
            <?php if ( $i % 2 == 1 ) : $field = 1; else : $field = 2; endif; ?>
          <?php else : ?>
            <?php if ( $i % 2 == 1 ) : $field = 2; else : $field = 1; endif; ?>
          <?php endif; ?>

          <?php if ( $field == 2 ) : ?>
            <?php $medium = imageProvider( $imageUrl[0], 500 ); ?>
            <?php $vert = imageProvider( $imageUrl[0], 396, 560 ); ?>  
          <?php else : ?>
            <?php $medium = imageProvider( $imageUrl[0], 396, 560 ); ?>  
          <?php endif; ?>

          <?php if ( $full ) : $zoom = $full; ?>
          <?php else : $zoom = $medium; ?>
          <?php endif; ?>

          <article class="sb-gallery-item">
            
            <picture>
              <?php if ( $field == 2 ) : ?>
              <source srcset="<?php echo $vert; ?>" media="(max-width:770px)">
              <?php endif; ?>
              <img data-echo="<?php echo $medium; ?>" data-src="<?php echo $zoom; ?>" class="js-has-zoom">
            </picture>
              <div class="sb-gallery-item__inner">
                <span><?php echo __( 'View Image','vogue.me' ); ?></span>
              </div>
            
          </article>

					<?php if ( $i % 4 == 0 ) : $row++; ?> </div> <?php endif; ?>

<?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>
					<?php if ( $i % 4 != 1 ) : ?> </div> <?php endif; ?>
					</div>
				</div>
			</div>
		</section>

	<?php endif; ?>

	</section>

<?php endif; get_footer(); ?>