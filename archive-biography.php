<?php
  
  get_header();

  $years = get_terms( array( 'taxonomy' => 'fashionprizeyear', 'hide_empty' => true, 'orderby' => 'slug', 'order' => 'DESC' ) );

?>

<div class="scroll" data-ui="jscroll-default" data-continue="true">

<?php $i = 0; foreach ( $years as $year ) : $year_id = $year->term_id; ?>

<?php $args = array( 'post_type' => array( 'biography' ), 'posts_per_page' => -1, 'tax_query' => array( array( 'taxonomy' => 'fashionprizeyear', 'field' => 'slug', 'terms' => $year->slug ) ) ); ?>

<?php $fp_finalists_query = new WP_Query( $args ); ?>

<?php if ( $fp_finalists_query->have_posts() ) : ?>

<?php $header_image = wp_get_attachment_url( get_field( 'header_image','fashionprizeyear_' . $year_id ) ); ?>
<?php if ( $header_image ) : ?>
  <div class="hero hero--fp">
    <div class="hero__bg" style="background-image: url('<?php echo $header_image; ?>');"></div>
  </div>
<?php endif; ?>

  <section class="pc pc--fp">
  
    <div class="pc__header pc-header">
      <div class="container container--mid--reduced">
        <div class="pc-header__inner">
          <div class="pc-header__top"> <h1 class="pc-header__title"><?php echo __( 'Fashion prize Finalists', 'vogue.me' ); ?> <?php echo $year->name; ?></h1> </div>
        </div>
      </div>
    </div>

    <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
    <div class="get--content" data-title="" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

      <div class="container container--mid--reduced">
      
        <div class="content-column">

          <section class="fp-finalists">

            <div class="fp-jury__members">

          <?php while ( $fp_finalists_query->have_posts() ) : $fp_finalists_query->the_post(); ?>

            <article class="fp-jury__member fp-jury-member"><a href="<?php echo get_permalink(); ?>">
              <div class="fp-jury-member__portrait" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); ?>');"></div>
              <h2 class="fp-jury-member__name"><?php echo the_title(); ?></h2>
            </a></article>

          <?php endwhile; wp_reset_query(); wp_cache_flush(); ?>

            </div>

          </section>

        </div>

      </div>

    </div>
    <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

  </section>

  <?php endif; ?>

<?php $i++; endforeach; ?>

</div>

<?php get_footer(); ?>