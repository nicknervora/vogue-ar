<?php

define( 'CLEAR_QUERY_CACHE', get_field( 'clear_query_cache', 'option' ) );

function getPaged()
{
  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

  return $paged;
}

function getIgnore_Cats( $ignore_cats )
{

  $ignore = array();

  if ( in_array( 'video', $ignore_cats ) )
  {
    if ( check_site( false, 'ar' ) )
    {
      $ignore_video = get_term_by( 'slug', 'video_ar', 'category' );
    }
    else
    {
      $ignore_video = get_term_by( 'slug', 'video', 'category' );
    }
    $ignore_video = (int) $ignore_video->term_id;

    array_push( $ignore, $ignore_video );
  }

  if ( in_array( 'runway', $ignore_cats ) )
  {
    $ignore_gallery = getRunwayCats();

    $ignore = array_merge( $ignore, $ignore_gallery );
  }

  if ( in_array( 'uncategorized', $ignore_cats ) )
  {
    $uncatObj = get_category_by_slug('uncategorized'); 
    $uncategorizedID = $uncatObj->term_id;
    array_push( $ignore, $uncategorizedID );
  }

  return $ignore;

}

function getIgnore_Posts( $ignore_posts )
{

  $ignore = array();
  $ignore_all = array();

  if ( in_array( 'promoted', $ignore_posts ) )
  {
    $ignore_promoted = get_field( 'promoted_article', HOME_ID );

    if ( !is_null( $ignore_promoted ) )
    {
      $ignore = array_merge( $ignore, $ignore_promoted );
    }

  }

  if ( in_array( 'recommends', $ignore_posts ) )
  {
    $ignore_recommends = get_field( 'vogue_recommends', HOME_ID );

    if ( !is_null( $ignore_recommends ) )
    {
      $ignore = array_merge( $ignore, $ignore_recommends );
    }

  }

  foreach ( $ignore as $ig )
  {
    if ( is_object($ig) )
    {
      $ignore_all[] = (int) $ig->ID;  
    }
    else
    {
      $ignore_all[] = (int) $ig;
    }
    
  }

  return $ignore_all;
}

function getQuery__ShowHide( $type )
{

  $allowed = array( 'show', 'hide' );


  if ( $type == 'hide' )
  {
    $hide_show_args = array(

      'post_type' => array(
        'legacy', 'post'
      ),
      'date_query' => array(
          array(
              'column' => 'post_date_gmt',
              'after' => '2 months ago'
          )
      ),
      'posts_per_page' => -1,
      'fields' => 'ids',
      'tax_query' => array(
        'relation' => 'AND',
        array(
          'taxonomy' => 'hide_post',
          'field'    => 'slug',
          'terms'    => 'hide_post_tag',
          'operator' => 'IN'
        ),
        array(
          'taxonomy' => 'hide_post',
          'operator' => 'EXISTS'
        )

      )

    );

  }
  else
  {

    $hide_show_args = array(

      'post_type' => array(
        'legacy', 'post'
      ),
      'date_query' => array(
          array(
              'column' => 'post_date_gmt',
              'after' => '2 months ago'
          )
      ),
      'posts_per_page' => -1,
      'fields' => 'ids',
      'tax_query' => array(
        'relation' => 'AND',
        array(
          'taxonomy' => 'hide_post',
          'field'    => 'slug',
          'terms'    => 'force_show',
          'operator' => 'IN'
        ),
        array(
          'taxonomy' => 'hide_post',
          'field'    => 'slug',
          'operator' => 'EXISTS'
        )
      )

    );

  }

  $hide_show_posts = get_posts( $hide_show_args );

  return $hide_show_posts;

}

function getQuery__Ignore()
{

	if(get_current_blog_id() == '3' ) {
		$ignore_read = $_SESSION['read_ar'];
	} else {
		$ignore_read = $_SESSION['read'];
	}
  //$ignore_read = $_SESSION['read'];

  $force_show = getQuery__ShowHide( 'show' );
  $force_hide = getQuery__ShowHide( 'hide' );
// IGNORE ALL POSTS IN RUNWAY & VIDEO CATEGORIES
  $ignore = getIgnore_Cats( array( 'runway', 'runway_ar', 'video_ar', 'video', 'uncategorized' ) );
// IGNORE ALL POSTS THAT HAVE BEEN SELECTED AS FEATURED OR RECOMMENDED
  $ignore_posts = getIgnore_Posts( array( 'promoted', 'recommends' ) );

  // $ignore_posts = array_merge($ignore_read,$ignore_posts);

  // $ignore_all = getQuery__Ignore();

  // $ignore = implode( ',', $ignore );

  // var_dump($ignore);

  $ignore_in_cats = array(
    'post_type' => array(
      'legacy', 'post'
    ),
    'date_query' => array(
        array(
            'column' => 'post_date_gmt',
            'after' => '1 year ago'
        )
    ),
    'fields' => 'ids',
    'posts_per_page' => -1,
    'category__in' => $ignore
  );

  $ignore_cat_posts = new WP_Query( $ignore_in_cats );

  $ignore_from_cats = array();

  if( !isset($post)){
    $post = array();
  }

  while ( $ignore_cat_posts->have_posts() ) : $ignore_cat_posts->the_post(); setup_postdata( $post );

    $ignore_from_cats[] = get_the_ID();

  endwhile; wp_reset_postdata(); wp_reset_query();

  $ignore_posts = array_merge( $ignore_posts, $ignore_from_cats );

  $ignore_posts = array_merge( $ignore_posts, $force_hide );

  $ignore_posts = array_diff( $ignore_posts, $force_show );

  $ignore_posts = array_unique( $ignore_posts );

  $ignore_all = $ignore_posts;

  return $ignore_all;
  
}


/* ============================================================================

  HOME PAGE QUERY

===============================================================================

  Outputs arguements for homepage query, which includes all posts,
  except promoted, recommended and any posts in Runway or Video categories

============================================================================ */

function getQuery__home( $paged = false, $results = false )
{
  
  $ignore_all = getQuery__Ignore();

  // $ignoring = implode( ',', $ignore_all );

  $uncatObj = get_category_by_slug('uncategorized'); 
  $uncategorizedID = $uncatObj->term_id;

// SET UP ARGUMENTS
  $args = array(
    'post_type' => array(
      'post'
    ),
    'posts_per_page' => 5,
    'fields' => 'ids',
    'category__not_in' => array($uncategorizedID),
    // 'posts__not_in' => $ignore_all,
    'paged' => $paged
  );

  return $args;

  // if ( $results == true )
  // {
  //   // return get_posts( $args );
  // }
  // else
  // {
  //   var_dump($args);
  //   return $args;
  // }

}



function getQuery__TrendingVideos( $ignore_all = false, $ignore_this = false )
{

  $featured_video = FEATURED_VIDEO;
  $trending_videos = get_field( 'trending_videos', HOME_ID );

  if ( $trending_videos )
  {

    $vids = array(); foreach ( $trending_videos as $vid ) : $vids[] = $vid->ID; endforeach;

  }

  $uncatObj = get_category_by_slug('uncategorized'); 
  $uncategorizedID = $uncatObj->term_id;

  $trending_args = array(
    'post_type' => array(
      'post'
    ),
    'fields' => 'ids',
    'posts_per_page' => 10,
    'meta_key' => 'video_view_count',
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'include_children' => false,
    'category__in' => get_video_categories(),
    'category__not_in' => array($uncategorizedID),
    'date_query' => array(
      'after' => '6 week ago'
    )

  );

  if ( CLEAR_QUERY_CACHE || CLEAR_QUERY_CACHE !== false ) : delete_transient( 'trending_videos' ); endif;

	delete_transient( 'trending_videos' );

  if ( false === ( $trending = get_transient( 'trending_videos' ) ) ) {

    $trending_posts = get_posts( $trending_args );

    if ( $trending_videos ) :

      $trending = array_merge( $vids, $trending_posts );

    else :

      $trending = $trending_posts;

    endif;

    $trending = array_unique( $trending );

    //set_transient( 'trending_videos', $trending, 3 * 60 * 60 );

  }

  if ( $ignore_this == true )
  {
    if ( ( $key = array_search( get_the_ID(), $trending ) ) !== false )
    {
        unset( $trending[$key] );
    }

  }

  if ( $ignore_all == false )
  {
    if ( ( $key = array_search( FEATURED_VIDEO, $trending ) ) !== false )
    {
        unset( $trending[$key] );
    }

  }

  return $trending;

}


function getQuery__VideoPosts()
{

    $paged = getPaged();

    $sort = '';

    $uncatObj = get_category_by_slug('uncategorized'); 
    $uncategorizedID = $uncatObj->term_id;

    $excludeTrendingVideos = false;


    $vidIDs = array();


    // Collect Videos by Category IDs

    $videoCategoryArgs = array
    (
        'post_type' => array('post'),
        'fields' => 'ids',
        'category__in' => get_video_categories(),
        'category__not_in' => array($uncategorizedID),
        'include_children' => false,
        'nopaging' => true,
    );

    $videoCategoryIDs = new WP_Query($videoCategoryArgs);
    if ($videoCategoryIDs->have_posts())
    {
        $vidIDs = array_merge($vidIDs, $videoCategoryIDs->posts);
    }


    // Collect Videos by Media Type IDs

    $videoMediaTypeArgs = array
    (
        'post_type' => array('post'),
        'fields' => 'ids',
        'category__not_in' => array($uncategorizedID),
        'meta_query' => array
        (
            'relation' => 'OR',
            array
            (
                'key' => 'media_type',
                'value' => 'video',
            ),
            array
            (
                'key' => 'media_type',
                'value' => 'video_plus',
            ),
        ),
        'nopaging' => true,
    );

    $videoMediaTypeIDs = new WP_Query($videoMediaTypeArgs);
    if ($videoMediaTypeIDs->have_posts())
    {
        $vidIDs = array_merge($vidIDs, $videoMediaTypeIDs->posts);
    }


    $vidIDs = array_unique($vidIDs);

    $args = array
    (
        'post_type' => array('post'),
        'posts_per_page' => 9,
        'offset' => $paged * 8,
        'post__in' => $vidIDs,
        'ignore_sticky_posts' => true,
    );

    if ( isset( $_GET[ 'sort' ] ) && $_GET['sort'] == 'popular' )
    {
        $args = array_merge($args, array
        (
            'meta_key' => 'video_view_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
        ));
    }
    else
    {
        $args = array_merge($args, array
        (
            'orderby' => 'date',
            'order' => 'DESC',
        ));
    }

    if ($excludeTrendingVideos)
    $args['post__not_in'] = getQuery__TrendingVideos(true);

    return $args;

}

function getQuery__Category( $paged, $cat_slug )
{

  $today = getdate();

  $uncatObj = get_category_by_slug('uncategorized'); 
  $uncategorizedID = $uncatObj->term_id;

  if ( isset( $_GET[ 'sort' ] ) && $_GET['sort'] == 'popular' )
  {
    $args = array(
      'post_type' => array( 'post' ),
      'fields' => 'ids',
      'posts_per_page' => 10,
      'include_children' => false,
      'category__not_in' => array($uncategorizedID),
      'date_query' => array(
          array(
              'column' => 'post_date_gmt',
              'after' => '2 months ago'
          )
      ),
      'tax_query' => array(
        array(
          'taxonomy' => 'category',
          'field' => 'slug',
          'terms' => $cat_slug
        )
      ),
      'meta_key' => 'vogue_view_count', 
      'orderby' => 'meta_value_num', 
      'order' => 'DESC',      
/*      
      'meta_query' => array(
        'relation' => 'OR',
        'share_count' => array(
          'key' => 'vogue_share_count',
          'compare' => 'NOT EXISTS'
        ),
        'view_count' => array(
          'key' => 'vogue_view_count',
          'compare' => 'NOT EXISTS'
        )
      ),
      'orderby' => array(
        'share_count' => 'DESC',
        'view_count' => 'DESC',
      ),
*/      
      // 'orderby' => 'meta_value_num',
      // 'order' => 'DESC',
      'paged' => $paged
    );
  }
  else
  {
    $args = array(
      'post_type' => array( 'post' ),
      'fields' => 'ids',
      'posts_per_page' => 10,
      'category__not_in' => array($uncategorizedID),
      'include_children' => false,
      'date_query' => array(
          array(
              'column' => 'post_date_gmt',
              'after' => '2 months ago'
          )
      ),
      'tax_query' => array(
        array(
          'taxonomy' => 'category',
          'field' => 'slug',
          'terms' => $cat_slug
        )
      ),
      'paged' => $paged
    );

  }

  return $args;

}



