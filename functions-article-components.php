<?php

/* ============================================================================

  GET ARTICLE GALLERY SLICK

============================================================================ */

function getArticleGallery()
{
	// for sharing buttons
  $posturl = get_permalink(get_the_ID());
  $posttitle = get_the_title(get_the_ID());
  
  
  $legacy = false;
  $class = false;
  $count = 20;
  if(get_post_type() == 'legacy'){
	 if ( get_field( 'linked_gallery' ) )
	  {

		$get_gallery = get_field( 'linked_gallery' );

		$gallery_id = $get_gallery->ID;

		$gallery = get_field( 'gallery_image_uploader_normal', $gallery_id );

		$legacy = true;

		foreach ( $gallery as $item ) :

		  $check = strtolower( $item['image_html_description'] );

		  if ( strpos( $check, 'shop here' ) || strpos($check,'US $') )
		  {
			$is_shoppable = true;
			$class = 'shoppable';
			break;
		  }

		endforeach;

	  } 
	  
	}else if(get_post_type() == 'gallery'){
		
		// fixing legacy galleries 
		
		$gallery_id = get_the_ID();

		$gallery = get_field( 'gallery_image_uploader_normal', $gallery_id );

		$legacy = true;

		foreach ( $gallery as $item ) :

		  $check = strtolower( $item['image_html_description'] );

		  if ( strpos( $check, 'shop here' ) || strpos($check,'US $') )
		  {
			$is_shoppable = true;
			$class = 'shoppable';
			break;
		  }

		endforeach;
	}else{

	  if ( get_field( 'shoppable' ) == false )
	  {
		$is_shoppable = false;
	  }
	  else
	  {
		$is_shoppable = true;
		$class = 'shoppable';
	  }

	  //var_dump(get_field( 'media_type' ));exit;
	  if( get_field( 'media_type' ) != "gallery" ){

		$gallery = false;

	  }
	  else if ( get_field( 'gallery_select' ) )
	  {

		$get_gallery = get_field( 'gallery_select' );

		$gallery_id = $get_gallery[0];

		if ( get_field( 'gallery', $gallery_id ) )
		{
		  $gallery = get_field( 'gallery', $gallery_id );

		  if ( get_field( 'shoppable', $gallery_id ) == false )
		  {
			$is_shoppable = false;
		  }
		  else
		  {
			$is_shoppable = true;
			$class = 'shoppable';
		  }

		}

	  }

	  

	  elseif ( get_field( 'gallery' ) )
	  {

		$gallery = get_field( 'gallery' );

	  }

	  else
	  {

		$gallery = false;

	  }
  }

?>

  <?php if ( $gallery !== false && !is_null( $gallery ) ) : ?>

  <div class="article--gallery no-js-initiate <?php if ( $class ) echo ' ' . $class; ?> js-the-article-gallery">

    <div class="article--gallery--inner">

    <?php $i = 1; foreach ( $gallery as $item ) : ?>

      <?php if ( $item[ 'image' ] ) : ?>

      <div class="gallery--item">

      
        <div class="item--image post-share-image">

          <img data-lazy="<?php echo $item['image']['sizes']['medium']; ?>" height="<?php echo $item['image']['height']; ?>" width="<?php echo $item['image']['width']; ?>" srcset="<?php echo $item['image']['url']; ?>" sizes="480px" alt="<?php echo $item['image']['title']; ?>" >

            <ul class="post-share-image__links">
              <li class="post-share-image__link"><a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode($posturl); ?>&amp;media=<?php  echo $item['image']['url']; ?>&amp;description=<?php echo urlencode($posttitle); ?>" class="fa fa-pinterest" target="_blank"></a></li>
              <li class="post-share-image__link"><a href="https://www.facebook.com/dialog/feed?app_id=164555587284821&display=popup&link=<?php echo urlencode($posturl); ?>&picture=<?php  echo $item['image']['url']; ?>" class="fa fa-facebook" target="_blank"></a></li>
              <li class="post-share-image__lightbox"><a href="#" class="fa fa-search js-has-lightbox" data-src="<?php echo $item['image']['url']; ?>"></a></li>
              <li class="post-share-image__toggle"><button class="js-post-share-image-toggle">
                <span class="open"><?php inline_svg( 'va-more' ); ?></span>
                <span class="close"><?php inline_svg( 'va-close' ); ?></span>
              </button></li>
            </ul>

            <?php if ( $i == 2 ): ?>
            <div class="article--gallery--meta__sticky">
              <div class="gallery--view--thumbs gallery--view--thumbs__sticky"></div>

              <div class="gallery--position__small">
                <b class="js-gallery--position__small--cnt">1</b>/<?php echo count( $gallery ); ?>
              </div>
            </div>
            <?php endif; ?>
        </div>
      

      <?php if ( $legacy == true ) : ?>

          <div class="item--content">

            <header class="item--content__header">

              <?php if ( $item[ 'image_caption' ] || $item['image']['caption'] ) : ?><div class="item--caption"> <?php if ( $item[ 'image_caption' ] ) : echo $item[ 'image_caption' ]; else : echo $item['image']['caption']; endif; ?> </div><?php endif; ?>
              <?php if ( $item[ 'photo_credit' ] ) : ?> <div class="item--credit"> Photo: <?php echo $item[ 'photo_credit' ]; ?> </div> <?php endif; ?>

            </header>

            <section class="item--content__copy">

              <?php if ( $item[ 'image_title_b' ] ) : ?><h2><?php echo $item[ 'image_title_b' ]; ?></h2><?php endif; ?>
              <?php if ( isset($item['image_html_description']) ) : echo $item[ 'image_html_description' ]; elseif ( isset($item['image_description']) ) : echo $item['image_description']; else : echo $item['image']['description']; endif; ?>

            </section>

          </div>

      <?php else : ?>

        <?php if ( !$is_shoppable ) : ?>
          <div class="item--content">

            <header class="item--content__header">

              <?php if ( $item[ 'image_caption' ] || $item['image']['caption'] ) : ?><div class="item--caption"> <?php if ( $item[ 'image_caption' ] ) : echo $item[ 'image_caption' ]; else : echo $item['image']['caption']; endif; ?> </div><?php endif; ?>
              <?php if ( $item[ 'photo_credit' ] ) : ?> <div class="item--credit"> Photo: <?php echo $item[ 'photo_credit' ]; ?> </div> <?php endif; ?>

              <div class="gallery--change to--list" data-to-list="<?php echo __('View as list','vogue.me'); ?>" data-to-slider="<?php echo __('View as slideshow','vogue.me'); ?>"></div>

            </header>

          <?php if ( $item['image_title'] || isset($item['image_html_description']) || $item['image_description'] ) : ?>
            <section class="item--content__copy">

              <?php if ( $item['image_title'] ) : echo '<h2>' . $item['image_title'] . '</h2>'; endif; ?>
              <?php /* <h2><?php if ( $item['image_title'] ) : echo $item['image_title']; else : echo $item['image']['title']; endif; ?></h2> */ ?>
              <?php if ( isset($item['image_html_description']) ) : echo $item[ 'image_html_description' ]; elseif ( isset($item['image_description']) ) : echo $item['image_description']; else : echo $item['image']['description']; endif; ?>

            </section>
          <?php endif; ?>

          </div>
        <?php else : ?>
          <div class="item--content">

            <?php if ( $item[ 'image_title' ] ) : ?><header class="item--content__header"><?php echo $item[ 'image_title' ]; ?></header><?php endif; ?>
            <?php if ( $item[ 'image_description' ] ) : ?><section class="item--content__copy"> <?php echo $item[ 'image_description' ]; ?> </section><?php endif; ?>

          <?php if ( $item[ 'shoppable' ] == true ) : ?>
            <section class="item--content__buy">
            <?php if ( $item[ 'product_price' ] ) : ?>
              <div class="item--content__pricing">
                <p><?php $i = 1; foreach ( $item[ 'product_price' ] as $price ) : ?><?php if ( $i > 1 ) : echo '<br />'; endif; ?> <?php echo $price[ 'currency' ]; ?>: <?php echo $price[ 'price' ]; ?><?php $i++; endforeach; ?> </p>
              </div>
            <?php endif; ?>
              <?php if ( $item[ 'product_link' ] ) : ?><a href="<?php echo $item[ 'product_link' ]; ?>" class="button brand" target="_blank">Buy Now</a><?php endif; ?>
            </section>
          <?php endif; ?>

          </div>
        <?php endif; ?>

      <?php endif; ?>

        <div class="clear"></div>

      </div>
      
      <?php endif; ?>

    <?php $i++; endforeach; ?>

    </div>

    <div class="article--gallery--meta">

    <?php if ( !$is_shoppable ) : ?>
      <div class="gallery--view--thumbs"></div>
    <?php endif; ?>

      <div class="gallery--position">
        <span class="current--slide">1</span>
        <span class="position--seperator"></span>
        <span class="total--slides"></span>
      </div>

      <div class="gallery--navigation"></div>

    </div>

  <?php if ( !$is_shoppable ) : ?>
    <div class="article--gallery--grid--view">

      <?php $i = 1; foreach ( $gallery as $item ) : ?>

        <?php if ( $item[ 'image' ] ) : ?>
          <div class="grid--item"> <a href="#" data-title="View image"> <img data-echo="<?php echo $item['image']['sizes']['thumbnail']; ?>" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/V-Loading-Small.gif" alt=""> </a> </div>
        <?php endif; ?>

      <?php $i++; endforeach; ?>

    </div>
  <?php endif; ?>

  </div>

  <?php /* <div class="clear" style="height: 80px;"></div> */ ?>

<?php endif; }


function getArticleShoppableGallery()
{

  getArticleGallery( 'shoppable' );

}

function getNextGenGallery( $post )
{
  if ( get_post_meta( $post->ID, 'style_linked_gallery' ) ) : ?>
    <?php $v1_content = get_post_meta( $post->ID, 'style_linked_gallery' ); ?>
    <?php if ( $v1_content ) : ?>
    <?php $v1 = json_decode($v1_content[0]); ?>

    <?php if ( is_numeric( $v1 ) ) : ?>
      <?php $nextgen = $v1; ?>
    <?php elseif ( isset( $v1->contents[0]->nextgen ) ) : ?>
      <?php $nextgen = $v1->contents[0]->nextgen; ?>
    <?php else : ?>
      <?php $nextgen = false; ?>
    <?php endif; ?>

    <?php if ( $nextgen ) : ?>
      <?php $image = getGalleryData( $nextgen ); ?>
      <div class="article--gallery">
        <div class="article--gallery--inner">
      <?php $i = 1; foreach ( $image->pictures as $img ) : $img_id = (int) $img->id; ?>
          <div class="gallery--item">
            <div class="item--image"> <img src="<?php echo get_bloginfo( 'url' ) . '/wp-content/gallery/' . $img->filename; ?>" alt=""> </div>
            <div class="item--content">

              <header class="item--content__header">

                <div class="item--caption"><?php the_title(); ?></div>

                <div class="gallery--change to--list" data-to-list="<?php echo __('View as list','vogue.me'); ?>" data-to-slider="<?php echo __('View as slideshow','vogue.me'); ?>"></div>

              </header>
  <?php /*
              <section class="item--content__copy">

                <p>With Ramadan coming to a close, the region’s style set is trading embellished kaftans and intricate abayas for laidback ensembles in preparation for Eid al-Fitr. Leading the fashion pack are Saudi sister influencers Thana and Sakhaa Abdul of The Abduls, who showed off their sporty style in athleisure-inspired outfits complete with sneakers. Also in the running (by way of rubber-soled shoes, that is) is Dalal AlDoub of Dalalid who paired a pistachio overlay with Converse sneakers, and Ascia Al Faraj of Ascia AKF who donned an eyelet tunic, striped culottes, and white kicks. Meanwhile, Dubai-based influencer Shahd Al Jumaily and television presenter Joelle Mardinian opted for understated elegance in an embroidered bisht signed Katya Kovtunovich. Ahead, the Ramadan 2016 street style looks that saw laidback luxe take over our Instagram feeds last week.</p>

              </section>
  */ ?>
            </div>
            <div class="clear"></div>
          </div>
      <?php $i++; endforeach; ?>
        </div>

        <div class="article--gallery--meta">

          <div class="gallery--view--thumbs"></div>

          <div class="gallery--position">
            <span class="current--slide">1</span>
            <span class="position--seperator"></span>
            <span class="total--slides"></span>
          </div>

          <div class="gallery--navigation"></div>

        </div>

        <div class="gallery--change to--list" data-to-list="View as list" data-to-slider="View as slideshow"></div>

        <div class="article--gallery--grid--view">

          <?php foreach ( $image->pictures as $img ) : ?>
            <div class="grid--item"> <a href="#"> <img src="<?php echo $img->filename; ?>" alt=""> </a> </div>
          <?php endforeach; ?>

        </div>

      </div>
    <?php endif; ?>
  <?php endif;

  endif;
}


/*
*   getVideoPlayerWrapper: Captures stdout from getVideoPlayer(), and put into string variable
*   used in shortcode embedvideo (custom-shortcode.php)
*/
function getVideoPlayerWrapper( $src = false, $thumbnail = false, $id = false, $height = false )
{
    ob_start(); 
    getVideoPlayer( $src, $thumbnail, $id, $height);
    $output = ob_get_contents(); 
    ob_end_clean(); 
    return $output;
}


function getVideoPlayer( $src = false, $thumbnail = false, $id = false, $height = false )
{
    
  $legacy = false;

  $check = array( 'fp_video_link', 'mp4_source_remote', 'mp4_source_hosted', 'ogv_source_hosted', 'wemb_source_hosted' );

  $get_src = array();

  if ( $thumbnail == false )
  {

    if ( get_field( 'poster_image', $id ) )
    {

      if ( is_object( get_field( 'poster_image', $id ) ) || is_array( get_field( 'poster_image', $id ) ) )
      {
        if ( is_object( get_field( 'poster_image', $id ) ) )
        {
          $thumbnail = get_field( 'poster_image', $id )->ID;
        }
        else
        {
          $thumbnail = get_field( 'poster_image', $id )['ID'];
        }
      }
      else
      {
        $thumbnail = (int) get_field('poster_image', $id);
      }

      $thumbnail = wp_get_attachment_url($thumbnail);
    }
    else
    {
      $thumbnail = '';
    }

  }
  elseif ( $thumbnail == 'no' )
  {
    $thumbnail = '';
  }
  else
  {
    $thumbnail = $thumbnail;
  }

  if ( $src == false )
  {
    if ( get_field( 'external_video' ) == true && get_field( 'video_select' ) != false ) :

      $vid_id = get_field( 'video_select' )[0];

      $check_id = $vid_id;

    elseif ( get_field( 'external_video' ) == false ) :

      $check_id = get_the_ID();

      foreach ( $check as $c )
      {
        if ( get_field( $c, $check_id ) )
        {
          $check_id = get_the_ID();
        }

      }

    endif;

  }
  elseif ( $src != false )
  {
    if ( !is_numeric($src) )
    {
      $check_id = 'no_check';
    }
    else
    {
      $check_id = $src;
    }

  }

  if ( $check_id || $check_id == 'no_check' )
  {
    if ( $check_id == 'no_check' )
    {
      $legacy = true;

      $remote = array();
      $remote['url'] = $src;

      if ( strpos( $src, '.webm' ) )
      {
        $remote['mime_type'] = 'video/webm';
      }
      elseif ( strpos( $src, '.m3u') )
      {
        $remote['mime_type'] = 'audio/x-mpequrl';
      }
      else
      {
        $remote['mime_type'] = 'video/mp4';
      }

      $get_src[] = $remote;

    }
    else
    {
      foreach ( $check as $c )
      {
        if ( get_field( $c, $check_id ) )
        {
          if ( $c == 'mp4_source_remote' || $c == 'fp_video_link' )
          {
            $remote = array();
            $remote['url'] = get_field( $c, $check_id );
            if ( strpos( $remote['url'], '.webm' ) )
            {
              $remote['mime_type'] = 'video/webm';
            }
            elseif ( strpos( $remote['url'], '.m3u') )
            {
              $remote['mime_type'] = 'audio/x-mpequrl';
            }
            else
            {
              $remote['mime_type'] = 'video/mp4';
            }

            $get_src[] = $remote;
          }
          else
          {
            $get_src[] = get_field( $c, $check_id );
          }

        }

      }

    }

    $src = $get_src;

    $sources = $src;

  if ( !empty( $sources ) ) :

  ?>


  <div class="video--container">

    <video<?php if ( $height != false ) : echo ' height="'.$height.'"'; endif; ?> preload="auto" x-webkit-airplay="allow" webkit-playsinline src="<?php echo $sources[0]['url']; ?>" class="video--player auto-fade-in"<?php if ( $thumbnail != '' ) : ?> poster="<?php echo $thumbnail; ?>"<?php endif; ?>>
      <?php foreach ( $sources as $s ) : ?>

      <?php if ( is_numeric($s) ) : ?>
      <?php $src = wp_get_attachment_url($s); ?>
      <?php if ( strpos( $src, '.webm' ) )
            {
              $mime_type = 'video/webm';
            }
            elseif ( strpos( $src, '.m3u') )
            {
              $mime_type = 'audio/x-mpequrl';
            }
            elseif ( strpos( $src, '.ogv') )
            {
              $mime_type = 'video/ogg';
            }
            else
            {
              $mime_type = 'video/mp4';
            } ?>
        <source src="<?php echo $src; ?>" type="<?php echo $mime_type; ?>">
      <?php else : ?>
        <source src="<?php echo $s['url']; ?>" type="<?php echo $s['mime_type']; ?>">
      <?php endif; ?>

      <object type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" width="640" height="360">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowFullScreen" value="true" />
        <param name="wmode" value="transparent" />
        <param name="flashVars" value="config={'playlist':{'url':'<?php echo $sources[0]['url']; ?>','autoPlay':true}]}" />
        <img alt="" src="<?php if ( $thumbnail != '' ) : echo $thumbnail; endif; ?>" width="640" height="360" title="No video playback capabilities." />
      </object>

      <?php /* if ( get_field( 'gif_hosted', $check_id ) ) : ?>

        <?php echo get_field(); ?>

      <?php elseif ( get_field( 'poster_image', $check_id ) ) : ?>

        <?php echo get_field(); ?>

      <?php else : ?>

      <?php endif; */ ?>

      <?php endforeach; ?>
    </video>

    <button class="video--play video--play__large" type="button"><?php inline_svg( 'va-play-large' ); ?></button>

    <div class="video--controls">

      <div class="video--seek"> <div class="video--progress"></div> </div>

      <div class="video--controls__inner">

        <button class="video--play video--play__small" type="button"></button>

        <div class="video--volume--container">

          <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/va-volume-mask.png" alt="">
          <div class="video--volume__progress"></div>

          <input class="video--volume" type="range" min="0" max="1" step="0.1" value="1">

        </div>

        <div class="video--time">
          <span class="video--current">00:00</span> / <span class="video--total">00:00</span>
        </div>

        <button class="video--fullscreen" type="button"></button>
      </div>

    </div>

  <?php if ( $legacy == false ) : ?>

    <?php $stored = (int) get_field( 'video_view_count', $check_id ); ?>

    <form class="form" method="post" style="display: none;">
      <?php $args = array( 'post_id' => $check_id, 'form' => false, 'fields' => array('field_57ecc8eab19cf'), 'submit_value' => '' ); acf_form( $args );  ?>
    </form>

  <?php endif; ?>

  </div>

  <?php endif; }

}

function getVideoPlayerHomepage( $src = false, $thumbnail = false, $id = false, $height = false, $autoplay = false, $muted = false)
{

  $legacy = false;

  $check = array( 'fp_video_link', 'mp4_source_remote_homepage', 'mp4_source_hosted_homepage', 'ogv_source_hosted_homepage', 'wemb_source_hosted_homepage' );

  $get_src = array();

  if ( $thumbnail == false )
  {

    if ( get_field( 'poster_image_homepage', $id ) )
    {

      if ( is_object( get_field( 'poster_image_homepage', $id ) ) || is_array( get_field( 'poster_image_homepage', $id ) ) )
      {
        if ( is_object( get_field( 'poster_image_homepage', $id ) ) )
        {
          $thumbnail = get_field( 'poster_image_homepage', $id )->ID;
        }
        else
        {
          $thumbnail = get_field( 'poster_image_homepage', $id )['ID'];
        }
      }
      else
      {
        $thumbnail = (int) get_field('poster_image_homepage', $id);
      }

      $thumbnail = wp_get_attachment_url($thumbnail);
    }
    else
    {
      $thumbnail = '';
    }

  }
  elseif ( $thumbnail == 'no' )
  {
    $thumbnail = '';
  }
  else
  {
    $thumbnail = $thumbnail;
  }

  if ( $src == false )
  {
    if ( get_field( 'external_video_homepage' ) == true && get_field( 'video_select_homepage' ) != false ) :

      $vid_id = get_field( 'video_select_homepage' )[0];

      $check_id = $vid_id;

    elseif ( get_field( 'external_video_homepage' ) == false ) :

      $check_id = get_the_ID();

      foreach ( $check as $c )
      {
        if ( get_field( $c, $check_id ) )
        {
          $check_id = get_the_ID();
        }

      }

    endif;

  }
  elseif ( $src != false )
  {
    if ( !is_numeric($src) )
    {
      $check_id = 'no_check';
    }
    else
    {
      $check_id = $src;
    }

  }

  if ( $check_id || $check_id == 'no_check' )
  {

    if ( $check_id == 'no_check' )
    {
      $legacy = true;

      $remote = array();
      $remote['url'] = $src;

      if ( strpos( $src, '.webm' ) )
      {
        $remote['mime_type'] = 'video/webm';
      }
      elseif ( strpos( $src, '.m3u') )
      {
        $remote['mime_type'] = 'audio/x-mpequrl';
      }
      else
      {
        $remote['mime_type'] = 'video/mp4';
      }

      $get_src[] = $remote;

    }
    else
    {
      foreach ( $check as $c )
      {
        if ( get_field( $c, $check_id ) )
        {

          if ( $c == 'mp4_source_remote_homepage' || $c == 'fp_video_link' )
          {

            $remote = array();
            $remote['url'] = get_field( $c, $check_id );
            if ( strpos( $remote['url'], '.webm' ) )
            {
              $remote['mime_type'] = 'video/webm';
            }
            elseif ( strpos( $remote['url'], '.m3u') )
            {
              $remote['mime_type'] = 'audio/x-mpequrl';
            }
            else
            {
              $remote['mime_type'] = 'video/mp4';
            }

            $get_src[] = $remote;
          }
          else
          {
            $get_src[] = get_field( $c, $check_id );
          }

        }

      }

    }

    $src = $get_src;

    $sources = $src;

  if ( !empty( $sources ) ) :

  ?>


  <div class="video--container">

    <video<?php if ( $height != false ) : echo ' height="'.$height.'"'; endif; ?> preload="auto" x-webkit-airplay="allow" webkit-playsinline src="<?php echo $sources[0]['url']; ?>" class="video--player auto-fade-in"<?php if ( $thumbnail != '' ) : ?> poster="<?php echo $thumbnail; ?>"<?php endif; ?> <?php if ($autoplay) : ?> autoplay loop<?php endif; if ($muted) : ?> muted<?php endif; ?>>
      <?php foreach ( $sources as $s ) : ?>

      <?php if ( is_numeric($s) ) : ?>
      <?php $src = wp_get_attachment_url($s); ?>
      <?php if ( strpos( $src, '.webm' ) )
            {
              $mime_type = 'video/webm';
            }
            elseif ( strpos( $src, '.m3u') )
            {
              $mime_type = 'audio/x-mpequrl';
            }
            elseif ( strpos( $src, '.ogv') )
            {
              $mime_type = 'video/ogg';
            }
            else
            {
              $mime_type = 'video/mp4';
            } ?>
        <source src="<?php echo $src; ?>" type="<?php echo $mime_type; ?>">
      <?php else : ?>
        <source src="<?php echo $s['url']; ?>" type="<?php echo $s['mime_type']; ?>">
      <?php endif; ?>

      <object type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" width="640" height="360">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowFullScreen" value="true" />
        <param name="wmode" value="transparent" />
        <param name="flashVars" value="config={'playlist':{'url':'<?php echo $sources[0]['url']; ?>','autoPlay':true}]}" />
        <img alt="" src="<?php if ( $thumbnail != '' ) : echo $thumbnail; endif; ?>" width="640" height="360" title="No video playback capabilities." />
      </object>

      <?php /* if ( get_field( 'gif_hosted', $check_id ) ) : ?>

        <?php echo get_field(); ?>

      <?php elseif ( get_field( 'poster_image', $check_id ) ) : ?>

        <?php echo get_field(); ?>

      <?php else : ?>

      <?php endif; */ ?>

      <?php endforeach; ?>
    </video>

    <button class="video--play video--play__large" type="button" style="opacity: 0;" ><?php inline_svg( 'va-play-large' ); ?></button>

    <div class="video--controls">

      <div class="video--seek"> <div class="video--progress"></div> </div>

      <div class="video--controls__inner">

        <button class="video--play video--play__small" type="button"></button>

        <div class="video--volume--container">

          <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/va-volume-mask.png" alt="">
          <div class="video--volume__progress"></div>

          <input class="video--volume" type="range" min="0" max="1" step="0.1" value="1">

        </div>

        <div class="video--time">
          <span class="video--current">00:00</span> / <span class="video--total">00:00</span>
        </div>

        <button class="video--fullscreen" type="button"></button>
      </div>

    </div>

  <?php if ( $legacy == false ) : ?>

    <?php $stored = (int) get_field( 'video_view_count', $check_id ); ?>

    <form class="form" method="post" style="display: none;">
      <?php $args = array( 'post_id' => $check_id, 'form' => false, 'fields' => array('field_57ecc8eab19cf'), 'submit_value' => '' ); acf_form( $args );  ?>
    </form>

  <?php endif; ?>

  </div>

  <?php endif; }

}


function getCredits($ID = false)
{

  if ( $ID == false )
  {
    $ID = get_the_ID();
  }
  else
  {
    $ID = $ID;
  }

  $terms = wp_get_post_terms( $ID, COLLECTION, 'ids' );

  $credits = array();

  foreach ( $terms as $term )
  {
    $credit = array();

    $credit['term_id'] = $term->term_id;
    $credit['slug'] = $term->slug;
    $credit['name'] = $term->name;
    $credit['parent'] = $term->parent;

    $credits[] = $credit;

  }

  return $credits;
}


function getBrandsPeople( $ID )
{


    // GETS IDs FOR BRAND AND CELEBRITY TERMS FROM COLLECTION TAXONOMY
      $b = get_term_by( 'slug', 'brand', COLLECTION );
      $c = get_term_by( 'slug', 'celebrity', COLLECTION );
      $m = get_term_by( 'slug', 'model', COLLECTION );
      $d = get_term_by( 'slug', 'fashion-designer', COLLECTION );
      $i = get_term_by( 'slug', 'fashion-influencer', COLLECTION );

    // BRAND TERM ID
      $b_id = $b->term_id;
    // CELEBRITY TERM ID
      $c_id = $c->term_id;
    // MODEL TERM ID
      $m_id = $m->term_id;
    // DESIGNER TERM ID
      $d_id = $d->term_id;
    // INFLUENCER TERM ID
      $i_id = $i->term_id;

      $terms = wp_get_post_terms( $ID, COLLECTION, 'ids' );

      $brands = array();
      $people = array();

      foreach ( $terms as $term )
      {
        if ( $term->parent == $b_id )
        {
          $brand = array();

          $brand['term_id'] = $term->term_id;
          $brand['slug'] = $term->slug;
          $brand['name'] = $term->name;
          $brand['parent'] = $term->parent;

          $brands[] = $brand;
        }
        else
        {
          $person = array();

          $person['term_id'] = $term->term_id;
          $person['slug'] = $term->slug;
          $person['name'] = $term->name;
          $person['parent'] = $term->parent;

          $people[] = $person;
        }

      }

      $class = false;

    // Generate list of people
      if ( !empty( $people ) )
      {

        $total = count( $people );

        if ( $total > 3 ) $class = ' related--minimise';

        echo '<ul class="related--tags related--people' . $class . '">';

        echo '<li class="related--heading">' . __( 'People in this post', 'vogue.me' ) . '</li>';

        foreach ( $people as $person )
        {

          $get_link = get_term_link( $person['slug'], COLLECTION );
          $profile_image = get_field( 'profile_image', COLLECTION . '_' . $person['term_id'] );

          $image = false;

          if ( $profile_image ) $image = ' <img src="' . $profile_image['url'] . '"
            alt="' . $person['name'] . '">';

          echo '<li> <a href="' . $get_link . '">' . $person['name'] . $image . '</a> </li>';

        }

        echo '</ul>';

      }

    // Generate list of brands
      if ( !empty( $brands ) )
      {

        $total = count( $brands );

        if ( $total > 3 ) $class = ' related--minimise';

        echo '<ul class="related--tags related--brands' . $class . '">';

        echo '<li class="related--heading">' . __( 'English', 'vogue.me' ) . '</li>';

        foreach ( $brands as $brand )
        {

          $get_link = get_term_link( $brand['slug'], COLLECTION );

          echo '<li> <a href="' . $get_link . '">' . $brand['name'] . '</a> </li>';

        }

        echo '</ul>';

      }

}







function getKeyTopic( $sep = false )
{

  $key_topic = get_field( 'key_topic', get_the_ID() );
  $key_category = get_field( 'key_category', get_the_ID() );

  $posttags = wp_get_post_terms( get_the_ID() , 'post_tag' );
  $postcats = wp_get_post_terms( get_the_ID() , 'category' );

  if ( $key_category )
  {
    $key = $key_category->name;
    $slug = get_term_link( $key_topic->slug, 'category' );
  }
  elseif ( $key_topic )
  {
    $key = $key_topic->name;
    $slug = get_term_link( $key_topic->slug, 'post_tag' );
  }
  elseif ( $postcats )
  {

    $i = 0; foreach ( $postcats as $cat )
    {
      if ( strpos( $cat->slug, 'legacy' ) === false )
      {
        $key = $cat->name;
        $slug = get_term_link( $cat->slug, 'category' );
        break;
      }

      $i++;
    }

  }
  else
  {

    if ( !is_null($posttags[0]) )
    {
      $key = $posttags[0]->name;
      $slug = get_term_link( $posttags[0]->slug, 'post_tag' );
    }

  }

  if ( isset($slug) ) : ?>
    <a class="post--tag" href="<?php echo $slug; ?>"><?php echo $key; ?></a>
      <?php if ( $sep != false ) : ?> <span class="post--seperator"></span><?php endif;
  else : 
    // This is here to prevent the element's parent from collapsing if there's nothing in it
    ?>
    <a class="post--tag" href="#">&nbsp;</a>
  <?php endif;

}

function getKeyTopicName( $sep = false )
{

  $key_topic = get_field( 'key_topic', get_the_ID() );
  $key_category = get_field( 'key_category', get_the_ID() );

  $posttags = wp_get_post_terms( get_the_ID() , 'post_tag' );
  $postcats = wp_get_post_terms( get_the_ID() , 'category' );

  if ( $key_category )
  {
    $key = $key_category->name;
    $slug = get_term_link( $key_topic->slug, 'category' );
  }
  elseif ( $key_topic )
  {
    $key = $key_topic->name;
    $slug = get_term_link( $key_topic->slug, 'post_tag' );
  }
  elseif ( $postcats )
  {

    $i = 0; foreach ( $postcats as $cat )
    {
      if ( strpos( $cat->slug, 'legacy' ) === false )
      {
        $key = $cat->name;
        $slug = get_term_link( $cat->slug, 'category' );
        break;
      }

      $i++;
    }

  }
  else
  {

    if ( !is_null($posttags[0]) )
    {
      $key = $posttags[0]->name;
      $slug = get_term_link( $posttags[0]->slug, 'post_tag' );
    }

  }



  if ( $slug ) {
    return $key;
  }

    

}

function getThumbnail( $id = false )
{

  if ( $id == false )
  {
    $id = get_the_ID();
  }
  else
  {
    $id = $id;
  }

  $thumbnail = '';
  $width = false;
  $height = false;

  $default = get_bloginfo( 'template_url' ) . '/assets/images/feed-placeholder.png';
  if ( has_post_thumbnail( $id ) )
  {
    $thumb = wp_get_attachment_metadata(get_post_thumbnail_id( $id ));
			
    if ( !empty( $thumb ) )
    {
      $width = $thumb['width'];
      $height = $thumb['height'];
    }
    $image = wp_get_attachment_url( get_post_thumbnail_id( $id ) );
		if(get_current_blog_id() == '3' ) { $image = str_replace('http://en.','http://ar.', $image); /*echo '<script>console.log("' . $image . '"); </script>';*/ }
	

    $handle = curl_init( $image );
    curl_setopt( $handle, CURLOPT_RETURNTRANSFER, TRUE );

    $response = curl_exec( $handle );
    $httpCode = curl_getinfo( $handle, CURLINFO_HTTP_CODE );

    if ( $httpCode == 404 || $httpCode == 500 )
    {
      $thumbnail = $default;
    }
    else
    {
      if ( $image == false || $image == '' )
      {
        $thumbnail = $default;
      }
      else
      {
        $thumbnail = $image;
      }

    }

    curl_close( $handle );

  }
  else
  {
    $thumbnail = $default;
  }

  return array( $thumbnail, $width, $height );

}


function get_ngg_table( $table )
{
  if ( check_site( 'women', false ) )
  {
    $gallery_table = 'wp_w_ngg_gallery';
    $pictures_table = 'wp_w_ngg_pictures';
    $fields_table = 'wp_w_ngg_fields';
    $field_table = 'wp_w_nggcf_field_values';
  }
  else
  {
    $gallery_table = 'wp_m_ngg_gallery';
    $pictures_table = 'wp_m_ngg_pictures';
    $fields_table = 'wp_m_ngg_fields';
    $field_table = 'wp_m_nggcf_field_values';
  }

  switch( $table )
  {
    case 'gallery';
        $table = $gallery_table;
        break;
    case 'pictures';
        $table = $pictures_table;
        break;
    case 'fields';
        $table = $fields_table;
        break;
    case 'field_value';
        $table = $field_table;
        break;
  }

  return $table;

}
// Adding Query Var to support sections on runway. Need to update to pretty urls
add_filter( 'query_vars', 'runway_section_query_vars' );

function runway_section_query_vars( $query_vars ) {
  $query_vars[] = 'section';
	return $query_vars;
}
// to make pretty url instead of ?section
add_rewrite_endpoint('section', EP_PERMALINK);
// to disable the auto redirection on the detail page but think for a better a way to isolate it for that case only
//remove_filter('template_redirect', 'redirect_canonical'); 
add_filter('redirect_canonical', 'bs_no_redirect_404');
function bs_no_redirect_404($redirect_url)
{
    if (is_404()) {
        return false;
    }
    return $redirect_url;
}

function get_designer_sidebar_new()
{
	
  $cat = get_queried_object();
  $cat_id = $cat->cat_ID;
  $runway = get_term_by( 'slug', 'runway', 'category' );
  $runway_id = $runway->term_id;
  $sub_cats = get_term_children( $runway_id, 'category' );
  $sub_cats = array_reverse( $sub_cats );
  if ( $cat_id == $runway_id )
  {
    $cat_id = $sub_cats[0];
    $latest = get_term_by( 'id', $sub_cats[0], 'category' );
    $cat_name = $latest->name;
   }
  $cat_args = array( 'post_type' => array( 'post','legacy' ), 'offset' => $offset, 'category__in' => array( $cat_id ), 'posts_per_page' => -1, 'orderby'=> 'title', 'order' => 'ASC' );
  $designers = get_posts( $cat_args );

  ?>
  <div class="show-grid-item is-sidebar">
    <div class="show-grid-item__inner">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/runwaylistbg.jpg">
      <div class="show-grid-item__sidebar">
        <?php $d = 1; foreach ( $designers as $designer )
          {
            $person = $designer->post_title;
			
			
            $firstLetter = substr( $person, 0, 1 );

            if ( $previous !== $firstLetter ) :
              if ( $d > 1 ) : echo '</ul>'; endif;
              echo '<ul>';
			  
			  $blog_id = get_current_blog_id();
			 // because arabic arlphabet didnt work
			  if($blog_id == 1){
				echo '<li class="first">' . $firstLetter . '</li>';
			  }
            endif;

            $previous = $firstLetter;

            echo '<li> <a href="' . get_permalink($designer->ID ) . '">' . $person . '</a> </li>';

            if ( $d == count($designers) ) : echo '</ul>'; endif;

            $d++;

          } ?>
      </div>
    </div>
  </div>
	 
<?php 
 wp_reset_postdata(); 
} ?>
<?php
function get_designer_mobile()
{
	
  $cat = get_queried_object();
  $cat_id = $cat->cat_ID;
  $runway = get_term_by( 'slug', 'runway', 'category' );
  $runway_id = $runway->term_id;
  $sub_cats = get_term_children( $runway_id, 'category' );
  $sub_cats = array_reverse( $sub_cats );
  if ( $cat_id == $runway_id )
  {
    $cat_id = $sub_cats[0];
    $latest = get_term_by( 'id', $sub_cats[0], 'category' );
    $cat_name = $latest->name;
   }
  $cat_args = array( 'post_type' => array( 'post','legacy' ), 'offset' => $offset, 'category__in' => array( $cat_id ), 'posts_per_page' => -1, 'orderby'=> 'title', 'order' => 'ASC' );
  $designers = get_posts( $cat_args );
?>
    <div class="filter-block__item mobile-show__list">
        <button type="button" class="toggle-filter-dropdown js-toggle-filter-dropdown"><span><?php echo __('All Shows', 'vogue.me' ); ?></span></button>
        <div class="filter-dropdown js-filter-dropdown">
          <ul class="filter-dropdown__list">
            <?php foreach ( $designers as $designer ) : 
				$person = $designer->post_title;
				echo '<li> <a href="' . get_permalink($designer->ID ) . '">' . $person . '</a> </li>';
			?>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
  
  
<?php 
 wp_reset_postdata(); 
} 


function getGalleryData( $gid ) {

  global $wpdb;

  $gallery_data = $wpdb->get_results("SELECT * FROM ".get_ngg_table('gallery')." WHERE gid='" . $gid . "'");

  if ( count($gallery_data) == 0 ) return false;

  $res = new stdClass;
  $res->id = $gid;
  $res->title = stripslashes( $gallery_data[0]->title );
  $res->path = stripslashes( $gallery_data[0]->path );
  $res->preview = stripslashes( $gallery_data[0]->previewpic );
  $res->title_ar = $res->title;

  $res->pictures = array();

  $pictures_data = $wpdb->get_results('SELECT * FROM '.get_ngg_table('pictures').' WHERE galleryid=' . $gid . ' ORDER BY sortorder, pid');

  foreach ( $pictures_data as $picture ) {

    $meta = getPictMetaCentral( $picture->pid );

    $pict = new stdClass;
    $pict->title = stripslashes( $picture->alttext );
    $pict->description = stripslashes( $picture->description );
    $pict->title_ar = ( $meta->title_ar!="" ? $meta->title_ar : $pict->title );
    $pict->description_ar = ( $meta->desc_ar!="" ? $meta->desc_ar : $pict->description );
    $pict->credits = $meta->credits;
    $pict->id = $picture->pid;
    $pict->filename = stripslashes($picture->filename);
    $pict->thumb = "thumbs/thumbs_" . $picture->filename;

    array_push( $res->pictures, $pict );

  }

  return $res;

}

/*
 * Pass a nexten picture id and get the associated metadata
 */
function getPictMetaCentral( $pict ) {

    global $wpdb;

    if ( !isset( $GLOBALS['cr_id'] ) || !isset( $GLOBALS['ti_id'] ) || !isset( $GLOBALS['ti_de'] ) ) {

        $pictures_data = $wpdb->get_results("SELECT id, field_name FROM ".get_ngg_table('fields')." WHERE field_name='Credits' OR field_name='Title Arabic' OR field_name='Description Arabic'");

        foreach ( $pictures_data as $row ) {

            if ( $row->field_name == "Credits" )  $GLOBALS['cr_id'] = $row->id;
            else if ( $row->field_name == "Title Arabic" ) $GLOBALS['ti_id'] = $row->id;
            else if ( $row->field_name == "Description Arabic" ) $GLOBALS['ti_de'] = $row->id;

        }

    }


    $picture_data = $wpdb->get_results("SELECT * FROM ".get_ngg_table('field_value')." WHERE pid='".$pict."'");

    $res = new stdClass;
    $res->credits = "";
    $res->title_ar = "";
    $res->desc_ar = "";

    foreach ( $picture_data as $row ) {

      if ( $row->fid == $GLOBALS['cr_id'] ) $res->credits = stripslashes( $row->field_value );
      else if ( $row->fid == $GLOBALS['ti_id'] ) $res->title_ar = stripslashes( $row->field_value );
      else if ( $row->fid == $GLOBALS['ti_de'] ) $res->desc_ar = stripslashes( $row->field_value );

    }

    return $res;

  }

  function getGalleryPreview( $gid ) {

  global $wpdb;

  $gallery_data = $wpdb->get_results("SELECT * FROM ".get_ngg_table('gallery')." WHERE gid='" . $gid . "'");
  if ( count($gallery_data) == 0 ) return false;

  $res = new stdClass;
  $res->preview_id = stripslashes( $gallery_data[0]->previewpic );
  $res->path = stripslashes( $gallery_data[0]->path );

  $pictures_data = $wpdb->get_results("SELECT * FROM ".get_ngg_table('pictures')." WHERE pid='" . $res->preview_id . "' ORDER BY sortorder, pid" );

  if ( count( $pictures_data ) >0 ) {
    $res->pict = stripslashes( $pictures_data[0]->filename );
    $res->thumb = "thumbs/thumbs_" . $res->pict;
  } else {
    return false;
  }

  return $res;


}



