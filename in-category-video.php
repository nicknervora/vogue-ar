<div class="page page--videos">

    <section class="video-details">
      <div class="container container--mid--reduced">
        <div class="video-details__inner">
          <div class="video-details__player">

<?php if ( get_field( 'fp_video_link' ) ) : ?>

<?php 

  $trending = array();

  $trending = getQuery__TrendingVideos( false, true ); // get all trending videos, but ignore this post, if it is trending

  $queued = array(); $queued[] = get_the_ID();

  if ( !empty( $queued ) )
  {
    $queued_playlist = array_unshift( $queued, get_the_ID() );
  }
  else
  {
    $queued_playlist = array_unshift( $trending, get_the_ID() );
  }

// merge queued and trending, with queued playing first
  $queued_playlist = array_merge( $queued, $trending );
// remove any duplicates from queue
  $queued_playlist = array_unique( $queued_playlist );
// if more than ten items in queue, trim to 10 items
  $count = count( $queued_playlist );
  if ( $count > 10 ) $queued_playlist = array_slice( $queued_playlist, 9 ); ?>



<?php elseif ( get_field( 'multivideo' ) == 'Yes' ) : ?>

  <?php $queued_playlist = get_field( 'fp_video_links' ); ?>

<?php else :

  $trending = array(); $trending = getQuery__TrendingVideos( false, true ); // get all trending videos, but ignore this post, if it is trending

  $queued = array();

  $queued[] = get_the_ID();

  if ( get_field( 'video_queue' ) ) :  foreach( get_field( 'video_queue' ) as $item ) :

  $queued[] = (int) $item;

  endforeach; endif;

  if ( !empty( $queued ) )
  {
    $queued_playlist = array_unshift( $queued, get_the_ID() );
  }
  else
  {
    $queued_playlist = array_unshift( $trending, get_the_ID() );
  }

// merge queued and trending, with queued playing first
  $queued_playlist = array_merge( $queued, $trending );
// remove any duplicates from queue
  $queued_list = array_unique( $queued_playlist, SORT_REGULAR );
// if more than ten items in queue, trim to 10 items
  $count = count( $queued_list );
  if ( $count > 10 ) $queued_list = array_slice( $queued_list, 0, 10 );

?>

<?php endif; ?>

  <?php if ( isset( $queued_list[0]['video_link'] ) ) : ?>
    <?php $src = $queued_list[0]['video_link']; $count = count($queued_list); ?>
  <?php else : ?>
    <?php $src = $queued_list[0]; ?>
  <?php endif; ?>

            <div class="video-details__video" id="currently_playing">
              <?php if ( get_field( 'poster_image' ) ) : $thumbnail = get_field('poster_image')['sizes']['large']; elseif ( get_field( 'feat_small_image' ) ) : $thumbnail = get_field( 'feat_small_image' ); else : $thumbnail = get_bloginfo( 'template_url' ) . '/assets/images/vogue-video-thumb.png'; endif; ?>
              <?php getVideoPlayer( $src, $thumbnail ); ?>
            </div>

            <div class="video-details__sidebar video-details-sidebar" id="video_queue">
              <div class="video-details-sidebar__inner">
              <?php $i = 1; foreach ( $queued_list as $post ) : if ( is_array( $post ) ) : ?>

              <?php $src = $post['video_link']; ?>
                <article class="video-details-sidebar__post js-in-queue<?php if ( $i == 1 ) : ?> js-is-playing<?php endif; ?><?php if ( $i == $count ) : ?> js-last-video<?php endif; ?>" id="queue_<?php echo sprintf( '%02d', $i ); ?>"<?php if ( $post['videotitle'] ) : ?> data-title="<?php echo $post['videotitle']; ?>"<?php endif; ?>>
                  <figure class="video-details-sidebar__thumb">
                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/vogue-video-thumb.png" alt="">
                    <figcaption class="fa fa-play"></figcaption>
                  </figure>
                  <h2 class="video-details-sidebar__title">
                    <span><?php echo $post['videotitle']; ?></span>
                  </h2>
                  <?php getVideoPlayer( $src, 'no' ); ?>
                  <?php $credits = getCredits(); if ( $credits ) : ?><div class="js-get-credits"><?php $c = 1; foreach( $credits as $credit ) : echo '<a href="' . get_term_link( $credit['slug'], COLLECTION ) . '">' . $credit['name'] . '</a>'; if ( $c > 1 ) echo ', '; $c++; endforeach; ?></div><?php endif; ?>
                </article>

              <?php else : setup_postdata( $post ); ?>
                <article class="video-details-sidebar__post js-in-queue<?php if ( $i == 1 ) : ?> js-is-playing<?php endif; ?><?php if ( $i == $count ) : ?> js-last-video<?php endif; ?>" id="queue_<?php echo sprintf( '%02d', $i ); ?>" data-title="<?php the_title(); ?>" data-url="<?php echo get_permalink(); ?>" data-date="<?php echo get_the_date(); ?>">
                  <figure class="video-details-sidebar__thumb">

                    <?php if ( get_field( 'poster_image' ) ) : ?>
                      <img src="<?php echo get_field('poster_image')['sizes']['medium']; ?>" alt="">
                    <?php elseif ( get_field( 'feat_small_image' ) ) : $l_feat_image = get_field( 'feat_small_image' ); ?>
                      <img src="<?php echo $l_feat_image; ?>" alt="">
                    <?php else : ?>
                      <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/vogue-video-thumb.png" alt="">
                    <?php endif; ?>
                    <figcaption class="fa fa-play"></figcaption>
                  </figure>
                  <h2 class="video-details-sidebar__title">
                    <span><?php the_title(); ?></span>
                  </h2>
                  <?php getVideoPlayer( get_the_ID(), 'no' ); ?>
                <?php if ( get_the_content() ) : ?>
                  <div class="video-details-sidebar__text">
                    <?php the_content(); ?>
                    <div class="clear"></div>
                  </div>
                <?php endif; ?>
                  <ul class="js-get-social"> <li><a href="<?php echo get_permalink(); ?>" target="_blank" class="fa fa-facebook"></a></li> <li><a href="<?php echo get_permalink(); ?>" target="_blank" class="fa fa-twitter"></a></li> </ul>
                  <?php $credits = getCredits(); if ( $credits ) : ?><div class="js-get-credits"><?php $c = 1; foreach( $credits as $credit ) : if ( $c > 1 ) echo ', '; echo '<a href="' . get_term_link( $credit['slug'], COLLECTION ) . '">' . $credit['name'] . '</a>'; $c++; endforeach; ?></div><?php endif; ?>
                </article>

              <?php endif; $i++; endforeach; wp_reset_postdata(); ?>
              </div>
            </div>
          </div>
          <div class="video-details__content video-details-content">
            <div class="video-details-content__top">
              <span class="video-details__category"> <?php getKeyTopic( true ); ?> </span>

              <span class="video-details__date"><strong class="js_video_date"><?php echo get_the_date(); ?></strong></span>

              <div class="clear"></div>
            </div>
            <h1 class="video-details-content__title">
                <?php the_title(); ?>
            </h1>
            <div class="video-details-content__text">
              <?php the_content(); ?>
              <div class="clear"></div>
            </div>       
          </div>
          <footer class="video-details__footer">
            <div class="video-details__share">
              <span>share</span>
              <?php $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>
              <ul class="js-put-social"> <li><a href="<?php echo $url; ?>" target="_blank" class="fa fa-facebook"></a></li> <li><a href="<?php echo $url; ?>" target="_blank" class="fa fa-twitter"></a></li> </ul>
            </div>

            <div class="video-details__credits">
            <?php $credits = getCredits(); if ( $credits ) : ?>
              <span><?php echo __('Credits'); ?></span>
              <p class="js-put-credits"> <?php $c = 1; foreach( $credits as $credit ) : if ( $c > 1 ) echo ', '; echo '<a href="' . get_term_link( $credit['slug'], COLLECTION ) . '">' . $credit['name'] . '</a>'; $c++; endforeach; ?> </p>
            <?php elseif (get_field('credits')) : echo '<span>'.__('Credits').'</span>'.get_field('credits'); endif; ?>
            </div>
          </footer>

        </div>
      </div>
    </section>

  </div>