<?php get_header(); ?>

<div class="scroll" data-ui="jscroll-default" data-continue="true">

<?php

  $paged = getPaged();

  $args = array( 'post_type' => array( 'post' ), 'paged' => $paged ); ?>

<?php $archive_query = new WP_Query( $args ); ?>

<?php if ( $archive_query->have_posts() ) : ?>

    <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
    <div class="get--content" data-title="" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

      <?php if ( $paged > 1 ) : getAdvert( 'strip' ); endif; ?>

      <div class="container container--mid">

        <ul class="list post--list post--latest">

        <?php $i = 1; while ( $archive_query->have_posts() ) : $archive_query->the_post(); ?>

          <?php getFeedItem( $i, $post ); ?>

        <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

        </ul>

      <?php if ( $paged == 1 ) : getAdvert( 'vert' ); endif; ?>

      </div>

      <?php getNextPageLink(); ?>

    </div>
    <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

  <?php endif; ?>

</div>

<?php get_footer(); ?>