<?php if ( is_user_logged_in() ) : if ( current_user_can( 'publish_posts' ) ) : header( 'location:' . get_bloginfo( 'url' ) . '/wp-admin' ); else : header( 'location:' . get_bloginfo( 'url' ) . '/account' ); endif; endif;

  /* Template Name: Account - Register */

  get_header();

?>

<section class="hero-banner">
  <div class="background" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/herobanner-runway.jpg');"></div>
</section>

<div class="scroll" data-ui="jscroll-default">

  <div class="get--content">

    <div id="account-header">
      <div id="ah-inner">
        <h1><?php echo __( 'Account registration', 'vogue.me' ); ?></h1>
      </div>
    </div>

    <div id="account-details">

      <span class="welcome"><?php echo __( 'Create an account', 'vogue.me' ); ?></span>

<?php if ( defined( 'REGISTRATION_ERROR' ) ) { foreach( unserialize( REGISTRATION_ERROR ) as $error ) { echo '<p class="order_error">'.$error.'</p><br>'; } } ?>

      <form id="my-registration-form" method="post" action="<?php echo add_query_arg('do', 'register', get_permalink( $post->ID )); ?>" class="form_comment">

        <div class="acc-hold left">
          <label><?php echo __( 'Email', 'vogue.me' ); ?></label>
          <input type="email" name="email" id="email" class="acc-text" value="<?php if(isset($_POST['email'])) echo $_POST['email'];?>">
        </div>

        <div class="acc-hold right">
          <label><?php echo __( 'Confirm Email', 'vogue.me' ); ?></label>
          <input type="email" name="cemail" id="cemail" class="acc-text">
        </div>

        <div class="acc-hold left">
          <label for="password"><?php echo __( 'Password', 'vogue.me' ); ?></label>
          <input type="password" name="pass" id="password" class="acc-pass">
        </div>

        <div class="acc-hold right">
          <label for="cpassword"><?php echo __( 'Confirm Password', 'vogue.me' ); ?></label>
          <input type="password" name="cpass" id="cpassword" class="acc-pass">
        </div>

        <button type="submit" name="submit" class="acc-submit"><span class="acc-arrow"><?php echo __( 'Create Account', 'vogue.me' ); ?></span></button>

      </form>

    </div>

  </div>

</div>

<?php get_footer(); ?>