<?php get_header();

  if ( is_tag() ) {
    $tag       = get_queried_object();
    $tag_title = $tag->name; // Same as single_tag_title()
    $tag_slug  = $tag->slug;
    $tag_id    = $tag->term_id;
    $tag_object = $tag->taxonomy;

    $colour = 'white';

    $get_taxonomy_term = $tag_object . '_' . $tag_id;

    $header_bg = getArchiveHeaderBG( $get_taxonomy_term );

    if(!$header_bg){
      $header_bg = get_template_directory_uri()."/assets/images/VA-tags-default-final.jpg";
    }

    $header_color = getArchiveHeaderCOLOR( $get_taxonomy_term );

  }

?>

<div class="scroll" data-ui="jscroll-default" data-continue="true">

<?php
  $paged = getPaged();

  $args = array( 'post_type' => array( 'post' ), 'tag' => $tag_slug, 'paged' => $paged ); ?>

<?php $tag_query = new WP_Query( $args ); ?>

<div class="archive--header"<?php if ( $header_bg ) : echo ' style="background-image: url(' . $header_bg . ');"'; endif; ?>>

  <h1 class="archive--heading archive--colour__<?php echo $header_color; ?>"><?php echo $tag_title; ?></h1>

</div>

<?php if ( $tag_query->have_posts() ) : $count = $tag_query->found_posts; ?>

    <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
    <div data-page="<?php echo $paged; ?>" data-max="<?php echo $tag_query->max_num_pages; ?>" class="get--content loaded" data-title="<?php echo single_tag_title(); ?>" data-url="<?php echo get_term_link( $tag_slug, $tag_object ); ?>" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

      <?php getAdvert( 'strip' ); ?>

      <div class="container container--mid">

        <ul class="list post--list post--latest">

        <?php $i = 1; while ( $tag_query->have_posts() ) : $tag_query->the_post(); ?>

         <?php getFeedItem( $i, $post ); ?>

        <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

        </ul>

        <?php getAdvert( 'vert' ); ?>

        <noscript>
          <div class="no-js-paganation">
            <a href="<?php echo get_term_link($tag_slug,$tag_object); ?>/page/<?php echo $paged + 1; ?>" class="button black"><?php echo __('Load more posts','vogue.me'); ?></a>
          </div>
        </noscript>

      </div>

    <?php getNextPageLink( $paged ); ?>

    </div>
    <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

  <?php endif; ?>

</div>

<?php get_footer(); ?>