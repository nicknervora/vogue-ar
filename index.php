<?php

  the_post();

  get_header();


  $paged = getPaged();

  $ignore_all = getQuery__Ignore();

  // $ignoring = implode( ',', $ignore_all );

  $ignore_uncategorized = get_category_by_slug( 'uncategorized' )->term_id;

// SET UP ARGUMENTS
  $args = array(
    'post_type' => array(
      'post'
    ),
    'post_status' => 'publish',
    'posts_per_page' => 5,
    'fields' => 'ids',
    'date_query' => array(
        array(
            'column' => 'post_date_gmt',
            'after' => '2 months ago'
        )
    ),
    'category__not_in' => array($ignore_uncategorized),
    'post__not_in' => $ignore_all,
    'paged' => $paged
  );

  //if ( CLEAR_QUERY_CACHE || CLEAR_QUERY_CACHE !== false ) : delete_transient( 'posts-'.$paged ); endif; if ( false === ( $posts = get_transient( 'posts-'.$paged ) ) ) {  
	$posts = new WP_Query($args); //set_transient( 'posts-'.$paged, $posts, 12 * 60 * 60 ); }

?>
<script>
  console.log("<?php echo json_encode($ignore_all); ?>");
</script>
<div class="scroll">

  <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
	<div data-page="<?php echo $paged; ?>" data-max="<?php echo $posts->max_num_pages; ?>" class="get--content loaded" data-title="" data-url="<?php echo bloginfo( 'url' ); ?><?php if ( $paged ) '/page/' . $paged . '/'; ?>">

<?php if ( $paged == 1 ) : ?>

    <?php getBreak_Promoted( true, false, true ); ?>

<div data-title="" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('TOPDESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('TOPMOB','pageid'); ?>">
  <div class="container container--mid">
    <?php getAdvert( 'strip', 'TOP' ); ?>
  </div>
  <?php getBreak_Recommends( 'voguearabia' ); ?>
</div>

<?php getBreak_Videos(); ?>

<?php endif; ?>

    <?php getSectionBreaks( $paged ); ?>

<div class="" data-title="" id="main_youcantblock<?php echo $paged; ?>" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

			<?php getAdvert( 'strip' ); ?>

      <h3 class="section--header auto-fade-in"> <?php echo __( 'Latest News', 'vogue.me' ); ?> </h3>

<div class="container container--mid">

      <ul class="list post--list post--latest">

      <?php $i = 1; while ( $posts->have_posts() ) : $posts->the_post(); ?>

         <?php getFeedItem( $i, $post ); ?>

      <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

      </ul>

      <?php getAdvert( 'vert' ); ?>

      <noscript>
        <div class="no-js-paganation">
          <a href="<?php bloginfo( 'url' ); ?>/page/<?php echo $paged + 1; ?>" class="button black"><?php echo __('Load more posts','vogue.me'); ?></a>
        </div>
      </noscript>

		</div>
	
	</div>

    <?php if ( $posts->max_num_pages != $paged ) : ?> <?php getNextPageLink( $paged ); ?> <?php endif; ?>

  </div>
  <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

</div>

<?php get_footer(); ?>