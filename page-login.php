<?php if ( is_user_logged_in() ) : if ( current_user_can( 'publish_posts' ) ) : header( 'location:' . get_bloginfo( 'url' ) . '/wp-admin' ); else : header( 'location:' . get_bloginfo( 'url' ) . '/account' ); endif; endif;

  /* Template Name: Account - Login */ get_header();

?>


<section class="hero-banner">
  <div class="background" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/herobanner-runway.jpg');"></div>
</section>

<div class="scroll" data-ui="jscroll-default">

  <div class="get--content">

    <div id="account-header">
      <div id="ah-inner">
        <h1><?php echo __( 'Account Login', 'vogue.me' ); ?></h1>
      </div>
    </div>

    <div id="account-details">

      <span class="welcome"><?php echo __( 'Welcome Back!', 'vogue.me' ); ?></span>
    <?php if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>
      <p style="margin-bottom: 30px;"><?php echo __( 'You have entered an incorrect Email or password, please try again.', 'vogue.me' ); ?></p>
    <?php endif; ?>

      <form name="loginform" id="loginform" action="<?php bloginfo( 'url' ); ?>/wp-login.php" method="post">

        <div class="acc-hold left">
          <label for="user_login"><?php echo __( 'Email', 'vogue.me' ); ?></label>
          <input type="text" name="log" id="user_login" class="acc-text">
        </div>

        <div class="acc-hold right">
          <label for="user_pass"><?php echo __( 'Password', 'vogue.me' ); ?></label>
          <input type="password" name="pwd" id="user_pass" class="acc-pass">
        </div>

        <div class="acc-clearer"></div>

        <p><?php echo __( 'Forgotten Password?', 'vogue.me' ); ?> <a href="<?php echo site_url( '/account/recover' ); ?>"><?php echo __( 'Reset', 'vogue.me' ); ?></a></p>

        <div class="acc-clearer" style="height: 35px;"></div>

        <button type="submit" name="wp-submit" id="wp-submit" class="acc-submit"><span class="acc-arrow"><?php echo __( 'Login', 'vogue.me' ); ?></span></button>
        
        <input type="hidden" name="redirect_to" value="<?php bloginfo( 'url' ); ?>/account" />
        
      </form>

    </div>

  </div>

</div>


<?php get_footer(); ?>