<?php get_header();

if ( is_category() ) :

  $cat = get_queried_object();
  $cat_id = $cat->cat_ID;
  $cat_name = $cat->name;
  $cat_description = $cat->description;
  $cat_slug = $cat->slug;
  $cat_object = $cat->taxonomy;

  $get_taxonomy_term = 'category_' . $cat_id;

  $header_bg = getArchiveHeaderBG( $get_taxonomy_term );

  if(!$header_bg){
    $header_bg = get_template_directory_uri()."/assets/images/VA-category-default-final.jpg";
  }

  $header_color = getArchiveHeaderCOLOR( $get_taxonomy_term );

  // A check to see whether the current category is the parent category or not

  function is_subcategory( $check_cat_id = 0 )
  {
    $pp = get_category_parents( $check_cat_id, false, '!' );

    if ( substr_count( $pp, '!' ) > 1 )
      return true;
    else
      return false;

  }

endif;

?>



<div class="scroll" data-ui="jscroll-default" data-continue="true">

<?php

  $paged = getPaged();

  $cat_query = getQuery__Category( $paged, $cat_slug );

  if ( !isset( $_GET['sort'] ) || ( isset( $_GET['sort'] ) && $_GET['sort'] == 'recent' ) ) : $sort = 'recent'; else : $sort = 'popular'; endif;

  //if ( CLEAR_QUERY_CACHE || CLEAR_QUERY_CACHE !== false ) : delete_transient( 'category_postlist-'.$sort.'-'.$paged ); endif; if ( false === ( $category_posts = get_transient( 'category_postlist-'.$sort.'-'.$paged ) ) ) {

	$category_posts = new WP_query($cat_query); //set_transient( 'category_postlist-'.$sort.'-'.$paged, $category_posts, 12 * 60 * 60 ); }

?>

  <div class="archive--header"<?php if ( $header_bg ) : echo ' style="background-image: url(' . $header_bg . '); background-size: cover;"'; endif; ?>>

    <h1 class="archive--heading archive--colour__<?php echo $header_color; ?>"><?php echo $cat_name; ?></h1>

  </div>

<?php if ( is_subcategory( $cat_id ) == false ) :

  $get_cats = $cat_id;
  $get_cats_p_slug = $cat_slug;
  $get_cats_name = $cat_name;

  $get_class = ' current-cat';

else :

  $cat_parent = get_category_by_slug( explode( ',', get_category_parents( $cat_id, false, ',', true ) )[0] );

  $get_cats = $cat_parent->term_id;
  $get_cats_p_slug = $cat_parent->slug;
  $get_cats_name = $cat_parent->name;

  $get_class = '';

endif; ?>

<?php $categories = get_the_category(); $category_id = $categories[0]->cat_ID; ?>

    <section class="filter-block filter-block--light filter-block--mobile">
      <div class="filter-block__item">
        <button type="button" class="toggle-filter-dropdown js-toggle-filter-dropdown"><span><?php echo $get_cats_name; ?></span></button>
        <div class="filter-dropdown js-filter-dropdown">
          <ul class="filter-dropdown__list">
            <?php wp_list_categories( array( 'child_of' => $get_cats, 'title_li' => '', 'hide_empty' => true, 'hierarchical' => false ) ); ?>
          </ul>
        </div>
      </div>
    </section>

    <ul class="container container--mid child--filter--list">

      <li class="cat-item"><a href="<?php echo get_term_link( $get_cats_p_slug, 'category' ); ?>"><?php echo __( 'All', 'vogue.me' ); ?></a></li>

      <?php $cat_list = get_field( 'category_navigation', 'option' ); ?>

      <?php foreach ( $cat_list as $item ) : ?>
      <?php if ( $item->parent == $get_cats ) : ?><li class="cat-item"><a href="<?php echo get_term_link( $item->term_id, 'category' ); ?>"><?php echo $item->name; ?></a></li><?php endif; ?>
      <?php endforeach; ?>

    </ul>


<?php if ( $category_posts->have_posts() ) : ?>

  <div class="container container--mid">
    <div class="archive-sort">
      <ul>
        <li>
          <a href="<?php get_term_link( $cat_slug, 'category' ); ?>?sort=recent" class="<?php if ( !isset( $_GET['sort'] ) || ( isset( $_GET['sort'] ) && $_GET['sort'] == 'recent' ) ) : echo ' is-active'; endif; ?>"><?php echo __( 'Most recent', 'vogue.me' ); ?></a>
        </li>
        <li>
          <a href="<?php get_term_link( $cat_slug, 'category' ); ?>?sort=popular" class="<?php if ( isset( $_GET['sort'] ) && $_GET['sort'] == 'popular' ) : echo ' is-active'; endif; ?>"><?php echo __( 'Most popular', 'vogue.me' ); ?></a>
        </li>
      </ul>
    </div>
  </div>

    <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
    <div data-page="<?php echo $paged; ?>" data-max="<?php echo $category_posts->max_num_pages; ?>" class="get--content loaded" data-title="<?php echo single_tag_title(); ?>" data-url="<?php echo get_category_link( $cat_id ); ?>" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

      <?php
          getAdvert('strip');
      ?>

      <div class="container container--mid">

        <ul class="list post--list post--latest">

        <?php $i = 1; while( $category_posts->have_posts() ) : $category_posts->the_post(); setup_postdata($post); ?>

        <?php // $i = 1; foreach ( $category_posts as $post ) : setup_postdata( $post ); ?>

          <?php getFeedItem( $i, $post ); ?>

        <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

        <?php // $i++; endforeach; wp_reset_postdata(); wp_reset_query(); ?>

        </ul>

        <?php getAdvert( 'vert' ); ?>

      </div>

      <?php
          if ($paged != $category_posts->max_num_pages)
          {
                   if ($paged % 3 == 1) getBreak_Newsletter();
              else if ($paged % 3 == 2) getBreak_Social();
              else if ($paged % 3 == 0) getBreak_Selection();
          }
      ?>

      <noscript>
        <div class="no-js-paganation">
          <a href="<?php echo get_term_link( $cat_slug, 'category' ); ?>page/<?php echo $paged + 1; ?>" class="button black"><?php echo __('Load more posts','vogue.me'); ?></a>
        </div>
      </noscript>

      <?php // if ( $category_posts->max_num_pages != $paged ) : ?> <?php getNextPageLink( $paged ); ?> <?php // endif; ?>

    </div>
    <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

  <?php endif; ?>

  </div>

<?php get_footer(); ?>