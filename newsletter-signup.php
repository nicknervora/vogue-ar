<?php 

// Template name: Newsletter signup

get_header(); 
// load_plugin_textdomain('vogue.me', get_template_directory() . '/languages/ar_SY.mo' );
?>

	<section class="nl-signup" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/newsletter-signup-bg.jpg');">
		<div class="container container--mid">
			<div class="nl-signup__inner">
				<div class="nl-signup__box">
					<form id="newsletter-signup" class="nl-signup__form nl-signup-form">
						<header class="nl-signup-form__head">
							<figure class="nl-signup-form__logo">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/vogue_g_b.svg">
								<figcaption><span><?php echo __('Weekly', 'vogue.me'); ?></span></figcaption>
							</figure>	
						</header>
						<div class="nl-signup-form__body">
							<div class="nl-signup-form__input-wrapper">
								<label for="name"><?php echo __('Your name', 'vogue.me'); ?></label>
								<input type="text" id="newsletter-name" name="name">
							</div>
							<div class="nl-signup-form__input-wrapper">
								<label for="email"><?php echo __('Your email', 'vogue.me'); ?></label>
								<input type="text" id="newsletter-email" name="name">
							</div>
						</div>
						<footer class="nl-signup-form__foot">
							<div class="message nl-signup-form__message"></div>
							<input type="submit" class="nl-signup-form__submit" value="<?php echo __('Sign up now', 'vogue.me'); ?>">
						</footer>
					</form>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>