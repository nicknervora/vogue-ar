<?php 

// Template name: Stylebase overview

	get_header();

	$b = get_term_by( 'slug', 'brand', COLLECTION );
  $c = get_term_by( 'slug', 'celebrity', COLLECTION );
  $m = get_term_by( 'slug', 'model', COLLECTION );
  $d = get_term_by( 'slug', 'fashion-designer', COLLECTION );
  $i = get_term_by( 'slug', 'fashion-influencer', COLLECTION );

// BRAND TERM ID
  $b_id = $b->term_id;
// CELEBRITY TERM ID
  $c_id = $c->term_id;
// MODEL TERM ID
  $m_id = $m->term_id;
// DESIGNER TERM ID
  $d_id = $d->term_id;
// INFLUENCER TERM ID
  $i_id = $i->term_id;

  $exclude = array( (int) $b_id, (int) $c_id, (int) $m_id, (int) $d_id, (int) $i_id );

  $all_tags = array(
  	'taxonomy' => array(COLLECTION),
  	'fields' => 'ids',
  	'exclude' => $exclude,
  	'hide_empty' => false
  );

  $include = array();
  $the_rest = array();

  $get_all_tags = get_terms( $all_tags );

  $i = 1; foreach ( $get_all_tags as $item )
  {
  	$tax_term = COLLECTION . '_' . $item;
  	$view_count = get_field( 'vogue_view_count', $tax_term );

  	if ( $view_count > 0 )
  	{
  		$int_item = (int) $item;

  		$include[(int) $view_count . '_' . $i ] = $int_item;
  	}
  	else
  	{
  		$int_item = (int) $item;

  		$the_rest[] = $int_item;
  	}

  	$i++;

  }
	
 	ksort( $include, SORT_NUMERIC );

  $push = array();

  $p = 0; foreach( $include as $item => $value )
  {
  	$push[$p] = (int) $value;

  	$p++;
  }

  $popular = $push;

?>

	<div class="hero hero--sb">
		<div class="hero__bg" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/sb-overview-hero.jpg');"></div>
	</div>

	<section class="pc pc--sb sb">

		<div class="pc__header pc-header">
			<div class="container container--mid--reduced">
				<div class="pc-header__inner">
					<div class="pc-header__top">
						<h1 class="pc-header__title"><?php echo __( 'Vogue Collection', 'vogue.me' ); ?></h1>
					</div>
					<div class="pc-header__search">

						<?php VogueCollectionSearch(); ?>

					</div>
				</div>
			</div>
		</div>

<?php $brands = array(); ?>

<?php

	$row = 1;

	$top_selection = get_field( 'top_selection', 'option' );

	$selected = array();
	$the_rest = array();

	foreach ( $top_selection as $item ) 
	{
		$selected[] = $item->term_id; 
	}

	shuffle( $selected );

	foreach ( $popular as $item )
  {
  		$item = (int) $item;

  		array_unshift( $selected, $item );
  }

  $selected = array_merge( $selected, $the_rest );

	$top_selection = array_unique( $selected );

	$top_selection = array_slice( $top_selection, 0, 16 );

  $collection_posts = get_field( 'collection_posts', 'option' );

?>

		<section class="sb-gallery">
			<div class="container container--mid">
				<header class="sb-gallery__head">
					<h1 class="sb-gallery__title"><?php echo __('Top selection','vogue.me'); ?></h1>
				</header>
				<div class="sb-gallery__inner">

<?php $i = 1; foreach ( $top_selection as $item ) : $item = get_term_by( 'id', $item, COLLECTION ); $term_tax = COLLECTION . '_' . $item->term_id; ?>

					<?php if ( $row % 2 == 0 ) : ?>
						<?php if ( $i % 2 == 1 ) : $field = 1; else : $field = 2; endif; ?>
					<?php else : ?>
						<?php if ( $i % 2 == 1 ) : $field = 2; else : $field = 1; endif; ?>
					<?php endif; ?>

					<?php if ( $field == 2 ) : ?>
						<?php $image = get_field( 'list_image_landscape', $term_tax ); ?>
					<?php else : ?>
						<?php $image = get_field( 'list_image_portrait', $term_tax ); ?>
					<?php endif; ?>

					<?php if ( $i % 4 == 1 ) : ?> <div class="row"> <?php endif; ?>

						<article class="sb-gallery-item">
							<a href="<?php echo get_term_link( $item->slug, COLLECTION ); ?>">

								<?php $image = $image['url']; ?>

								<div class="sb-gallery-item__bg" style="background-image: url(' <?php if ( $image ) : if ( $field == 1 ) : echo imageProvider( $image, 180, 265 ); else : echo imageProvider( $image, 380, 265 ); endif; endif; ?> ');"></div>

								<div class="sb-gallery-item__inner">

							<?php if ( $item->parent == $b_id ) : $brands[] = $item->term_id; ?>

								<?php if ( get_field( 'brand_logo', $term_tax ) ) : ?>
									<img data-echo="<?php echo get_field( 'brand_logo', $term_tax ); ?>" alt="<?php echo $item->name; ?>"<?php if ( $field == 1 ) : ?> class="plus_logo"<?php endif; ?>>
								<?php else : ?>
									<h1><?php echo $item->name; ?></h1>
								<?php endif; ?>

							<?php else : ?>
									<h1><?php echo $item->name; ?></h1>
									<span><?php echo str_replace( '-', ' ', get_term_by( 'id', $item->parent, COLLECTION )->slug ); ?></span>
							<?php endif; ?>

								</div>

							</a>
						</article>

					<?php if ( $i % 4 == 0 ) : $row++; ?> </div> <?php endif; ?>

<?php $i++; endforeach; ?>
					
					<?php if ( $i % 4 != 1 ) : ?> </div> <?php endif; ?>

				</div>
			</div>
		</section>


    <div class="clear"></div>

<?php // WITH RUNWAY POST ?>
    <section class="sb-section--splits container container--mid">

      <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
      <div class="get--content sb-content--split width--max" data-title="<?php echo single_tag_title(); ?>" data-url="<?php echo get_category_link( $cat_id ); ?>">

          <h3 class="section--header left--aligned auto-fade-in"> <?php echo __( 'Articles' ); ?> </h3>

        <?php $group_01 = array_slice( $collection_posts, 0, 3 ); ?>

          <ul class="list post--list post--latest narrow">

      <?php $i = 1; foreach ( $group_01 as $post ) : setup_postdata( $post ); ?>

        <?php getFeedItem( $i, $post ); ?>

      <?php $i++; endforeach; wp_reset_postdata(); wp_reset_query(); ?>

          </ul>

      </div>
      <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

      <div class="sb-promoted-area area--runway">

        <h3 class="section--header auto-fade-in"> <?php echo __( 'Runway','vogue.me' ); ?> </h3>

      <?php

        $collection_runway = get_field( 'collection_runway', 'option' );

        $runway_id = $collection_runway[0]->ID;

        $runway = get_term_by( 'slug', 'runway', 'category' );
        $runway_get_id = $runway->term_id;

        $in_cat = wp_get_post_categories( $runway_id );

        $runway_cat = false;

        foreach ( $in_cat as $cat )
        {
          $is_cat = get_term_by( 'id', $cat, 'category' );

          if ( $is_cat->parent == $runway_get_id )
          {
            $runway_cat = $is_cat->name;
            break;
          }

        }


      ?>

      <?php setup_postdata( $runway_id ); $gallery_id = $runway_id; ?>

      <?php if ( get_field( 'linked_gallery' ) && get_post_type() !== 'legacy' ) :

          $gallery = get_field( 'linked_gallery' ); $gallery_id = $gallery->ID;

          $thumbnail = getThumbnail( $gallery_id )[0];

        elseif ( get_post_type() == 'legacy' ) : 

          $new_gallery = get_field("linked_gallery");

          $thumbnail = get_runway_legacy_thumbnail( $new_gallery );

          if ( is_null( $thumbnail ) ) :
            $thumbnail = getThumbnail( $gallery_id )[0];
          endif;

        else :

          $thumbnail = getThumbnail( $gallery_id )[0];

        endif; ?>

        <a href="<?php echo get_the_permalink( $runway_id ); ?>" class="sb-promoted-area__inner">

          <div class="area--runway__image" style="background-image: url( '<?php echo $thumbnail; ?>' );"></div>

          <div class="area--runway__title">
            <?php if ( $runway_cat ) : ?><span class="runway--category"> <?php echo $runway_cat; ?> </span><?php endif; ?>
            <span class="runway--show"><?php echo get_the_title( $runway_id ); ?></span>
          </div>

        </a>

      <?php wp_reset_postdata(); ?>

      </div>

    </section>


		<section class="sb-content-section sb-content-section--people">

			<div class="container container--mid">

				<div class="section-hero" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/stylebase/people-hero.jpg');"></div>

				<div class="sb-content-section__mid">
					<h1><?php echo __( 'People','vogue.me' ); ?></h1>
					<a href="<?php echo bloginfo( 'url' ); ?>/vogue-view/?all=people">
						<?php echo __( 'Explore more','vogue.me' ); ?><?php inline_svg('va-gallery-next'); ?>
					</a>
				</div>

				<div class="sb-content-section__items">
					<article class="sb-content-section-item">
						<a href="<?php echo bloginfo( 'url' ); ?>/vogue-view/?all=celebrities">
							<div class="sb-content-section-item__bg" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/stylebase/celebrities.jpg');"></div>
							<h1><?php echo __( 'Celebrities','vogue.me' ); ?></h1>
						</a>
					</article>

					<article class="sb-content-section-item">
						<a href="<?php echo bloginfo( 'url' ); ?>/vogue-view/?all=influencers">
							<div class="sb-content-section-item__bg" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/stylebase/fashion-influences.jpg');"></div>
							<h1><?php echo __( 'Fashion Influencers','vogue.me' ); ?></h1>
						</a>
					</article>

					<article class="sb-content-section-item">
						<a href="<?php echo bloginfo( 'url' ); ?>/vogue-view/?all=models">
							<div class="sb-content-section-item__bg" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/stylebase/models.jpg');"></div>
							<h1><?php echo __( 'Models','vogue.me' ); ?></h1>
						</a>
					</article>

					<article class="sb-content-section-item">
						<a href="<?php echo bloginfo( 'url' ); ?>/vogue-view/?all=designers">
							<div class="sb-content-section-item__bg" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/stylebase/fashion-designers.jpg');"></div>
							<h1><?php echo __( 'Fashion Designers','vogue.me' ); ?></h1>
						</a>
					</article>

				</div>	
			</div>
		</section>


    <div class="clear"></div>

<?php // WITH SERIES LINK ?>
    <section class="sb-section--splits container container--mid">

    <?php $collection_series = get_field( 'collection_series', 'option' ); ?>

    <?php $series = get_term_by( 'id', $collection_series, 'series' ); ?>

      <div class="sb-promoted-area area--series">

        <h3 class="section--header auto-fade-in"> <?php echo __( 'Hot right now','vogue.me' ); ?> </h3>

        <div class="sb-promoted-area__inner">

          <a href="<?php echo get_term_link( $collection_series, 'series' ); ?>">

            <div class="series--image">

            <?php if ( get_field( 'promoted_series_image', 'series_' . $collection_series ) ) : ?>
              <?php $image = get_field( 'promoted_series_image', 'series_' . $collection_series ); ?>
            <?php else : ?>
              <?php $image = get_field( 'series_image', 'series_' . $collection_series ); ?>
            <?php endif; ?>

              <?php $size_01 = imageProvider( $image['sizes']['large'], 400 ); ?>
              <?php $size_02 = imageProvider( $image['url'], 800 ); ?>

              <img data-echo="<?php echo $size_01; ?>" srcset="<?php echo $size_02; ?> 700w" alt="">
            </div>

            <div class="series--tag">
              <h3 class="series--tag__main"> <?php echo $series->name; ?> <span class="series--tag__count"> <?php echo $series->count . ' ' . __( 'Posts','vogue.me' ); ?> </span> </h3>
            </div>

          </a>

        </div>

      </div>

      <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
      <div class="get--content sb-content--split width--max" data-title="<?php echo single_tag_title(); ?>" data-url="<?php echo get_category_link( $cat_id ); ?>">

          <h3 class="section--header auto-fade-in"> <?php echo __( 'Articles','vogue.me' ); ?> </h3>

        <?php $group_02 = array_slice( $collection_posts, 3, 3 ); ?>

          <ul class="list post--list post--latest narrow">

      <?php $i = 1; foreach ( $group_02 as $post ) : setup_postdata( $post ); ?>

        <?php getFeedItem( $i, $post ); ?>

      <?php $i++; endforeach; wp_reset_postdata(); wp_reset_query(); ?>

          </ul>

      </div>
      <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

    </section>


		<section class="sb-content-section sb-content-section--brands">
			<div class="container container--mid">
				<div class="section-hero" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/stylebase/brands-hero.jpg');"></div>
				<div class="sb-content-section__mid">
					<h1><?php echo __( 'Brands' ); ?></h1>
					<a href="<?php echo bloginfo( 'url' ); ?>/vogue-view/?all=brands">
						<?php echo __( 'Explore more','vogue.me' ); ?><?php inline_svg('va-gallery-next'); ?>
					</a>
				</div>
				<div class="sb-content-section__items">

<?php foreach ( $brands as $item ) : $item = get_term_by( 'id', $item, COLLECTION ); $term_tax = COLLECTION . '_' . $item->term_id; ?>

	        <?php $image = get_field( 'list_image_portrait', $term_tax ); ?>

		      <?php $image = $image['url']; ?>

					<article class="sb-content-section-item">
						<a href="<?php echo get_term_link( $item->slug, COLLECTION ); ?>">
							<div class="sb-content-section-item__bg" style="background-image: url('<?php if ( $image ) echo imageProvider( $image, 192, 260 ); ?>');"></div>
							
							<?php if ( get_field( 'brand_logo', $term_tax ) ) : ?>
								<img data-echo="<?php echo get_field( 'brand_logo', $term_tax ); ?>" alt="<?php echo $item->name; ?>" class="sb-content-section-item__logo">
							<?php else : ?>
								<h1><?php echo $item->name; ?></h1>
							<?php endif; ?>

						</a>
					</article>

<?php endforeach; ?>

				</div>	
			</div>
		</section>

	</section>

<?php get_footer(); ?>