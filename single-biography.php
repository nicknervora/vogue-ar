<?php

  get_header();

  the_post();

  $current_id = get_the_ID();

  $get_taxonomy_term = 'biography' . '_' . $tag_id;

  $header_bg = getArchiveHeaderBG( get_the_ID() );

  $get_user = false;

  $video_cat_id = get_category_by_slug( 'video' )->term_id;

  $ignore = array();

  $ignore[] = $video_cat_id;
  foreach( getRunwayCats() as $cat )
  {
    $ignore[] = $cat;
  }

?>


<div class="scroll" data-ui="jscroll-default" data-continue="true">



    <div class="hero hero--sb">

      <div class="hero__bg"<?php if ( $header_bg ) : echo ' style="background-image: url(' . $header_bg . ');"'; endif; ?>></div>

    </div>

    <section class="pc pc--sb sb">

      <div class="pc__header pc-header">

        <div class="container container--mid--reduced">

          <div class="pc-header__inner">

            <div class="pc-header__top">

              <h1 class="pc-header__title"><?php echo the_title(); ?></h1>
            <?php if ( get_field( 'social_profiles' ) ) : $social = get_field( 'social_profiles' ); ?>
              <ul class="pc-social">
            <?php foreach ( $social as $profile ) : $network = $profile['network']; $account = $profile['tag']; ?>
              <?php if ( $network == 'instagram' ) : $get_user = $account; endif; ?>
                <li><a class="fa fa-<?php echo $network; ?>" href="<?php echo get_social_url( $network, $account ); ?>" target="_blank"></a></li>
            <?php endforeach; ?>
              </ul>
            <?php endif; ?>
            </div>

          </div>

        </div>

      </div>

      <section class="sb-info">
      
        <div class="container container--mid">
        
          <div class="sb-info__inner">
          
            <div class="sb-info__left">

          <?php if ( get_field( 'born' ) ) : ?> <p class="sb-info__born"> <span><?php echo __( 'Born', 'vogue.me' ); ?></span> <?php echo get_field( 'born' ); ?> </p> <?php endif; ?>
          <?php if ( get_field( 'nationality' ) ) : ?> <p class="sb-info__nationality"> <span><?php echo __( 'Nationality', 'vogue.me' ); ?></span> <?php echo get_field( 'nationality' ); ?> </p> <?php endif; ?>
          <?php if ( get_field( 'location' ) ) : $location_id = get_field( 'location' ); $location = get_term( $location_id, 'countries' ); ?> <p class="sb-info__location"> <span><?php echo __( 'Location', 'vogue.me' ); ?></span> <a href="<?php echo get_term_link( $location->slug, 'countries' ); ?>"><?php echo $location->name; ?></a> </p> <?php endif; ?>

            </div>

            <div class="sb-info__content">
              <h2 class="sb-info__title"><?php echo __( 'Biography', 'vogue.me' ); ?></h2>
              <div class="sb-info__intro"> <?php echo the_content(); ?> </div>
            </div>
          </div>
          <hr />
        </div>

      </section>

<?php $paged = getPaged(); $args = array( 'post_type' => array( 'legacy', 'post' ), 'posts_per_page' => 5, 'category__not_in' => $ignore, 'paged' => $paged, 'meta_key' => 'linked_contestent', 'meta_value' => get_the_ID() ); $get_posts = new WP_Query( $args ); ?>

<?php if ( $get_posts->have_posts() ) : ?>

      <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
      <div class="get--content sb-post-list loaded" data-title="" data-url="<?php echo get_term_link( $current_id, 'biography' ); ?>" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

        <div class="container container--mid">

          <ul class="list post--list post--latest post--list--centered">

          <?php $i = 1; while ( $get_posts->have_posts() ) : $get_posts->the_post(); setup_postdata( $post ); ?>

            <?php getFeedItem( $i, $post, 'simple' ); ?>

          <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

          </ul>

        </div>

        <?php getNextPageLink(); ?>

        <?php if ( $paged == 1 && $get_user != false ) : getBreak_Instagram( $get_user ); endif; ?>

      </div>
      <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

<?php endif; ?>

    </div>

  </section>

<?php get_footer(); ?>
