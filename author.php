<?php

  get_header();

  $author = get_user_by( 'slug', get_query_var( 'author_name' ) );

  $author_id = $author->ID;

  $get_taxonomy_term = 'user_' . $author_id;

  $header_bg = getArchiveHeaderBG( $get_taxonomy_term );

  $header_color = 'white';

  if ( getArchiveHeaderCOLOR( $get_taxonomy_term ) )
  {
    $header_color = getArchiveHeaderCOLOR( $get_taxonomy_term );
  }

?>

<div class="scroll" data-ui="jscroll-default" data-continue="true">

   <header class="section--header header--author"<?php if ( $header_bg ) : ?> style="background-image: url(<?php echo $header_bg; ?>);"<?php endif; ?>>

    <div class="header--inner">

      <div class="inner--profile">
        <?php $args = array( 'class' => 'profile--image' ); ?>
        <?php
          $avatar = get_avatar_url( $author_id=$post->post_author, array( 'size' => 96, 'default' => 404 ) );

          if( $avatar !== false )
          {
              $profile_image = $profile_image; 
          }
          else
          {
            $profile_image = get_bloginfo('template_url').'/assets/images/Vogue-V-icon.svg';
          }

        ?>

        <?php $social_profiles = array(); ?>

        <?php $count = get_user_meta($author_id,'social_profiles'); $count = (int) $count[0]; $count = ( $count - 1 ); ?>

        <?php for ($x = 0; $x <= $count; $x++) : ?>

          <?php $social_profile = array(); ?>

          <?php $network = 'social_profiles_' . $x . '_network'; ?>
          <?php $account = 'social_profiles_' . $x . '_tag'; ?>

          <?php $get_network = get_user_meta( $author_id, $network ); ?>
          <?php $get_account = get_user_meta( $author_id, $account ); ?>

          <?php $social_profile[] = $get_network; ?>
          <?php $social_profile[] = $get_account; ?>

          <?php $social_profiles[] = $social_profile; ?>

        <?php endfor; ?>

        <div class="profile--image <?php if ( empty( $social_profiles ) ) : ?>no--social<?php endif; ?>"> <?php if ( strpos( $profile_image, '404' ) !== false ) : ?> <img src="<?php echo $profile_image; ?>" alt=""> <?php else : ?> <img class="default" src="<?php echo get_bloginfo('template_url') . '/assets/images/Vogue-V-icon.svg'; ?>" alt=""> <?php endif; ?> </div>

        <?php if ( !empty( $social_profiles ) ) : ?>

        <ul class="pc-social brand">

        <?php foreach( $social_profiles as $profile ) : ?>

          <?php $network = $profile[0][0]; ?>
          <?php $account = $profile[1][0]; ?>

          <li><a class="fa fa-<?php echo $network; ?>" href="<?php echo get_social_url( $network, $account ); ?>" target="_blank"></a></li>
        
        <?php endforeach; ?>

        </ul>

        <?php endif; ?>

      </div>

      <div class="inner--description archive--colour__<?php echo $header_color; ?>">
        <h1 class="section--heading<?php if ( get_field( 'author_bio_ar', $get_taxonomy_term ) || get_the_author_meta( 'description' ) ) : else : echo ' no--description'; endif; ?>"> <?php if ( check_site( false, 'ar' ) && get_field( 'author_name_ar', $get_taxonomy_term ) ) : echo get_field( 'author_name_ar', $get_taxonomy_term ); else : echo nl2br( get_the_author() ); endif; ?> </h1>
        <?php if ( check_site( false, 'ar' ) && get_field( 'author_bio_ar', $get_taxonomy_term ) ) : echo '<p>' . get_field( 'author_bio_ar', $get_taxonomy_term ) . '</p>'; else : if ( get_the_author_meta( 'description' ) ) : echo '<p>' . get_the_author_meta( 'description' ) . '</p>'; endif; endif; ?>
      </div>

    </div>

  </header>

<?php $ignore_all = getQuery__Ignore(); $paged = getPaged(); $args = array( 'author' => $author_id, 'post__not_in' => $ignore_all, 'post_type' => array( 'legacy', 'post' ), 'paged' => $paged ); $author_query = new WP_Query( $args ); ?>
    
    <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
    <div data-page="<?php echo $paged; ?>" data-max="<?php echo $author_query->max_num_pages; ?>"  class="get--content" data-title="<?php echo nl2br( get_the_author() ); ?><?php if ( $paged >= 2 ) : ?> || Page <?php echo $paged; ?><?php endif; ?>" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

      <?php if ( $author_query->have_posts() ) : ?>

      <?php if ( $paged != 1 ) : getAdvert( 'strip' ); endif; ?>

      <div class="container container--mid">

        <?php if ( $paged == 1 ) : ?><h3 class="section--header auto-fade-in"> <?php echo __( 'Latest News', 'vogue.me' ); ?> </h3><?php endif; ?>

        <ul class="list post--list post--latest">

        <?php $i = 1; while ( $author_query->have_posts() ) : $author_query->the_post(); ?>

          <?php getFeedItem( $i, $post ); ?>

        <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

        </ul>

      <?php if ( $paged == 1 ) : getAdvert( 'vert' ); endif; ?>

        <?php endif; ?>

        <div class="clear" style="height:60px;"></div>

      </div>

      <?php getAdvert('strip'); ?>

      <?php getNextPageLink(); ?>

    </div>
    <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

</div>

<?php get_footer(); ?>