<?php

  get_header();

if ( is_category() ) :

  $cat = get_queried_object();
  $cat_id = $cat->cat_ID;
  $cat_name = $cat->name;
  $cat_description = $cat->description;
  $cat_slug = $cat->slug;
  $cat_object = $cat->taxonomy;
//var_dump($cat_id);
  $cat_id = 109; // fixed cat, same as runway homepage (with no filter)
  $get_taxonomy_term = 'category_' . $cat_id;

  $header_bg = getArchiveHeaderBG( $get_taxonomy_term );
   if(!$header_bg){
    $header_bg = get_template_directory_uri()."/assets/images/Runway.jpg";
  }

  $header_color = getArchiveHeaderCOLOR( $get_taxonomy_term );

endif;

function get_designer_sidebar()
{
  $d = get_term_by( 'slug', 'fashion-designer', COLLECTION );
  $d_id = $d->term_id;

  $designers = get_term_children( $d_id, COLLECTION );

  $previous = null;

  $get_designers = array();

  ?>
  <div class="show-grid-item is-sidebar">
    <div class="show-grid-item__inner">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/runwaylistbg.jpg">
      <div class="show-grid-item__sidebar">
        <?php $d = 1; foreach ( $designers as $designer )
          {
            $person = get_term_by( 'id', $designer, COLLECTION );

            $firstLetter = substr( $person->name, 0, 1 );

            if ( $previous !== $firstLetter ) :
              if ( $d > 1 ) : echo '</ul>'; endif;
              echo '<ul> <li class="first">' . $firstLetter . '</li>';
            endif;

            $previous = $firstLetter;

            echo '<li> <a href="' . get_term_link( $person->slug, COLLECTION ) . '">' . $person->name . '</a> </li>';

            if ( $d == count($designers) ) : echo '</ul>'; endif;

            $d++;

          } ?>
      </div>
    </div>
  </div>
<?php } ?>

<div class="scroll extra-class-3" data-ui="jscroll-default" data-continue="true">

  <div class="archive--header no--margin"<?php if ( $header_bg ) : echo ' style="background-image: url(' . $header_bg . ');"'; endif; ?>>

    <h1 class="archive--heading archive--colour__<?php echo $header_color; ?>"><?php echo __( 'Runway', 'vogue.me' ); ?></h1>

  </div>



  <?php


    $runway = get_term_by( 'slug', 'runway_ar', 'category' );
    $runway_id = $runway->term_id;

    $sub_cats = get_term_children( $runway_id, 'category' );

    $sub_cats = array_reverse( $sub_cats );

    if ( $cat_id == $runway_id )
    {
      $cat_id = $sub_cats[0];
      $latest = get_term_by( 'id', $sub_cats[0], 'category' );
      $cat_name = $latest->name;
    }

    $current_cat_postion = array_search( $cat_id, $sub_cats ); ?>


    <section class="filter-block">
      <!--
      <div class="filter-block__item">
        <button type="button" class="toggle-filter-dropdown js-toggle-filter-dropdown"><span><?php echo __('Designer'); ?></span></button>
        <div class="filter-dropdown js-filter-dropdown">
          <ul class="filter-dropdown__list">
            <?php foreach ( $sub_cats as $cat ) : ?>
            <li>
              <a href="<?php echo get_term_link( $cat ); ?>?i=true"><?php echo get_cat_name( $cat ); ?></a>
            </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
      -->
      <div class="filter-block__item">
        <button type="button" class="toggle-filter-dropdown js-toggle-filter-dropdown"><span><?php echo __('Season'); ?></span></button>
        <div class="filter-dropdown js-filter-dropdown">
          <ul class="filter-dropdown__list">
            <?php foreach ( $sub_cats as $cat ) : ?>
            <li>
              <a href="<?php echo get_term_link( $cat ); ?>?i=true"><?php echo get_cat_name( $cat ); ?></a>
            </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </section>


    <?php $c = 0; $max = count( $sub_cats ); foreach ( $sub_cats as $key => $cat )
    {
      $paged = getPaged();

      $offset = 0;

      $post_count = 25;

      if ( $paged > 1 )
      {
        //$post_count = 25;
        $offset = ( ($paged-1) * $post_count );
      }

      $cat_args = array( 'post_type' => array( 'post','legacy' ), 'offset' => $offset, 'category__in' => array( $cat_id ), 'paged', $paged, 'posts_per_page' => $post_count );

      // $runway_categories = get_posts($cat_args);

      $runway_categories = new WP_Query($cat_args);

      if ( $runway_categories ) :

        // $latest_collection = $runway_categories[0];
        // $first_collection = end($runway_categories);

        $start_date = '';
        $end_date = '';
        // $start_date = get_the_time( 'F j, Y', $first_collection );
        // $end_date = get_the_time( 'F j, Y', $latest_collection );

?>
    
    <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
    <div data-page="<?php echo $paged; ?>" data-max="<?php echo $runway_categories->max_num_pages; ?>"<?php /* data-page="<?php echo $current_cat_postion + 1; ?>" data-max="<?php echo $max; ?>" */ ?> data-set="true" class="get--content loaded" data-title="<?php echo $cat_name; ?>" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

    <?php if ( !isset( $_GET['f'] ) ) : ?>

      <div class="show-grid__header">
        <h2 class="show-grid__entry-title"><?php echo $cat_name; ?></h2>
        <p class="show-grid__entry-date"><?php echo $start_date; ?> - <?php echo $end_date; ?></p>
      </div>

      <div class="show-grid__titles">
        <!--<h2 class="show-grid__title"><?php echo __( 'All Designers', 'vogue.me' ); ?></h2>-->
        <h2 class="show-grid__title"><?php echo __( 'Top Shows', 'vogue.me' ); ?></h2>
      </div>
	  <div class="show-grid__title-mobile">
          <?php get_designer_mobile(); ?>
        </div>

    <?php else : ?>

      <div class="show-grid__header">
        <div class="show-grid__title-mobile">
          <h2><?php echo __( 'Most recent shows', 'vogue.me' ); ?></h2>
        </div>
        <h2 class="show-grid__entry-title"><?php echo $cat_name; ?></h2>
        <p class="show-grid__entry-date"><?php echo $start_date; ?> - <?php echo $end_date; ?></p>
      </div>

      <div class="show-grid__titles">
        <h2 class="show-grid__title"><?php echo __( 'Most recent shows', 'vogue.me' ); ?></h2>
      </div>

    <?php endif; ?>

      <section class="show-grid with--padding" style='min-height: 100vh;'>

      <?php if ( $paged == 1 ) : get_designer_sidebar_new(); endif; ?>

      <?php while ( $runway_categories->have_posts() ) : $runway_categories->the_post(); $gallery_id = $post; ?>

      <?php // foreach ( $runway_categories as $post ) : setup_postdata($post); $gallery_id = $post; ?>

      <?php if ( get_field( 'linked_gallery' ) && get_post_type() !== 'legacy' ) :

          $gallery = get_field( 'linked_gallery' ); $gallery_id = $gallery->ID;

          $thumbnail = getThumbnail( $gallery_id )[0];

        elseif ( get_post_type() == 'legacy' ) : 

          //$new_gallery = get_field("linked_gallery");
          //$thumbnail = get_runway_legacy_thumbnail( $new_gallery );

          $new_gallery = get_field("linked_gallery");
          $linked_gallery = get_field("linked_gallery");
          $linked_gallery_id = $new_gallery->ID;
          $gallery_image_uploader = get_field('gallery_image_uploader', $linked_gallery_id);
          $thumbnail = $gallery_image_uploader[0]['image']['url'];

          if ( is_null( $thumbnail ) ) :
            $thumbnail = getThumbnail( $gallery_id )[0];
          endif;

        else :

          $thumbnail = getThumbnail( $gallery_id )[0];

        endif;

        $thumb_large = imageProvider($thumbnail,360,540);
        $thumb_small = imageProvider($thumbnail,170,255);
        $thumb_medium = imageProvider($thumbnail,270,420);
        ?>

      <?php // if ( get_field( 'gallery_type', $gallery_id ) == 'Runway' || $gallery ) : ?>
        <div class="show-grid-item">
          <div class="show-grid-item__inner" style='width: 92%;'>
            <?php /* <div class="loading"></div> */ ?>
            <a class="show-grid-item__bg" href="<?php echo get_permalink(/* $gallery_id */); ?><?php if ( !get_the_content() ) : echo '#/gallery/'; endif; ?>">
              <picture class="auto-fade-in">
                <source srcset="<?php echo $thumb_large; ?>" media="(min-width:401px)">
                <source srcset="<?php echo $thumb_medium; ?>" media="(min-width:768px)">
                <img data-echo="<?php echo $thumb_small; ?>" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/V-loading.gif" alt="">
              </picture>
              <div class="show-grid-item__content">
                  <div class="show-grid-item__content-inner">
                    <?php the_title(); ?>
                  </div>
              </div>
            </a>
          </div>
        </div>
      <?php // endif; ?>

      <?php endwhile; wp_reset_postdata(); wp_reset_query(); // endforeach; wp_reset_postdata(); wp_reset_query(); ?>

      </section>

      <?php //if ( !isset( $_GET['i'] ) ) : ?>
        <?php if ( $runway_categories->max_num_pages >= $paged ) : ?> <?php getNextPageLink( $paged ); ?> <?php endif; ?>
        <?php /* <div class="next jscroll-next-parent"> <a href="<?php echo get_term_link( $sub_cats[ $current_cat_postion + 1 ], 'category' ); ?>?f=true"></a> </div> */ ?>
      <?php // endif; ?>

      <?php getAdvert( 'strip' ); ?>

    </div>
    <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

      <?php endif; ?>


    <?php $c++; break;
    }

  ?>

</div>

<?php get_footer(); ?>