<?php 

	if ( !isset( $_GET[ 'all' ] ) )
	{
		$view = __('all','vogue.me');
	}
	else
	{
		$allowed = array( 'all', 'people', 'brands', 'designers', 'influencers', 'models', 'celebrities' );

		$view = $_GET[ 'all' ];

		if ( !in_array( $view, $allowed ) )
		{
			$view = __('all','vogue.me');
		}

	}

// Template name: Stylebase listings

	get_header();

	$b = get_term_by( 'slug', 'brand', COLLECTION );
  $c = get_term_by( 'slug', 'celebrity', COLLECTION );
  $m = get_term_by( 'slug', 'model', COLLECTION );
  $d = get_term_by( 'slug', 'fashion-designer', COLLECTION );
  $i = get_term_by( 'slug', 'fashion-influencer', COLLECTION );

// BRAND TERM ID
  $b_id = $b->term_id;
// CELEBRITY TERM ID
  $c_id = $c->term_id;
// MODEL TERM ID
  $m_id = $m->term_id;
// DESIGNER TERM ID
  $d_id = $d->term_id;
// INFLUENCER TERM ID
  $i_id = $i->term_id;

  $brand_array = array( $b_id );
  $people_array = array( $c_id, $m_id, $d_id, $i_id );

  switch( $view )
  {
  	case 'brands';
  		$get_collection = $brand_array;
  		break;
  	case 'people';
  		$get_collection = $people_array;
  		break;
  	case 'designers';
  		$get_collection = array( $d_id );
  		break;
  	case 'influencers';
  		$get_collection = array( $i_id );
  		break;
  	case 'models';
  		$get_collection = array( $m_id );
  		break;
  	case 'celebrities';
  		$get_collection = array( $c_id );
  		break;
  	default;
  		$get_collection = array( $b_id, $c_id, $m_id, $d_id, $i_id );
  		break;

  }

	$group = array();

	foreach ( $get_collection as $collection )
	{
		$get = get_term_children( $collection, COLLECTION );

		foreach ( $get as $item )
		{
			array_push( $group, (int) $item );
		}

	}

// ALPHABET ARRAY
	$alphabet = array( __('a','vogue.me'), __('b','vogue.me' ), __('c','vogue.me'), __('d','vogue.me'), __('e','vogue.me'), __('f','vogue.me'), __('g','vogue.me'), __('h','vogue.me'), __('i','vogue.me'), __('j','vogue.me'), __('k','vogue.me'), __('l','vogue.me'), __('m','vogue.me'), __('n','vogue.me'), __('o','vogue.me'), __('p','vogue.me'), __('q','vogue.me'), __('r','vogue.me'), __('s','vogue.me'), __('t','vogue.me'), __('u','vogue.me'), __('v','vogue.me'), __('w','vogue.me'), __('x','vogue.me'), __('y','vogue.me'), __('z','vogue.me') );

// ARGUMENTS FOR SORTED LIST OF PEOPLE/BRANDS
	$list_args = array(
  	'taxonomy' => array(COLLECTION),
  	'fields' => 'ids',
  	'include' => $group,
  	'hide_empty' => false
  );

// GET LIST OF PEOPLE/BRANDS
	$list = get_terms( $list_args );

?>

  <div class="archive--header no--margin" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/sb-overview-hero.jpg');">

    <h1 class="archive--heading"><?php if ( $view == 'all' ) : echo __('Vogue Collection'); else : echo ucfirst( $view ); endif; ?></h1>

  </div>

<div class="scroll">

	<section class="pc pc--sb sb" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

		<div class="container container--mid">
			<div class="sb-listings section--header">
				<nav class="sb-listings-pagination">
          <div class="inner">
            <div class="inner--wrap">
					<ul>
            <li class="all"><a href="#" data-filter="all"><?php echo __('All','vogue.me'); ?></a></li>
						<?php foreach ( $alphabet as $letter ) : ?><li><a href="#<?php echo $letter; ?>" data-filter="<?php echo strtoupper( $letter ); ?>"><?php echo $letter; ?></a></li><?php endforeach; ?>
					</ul>
            </div>
          </div>
				</nav>

				<div class="sb-gallery sb-gallery--listings  section--header">

				<?php

					$row = 1;

					$i = 1; foreach( $list as $item ) :

					$is = get_term_by( 'id', $item, COLLECTION );

					$term_tax = COLLECTION . '_' . $is->term_id;

					$firstLetter = substr( $is->name, 0, 1 );

					if ( $previous !== $firstLetter ) : if ( $i > 1 ) : echo '</article>'; endif; ?>

				    	<article data-filter="<?php echo $firstLetter; ?>" data-to_filter="<?php echo $firstLetter; ?>" data-filter="false" class="sb-gallery-item sb-gallery-item--letter" id="<?php echo strtoupper( $firstLetter ); ?>">
								<a href="#">
									<div class="sb-gallery-item__inner sb-gallery-item__inner--lettter">
										<span><?php echo $firstLetter; ?></span>
									</div>
								</a>
							</article>
				    
				    <?php endif; $previous = $firstLetter; ?>

				    <?php if ( $row % 2 == 0 ) : ?>
							<?php if ( $i % 2 == 1 ) : $field = 1; else : $field = 2; endif; ?>
						<?php else : ?>
							<?php if ( $i % 2 == 1 ) : $field = 2; else : $field = 1; endif; ?>
						<?php endif; ?>

				    <?php if ( $field == 2 ) : ?>
							<?php $image = get_field( 'list_image_landscape', $term_tax ); ?>
						<?php else : ?>
							<?php $image = get_field( 'list_image_portrait', $term_tax ); ?>
						<?php endif; ?>

					    <article data-to_filter="<?php echo $firstLetter; ?>" data-filter="false" class="sb-gallery-item">
								<a href="<?php echo get_term_link( $is->slug, COLLECTION ); ?>">

									<?php if ( isset( $image['sizes']['medium'] ) ) : ?>
										<?php $image = $image['sizes']['medium']; ?>
									<?php elseif ( isset( $image['sizes']['medium_large'] ) ) : ?>
										<?php $image = $image['sizes']['medium_large']; ?>
									<?php else : ?>
										<?php $image = $image['url']; ?>
									<?php endif; ?>


									<div class="sb-gallery-item__bg" style="background-image: url('<?php if ( $image ) echo $image; ?>');"></div>
									<div class="sb-gallery-item__inner">

									<?php if ( get_field( 'brand_logo', $term_tax ) ) : ?>
										<img src="<?php echo get_field( 'brand_logo', $term_tax ); ?>" alt="<?php echo $is->name; ?>">
									<?php else : ?>
										<h1><?php echo $is->name; ?></h1>
									<?php if ( in_array( $is->parent, $people_array ) ) : ?>
										<span><?php $cat = get_term_by( 'id', $is->parent, COLLECTION )->slug; echo str_replace( array('-', 'fashion'), array( ' ', '' ), $cat ); ?></span>
									<?php endif; ?>
									<?php endif; ?>

									</div>
								</a>
							</article>

				    <?php if ( $i == count($list) ) : echo '</ul>'; endif; ?>

				    <?php $i++; $row++; endforeach; ?>

				</div>

			</div>
      <?php getAdvert('vert'); ?>
      <div id="temp-storage" class="clear"></div>
		</div>

	</section>
</div>
<?php get_footer(); ?>