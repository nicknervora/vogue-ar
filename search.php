<?php get_header();

      $video_cat_id = get_category_by_slug( 'video' )->term_id;

      $ignore = array();

      $ignore[] = $video_cat_id;
      foreach( getRunwayCats() as $cat )
      {
        $ignore[] = $cat;
      }

      $paged = getPaged();

      $args = array( 's' => $_GET['s'], 'post_type' => array( 'post' ), 'posts_per_page' => 10, 'category__not_in' => $ignore, 'paged' => $paged );

      $search_query = new WP_Query( $args ); if ( $search_query->have_posts() ) :

?>

<div class="scroll" data-ui="jscroll-default" data-continue="true">

  <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
  <div data-page="<?php echo $paged; ?>" data-max="<?php echo $search_query->max_num_pages; ?>" class="get--content loaded" data-title="" data-url="" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

<?php /* FIRST PAGE ONLY */ if ( $paged == 1 ) : ?>

  <?php //getBreak_Promoted(); ?>

<?php endif; ?>

      <?php getAdvert( 'strip' ); ?>

      <div class="container container--mid">

        <?php if ( $paged == 1 ) : ?><h3 class="section--header auto-fade-in"> <?php echo __( 'Search Results', 'vogue.me' ); ?> </h3><?php endif; ?>

        <ul class="list post--list post--latest">

        <?php $i = 1; while ( $search_query->have_posts() ) : $search_query->the_post(); ?>

          <?php getFeedItem( $i, $post ); ?>

        <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

        </ul>

        <?php getAdvert( 'vert' ); ?>

        <noscript>
          <div class="no-js-paganation">
            <a href="<?php echo get_bloginfo('url'); ?>/page/<?php echo $paged + 1; ?>?s=<?php echo $_GET['s']; ?>" class="button black"><?php echo __('Load more posts','vogue.me'); ?></a>
          </div>
        </noscript>

      </div>

    <?php getNextPageLink( $paged ); ?>

  <?php endif; ?>

  </div>
  <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

</div>

<?php get_footer(); ?>