<?php

function embed_gallery( $id = false )
{
  global $post;

  $p = shortcode_atts( array(
    'id' => $id['number']
  ), $id );

  if ( $p['id'] == 1 || $p['id'] == false )
  {
    $post_id = get_the_ID();
  }
  else
  {
    $post_id = $p['id'];
  }

  $gallery = get_field( 'custom_gallery_embed', $post_id );

  // var_dump(get_field( 'custom_gallery_embed', $post_id ));

  $shoppable = false;

  $output = '';

if ( $gallery ) :

  $output .= '<div class="article--gallery no-js-initiate js-the-article-gallery">';

  $i = 1; while ( have_rows( 'custom_gallery_embed', $post_id ) ) : the_row(); if ( get_row_layout() == 'embed_gallery' ) : $item = get_sub_field( 'custom_gallery' ); $c = 1; /* var_dump($item); */ $output .= '<div class="article--gallery--inner">'; foreach ( $item as $custom ) :

    if ( $custom[ 'cdescription' ] && ( strpos( $custom[ 'cdescription' ], 'Buy it now' ) !== false || strpos( $custom[ 'cdescription' ], 'US $' ) !== false || strpos( $custom[ 'cdescription' ], 'amazon' ) !== false || strpos( $custom[ 'cdescription' ], 'Shop here' ) !== false || strpos( $custom[ 'cdescription' ], 'Shop now' ) !== false ) ) { $shoppable = true; }

      $output .= '<div class="gallery--item">';

      if ( $custom[ 'cimage' ] ) : $output .= '<div class="item--image"> <img data-lazy="' . $custom['cimage'] . '" alt=""> </div>'; endif;

      if ( $custom[ 'cdescription' ] ) :

        $output .= '<div class="item--content">';

        if ( $shoppable == true ) : $output .= '<div class="make--shoppable"></div>'; endif;

        if ( !$shoppable ) :
          
          $output .= '<header class="item--content__header"> <div class="item--caption">' . get_the_title() . '</div>';

            if ( $c == 1 ) : $output .= '<div class="gallery--change to--list"></div>'; endif;

          $output .= '</header>';
        
        endif;

        $output .= '<section class="item--content__copy"> <p>' . $custom[ 'cdescription' ] . '</p> </section> </div>';

      endif;

      $output .= '<div class="clear"></div> </div>';

  $c++; endforeach;

    $output .= '</div> <div class="article--gallery--meta">';

  // If not a shoppable gallery show option to view thumbs
    if ( !$shoppable ) : $output .= '<div class="gallery--view--thumbs"></div>'; endif;

  // Gallery position
    $output .= '<div class="gallery--position"> <span class="current--slide">1</span> <span class="position--seperator"></span> <span class="total--slides"></span> </div> <div class="gallery--navigation"></div> </div>';

  if ( !$shoppable ) : 
    
  // if is not shoppable generate thumb view panel
    $output .= '<div class="article--gallery--grid--view">';

    if ( get_row_layout() == 'embed_gallery' ) : $item = get_sub_field( 'custom_gallery' ); $c = 1; foreach ( $item as $custom ) :

      $output .= '<div class="grid--item"> <a href="#"> <img src="' . $image['cimage'] . '" alt=""> </a> </div>';

    endforeach; endif;


            $output .= '<div class="gallery--change to--list"></div>';


    $output .= '</div>';

  endif; endif; $i++; endwhile;

  $output .= '</div>';

  return $output;

  elseif ( get_post_type( $post_id ) == 'post' && get_field( 'gallery', $post_id ) ) :

// RUSHED DEVELOPMENT OF EMBED GALLERIES FOR NEW POST TYPE

    $legacy = false;
    $class = false;
    $count = 20;

    var_dump($post_id . ' 1' );

    if ( get_field( 'shoppable', $post_id ) == false )
    {
      $is_shoppable = false;
    }
    else
    {
      $is_shoppable = true;
      $class = 'shoppable';
    }

    if ( get_field( 'gallery', $post_id ) )
    {

      $gallery = get_field( 'gallery', $post_id );

    }

    else
    {

      $gallery = false;

    }

    if ( $gallery !== false && !is_null( $gallery ) ) :

      $output .= '<div class="article--gallery no-js-initiate'; if ( $class ) $output .= ' ' . $class; $output .= ' js-the-article-gallery">';

      $output .= '<div class="article--gallery--inner">';

      $i = 1; foreach ( $gallery as $item ) :

        $output .= '<div class="gallery--item">';

        if ( $item[ 'image' ] ) :

          $output .= '<div class="item--image post-share-image">';

            $output .= '<img data-lazy="' . $item['image']['sizes']['medium'] . '" height="' . $item['image']['height'] . '" width="' . $item['image']['width'] . '" srcset="' . $item['image']['url'] . '" sizes="480px" alt="' . $item['image']['title'] . '">';

            $output .= '<ul class="post-share-image__links">';
              $output .= '<li class="post-share-image__link"><a href="#" class="fa fa-pinterest"></a></li>';
              $output .= '<li class="post-share-image__link"><a href="#" class="fa fa-facebook"></a></li>';
              $output .= '<li class="post-share-image__lightbox"><a href="#" class="fa fa-search js-has-lightbox" data-src="' . $item['image']['url'] . '"></a></li>';
              $output .= '<li class="post-share-image__toggle"><button class="js-post-share-image-toggle">';
                $output .= '<span class="open">' . inline_svg( 'va-more','','',true ) . '</span>';
                $output .= '<span class="close">' . inline_svg( 'va-close','','',true ) . '</span>';
              $output .= '</button></li>';
            $output .= '</ul>';

            if ( $i == 2 ) :

              $output .= '<div class="article--gallery--meta__sticky">';
                $output .= '<div class="gallery--view--thumbs gallery--view--thumbs__sticky"></div>';

                $output .= '<div class="gallery--position__small">';
                  $output .= '<b class="js-gallery--position__small--cnt">1</b>/' . count( $gallery );
                $output .= '</div>';
              $output .= '</div>';

            endif;

            $output .= '</div>';

      endif;

      if ( $legacy == true ) :

          $output .= '<div class="item--content">';

            $output .= '<header class="item--content__header">';

              if ( $item[ 'image_caption' ] || $item['image']['caption'] ) : $output .= '<div class="item--caption">'; if ( $item[ 'image_caption' ] ) : $output .= $item[ 'image_caption' ]; else : $output .= $item['image']['caption']; endif; $output .= '</div>'; endif;
              if ( $item[ 'photo_credit' ] ) : $output .= '<div class="item--credit"> Photo: ' . $item[ 'photo_credit' ] . '</div>'; endif;

            $output .= '</header>';

            $output .= '<section class="item--content__copy">';

              if ( $item[ 'image_title_b' ] ) : $output .= '<h2>' . $item[ 'image_title_b' ] . '</h2>'; endif;
              if ( isset($item['image_html_description']) ) : $output .= $item[ 'image_html_description' ]; elseif ( isset($item['image_description']) ) : $output .= $item['image_description']; else : $output .= $item['image']['description']; endif;

            $output .= '</section>';

          $output .= '</div>';

      else :

        if ( !$is_shoppable ) :
          
          $output .= '<div class="item--content">';

            $output .= '<header class="item--content__header">';

              if ( $item[ 'image_caption' ] || $item['image']['caption'] ) : $output .= '<div class="item--caption">'; if ( $item[ 'image_caption' ] ) : $output .= $item[ 'image_caption' ]; else : $output .= $item['image']['caption']; endif; $output .= '</div>'; endif;
              if ( $item[ 'photo_credit' ] ) : $output .= '<div class="item--credit"> Photo: ' . $item[ 'photo_credit' ] . '</div>'; endif;

              $output .= '<div class="gallery--change to--list" data-to-list="' . __('View as list','vogue.me') . '" data-to-slider="' . __('View as slideshow','vogue.me') . '"></div>';

            $output .= '</header>';

          if ( $item['image_title'] || isset($item['image_html_description']) || $item['image_description'] ) :
            $output .= '<section class="item--content__copy">';

              if ( $item['image_title'] ) : $output .= '<h2>' . $item['image_title'] . '</h2>'; endif;
              if ( isset($item['image_html_description']) ) : $output .= $item[ 'image_html_description' ]; elseif ( isset($item['image_description']) ) : $output .= $item['image_description']; else : $output .= $item['image']['description']; endif;

            $output .= '</section>';

          endif;

          $output .= '</div>';
        
        else :
          
          $output .= '<div class="item--content">';

            if ( $item[ 'image_title' ] ) : $output .= '<header class="item--content__header">' . $item[ 'image_title' ] . '</header>'; endif;
            if ( $item[ 'image_description' ] ) : $output .= '<section class="item--content__copy">' . $item[ 'image_description' ] . '</section>'; endif;

          if ( $item[ 'shoppable' ] == true ) :
            $output .= '<section class="item--content__buy">';
            
            if ( $item[ 'product_price' ] ) :
            
              $output .= '<div class="item--content__pricing">';
                $output .= '<p>'; $i = 1; foreach ( $item[ 'product_price' ] as $price ) : if ( $i > 1 ) : $output .= '<br />'; endif; $output .= $price[ 'currency' ] . ':' . $price[ 'price' ]; $i++; endforeach; $output .= '</p>';
              $output .= '</div>';
            endif;
            
            if ( $item[ 'product_link' ] ) : $output .= '<a href="' . $item[ 'product_link' ] . '" class="button brand" target="_blank">Buy Now</a>'; endif;
            $output .= '</section>';
          
          endif;

          $output .= '</div>';
        
        endif;

      endif;

      $output .= '<div class="clear"></div>';

      $output .= '</div>';

    $i++; endforeach;

    $output .= '</div>';

    $output .= '<div class="article--gallery--meta">';

    if ( !$is_shoppable ) : $output .= '<div class="gallery--view--thumbs"></div>'; endif;

      $output .= '<div class="gallery--position"> <span class="current--slide">1</span> <span class="position--seperator"></span> <span class="total--slides"></span> </div>';

      $output .= '<div class="gallery--navigation"></div>';

    $output .= '</div>';

    if ( !$is_shoppable ) :
    $output .= '<div class="article--gallery--grid--view">';

      $i = 1; foreach ( $gallery as $item ) :

        if ( $item[ 'image' ] ) :
          $output .= '<div class="grid--item"> <a href="#" data-title="View image"> <img data-echo="' . $item['image']['sizes']['thumbnail'] . '" src="' . bloginfo('template_url') . '/assets/images/V-Loading-Small.gif" alt=""> </a> </div>';
        endif;

      $i++; endforeach;

    $output .= '</div>';
  endif;

  $output .= '</div>';

    
      return $output;

    endif; 

  endif;

}

add_shortcode( 'embedgallery', 'embed_gallery' );



function embed_video($atts)
{
  $p = shortcode_atts( array(
      'id' => -1,
      'number' => -1
  ), $atts );

  if ( $p['id'] == 1 )
  {
    $post_id = false;
  }
  else
  {
    if ($p['id'] != -1) {
      $post_id = $p['id'];
    } else {
        if ($p['number'] != -1) {
              $post_id = $p['number'];
        } else {
            $post_id = false;
        }       
    }
  }
  
  if ($post_id) {
    return 
        "<div class='video--container--wrapper'>" . 
        getVideoPlayerWrapper($post_id) . 
        "</div>";
  }
  
}

add_shortcode( 'embedvideo', 'embed_video' );


