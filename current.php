<?php

  $post_ID = get_the_ID();
  $post_id = $post_ID;

  if ( in_category( 'fashionprize' ) && get_field( 'linked_contestent' ) ) :

      $contestant = get_field( 'linked_contestent' );
      $contestant = $contestant->ID;

      $author = get_the_title( $contestant );
      $link = get_permalink( $contestant );

      $by = '<a href="'.$link.'">'. $author .'</a>';

      $profile_image = get_the_post_thumbnail($contestant);

  else :

    // if( $avatar !== false )
    // {
    //     echo $avatar; 
    // }

      
    $avatar = get_avatar_url( $author_id=$post->post_author, array( 'size' => 96, 'default' => 404 ) );

    if( $avatar !== false )
    {
        $profile_image = $profile_image; 
    }
    else
    {
      $profile_image = '';
    }


      
      $get_taxonomy_term = 'user_' . $post->post_author;

  endif;

  $content = get_the_content();

  $gallery_short = '';

  $remove = false;

  if ( get_field( 'shift_gallery' ) == true && ( has_shortcode( $content, 'gallery' ) || has_shortcode( $content, 'embedgallery' ) ) )
  {

    if ( has_shortcode( $content, 'gallery' ) )
    {
      $gallery_short = do_shortcode( _get_shortcodes( get_the_content(), 'gallery' ) );

      $remove = 'gallery';
    }

    if ( has_shortcode( $content, 'embedgallery' ) )
    {
      $gallery_short = do_shortcode( _get_shortcodes( get_the_content(), 'embedgallery' ) );

      $remove = 'embedgallery';
    }

  }

  getAdvert('strip');

?>

<div class="container container--mid--reduced the--article js-the-article">

  <div class="post--snippet post--introduction">
    <div class="snippet--inner">
      <div class="post--meta"> <?php if ( check_site( 'men', false ) ) : ?><small class="tag--men in--post"><?php echo __('Men'); ?></small><?php endif; ?> <?php getKeyTopic( true ); ?> <span><?php echo time_since( abs( strtotime( $post->post_date. " GMT" ) ) ); ?></span> </div>
      <h1 class="post--title"> <?php echo the_title(); ?> </h1>
      <div class="post--meta"> <?php echo __( 'by', 'vogue.me' ); ?> 
                              <?php if ( function_exists( 'coauthors_posts_links' ) ) {
                                        coauthors_posts_links();
                                    } else {
                                        the_author_posts_link();
                                    } ?>
																		<?php isSponsored(get_the_ID()); ?>
      </div>
    </div>
  </div>

</div>

<?php if ( get_field( 'feat_large_image' ) ) : $l_feat_image = get_field( 'feat_large_image' ); ?>

<?php

    $feat_image = wp_get_attachment_image_src( $l_feat_image, 'full' );


    if ( !empty( $feat_image ) || $feat_image != false ) :

      $width = $feat_image[1];
      $height = $feat_image[2];
    
      $image = wp_get_attachment_image_src( $l_feat_image, 'full' )[0];

      /*$x_375 = imageProvider( $img_small, 375, 281 );
      $x_400 = imageProvider( $img_small, 400, 300 );
      $x2_400 = imageProvider( $img_small, 800, 600 );
      $x_600 = imageProvider( $img_small, 600, 400 );
      $x_800 = imageProvider( $img_small, 800 );
      $x_870 = imageProvider( $image, 870 );
      $x_1260 = imageProvider( $image, 1260, 600 );
      $x_1400 = imageProvider( $image, 1400, 700 );
      $x_1600 = imageProvider( $image, 1600, 800 );*/
      $x_1920 = imageProvider( $image, 1920, 800 );

    ?>
  <!--
  <picture class="auto-fade-in">
    <source srcset="<?php echo $x_800; ?>" media="(max-width: 400px) and (min-resolution: 144dpi)">
    <source srcset="<?php echo $x_600; ?>" media="(max-width: 600px)">
    <source srcset="<?php echo $x_800; ?>" media="(max-width: 800px)">
    <source srcset="<?php echo $x_1260; ?>" media="(min-width: 801px) and ( max-width: 1260px )">
    <source srcset="<?php echo $x_1400; ?>" media="(min-width: 1261px) and ( max-width: 1400px )">
    <source srcset="<?php echo $x_1600; ?>" media="(min-width: 1401px) and ( max-width:1600px )">
    <source srcset="<?php echo $x_1920; ?>" media="(min-width: 1601px)">
    <img data-echo="<?php echo $x_375; ?>" class="featured--image" src="" alt="" style="width: 100%;height: auto;margin-bottom: 80px;">
  </picture>
  -->

  <picture class="auto-fade-in">
    <img data-echo="<?php echo $x_1920; ?>" class="featured--image" src="" alt="" style="width:100%;height: auto; margin-bottom: 80px; max-width: 1920px; display:block;margin-left:auto;margin-right:auto;">
  </picture>


<?php endif; endif; ?>

<div class="container container--mid--reduced">

  <div class="content--column">

    <?php Vogue_sharing_buttons( 'post-social-links post-social-links--top post-social-links--sticky' ); ?>

    <?php if ( get_field( 'gallery_type' ) !== 'Runway' ) : ?>
        <div class="featured--area left--anchor">

          <?php echo $gallery_short; // ONLY ACTIONED ON POSTS WITH EMBEDDED GALLERIES AND HAS SHIFT GALLERY CHECKED ?>

          <?php getVideoPlayer( false, false, false, 610 ); ?>

          <?php if ( get_field( 'feat_small_image' ) ) : ?> <img data-echo="<?php echo get_field( 'feat_small_image' ); ?>" class="featured--image" alt=""> <?php endif; ?>

          <?php getArticleGallery(); ?>

          <div class="clear"></div>

        </div>
    <?php endif; ?>

    <div class="column--inner js-the-article-content">

    <?php getNextGenGallery( $post ); ?>

    <?php if ( in_category( 'fashionprize' ) && get_field( 'post_content' ) ) : ?>
    <?php echo get_field( 'post_content' ); ?>
    <?php endif; ?>


<?php if ( in_category( getRunwayCats() ) || get_field( 'gallery_type' ) == 'Runway' ){ ?>

      <?php $gallery_id = $post->ID; if ( !is_null( get_field( 'linked_gallery' ) ) ) : $gallery = get_field( 'linked_gallery' ); $linked = $gallery->ID; endif; $thumbnail = getThumbnail( $gallery_id )[0]; ?>

  				<?php
				
				$g_collection = $gallery->ID;
				$g_details_new = get_field("linked_gallery_detail");
				if(isset($g_details_new->ID)){
				$g_details = $g_details_new->ID;
				}
				$g_frontrow_new = get_field("linked_gallery_front_row");
				if(isset($g_frontrow_new->ID)){
				$g_frontrow = $g_frontrow_new->ID;
				}
				$g_beauty_new = get_field("linked_gallery_beauty");
				if(isset($g_beauty_new->ID)){
				$g_beauty = $g_beauty_new->ID;
				}
				$g_atmosphere_new = get_field("linked_gallery_atmosphere");
				if(isset($g_atmosphere_new->ID)){
					$g_atmosphere = $g_atmosphere_new->ID;
				}
								
				
				$section = get_query_var('section'); 
				
				switch ( $section ) {
					case 'review':
						$active_section = 'Review';
						$gallery_data = getNewGalleryDataRunway( $g_collection );
						
						break;
					case 'collection':
						$active_section = 'Collection';
						$gallery_data = getNewGalleryDataRunway( $g_collection );
						break;
					case 'details':
						$active_section = 'Details';
						$gallery_data = getNewGalleryDataRunway( $g_details );
						break;
					case 'beauty':
						$active_section = 'Beauty';
						$gallery_data = getNewGalleryDataRunway( $g_beauty );
						break;
					case 'front-row':
						$active_section = 'Front Row';
						$gallery_data = getNewGalleryDataRunway( $g_frontrow );
						break;
					default :
						$active_section = 'Collection';
						$gallery_data =  getNewGalleryDataRunway( $g_collection );
						break;
				}
				?>

				<style>
					.mobile-dropdown {font-family: "Nazanin W20","Avenir Next W01","Arial","sans-serif";  font-size: 16px; font-family: "Nazanin W20","Avenir Next W01","Arial","sans-serif"; font-weight: bold; margin: 0 auto; margin-bottom: 30px; text-align: center; }
					.mobile-dropdown .active a:before { display: block; width: 100%; height: 2px; background-color: #ba954e; margin-bottom: -27px; content: ''; }
					.mobile-dropdown .mobile-dropdown__item { display: inline-block; margin: 0 10px; }
					.mobile-dropdown .mobile-dropdown__default{ display: none;  }
					.fashion-grid--listing .fashion-grid__item { width: 25%; padding-left: 20px; padding-top: 40px; padding-right: 0; }
					[dir=rtl] .fashion-grid--listing .fashion-grid__item{ float: right}
					.fashion-grid-item { position: relative; }
					.fashion-grid__item { text-align: left; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; display: inline-block; vertical-align: top; padding-left: 3.27869%; padding-top: 3.27869%; width: 16.66667%; }
					.fashion-grid { text-align: left;	position: relative;	margin-left: -20px;	margin-top: -3.27869%;	margin-right: 0; }
					.fashion-grid--listing { margin-top: -40px; margin-left: -40px; }
					.fashion-grid a { display: block; color: inherit; text-decoration: none; }
					.fashion-grid-item__image-wrapper { position: relative; height: 0; padding-bottom: 150%; margin-bottom: 19px; }
					.fashion-grid a { display: block; color: inherit; text-decoration: none; }
					.fashion-grid__item { text-align: left; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; display: inline-block; vertical-align: top; padding-left: 3.27869%; padding-top: 3.27869%; width: 16.66667%; }
					.fashion-grid { text-align: left; position: relative; margin-left: -3.27869%; margin-top: -3.27869%; margin-right: 0; }
					.fashion-grid-item__image-wrapper img { position: absolute; top: 0; bottom: 0; left: 0; right: 0; width: 100%; height: 100%; }
					.fashion-grid__item img { width: 100%; display: block; }
					.fashion-grid-item__title { margin: 0; font-family: "Akhbar W23","ITC Caslon No. 224 W01","Times New Roman","serif";  font-size: 20px;   font-weight: 400;  line-height: 1.1;  text-align: center; color: #2B2B2B; }
					.fashion-grid-item__date { margin: 0; padding-top: 9px; font-size: 13px; line-height: 14px; font-weight: bold; text-transform: uppercase; color: #AEB0B2; -webkit-font-smoothing: antialiased; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; }
					.fashion-grid--listing .fashion-grid-item__date { text-transform: none; font-weight: normal; font-size: 16px; }
					.fashion-grid a { display: block; color: inherit; text-decoration: none; }


					@media screen and (max-width: 960px) {
						.mobile-dropdown .active a:before { display: none; }
						.mobile-dropdown { margin: 0 0 30px; padding: 0; text-align: left; border: 1px solid #E5E5E5; }
						.mobile-dropdown__item + .mobile-dropdown__item { border-top: 1px solid #E5E5E5; }
						.mobile-dropdown.is-open .mobile-dropdown__item { display: block; }
						.mobile-dropdown .mobile-dropdown__default.mobile-dropdown__default { display: block; color: #2B2B2B; background-color: #FFF; background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMyI+PHBvbHlnb24gZmlsbD0iIzMxMzEzMSIgcG9pbnRzPSIxLDQgMTIsNCA2LjUsMTAiLz48L3N2Zz4='); background-position: right 25px center; background-repeat: no-repeat; }
						.mobile-dropdown__item.mobile-dropdown__item a { display: block; padding: 18px 26px; }
						.mobile-dropdown__item.mobile-dropdown__item { display: none; margin: 0; padding: 0; }
						.mobile-dropdown.is-open .mobile-dropdown__item { display: block; }
						.mobile-dropdown .mobile-dropdown__default{ display: block;  }

					}
					@media screen and (max-width: 550px) {
							.fashion-grid--listing { margin-top: -5%; margin-left: -5%;	}
							.fashion-grid--listing .fashion-grid__item { padding-top: 8% !important; padding-left: 5%; padding-top: 5%; width: 50%; }
					}
				</style>
				
				<ul id="sublinks-dropdown" class="fashion-headline__sub-links mobile-dropdown">
					<li class="mobile-dropdown__default mobile-dropdown__item"><a href="#sublinks-dropdown" data-behavior="toggle" data-toggle-class="is-open"><?php echo __($active_section); ?></a></li><?php
					$active = ($section == 'Collection' || $section == '') ? 'active' : '';
					
					?><li class="mobile-dropdown__item <?php echo $active ?>"><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo __('Collection', 'vogue.me'); ?></a></li><?php
					if ( $g_collection ) {
						$has_review = get_field('has_review', $post_id);
						if($has_review != 'No'){
						$active = ( $active_section == 'Review' ) ? 'active' : '';
					?>		<li class="mobile-dropdown__item <?php echo $active?>"><a href="<?php echo get_permalink(get_the_ID()); ?>section/review"><?php echo __('Review', 'vogue.me'); ?></a></li><?php
						}
					}
					if ( $g_details ) {
						$active = ($section == 'details') ? 'active' : '';
					?><li class="mobile-dropdown__item <?php echo $active?>"><a href="<?php echo get_permalink(get_the_ID()); ?>section/details"><?php echo __('Details', 'vogue.me'); ?></a></li><?php
					}
					if ( $g_beauty ) {
						$active = ($section == 'beauty') ? 'active' : '';
					?><li class="mobile-dropdown__item <?php echo $active?>"><a href="<?php echo get_permalink(get_the_ID()); ?>section/beauty"><?php echo __('Beauty', 'vogue.me'); ?></a></li><?php
					}
					if ( $g_frontrow ) {
						$active = ($section == 'front-row') ? 'active' : '';
					?><li class="mobile-dropdown__item <?php echo $active?>"><a href="<?php echo get_permalink(get_the_ID()); ?>section/front-row"><?php echo __('Front Row','vogue.me'); ?></a></li><?php
					}
					?>
				</ul>
				
				<script>
					jQuery('#sublinks-dropdown li a[data-behavior = toggle]').click(function (e) {e.preventDefault();jQuery('#sublinks-dropdown').toggleClass('is-open')});
				</script>

	<?php if ($section == 'review') { ?>
      <div class="runway--thumbnail">
        <div class="item-inner">
          <div class="img"><a href="#"><img src="<?php echo imageProvider($thumbnail,360); ?>" alt=""></a></div>
          <div class="content"><a href="#"><?php echo __('View Slideshow'); ?></a></div>
        </div>
      </div>
	<?php echo get_stripped_content( $content, $remove ); ?>
	<?php  }else{
					if(get_current_blog_id() == '3' ) {
						$is_arabic = true;
					} else {
						$ignore_read = false;
					}
					// Show the relevant gallery	
					?>
					<div class="fashion-grid fashion-grid--listing post">
						<?php
						// Set the image src blank in case there is no image set
						$image_src = '';
						
						// Iterate through all images in the gallery to find the filename for the image matching the preview ID
						$totalcount = count( $gallery_data->pictures);
						$counter = 1;
						for ( $i=0; $i<$totalcount; $i++ ) {
						
						  $gallerSubLink = $gallery_data->link;
						  $runwayGalImage = $gallery_data->pictures[$i]->imagepath;
						  $runwayGalURL =  parse_url($runwayGalImage);
						  $runwayGalfilepath = $runwayGalURL['path'];
						  $image_src = imageProvider($runwayGalImage,215);

						?><div class="grid--item fashion-grid__item fashion-grid-item">
								
									<div class="fashion-grid-item__image-wrapper single-runway-image" data-number = '<?php echo $i+1; ?>'>
									<?php if ($section == 'details' || $section == 'beauty' || $section == 'front-row'){?> 
									<a href="<?php echo get_permalink(get_the_ID()); ?>section/<?php echo $section; ?>/#/gallery/<?php echo $i+1; ?>/" target='_self' >
									<?php }else{ ?>
									<a href="#/gallery/<?php echo $i+1; ?>/">
									<?php } ?>
										<img data-echo="<?php echo $image_src; ?>" src="<?php bloginfo('template_url'); ?>/assets/images/runway-placeholder.png" style="display: block;"/>
									</a>
											
									</div>
									<h4 class="fashion-grid-item__title">
									<?php
									
									if ($is_arabic){
										$standard = array("0","1","2","3","4","5","6","7","8","9");
										$eastern_arabic_symbols = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
										$arcounter = str_replace($standard , $eastern_arabic_symbols , $counter);
									}
									
									
										switch ( $active_section ) {
										case 'Collection':
												if($is_arabic){ $gallerySectionTitle =  "اطلالة " . $arcounter ;
												}else {
												 $gallerySectionTitle = "Look " . $counter; 
												}
											break;
										case 'Details':
												if($is_arabic){ $gallerySectionTitle =  "اطلالة تفضیلیة " . $arcounter; 
												}else {
												$gallerySectionTitle = "Detail Look " . $counter; 
												}
											break;
										case 'Beauty':
											if($is_arabic){ $gallerySectionTitle =  "جمال " . $arcounter;
												}else {
												$gallerySectionTitle = "Beauty " . $counter;
												}
											break;
										case 'Front Row':
											 if($is_arabic){$gallerySectionTitle = $gallery_data->pictures[$i]->title_ar;
											 } else {
											 $gallerySectionTitle = $gallery_data->pictures[$i]->title;
											 } 
											break;
										default :
											if($is_arabic){ $gallerySectionTitle =  "اطلالة " . $arcounter ;
												}else {
												 $gallerySectionTitle = "Look " . $counter; 
												}
											break;
										}
									?>
									
									
										<?php echo $gallerySectionTitle; ?>
									</h4>
								
							</div><?php
							$counter++;
						} // end for
						?>
						</div><!-- /fashion-grid -->
					<?php
					} // end else ?>
	
<?php 
	} 
	else{
		echo get_stripped_content( $content, $remove ); 
	}
	?>

        

      <div class="clear"></div>

      <?php Vogue_sharing_buttons( 'post-social-links post-social-links--bottom' ); ?>

      <?php
        ob_start();

        $url = get_the_permalink();
        $text = get_the_title();
      ?>
      <div class="post-share-image">
          <ul class="post-share-image__links">
            <li class="post-share-image__link"><a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode($url); ?>&amp;media=IMGSRC&amp;description=<?php echo urlencode($text); ?>" class="fa fa-pinterest" target="_blank"></a></li>
            <li class="post-share-image__link"><a href="https://www.facebook.com/dialog/feed?app_id=164555587284821&display=popup&link=<?php echo urlencode($url); ?>&picture=IMGSRC" class="fa fa-facebook" target="_blank"></a><?php /*<a href="https://www.facebook.com/dialog/share?app_id=164555587284821&amp;display=popup&amp;href=<?php echo urlencode($url); ?>&picture=IMGSRC&amp;redirect_uri=<?php echo urlencode($url); ?>" class="fa fa-facebook" target="_blank"></a>*/ ?></li>
            <li class="post-share-image__lightbox"><a href="#" class="fa fa-search js-has-lightbox" data-src="IMGSRCFULL"></a></li>
              <li class="post-share-image__toggle"><button class="js-post-share-image-toggle">
                <span class="open"><?php inline_svg( 'va-more' ); ?></span>
                <span class="close"><?php inline_svg( 'va-close' ); ?></span>
              </button></li>
          </ul>
      </div>

      <?php
        $output = ob_get_contents();
        ob_end_clean();
      ?>
      <script type="text/javascript">
        var vgar_share_image = <?php echo json_encode( $output ); ?>
      </script>
    </div>

    <div class="post--after">

       <?php if ( !wp_is_mobile() ) : ?>

           <div class="post--author">
            <?php if ( strpos( $profile_image, '404' ) !== false ) : ?> <div class="author--image"> <img src="<?php echo $profile_image; ?>" alt=""> </div> <?php else : ?> <div class="author--image default"> <img src="<?php echo get_bloginfo('template_url') . '/assets/images/Vogue-V-icon.svg'; ?>" alt=""> </div> <?php endif; ?>
            <div class="post--meta<?php if ( !get_field( 'social_profiles', $get_taxonomy_term ) ) : ?> no--social<?php endif; ?>">
            <?php echo __( 'by', 'vogue.me' ); ?> 
                              <?php if ( function_exists( 'coauthors_posts_links' ) ) {
                                        coauthors_posts_links();
                                    } else {
                                        the_author_posts_link();
                                    } ?>
            <?php // TO DO: Social for fashion prize profiles ?>
            <?php if ( get_field( 'social_profiles', $get_taxonomy_term ) ) : $social = get_field( 'social_profiles', $get_taxonomy_term ); ?>
            <ul class="pc-social in--post">
            <?php foreach ( $social as $profile ) : $network = $profile['network']; $account = $profile['tag']; ?>
            <li><a class="fa fa-<?php echo $network; ?>" href="<?php echo get_social_url( $network, $account ); ?>" target="_blank"></a></li>
            <?php endforeach; ?>
            </ul>
            <?php endif; ?>
            </div>
          </div>
          
        <?php endif; ?>

      <?php getBrandsPeople($post->ID); ?>
      <div class="clear"></div>

    </div>

  </div>

  <?php getAdvert( 'vert' ); ?>



</div>

<?php if ( get_field( 'media_type' ) == 'video' ) : ?>

<?php else : ?>

  <?php getBreak_Explore(get_the_ID()); ?>
  <?php // getAdvert( 'strip' ); ?>

  <?php if ( in_category( getRunwayCats() ) || get_field( 'gallery_type' ) == 'Runway' ) : ?>

  <?php else : ?>

    <?php getNextPostLink(get_the_ID()); ?>

		<script type="text/javascript">
			if (window.instgrm) { window.instgrm.Embeds.process(); }
		</script>

  <?php endif; ?>

<?php endif; ?>