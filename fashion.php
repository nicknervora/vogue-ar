<?php 
	
	// Template name: Fashion Prize
	
	get_header();
	the_post();

	$progress = get_field( 'progress' );

	$profile_term = 'profile_';
	$year = get_field( 'year' );
	$finalist_group = get_field( 'finalist_group' );

	$header_bg = getArchiveHeaderBG( 'fashionprizeyear_' . $year );

?>

	<div class="hero hero--fp">
		<div class="hero__bg"<?php if ( $header_bg ) : echo ' style="background-image: url(' . $header_bg . ');"'; endif; ?>></div>
	</div>

	<section class="pc pc--fp">
	
		<div class="pc__header pc-header">
			<div class="container container--mid--reduced">
				<div class="pc-header__inner">
					<div class="pc-header__top">
						<h1 class="pc-header__title"><?php echo __( 'Fashion Prize', 'vogue.me' ); ?></h1>
						<span><?php echo __( 'In partnership with', 'vogue.me' ); ?></span>
						<figure class="pc-header__partner">
							<img src="<?php bloginfo( 'template_url' ); ?>/assets/images/FARFETCH.png">
						</figure>
					</div>
				</div>
				<div class="fp-nav">
					<a data-content="about" href="#/" class="fp-nav__item current-menu-item"><?php echo __( 'About', 'vogue.me' ); ?></a>
				<?php if ( $progress == 'Accepting' ) : ?> <a data-content="eligibillity" href="#/" class="fp-nav__item"><?php echo __( 'Eligibillity', 'vogue.me' ); ?></a> <?php endif; ?>
				<?php if ( $progress == 'Accepting' ) : ?> <a data-content="apply" href="#/" class="fp-nav__item"><?php echo __( 'Apply', 'vogue.me' ); ?></a> <?php endif; ?>
				<?php if ( $progress == 'Finalists' ) : ?> <a data-content="finalists" href="#/" class="fp-nav__item"><?php echo __( 'Finalists', 'vogue.me' ); ?></a> <?php endif; ?>
					<a data-content="jury" href="#/" class="fp-nav__item"><?php echo __( 'Jury', 'vogue.me' ); ?></a>
				<?php /* if ( $progress == 'Finished' ) : ?> <a data-content="winner" href="#/" class="fp-nav__item"><?php echo __( 'Winner', 'vogue.me' ); ?></a> <?php endif; */ ?>
				</div>
			</div>
		</div>

		<div class="container container--mid--reduced">
			<div class="content-column fb--content">

					<section class="fb-judge__information">

						<article class="fp-jury__member fp-jury-member">
							<div class="fp-jury-member__portrait"></div>
							<h2 class="fp-jury-member__name"></h2>
							<p class="fp-jury-member__title"></p>
						</article>

						<div class="fp-judge__information--content"></div>

					</section>


					<section class="fp-about fp-content is-active" id="about">
						<div class="text-content">
							<?php the_content(); ?>
						</div>
					</section>

				<?php if ( $progress == 'Finalists' ) : ?>
					<section class="fp-finalists fp-content" id="finalists">

						<?php foreach ( $finalist_group as $group ) : ?>

						<?php if ( $group['heading'] ) : ?><h3 class="section--header auto-fade-in"><?php echo $group['heading']; ?></h3><?php endif; ?>

						<?php if ( $group['finalists'] ) : ?>

							<div class="fp-jury__members">

						<?php foreach ( $group['finalists'] as $finalist ) : $finalist_id = $finalist->ID; ?>

							<article class="fp-jury__member fp-jury-member"><a href="<?php echo get_permalink( $finalist_id ); ?>">
								<div class="fp-jury-member__portrait" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $finalist_id ) ); ?>');"></div>
								<h2 class="fp-jury-member__name"><?php echo get_the_title( $finalist_id ); ?></h2>
							</a></article>

						<?php endforeach; ?>

							</div>

						<?php endif; ?>

						<?php endforeach; ?>

					</section>
				<?php endif; ?>
				
				<?php if ( $progress == 'Accepting' ) : ?>

					<section class="fp-eligibillity fp-content" id="eligibillity">
					<?php if ( get_field( 'eligibillity' ) ) : ?>
						<div class="text-content">
							<?php echo get_field( 'eligibillity' ); ?>
						</div>
					<?php endif; ?>
					</section>

					<section class="fp-apply fp-content" id="apply">
					<?php if ( get_field( 'Applyform' ) ) : ?>
						<div class="text-content">
							<?php echo get_field( 'Applyform' ); ?>
						</div>
					<?php endif; ?>
						<form id="form-apply" class="fp-form">
							<div class="fp-form__inner">
								<div class="input-wrapper">
									<label class="label">Name *</label>
									<input type="text" />
								</div>
								<div class="input-wrapper">
									<label class="label">Name of company or brand *</label>
									<input type="text" />
								</div>
								<div class="input-wrapper">
									<label class="label">Email *</label>
									<input type="text" />
								</div>
								<div class="input-wrapper">
									<label class="label">Contact Number (Including Country Code) *</label>
									<input type="text" />
								</div>
								<div class="input-wrapper">
									<label class="label">City of residence *</label>
									<input type="text" />
								</div>
								<div class="input-wrapper">
									<label class="label">Country of residence *</label>
									<div class="select-wrapper">
									<?php $countries = get_terms( array( 'taxonomy' => 'countries', 'hide_empty' => false ) ); ?>
										<select>
											<option></option>
									<?php foreach ( $countries as $country ) : ?>
                      <option value="<?php echo $country->term_id; ?>"><?php echo $country->name; ?></option>
                  <?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="input-wrapper">
									<label class="label">Do you have a degree/diploma in fashion/design? *</label>
									<div class="select-wrapper">
										<select>
											<option></option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
										</select>
									</div>
								</div>
							</div>
							<div class="fp-form__inner">
								<div class="input-wrapper">
									<label class="label">Brand website</label>
									<input type="text" />
								</div>
								<div class="input-wrapper">
									<label class="label">Instagram handle</label>
									<input type="text" />
								</div>
								<div class="input-wrapper">
									<label class="label">Number of years in business? *</label>
									<div class="select-wrapper">
										<select>
											<option></option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
										</select>
									</div>
								</div>
								<div class="input-wrapper">
									<label class="label">No. of Collections Designed *</label>
									<div class="select-wrapper">
										<select>
											<option></option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
										</select>
									</div>
								</div>
								<div class="input-wrapper">
									<label class="label">Total number of staff employed (paid/volunteer) *</label>
									<div class="select-wrapper">
										<select>
											<option></option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
										</select>
									</div>
								</div>
								<div class="input-wrapper">
									<label class="label">Type of designer *</label>
									<div class="select-wrapper">
										<select>
											<option></option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
											<option>Lorem ipsum</option>
										</select>
									</div>
								</div>
								<div class="input-wrapper input-wrapper--submit">
									<button type="submit"><?php echo __( 'Submit', 'vogue.me' ); ?></button>
								</div>
							</div>
						</form>
					</section>
				<?php endif; ?>

				<?php /* if ( $progress == 'Finished' ) : ?>

					<section class="fp-winner fp-content" id="winner">					      

					<?php foreach ( $finalist_group as $group ) : ?>

						<?php if ( $group[ 'winning_article' ] ) : ?>

						<?php $post_id = $group['winning_article']; ?>

							<?php $title = get_the_title( $post_id ); ?>
							<?php $content = get_the_content( $post_id ); ?>

							<?php $content = apply_filters('the_content', $content ); ?>

							<h1 class="post--title"> <?php echo $title; ?> </h1>

							<?php echo get_the_post_thumbnail( $post_id, 'full' ); ?>

						<?php endif; ?>


						<?php if ( $group['heading'] ) : ?><h3 class="section--header auto-fade-in"><?php echo $group['heading']; ?></h3><?php endif; ?>

						<?php if ( $group['finalist'] ) : ?>

							<div class="fp-jury__members winner">

						<?php foreach ( $group['finalist'] as $finalist ) : $finalist_id = $finalist->ID; ?>

							<article class="fp-jury__member fp-jury-member"><a href="<?php echo get_permalink( $finalist_id ); ?>">
								<div class="fp-jury-member__portrait" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $finalist_id ) ); ?>');"></div>
								<h2 class="fp-jury-member__name"><?php echo get_the_title( $finalist_id ); ?></h2>
							</a></article>

						<?php endforeach; ?>

							</div>

						<?php endif; ?>

							<?php echo $content; ?>

						<?php endforeach; ?>

					</section>

				<?php endif; */ ?>
				

					<section class="fp-jury fp-content" id="jury">
						
					<?php if ( get_field( 'jury' ) ) : echo get_field( 'jury' ); endif; ?>

					<?php if ( get_field( 'panel' ) ) : ?>

						<div class="fp-jury__members">

						<?php foreach ( get_field( 'panel' ) as $judge ) : ?>
							<article class="fp-jury__member fp-jury-member">
								<?php $image = $judge['featureimage']; ?>
								<div class="fp-jury-member__portrait" data-src="<?php echo $image['url']; ?>" style="background-image: url('<?php echo $image['url']; ?>');"></div>
								<?php if ( $judge['title'] ) : ?><h2 class="fp-jury-member__name"><?php echo $judge['title']; ?></h2><?php endif; ?>
								<?php if ( $judge['company'] ) : ?><p class="fp-jury-member__title"><?php echo $judge['company']; ?></p><?php endif; ?>
								<?php if ( $judge['description'] ) : ?><div class="fp-jury-member__description"><?php echo $judge['description']; ?></div><?php endif; ?>
							</article>
						<?php endforeach; ?>
						</div>

					<?php endif; ?>
					</section>

				<?php if ( get_field('sponsers') ) : $sponsor_count = count( get_field('sponsers') ); ?>
					<section class="fp-sponsors" data-count="<?php echo $sponsor_count; ?>">
				<?php foreach( get_field('sponsers') as $sponsor ) : ?>
					<div class="fp-sponsor">
						<div class="fp-sponsor__heading"><?php echo $sponsor['heading_title']; ?></div>
						<a href="<?php echo $sponsor['sponserlink']; ?>" class="fp-sponsor__link">
							<img src="<?php echo $sponsor['sponserlogo']; ?>" alt="">
						</a>
					</div>
				<?php endforeach; ?>
					</div>
				<?php endif; ?>

			</div>
		</div>
	</section>

<?php get_footer(); ?>