

    <section class="search--area js-search--area">
      <form>
        <input type="text" name="search" id="s" value="" placeholder="<?php echo __('Search...','vogue.me'); ?>">
        <input type="hidden" name="search" id="search_by_posts" value="">
        <input type="hidden" name="search" id="search_get_tags" value="">
        <input type="hidden" name="search" id="search_get_stylebase" value="">
      </form>
      <div class="search--area--results" id="search_results">
        <div class="container container--mid--increased">
          <div class="search--suggestions search--key--words" id="search_key_words">
            <h5 class="search--heading"><?php echo __( 'Suggestions', 'vogue.me' ); ?></h5>
            <div class="key_words--inner">
            </div>
          </div>
          <div class="search--suggestions search--post--list" id="search_post_list">
            <h5 class="search--heading"><?php echo __( 'Articles', 'vogue.me' ); ?></h5>
            <div class="post--slider ui-autocomplete">
            </div>
            <a href="" data-home="<?php echo get_bloginfo('url'); ?>" class="button full white absolute--after view--all"><?php echo __( 'View All', 'vogue.me' ); ?></a>
          </div>
          <div class="search--sidebar">
            <div class="search--suggestions search--simple search--style-base" id="search_style_base">
              <h5 class="search--heading"><?php echo __( 'Vogue Collection', 'vogue.me' ); ?></h5>
            </div>
            <div class="search--suggestions search--simple search--topics">
              <h5 class="search--heading"><?php echo __( 'Topics', 'vogue.me' ); ?></h5>
              <ul id="search_topics" class="search--topics"></ul>
            </div>
          </div>
        </div>
      </div>
    </section>

    <nav class="side-nav js-side-nav">
      <div class="side-nav__inner">
        <div class="side-nav__content">
          <header class="side-nav__header">
            <div class="side-nav__top">
               <button type="button" class="site-head__toggle site-head__toggle--close js-close-side-nav">
                  <?php inline_svg( 'va-close' ); ?>
                </button>
                <button type="button" class="site-head__toggle site-head__toggle--search js-toggle-search js-search js-toggle-search--side-nav">
                  <?php inline_svg( 'va-search' ); ?>
                </button>
            </div>
            <a href="<?php bloginfo( 'url' ); ?>/" class="side-nav__branding">
              <?php if ( check_site('men',false) ) : ?>
              <?php inline_svg('vogue_b'); ?>
              <?php else : ?>
              <?php inline_svg('vogue_g_b'); ?>
              <?php endif; ?>
            </a>
          </header>
          <div class="side-nav__languages">

            <?php if ( check_site( 'women', 'en' ) ) : ?>
              <a href="<?php echo get_site_url( WOMEN_AR ); ?>?l">العربية</a>
            <?php elseif ( check_site( 'men', 'en' ) ) : ?>
              <a href="<?php echo get_site_url( MEN_AR ); ?>?l">العربية</a>
            <?php elseif ( check_site( 'women', 'ar' ) ) : ?>
              <a href="<?php echo get_site_url( WOMEN_EN ); ?>?l">English</a>
            <?php else : ?>
              <a href="<?php echo get_site_url( MEN_EN ); ?>?l">English</a>
            <?php endif; ?>

          </div>

          <?php $menu_args = array(
              'theme_location'=>'secondary',
              'container'     => 'div',
              'container_class' => 'side-nav__menu',
              'menu_class' => false,
              'menu_id'=>false,
              'echo'        => true,
              // 'walker' => new custom_walker(),
              'items_wrap'    => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          ); ?>

          <?php wp_nav_menu( $menu_args ); ?>
          <footer class="side-nav__footer">
            <div class="side-nav__social">
              <?php get_vogue_social(); ?>
            </div>
          <?php if ( check_site( 'women', false ) ) : ?>
            <a href="#" class="button brand"><span><?php inline_svg('va-user'); ?><?php echo __( 'Sign up', 'vogue.me' ); ?></span></a>
          <?php else : ?>
            <a href="#" class="button black"><span><?php inline_svg('va-user'); ?><?php echo __( 'Sign up', 'vogue.me' ); ?></span></a>
          <?php endif; ?>
            <p class="side-nav__copyright">
              <?php echo __( '&copy; ' . date( 'Y' ) . ' Nervora Fashion, Inc. and Condé Nast International. All rights reserved. The material on this site may not be reproduced, distributed, transmitted, cached, or otherwise used, except with the prior written permission of Nervora Fashion, Inc. and Condé Nast International.', 'vogue.me' ); ?>
            </p>
          </footer>
        </div>
      </div>
    </nav>

    <div class="opaque-mask mask-menu js-mask-menu"></div>
    <div class="opaque-mask mask-search js-mask-search"></div>

    <?php wp_footer(); ?>

    <script src="//nervora-d.openx.net/w/1.0/jstag"></script>

    <script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
    </script>

    <script src="<?php echo get_bloginfo('template_url'); ?>/assets/javascript/lib/polyfills.js" async></script>

<script>

  var scaleAd = function(adContainer) {
  var ad_container = jQuery(adContainer);
  var ad = jQuery('iframe[id^=ox_]', ad_container);
  var cont_width = ad_container.width();
  var ad_width = ad.attr('width');
  var scale = Math.min(1, cont_width / ad_width);
  var lmargin = Math.min((ad_width - cont_width) / -2, 0);
<?php if (check_site(false, 'ar')) { ?>
  ad.css("cssText", '-ms-transform : scale('+scale+','+scale+'); -webkit-transform : scale('+scale+','+scale+'); transform : scale('+scale+','+scale+'); margin-right : '+lmargin+'px !important');
<?php } else {?>
  ad.css("cssText", '-ms-transform : scale('+scale+','+scale+'); -webkit-transform : scale('+scale+','+scale+'); transform : scale('+scale+','+scale+'); margin-left : '+lmargin+'px !important');
<?php }?>
 };

//jQuery(window).resize(function(){scaleAd(jQuery('.ad_970.desktop_ads, .ad_970.mobile_ads'))});


</script>

    <div class="zoom js-zoom">
        <button type="button" class="zoom__close js-zoom__close"></button>
        <div class="zoom__inner js-zoom__inner"></div>
    </div>

  </body>

</html>