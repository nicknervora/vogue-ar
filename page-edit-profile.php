<?php if ( is_user_logged_in() && current_user_can( 'publish_posts' ) ) : header( 'location:' . get_bloginfo( 'url' ) . '/wp-admin' ); endif;

/* Template Name: Account - Edit */
    

    global $current_user, $wp_roles;

    $error = array();

/* If profile was saved, update profile. */
    if ( 'POST' == $_SERVER[ 'REQUEST_METHOD' ] && !empty( $_POST[ 'action' ] ) && $_POST[ 'action' ] == 'update-user' )
    {

/* Update user password. */
    if ( !empty( $_POST[ 'pass1' ] ) && !empty( $_POST[ 'pass2' ] ) )
    {
        if ( $_POST[ 'pass1' ] == $_POST[ 'pass2' ] )
            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST[ 'pass1' ] ) ) );
        else
            $error[] = __( 'The passwords you entered do not match. Your password was not updated.', 'vogue.me' );
    }

    if ( !empty( $_POST[ 'email' ] ) )
    {
        if ( $_POST[ 'email' ] )
        {
            if ( !is_email( esc_attr( $_POST[ 'email' ] ) ) )
                $error[] = __( 'The Email you entered is not valid.  please try again.', 'vogue.me' );

            elseif ( email_exists( esc_attr( $_POST[ 'email' ] ) ) != $current_user->ID )
                $error[] = __( 'This email is already used by another user. try a different one.', 'vogue.me' );

            else
            {
                // if ( $_POST[ 'email' ] !== $_POST[ 'email2' ] )
                // {
                //     $error[] = __( 'The emails you entered do not match. Your email was not updated.', 'vogue.me' );
                // }
                // else
                // {
                    wp_update_user( array ( 'ID' => $current_user->ID, 'user_email' => esc_attr( $_POST[ 'email' ] ) ) );
                // }

            }

        }

    }

    if ( !empty( $_POST[ 'first-name' ] ) )
    {
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST[ 'first-name' ] ) );
    }

    if ( !empty( $_POST[ 'last-name' ] ) )
    {
        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST[ 'last-name' ] ) );
    }

    // var_dump( $_POST['dob_day'] . ' ' . $_POST['dob_month'] . ' ' . $_POST['dob_year'] );

    if ( !empty( $_POST[ 'dob_day' ] ) && !empty( $_POST[ 'dob_month' ] ) && !empty( $_POST[ 'dob_year' ] ) )
    {
        $submit_dob = $_POST[ 'dob_year' ] . str_pad( $_POST[ 'dob_month' ], 2, '0', STR_PAD_LEFT ) . str_pad( $_POST[ 'dob_day' ], 2, '0', STR_PAD_LEFT );

        update_user_meta($current_user->ID, 'date_of_birth', $submit_dob );
    }

    if ( !empty( $_POST[ 'country_of_residence' ] ) )
    {
        update_user_meta( $current_user->ID, 'country_of_residence', $_POST['country_of_residence'] );
    }

    if ( !empty( $_POST[ 'security_question' ] ) )
    {
        update_user_meta( $current_user->ID, 'security_question', $_POST['security_question'] );
    }

    if ( !empty( $_POST[ 'gender' ] ) )
    {
        update_user_meta( $current_user->ID, 'gender', $_POST['gender'] );
    }

    if ( !empty( $_POST[ 'vmc_langs' ] ) )
    {
        update_user_meta( $current_user->ID, 'vmc_langs', $_POST['vmc_langs'] );
    }

    if ( !empty( $_POST[ 'interests' ] ) )
    {

        if ( $_POST['vmc_langs'] == 'none' )
        {
            update_user_meta( $current_user->ID, 'vmc_interests', '' );
        }
        else
        {
            if ( is_null( get_user_meta( $current_user->ID, 'vmc_interests' ) ) )
            {
                add_user_meta( $current_user->ID, 'vmc_interests', $_POST[ 'interests' ] );
            }
            else
            {
                update_user_meta( $current_user->ID, 'vmc_interests', $_POST['interests'] );
            }

        }

    }

    /* Redirect so the page will show updated info.*/
    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        // do_action('edit_user_profile_update', $current_user->ID);
        try{
            do_action('vmc_update', $current_user->ID);
            do_action('acf/save_post', $current_user->ID);
            wp_redirect( get_permalink() );
            exit;
        }
        catch (\Exception $e){
            // $error[] = $e->getMessage();
            $error[] = __( 'There was an issue saving your profile, please try again.', 'vogue.me' );
        }
    }

}

get_header();


?>

<section class="hero-banner">
  <div class="background" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/herobanner-runway.jpg');"></div>
</section>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="scroll" data-ui="jscroll-default">

      <div class="get--content">

        <div id="account-header">
          <div id="ah-inner">
            <h1><?php echo __( 'Account Settings', 'vogue.me' ); ?></h1>
          </div>
        </div>


        <div id="account-details">
        <?php if ( !empty( $error ) ) : ?>
            <span style="font-size:14px;">
            <?php foreach ( $error as $e ) : echo $e . '<br />'; endforeach; ?>
            </span>
        <?php endif; ?>

            <?php if ( !is_user_logged_in() ) : ?>
                <span class="welcome"> <?php echo __('You must be logged in to edit your profile.' ); ?> <a href="<?php echo site_url(); ?>/account/login"><?php echo __( 'Log in', 'vogue.me' ); ?></a> </span><!-- .warning -->
            <?php else : ?>

            <span class="welcome">Hey <?php the_author_meta( 'first_name', $current_user->ID ); ?>! <a href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo __( 'Log out', 'vogue.me' ); ?></a></span>

            <form method="post" id="adduser" action="<?php the_permalink(); ?>">
                <div class="acc-hold left">
                    <label for="first-name"><?php echo __('First Name', 'vogue.me'); ?></label>
                    <input class="text-input acc-text" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>">
                </div>

                <div class="acc-hold right">
                    <label for="last-name"><?php echo __('Last Name', 'vogue.me'); ?></label>
                    <input class="text-input acc-text" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                </div>

                <div class="acc-hold left">
                    <label for="email"><?php echo __('Email', 'vogue.me'); ?></label>
                    <input class="text-input acc-text" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
                </div>

                <div class="acc-hold right">
                    <label for="email"><?php echo __('Confirm Email', 'vogue.me'); ?></label>
                    <input class="text-input acc-text" name="email2" type="text" id="email2" value="" />
                </div>

                <div class="acc-hold left">
                    <label for="pass1"><?php echo __('Password', 'vogue.me'); ?> </label>
                    <input class="text-input acc-pass" name="pass1" type="password" id="pass1" />
                </div>

                <div class="acc-hold right">
                    <label for="pass2"><?php echo __('Confirm Password', 'vogue.me'); ?></label>
                    <input class="text-input acc-pass" name="pass2" type="password" id="pass2" />
                </div>

                <div class="acc-hold left select-icon">
                    <?php $security_question = get_field( 'security_question', 'user_' . $current_user->ID  ); ?>
                    <label for="security_questions"><?php echo __( 'Security Question', 'vogue.me' ); ?></label>
                <?php
                    $questions = get_field( 'security_questions', 'option' );
                    $questions = explode( ',', $questions );
                ?>
                    <select class="acc-select" name="security_question">
                        <option><?php echo __( 'Please Select', 'vogue.me' ); ?></option>
                    <?php foreach ( $questions as $question ) : ?>
                        <option value="<?php echo $question; ?>"<?php if ( $security_question == $question ) : ?> selected<?php endif; ?>><?php echo $question; ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>

                <div class="acc-hold right">
                    <label for="security_answer"><?php echo __('Security Answer', 'vogue.me'); ?></label>
                    <input class="text-input acc-text" name="security_answer" type="text" id="security_answer" value="" />
                </div>

                <div class="acc-hold left select-icon">
                <?php $cor = get_field( 'country_of_residence', 'user_' . $current_user->ID  ); ?>
                    <label for="country_of_residence"><?php echo __( 'Country of Residence', 'vogue.me' ); ?></label>
                <?php $countries = get_terms( array( 'taxonomy' => 'countries', 'hide_empty' => false ) ); ?>
                    <select class="acc-select" name="country_of_residence">
                        <option>Please Select</option>
                    <?php foreach ( $countries as $country ) : ?>
                        <option value="<?php echo $country->term_id; ?>"<?php if ( $cor == $country->term_id ) : ?> selected<?php endif; ?>><?php echo $country->name; ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>

                <div class="acc-hold right blank"></div>

                <?php

                    $date_of_birth = get_user_meta( 'date_of_birth', 'user_' . $current_user->ID  );

                    $stored_y = false;
                    $stored_m = false;
                    $stored_d = false;

                    if ( $date_of_birth != false ) :

                        $stored_dob = DateTime::createFromFormat( 'd/m/Y', $date_of_birth );

                        $stored_y = $stored_dob->format( 'Y' );
                        $stored_m = $stored_dob->format( 'm' );
                        $stored_d = $stored_dob->format( 'd' );

                    endif;

                    $current_year = date( 'Y' );

                    $previous = 110;

                    $years = array_combine( range( $current_year, $current_year - $previous), range( $current_year, $current_year - $previous ) );
                
                ?>

                <div class="acc-hold left select-icon">
                    <label><?php echo __( 'Date of birth', 'vogue.me' ); ?></label>
                    <select class="acc-select dob-day" name="dob_day">
                        <option><?php echo __( 'Day', 'vogue.me' ); ?></option>
                    <?php for ( $d = 1; $d <= 31; $d++ ) : ?>
                        <?php $day = sprintf( '%02d', $d ); ?>
                        <option value="<?php echo $d; ?>"<?php if ( $d == $stored_d ) : ?> selected<?php endif; ?>><?php echo $day; ?></option>
                    <?php endfor; ?>
                    </select>

                    <select class="acc-select dob-month" name="dob_month">
                        <option><?php echo __( 'Month', 'vogue.me' ); ?></option>
                    <?php for ( $m = 1; $m <= 12; $m++ ) : ?>
                        <?php $month = sprintf( '%02d', $m ); ?>
                        <option value="<?php echo $m; ?>"<?php if ( $m == $stored_m ) : ?> selected<?php endif; ?>><?php echo $month; ?></option>
                    <?php endfor; ?>
                    </select>

                    <select class="acc-select dob-year" name="dob_year">]
                        <option><?php echo __( 'Year', 'vogue.me' ); ?></option>
                    <?php foreach ( $years as $year ) : ?>

                        <option value="<?php echo $year; ?>"<?php if ( $year == $stored_y ) : ?> selected<?php endif; ?>><?php echo $year; ?></option>

                    <?php endforeach; ?>
                    </select>
                </div>

                <div class="acc-hold right">
                <?php $gender = get_user_meta( $current_user->ID, 'gender' ); ?>
                    <label><?php echo __( 'Gender', 'vogue.me' ); ?></label>
                    <ul>
                        <li>
                            <input type="radio" id="option_01" name="gender" value="Male"<?php if ( in_array( 'Male', $gender ) ) : ?> checked<?php endif; ?>>
                            <label for="option_01"><?php echo __( 'Male', 'vogue.me' ); ?></label>
                            <div class="check"></div>
                        </li>
                        <li>
                            <input type="radio" id="option_02" name="gender" value="Female"<?php if ( in_array( 'Female', $gender ) ) : ?> checked<?php endif; ?>>
                            <label for="option_02"><?php echo __( 'Female', 'vogue.me' ); ?></label>
                            <div class="check"></div>
                        </li>
                    </ul>
                </div>

                <div class="acc-clearer"></div>

            </div>

<?php $vmc_interests = get_user_meta( $current_user->ID, 'vmc_interests'); ?>

<?php var_dump($vmc_interests); ?>

<?php $groupsByList = get_site_option('_vmc_interest_groups'); ?>

<?php var_dump($groupsByList); ?>

                <div class="acc-newsletter-hero">
                    <div class="acc-newsletter-pref">
                        <h2><?php echo __( 'The Vogue Newsletter', 'vogue.me' ); ?></h2>
                        <span><?php echo __( 'You are subscribed to the following newsletters(s)', 'vogue.me' ); ?></span>
                    </div>

<?php foreach ( $groupsByList as $lang => $language ) : ?>

<?php if ( !empty( $language ) ) : ?>

<?php foreach ( $language as $key => $value ) : $group_id = $value['id']; $group_title = $value['title']; $type = $value['type']; $the_group = $value['interests']; ?>

<?php if ( $type == 'checkboxes' ) : ?>

                    <fieldset id="group_<?php echo $lang; ?>">

                        <div class="acc-newsletter-check"> <div class="acc-hold"> <ul>

<?php $c = 1; foreach ( $the_group as $group => $check ) : ?>

    <?php $check_id = $check['id']; ?>
    <?php $check_name = $check['name']; ?>

                            <li> <input type="checkbox" name="interests[]" value="<?php echo $check_id; ?>" id="interest-<?php echo $group_id . $c; ?>"<?php if ( in_array( $check_id, $vmc_interests[0] ) ) : ?> checked<?php endif; ?>> <label for="interest-<?php echo $group_id . $c; ?>"><?php echo $check_name; ?></label> <div class="check"></div> </li>
<?php $c++; endforeach; ?>

                        </ul> </div> </div>

                    </fieldset>
<?php endif; ?>

<?php endforeach; ?>

<?php endif; ?>

<?php endforeach; ?>


<?php $vmc_langs = get_user_meta( $current_user->ID, 'vmc_langs' ); ?>

<?php var_dump($vmc_langs); ?>
                    <div class="acc-language">
                        <p> <?php echo __('Langauge Preference','vogue.me'); ?> <?php $i = 1; foreach ( $groupsByList as $lang => $langauge ) : if ( $lang == 'en' ) : $label = 'English'; else : $label = 'Arabic'; endif; ?> <?php if ( $i > 1 ) : ?> / <?php endif; ?> <input type="radio" value="<?php echo $lang; ?>" name="vmc_langs" data-groups="group_<?php echo $lang; ?>" id="lang_<?php echo $lang; ?>" <?php if ( in_array( $lang, $vmc_langs ) ) : ?>checked<?php endif; ?>> <label for="lang_<?php echo $lang; ?>"><?php echo $label; ?></label> <?php $i++; endforeach; ?>
                            / <input type="radio" value="both" name="vmc_langs" data-groups="" id="lang_both"<?php if ( in_array( 'both', $vmc_langs ) ) : ?> checked<?php endif; ?>> <label for="lang_both">Both</label>
                            / <input type="radio" value="none" name="vmc_langs" data-groups="" id="lang_unsubcribe"> <label for="lang_unsubcribe">Unsubscribe All</label>
                        </p> </div>
                    
                    <div class="acc-newsletter-float"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ipad-newsletter.png"> </div>

                </div>

                <div class="acc-clearer"></div>

                <button name="updateuser" type="submit" id="updateuser" class="acc-submit"><span class="acc-arrow"><?php echo __('Save Changes', 'vogue.me'); ?></span></button>
                <?php wp_nonce_field( 'update-user' ) ?>
                <input name="action" type="hidden" id="action" value="update-user" />

            </form>

            <?php endif; ?>

        </div>

    </div>

<?php endwhile; endif; ?>

  </div>

</div>

<?php get_footer(); ?>