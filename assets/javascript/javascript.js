var Runway = {index: 1};

jQuery(document).ready(function($)
{

    if (!!window.jQuery)
    {
        if (window.OX !== undefined)
        {
			window.NERV = [];
            window.NERV.push(new OX_responsive('main_youcantblock'));
            window.NERV[0].setup();
            window.NERV[0].addPage();
            window.NERV[0].setAdSlots();
            window.NERV[0].load();
			var date = new Date();
			//if (date.getHours() >= 18 || date.getHours() <= 7 || window.mobilecheck()) { setInterval('window.NERV[0].load();', 70000); }

            window.TNERV = new OX_responsive('main_youcantblock1');
            window.TNERV.setup();
            window.TNERV.addPage();
            window.TNERV.setAdSlots();
            window.TNERV.load();
			//if (date.getHours() >= 18 || date.getHours() <= 7 || window.mobilecheck()) { setInterval('window.TNERV.load();', 70000); }
        }
    }

    echo.init(
    {
        offset: 100,
        throttle: 250,
        unload: false
    });


    $(function()
    {
        $(function()
        {
            FadeIn.gather();
            FadeIn.bypassNow();
            Newsletter.parseNewsletterPagebreaks();

            $(window).on('scroll', function()
            {
                FadeIn.check();
            });
        });

        $('.special--container').each(function()
        {
            if ($(this).hasClass('no-js-remove'))
            {
                $(this).removeClass('no-js-remove');
            }
        });
        $('.get--content').each(function()
        {
            if ($(this).hasClass('no-js-initiate'))
            {
                $(this).removeClass('no-js-initiate');
            }
        });
        $('.article--gallery').each(function()
        {
            if ($(this).hasClass('no-js-initiate'))
            {
                $(this).removeClass('no-js-initiate');
            }
        });
        $('.selection--gallery').each(function()
        {
            if ($(this).hasClass('no-js-initiate'))
            {
                $(this).removeClass('no-js-initiate');
            }
        });

        function series_size(ths)
        {

            $t = $(ths);

            var getHeight = $t.find('.sb-promoted-area__inner').height() - 10;

            var setHeight = $t.parents('.sb-section--splits').find('.sb-content--split').find('.post--list').height();

            if (getHeight !== setHeight)
            {
                $t.find('.sb-promoted-area__inner').height(setHeight - 10);

                var getImgHeight = $t.find('img').outerHeight(true);

                if ($t.find('.series--tag').length)
                {
                    $t.find('.series--tag').height(setHeight - 10 - getImgHeight);
                }

            }

        }

        $(window).on('resize', function()
        {
            $('.sb-promoted-area').each(function()
            {
                series_size($(this));
            });
        });

        function fashion_prize_content(ths, event)
        {

            var target = $(ths);
            var get_hash = target.attr('data-content');
            var target_panel = $('#' + get_hash);

            if (!target_panel.hasClass('is-active'))
            {
                target.addClass('current-menu-item').siblings().removeClass('current-menu-item');
                target_panel.addClass('is-active').siblings().removeClass('is-active');
            }

            return false;
            event.preventDefault();


        }

        $('.fp-nav a').each(function(event)
        {
            $(this).on('click', function()
            {
                fashion_prize_content($(this), event);
            });

        });



        function fp_JuryMember(ths)
        {

            $('html,body').animate(
            {
                scrollTop: ($('.fp-nav').offset().top) - 61
            }, 500);

            jury = $(ths);
            jury_portrait = jury.find('.fp-jury-member__portrait').attr('data-src');
            jury_name = jury.find('.fp-jury-member__name').text();
            jury_title = jury.find('.fp-jury-member__title').text();
            jury_content = jury.find('.fp-jury-member__description').html();

            jury_overlay = $('.fb-judge__information');
            jury_overlay_portrait = jury_overlay.find('.fp-jury-member__portrait');
            jury_overlay_name = jury_overlay.find('.fp-jury-member__name');
            jury_overlay_title = jury_overlay.find('.fp-jury-member__title');
            jury_overlay_content = jury_overlay.find('.fp-judge__information--content');

            jury_overlay_portrait.css(
            {
                'background-image': 'url(' + jury_portrait + ')'
            });
            jury_overlay_content.html(jury_content);
            jury_overlay_name.text(jury_name);
            jury_overlay_title.text(jury_title);

            jury_overlay.addClass('is-active');

            jury_overlay.bind('click', function()
            {
                $(this).removeClass('is-active');
            });

        }

        $('#jury').find('.fp-jury__member').each(function()
        {

            $(this).on('click', function(event)
            {
                fp_JuryMember($(this), event);
            });

        });


        var set_rtl = false;

        if ($('html').attr('dir') == 'rtl')
        {
            set_rtl = true;
        }




        function round(value, decimals)
        {
            return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
        }

        /* ============================================================================

          ON SCROLL NAVIGATION

        ============================================================================ */

        var html = $('html');
        var body = $('body');
        var body_html = $('html, body');

        var header = $('#vogue_header');

        var navigation = $('.navigation');
        var small_logo = $('.branding--small');
        var main_logo = $('.branding--main');

        var menu = $('.side--menu');

        var side_menu = $('#side_menu_navigation');
        var side_menu_item = $('li', side_menu);
        var side_menu_height = side_menu.outerHeight(true) - 60;

        var active_class = 'is-active';
        var in_active_class = 'in-active';
        var fixed_class = 'is-fixed';
        var visible_class = 'is-visible';
        var hidden_class = 'is-hidden';
        var search_active = 'search-active';
        var autoComplete_class = '.ui-autocomplete';

        var mask = $('.opaque-mask');
        var menu_mask = $('#mask_01');
        var search_mask = $('.js-mask-search');

        var searchForm = $('#s');
        var searchFormPosts = $('#search_by_posts');
        var searchFormGetTags = $('#search_get_tags');
        var searchFormGetStylebase = $('#search_get_stylebase');
        var searchFormResultsID = '#search_results';
        var searchFormResults = $(searchFormResultsID);
        var search_area = $('.search--area');

        var control_search = $('.js-search');
        var control_menu = $('.js-menu');

        var scroll_class = $('.scroll');
        var ajax_get_content_class = '.get--content';
        var ajax_get_content = $(ajax_get_content_class);

        var change_gallery_view = $('.gallery--change');

        var set_offset_Y = 80;

        var scroll = function(scrollY)
        {
            // If scroll position if over 175
            if ($(window).scrollTop() >= set_offset_Y)
            {
                // Fix Navigation bar below header strip by adding class "is-fixed"
                navigation.addClass(fixed_class);
                // Display smaller logo in header strip by adding class "is-visible" this will bring in the logo from the bottom of the strip
                small_logo.addClass(visible_class);
            }
            else
            {
                // Else allow navigation to sit back in place below the main branding
                navigation.removeClass(fixed_class);
                // If the small logo is already visible
                if (small_logo.hasClass(visible_class))
                {
                    // We'll add "is-hidden" on the way back up to shift the logo up then we'll remove the "is-visible"
                    small_logo.addClass(hidden_class).removeClass(visible_class);
                }

            }

            // If the offset is above 0 and lower than 180
            if ($(window).scrollTop() > 0 && $(window).scrollTop() <= 180 && main_logo.length)
            {
                // Adjust the opacity of the main logo down to 0
                main_logo.css(
                {
                    'top': (1 - $(window).scrollTop())
                });
            }
            // Else if right at the top of the page
            else if ($(window).scrollTop() == 0)
            {
                // Remove the style attribute from the main logo to make sure it is at full opacity
                if (main_logo.length)
                {
                    main_logo.css(
                    {
                        'top': (1 - $(window).scrollTop())
                    });
                }
                // If the small logo has "is-hidden"
                if (small_logo.hasClass(hidden_class) || small_logo.hasClass(search_active))
                {
                    // We'll remove this once we're right at the top of the page so when/if we scroll back down the small logo will re-appear from the bottom of the header strip
                    small_logo.removeClass(hidden_class);
                    small_logo.removeClass(search_active);
                }

            }

        };

        var raf = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame;
        var $window = $(window);
        var lastScrollTop = $window.scrollTop();

        function loop()
        {
            var scrollTop = $(window).scrollTop();

            if (lastScrollTop === scrollTop)
            {
                lastScrollTop = scrollTop;

                scroll(lastScrollTop);

                raf(loop);
                return;
            }

        }

        if (raf)
        {
            loop();
        }


        /* ============================================================================

          INSTAGRAM

        ============================================================================ */

        var instagram = $('.instagram--feed');

        function randomPosition(min, max)
        {

            var randomNumber = Math.floor(Math.random() * (max - min));

            return randomNumber;

        }

        function cycleInstagramFeed(ths)
        {

            var feed = $(ths);
            var feed_items = feed.find('.instagram--item');
            var feed_count = feed_items.length;

            var feed_items_active = feed.find('.instagram--item.' + active_class);
            var active_count = feed_items_active.length;
            var feed_items_inactive = feed.find('.instagram--item.' + in_active_class);
            var inactive_count = feed_items_inactive.length;

            var activeMin = 0;
            var activeMax = active_count;

            if (!feed.hasClass('init'))
            {

                if (feed_count > 5)
                {
                    function pickActiveLocation()
                    {
                        var getPosition = randomPosition(activeMin, activeMax);

                        return getPosition;
                    }

                    function pickInactiveItem()
                    {
                        var getPosition = randomPosition(activeMin, inactive_count);

                        return getPosition;
                    }

                    function pickPosition()
                    {

                        var active_item = pickActiveLocation();
                        var inactive_item = pickInactiveItem();

                        var active_location_class = 'instagram--0' + active_item;

                        var active_location = feed.find('.instagram--item.' + active_location_class);
                        var active_location_img = active_location.find('img');
                        var inactive_location = feed.find('.instagram--item.' + in_active_class + ':eq(' + inactive_item + ')');
                        var inactive_location_img = inactive_location.find('img');

                        // Before animation
                        inactive_location.attr('class', 'instagram--item' + ' ' + active_class + ' ' + active_location_class);
                        inactive_location.css(
                        {
                            zIndex: 3
                        });

                        TweenLite.set(inactive_location_img,
                        {
                            perspective: 1000,
                            rotationY: 180
                        });

                        // Animation
                        var tl = new TimelineLite();
                        tl.pause();

                        tl.to(
                            inactive_location_img,
                            1,
                            {
                                rotationY: 0,
                                ease: Power3.easeInOut
                            },
                            0
                        );

                        tl.to(
                            active_location_img,
                            1,
                            {
                                rotationY: -180,
                                ease: Power3.easeInOut
                            },
                            0
                        );

                        tl.call(function()
                        {
                            active_location_img.css(
                            {
                                transform: ''
                            });
                            inactive_location_img.css(
                            {
                                transform: ''
                            });

                            active_location.addClass(in_active_class).removeClass(active_location_class + ' ' + active_class);
                            inactive_location.css(
                            {
                                zIndex: ''
                            });
                        });

                        tl.play();
                    }

                    var run = setInterval(pickPosition, 2000);

                }

                feed.addClass('init');
            }

        }

        $('.instagram--feed').each(function()
        {
            cycleInstagramFeed(this);
        });


        /* ============================================================================

          SIDE MENU

        ============================================================================ */

        /*
         function controlSubMenu()
         {
             var target = $( this );
             var sub_menu = target.siblings( 'ul' );
             var children_count = sub_menu.find( 'li' ).length;
             var children_height = sub_menu.find( 'li' ).first().outerHeight( true );

             if ( !sub_menu.hasClass( active_class ) )
             {
                 // side_menu.addClass( 'is-open' );

                 // sub_menu.addClass( active_class ).css({ 'height' : children_height * children_count, 'max-height' : side_menu_height });
                 sub_menu.addClass( active_class ).css({ 'height' : children_height * children_count });

                 target.parents( '.has--children' ).css({ 'height' : children_height * ( children_count + 1 ) }).addClass( active_class );
                 target.parents( '.has--children' ).siblings().removeClass( active_class ).removeAttr( 'style' );
                 // target.parents( '.has--children' ).addClass( active_class ).siblings( 'li' ).css({ 'height' : 0 });

                 target.parents( '.has--children' ).siblings().find( 'ul' ).removeClass( active_class ).removeAttr( 'style' );
             }
             else
             {
                 // side_menu.removeClass( 'is-open' );

                 sub_menu.removeClass( active_class ).removeAttr( 'style' );
                 target.parents( '.has--children' ).removeAttr( 'style' ).removeClass( active_class );

                 target.parents( '.has--children' ).siblings().removeClass( active_class ).removeAttr( 'style' );
                 target.parents( '.has--children' ).siblings().find( 'ul' ).removeClass( active_class ).removeAttr( 'style' );
             }

         };

         side_menu_item.each( function()
         {
             var item = $( this );

             if ( item.hasClass( 'has--children' ) )
             {
                 item.prepend( function()
                 {
                      return $( '<span class="has--sub--menu"></span>' ).bind( 'click', controlSubMenu );
                 });

             }

         });
         */

        var controlMenu = function(event)
        {
            if (!menu.hasClass(active_class))
            {
                body.addClass(fixed_class);
                menu_mask.addClass(active_class);
                menu.addClass(active_class);

                // body_html.animate({ scrollTop: 0 }, 500 );

                // small_logo.addClass( search_active );

                $('.navigation--search').removeClass('toggle--close');

                if (search_area.hasClass(active_class))
                {
                    search_area.removeClass(active_class);
                }

                if (!header.hasClass(in_active_class))
                {
                    header.addClass(in_active_class);
                }

                if (search_mask.hasClass(active_class))
                {
                    search_mask.removeClass(active_class);
                }

            }
            else
            {
                body.removeClass(fixed_class);
                menu_mask.removeClass(active_class);
                menu.removeClass(active_class);

                if (header.hasClass(in_active_class))
                {
                    header.removeClass(in_active_class);
                }

                $('.has--children').removeClass(active_class).removeAttr('style');
                $('.has--children').find('ul').removeClass(active_class).removeAttr('style');

            }


        }

        control_menu
            .on('click', controlMenu)
            .bind('keyup', function(e)
            {
                // Bind the esc key to close the search
                if (e.keyCode === 27)
                {
                    controlMenu(event);
                };

            });

        /* ============================================================================

          SCROLLING

        ============================================================================ */

        // var windowHeight = $( window ).outerHeight( true );
        // var currentPage = 1;

        // var titleTag = '';

        // var nextPage = '';
        // var nextPageHREF = '';

        var lastScrollTop = 0;

        // $( '.scroll' ).find( 'div' ).first().attr( 'data-url', window.location.href );

        $(window).on('scroll', function()
        {
            // nextPage = $( '.next a' );
            // nextPageHREF = nextPage.attr( 'href' );

            // titleTag = nextPage.parents( ajax_get_content_class ).attr( 'data-title' );

            /* ============================================================================

              INFINITE SCROLL

            ============================================================================ */

            var st = $(this).scrollTop();

            // if ( st < lastScrollTop )
            // {
            //     var split = $( '.previous' );
            //     var testVis = function ()
            //     {

            //       split.each( function ()
            //       {
            //         if ( $( this ).visible( true ) )
            //         {

            //             var prevPageHREF = $( this ).attr( 'data-url' );
            //             var prevTitle = $( this ).find( ajax_get_content_class ).attr( 'data-title' );

            //             if ( $( this ).attr( 'data-title' ) )
            //             {
            //                 prevTitle = $( this ).attr( 'data-title' );
            //             }

            //             $( this ).next().removeClass('reloaded');

            //             $( this ).addClass( 'reloaded' ).removeClass( 'previous' ).prev().addClass( 'previous loaded' );

            //             history.pushState( {'state':prevPageHREF}, $( 'head > title' ).html( prevTitle ), prevPageHREF );
            //             return false;
            //         }

            //       });

            //     };

            //     testVis();

            // }


            lastScrollTop = st;


            /* ============================================================================

              STICKY SIDE ADS

            ============================================================================ */
            // $('.special--container.vert').each(function() {
            //     var ad = $(this);
            //     var container = ad.closest('.container');
            //     if (container.lengh == 0)
            //       return;
            //     var headHeight = $('.site-head').outerHeight();

            //     // Reset ad position
            //     ad.attr('style', '');

            //     var spaceTop = container.find('.section--header').outerHeight(true);

            //     var adOffset = ad.offset();
            //     var adTop = adOffset.top-headHeight;

            //     var containerOffset = container.offset();
            //     var containerTop = containerOffset.top+spaceTop-headHeight;
            //     var containerBottom = containerOffset.top+container.outerHeight();

            //   var lastImage = container.find('.post--image').last();

            //     // if (ad.outerHeight() > $(window).height() || containerBottom < st || containerTop > st+$(window).height()) {
            //     //     ad.css({transform: ''});
            //     //     return;
            //     // } else {
            //     //     var minY = 0;
            //     //     var maxY = container.outerHeight()-ad.outerHeight();

            //     //     var newY = st-containerTop;
            //     //     newY = Math.min(newY, maxY);
            //     //     newY = Math.max(newY, minY);

            //     //     ad.css({transform: 'translate3d(0, '+newY+'px, 0)'});
            //     // }

            //     if (lastImage.length == 1)
            //      var maxY = (lastImage.offset().top-containerOffset.top+lastImage.outerHeight())-ad.outerHeight(true)-spaceTop;
            //     else
            //      var maxY = container.outerHeight()-ad.outerHeight(true)-spaceTop;

            //     if (ad.outerHeight() > $(window).height() || st+headHeight <= containerTop) {
            //         ad.attr('style', '');
            //         return;
            //     } else if (st+headHeight > containerTop+maxY) {
            //         ad.css({position: '', top: maxY});
            //     } else if (st+headHeight > containerTop) {
            //         ad.css({position: 'fixed', top: headHeight*2});
            //     }
            // });

            $('.special--container.vert.desktop_ads').each(function()
            {

                var ad = $(this);
                var container = ad.closest('.container');

                if (container.length == 0)
                    return;
                var headHeight = $('.site-head').outerHeight();

                // Reset ad position
                // ad.attr('style', '');

                var spaceTop = container.find('.section--header').outerHeight(true);

                var adOffset = ad.offset();
                var adTop = adOffset.top - headHeight;

                var containerOffset = container.offset();
                var containerTop = containerOffset.top + spaceTop - headHeight;
                var containerBottom = containerOffset.top + container.outerHeight();

                var lastImage = container.find('.post--image').last();

                if (lastImage.length == 1)
                    var maxY = (lastImage.offset().top - containerOffset.top + lastImage.outerHeight()) - ad.outerHeight(true) - spaceTop - 300;
                else
                    var maxY = container.outerHeight() - ad.outerHeight(true) - spaceTop - spaceTop - container.find('.post--after').outerHeight(true) - 300;

                if (ad.outerHeight() > $(window).height() || st + headHeight <= containerTop)
                {
                    if (ad.hasClass('scrolling'))
                    {
                        ad.removeClass('scrolling').attr('style', '');
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                else if (st + headHeight > containerTop + maxY)
                {
                    ad.css(
                    {
                        position: '',
                        top: maxY,
                        left: 'auto'
                    }).removeClass('scrolling');
                }
                else if (st + headHeight > containerTop)
                {
                    if (!ad.hasClass('scrolling'))
                    {

                        var offsetLeft = ad.offset().left;

                        if ($('body').hasClass('browser-msie'))
                        {
                            ad.css(
                            {
                                position: 'fixed',
                                top: headHeight * 2,
                                left: offsetLeft - 60
                            }).addClass('scrolling');
                        }
                        else
                        {
                            ad.css(
                            {
                                position: 'fixed',
                                top: headHeight * 2
                            }).addClass('scrolling');
                        }


                    }
                    // ad.css({position: 'fixed', top: headHeight*2});
                }
            });


            /* ============================================================================

              STICKY POST SOCIAL

            ============================================================================ */
            $('.post-social-links--sticky').each(function()
            {
                var links = $(this);
                var container = links.closest('.content--column');
                var featured = container.find('> .featured--area');
                var gallery = featured.find('.article--gallery');
                var galleryItem = gallery.find('.gallery--item').first();
                var footer = container.find('.post--after');
                var headHeight = $('.site-head').outerHeight();

                // Reset ad position
                links.attr('style', '');

                var linksTop = links.position().top;
                if (featured.length == 1)
                {
                    if (gallery.length == 1 && gallery.hasClass('list--view') && galleryItem.length == 1)
                    {
                        linksTop = galleryItem.outerHeight();
                    }
                    else if (gallery.length == 1 && !gallery.hasClass('list--view') && galleryItem.length == 1)
                    {
                        linksTop = galleryItem.outerHeight() + 30;
                    }
                    else
                    {
                        linksTop = featured.outerHeight(true);
                    }
                }

                var containerOffset = container.offset();
                var containerTop = containerOffset.top - headHeight;
                var containerBottom = containerOffset.top + container.outerHeight();

                var maxY = container.outerHeight() - links.outerHeight(true) - footer.outerHeight();

                if (links.outerHeight() > $(window).height() || st + headHeight <= containerTop + linksTop)
                {
                    if (featured.length == 1)
                        links.css(
                        {
                            position: '',
                            top: linksTop
                        });
                    else
                        links.attr('style', '');

                    return;
                }
                else if (st + headHeight > containerTop + maxY)
                {
                    links.css(
                    {
                        position: '',
                        top: maxY
                    });
                }
                else if (st + headHeight > containerTop)
                {
                    links.css(
                    {
                        position: 'fixed',
                        top: headHeight * 2
                    });
                }
            });


            /* ============================================================================

              STICKY GALLERY META

            ============================================================================ */

            $('.article--gallery.list--view .article--gallery--meta__sticky').each(function()
            {
                var meta = $(this);
                var gallery = meta.closest('.article--gallery');
                var container = meta.closest('.content--column');
                var footer = container.find('.post--after');
                var headHeight = $('.site-head').outerHeight();
                var images = gallery.find('.item--image');

                // Reset ad position
                meta.attr('style', '');

                var metaTop = gallery.find('.gallery--item').first().outerHeight();

                var containerOffset = container.offset();
                var containerTop = containerOffset.top - headHeight;
                var containerBottom = containerOffset.top + container.outerHeight();

                var firstImage = images.eq(1);
                var lastImage = images.last();
                if (lastImage.length == 1)
                    var maxY = (lastImage.offset().top - firstImage.offset().top + lastImage.outerHeight()) - meta.outerHeight(true);
                else
                    var maxY = container.outerHeight() - meta.outerHeight(true) - footer.outerHeight();

                if (meta.outerHeight() > $(window).height() || st + headHeight <= containerTop + metaTop)
                {
                    meta.attr('style', '');
                    return;
                }
                else if (st + headHeight > containerTop + maxY + firstImage.position().top)
                {
                    meta.css(
                    {
                        position: '',
                        top: maxY
                    });
                }
                else if (st + headHeight > containerTop)
                {
                    meta.css(
                    {
                        position: 'fixed',
                        top: headHeight * 2
                    });
                }

                // Get active image
                images.each(function()
                {
                    var image = $(this);
                    var imageBottom = image.offset().top + image.outerHeight();

                    if (st + headHeight > imageBottom)
                        return;

                    var index = images.index(image);
                    meta.find('.js-gallery--position__small--cnt').text(index + 1);
                    return false;
                });
            });

        }).trigger('scroll');



        /* ============================================================================

          RESIZE

        ============================================================================ */

        $(window).on('resize', function()
        {

            /* ============================================================================

              FEATURED AREA

            ============================================================================ */
            $('.featured--area.left--anchor').each(function()
            {
                var area = $(this);
                var parent = area.parent();

                if ($('html').attr('dir') == 'rtl')
                {
                    var newWidth = (parent.offset().left + parent.outerWidth() - 360);
                }
                else
                {
                    var newWidth = parent.offset().left + parent.outerWidth();
                }

                area.css(
                {
                    width: newWidth
                });
            });

            $(window).trigger('scroll');
        }).trigger('resize');

        var image_url = VGAR.tmpl_path + '/assets/images';

        $('.post--slider').slick(
        {
            dots: true,
            infinite: false,
            speed: 300,
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: false,
            draggable: false,
            touchMove: false,
            fade: false,
            rtl: set_rtl,
            // vertical: true,
            arrows: true,
            prevArrow: '<div class="gallery--navigation--button gallery--previous"><img src="' + image_url + '/va-gallery-previous-white.svg' + '" alt="previous"></div>',
            nextArrow: '<div class="gallery--navigation--button gallery--next"><img src="' + image_url + '/va-gallery-next-white.svg" alt="previous"></div>'

        });


        /* ============================================================================

          SEARCH

        ===============================================================================

            - Autocomplete

            - http://gabrieleromanato.name/adding-jquery-ui-autocomplete-to-the-wordpress-search-form/

        ============================================================================ */

        if (!$('body').hasClass('mobile'))
        {

            var xTriggered = 0;
            var keyPressCount = 3;
            var delayTime = 500;

            var cache = {};

            (function($)
            {

                function buildSearchSlider()
                {
                    if ($(window).outerWidth(true) > 1250 || ($(window).outerWidth(true) <= 768 && $(window).outerHeight(true) > 600))
                    {
                        while (($children = $(':not(.results-wrap) > .search--post--item:lt(3)')).length)
                        {
                            $children.wrapAll($('<div class="results-wrap"></div>'));
                        }
                    }
                    else if (($(window).outerWidth(true) > 768 && $(window).outerWidth(true) <= 1250) || ($(window).outerHeight(true) < 768 && $(window).outerWidth(true) > 321))
                    {

                        while (($children = $(':not(.results-wrap) > .search--post--item:lt(2)')).length)
                        {
                            $children.wrapAll($('<div class="results-wrap"></div>'));
                        }

                    }
                    else if ($(window).outerWidth(true) <= 320)
                    {

                        while (($children = $(':not(.results-wrap) > .search--post--item:lt(1)')).length)
                        {
                            $children.wrapAll($('<div class="results-wrap"></div>'));
                        }

                    }

                    if ($('#search_post_list').find('.results-wrap').length)
                    {

                        $('#search_post_list').find('.results-wrap').each(function(index)
                        {
                            $('.post--slider').addClass('stop-load');
                            $('.post--slider').slick('slickAdd', $(this));

                            if (index > 10)
                            {
                                return false;
                            }

                        });

                        $('#search_post_list ul.autocomplete').find('.search--post--item').unwrap();

                    }

                }


                // AJAX CHECKER FOR SEARCH
                $(document).ajaxComplete(function()
                {

                    buildSearchSlider();

                    if ($('#vogue_collection__results').find('.vogue-search-item').length)
                    {

                        if (!$('#vogue_collection__results').find('.no-results').length)
                        {
                            var goToLocation = $('#vogue_collection__results').find('.vogue-search-item a').attr('href');

                            $('#search_vogue_collection').on('submit', function()
                            {
                                window.location = goToLocation;
                                return false;
                            });

                            $('#vogue_collection__results').find('button').on('click', function()
                            {
                                window.location = goToLocation;
                                return false;
                            });
                        }

                        $('#searching_vogue').bind('keyup', function(e)
                        {
                            var pressCount = $(this).val().length;

                            if (pressCount == 0)
                            {
                                $('#vogue_collection__results').find('.vogue-search-item').remove();
                            }

                            // Bind the esc key to close the search
                            if (e.keyCode === 27)
                            {
                                $('#vogue_collection__results').find('.vogue-search-item').remove();
                                pressCount = 0;
                            }

                            if (e.which == 13)
                            {
                                hideKeyboard($('input'));
                            }

                        })

                    }

                    else
                    {
                        $('#search_vogue_collection').on('submit', function(event)
                        {

                            $('#search_vogue_collection').find('#searching_vogue').focus();
                            event.preventDefault();
                            event.stopPropagation();
                            return false;
                        });
                        $('#vogue_collection__results').find('button').on('click', function(event)
                        {
                            $('#search_vogue_collection').find('#searching_vogue').focus();
                            event.preventDefault();
                            event.stopPropagation();
                            return false;
                        });
                    }

                });

                function hideKeyboard(element)
                {
                    element.attr('readonly', 'readonly'); // Force keyboard to hide on input field.
                    element.attr('disabled', 'true'); // Force keyboard to hide on textarea field.
                    setTimeout(function()
                    {
                        element.blur(); //actually close the keyboard
                        // Remove readonly attribute after keyboard is hidden.
                        element.removeAttr('readonly');
                        element.removeAttr('disabled');
                    }, 100);

                }

                var completeURL = Globals.ajaxEndPoint;

                var find_tags = completeURL + "?action=search_by_tags";
                var find_posts = completeURL + "?action=search_posts";
                var get_tags = completeURL + "?action=display_relevant_tags";

                var search_vogue_collection = completeURL + "?action=search_vogue_collection";

                var get_stylebase = completeURL + "?action=display_stylebase_tags";

                function hideVogue()
                {
                    // $( '#vogue_collection__results' ).
                }


                if ($('#searching_vogue').length)
                {


                    var vogueCache = {};
                    $('#searching_vogue').autocomplete(
                        {
                            delay: delayTime,
                            minLength: keyPressCount,
                            appendTo: '#vogue_collection__results',
                            source: function(request, response)
                            {
                                var term = request.term.toLowerCase(),
                                    element = this.element;
                                if (term in vogueCache)
                                {
                                    response(vogueCache[term]);
                                    return;
                                }

                                $.ajax(
                                {
                                    url: search_vogue_collection,
                                    dataType: 'json',
                                    data: request,
                                    cache: true,
                                    success: function(data)
                                    {
                                        vogueCache[term] = data;
                                        element.data('autocompleteCache', vogueCache);
                                        response(data.slice(0, 20));

                                        if (!data.length)
                                        {

                                            if (!$('#vogue_collection__results').find('.ui-autocomplete').find('.no-results').length)
                                            {

                                                $('#vogue_collection__results').find('.vogue-search-item').remove();

                                                $('#vogue_collection__results').find('.ui-autocomplete').css(
                                                {
                                                    'display': 'block'
                                                }).append('<li class="vogue-search-item no-results">No matches</li>');
                                            }
                                            return;

                                        }
                                        else
                                        {

                                            $('#vogue_collection__results').find('.no-results').remove();

                                        }

                                    }
                                });
                            }
                        })
                        .data('ui-autocomplete')._renderItem = function(ul, item)
                        {

                            return $('<li class="vogue-search-item"></li>')
                                .data('item.autocomplete', item)
                                .append('<a href="' + item.link + '"> ' + item.thumbnail + ' <span class="vogue-seach-item__name">' + item.label + '</span> </a>')
                                .appendTo(ul);

                        };

                }

                // GET LIST OF POSTS
                var postCache = {};
                searchFormPosts.autocomplete(
                    {
                        delay: delayTime,
                        minLength: keyPressCount,
                        appendTo: '#search_post_list',
                        source: function(request, response)
                        {
                            var term = request.term.toLowerCase()
                                /*,
                                                          element = this.element;

                                                      if ( term in postCache ) {

                                                        console.log('Cached Response');

                                                        response( postCache[ term ] );
                                                        $('.view--all').css('opacity',1);
                                                        return;
                                                      }*/

                            $.ajax(
                            {
                                url: find_posts,
                                dataType: 'json',
                                data: request,
                                cache: true,
                                success: function(data)
                                {
                                    if (!$.isEmptyObject(data))
                                    {
                                        console.log('AJAX Response');

                                        $('.post--slider').find('.no-results').remove();
                                        $('.view--all').css('opacity', 1);

                                        /*postCache[term] = data;
                                        element.data( 'autocompleteCache', postCache );*/
                                        response(data.slice(0, 20));
                                    }
                                    else
                                    {
                                        console.log('AJAX No Results Response');
                                        $('.view--all').css('opacity', 0);
                                        $('.post--slider').find('.slick-track').html('<div class="results-wrap no-results" style="width: 520px;"><div class="search--post--item ui-menu-item"><span>NO RESULTS</span></div></div>');
                                    }
                                }
                            });
                        }
                    })
                    .data('ui-autocomplete')._renderItem = function(ul, item)
                    {
                        return $('<div class="search--post--item"></div>')
                            .data('item.autocomplete', item)
                            .append('<a href="' + item.link + '"><div class="search--post--thumbnail" style="background-image: url(' + item.thumbnail + ');"></div><div class="search--post--item--content"><div class="item--topic">' + item.key_topic_label + '</div><div class="item--title">' + item.label + '</div></div></a>')
                            .appendTo(ul);
                    };

                // GET STANDARD TAGS
                searchFormGetTags.autocomplete(
                    {
                        delay: delayTime,
                        minLength: keyPressCount,
                        appendTo: '#search_topics',
                        source: function(request, response)
                        {
                            var term = request.term.toLowerCase(),
                                element = this.element
                                /*,
                                                            cache         = this.element.data( 'autocompleteCache' ) || {},
                                                            foundInCache  = false*/
                            ;

                            /*$.each( cache, function( key, data )
                            {
                                if ( term.indexOf( key ) === 0 && data.length > 0 )
                                {
                                    response( data );
                                    foundInCache = true;
                                    return;
                                }
                            });*/

                            //if ( foundInCache ) return;

                            $('#search_topics').html('');

                            $.ajax(
                            {
                                url: get_tags,
                                dataType: 'json',
                                data: request,
                                success: function(data)
                                {
                                    /*cache[term] = data;
                                    element.data( 'autocompleteCache', cache );*/
                                    response(data.slice(0, 10));
                                }
                            });
                        }
                    })
                    .data('ui-autocomplete')._renderItem = function(ul, item)
                    {
                        var get_url = '';
                        var get_protocol = document.location.protocol + '//';

                        if (document.domain)
                        {
                            get_url = get_protocol + document.domain + '/vg';
                        }
                        else
                        {
                            get_url = get_protocol + document.location.protocol + document.domain;
                        }

                        return $('<li class="item--topic"></li>')
                            .data('item.autocomplete', item)
                            .append('<a href="' + item.link + '">' + item.label + '</a>')
                            .appendTo($('#search_topics'));
                    };

                // GET STYLE BASE TAGS
                searchFormGetStylebase.autocomplete(
                    {
                        source: function(request, response)
                        {
                            var term = request.term.toLowerCase(),
                                element = this.element,
                                cache = this.element.data('autocompleteCache') ||
                                {},
                                foundInCache = false;

                            $.each(cache, function(key, data)
                            {
                                if (term.indexOf(key) === 0 && data.length > 0)
                                {
                                    response(data);
                                    foundInCache = true;
                                    return;
                                }
                            });

                            if (foundInCache) return;

                            $.ajax(
                            {
                                url: get_stylebase,
                                dataType: 'json',
                                data: request,
                                success: function(data)
                                {
                                    cache[term] = data;
                                    element.data('autocompleteCache', cache);
                                    response(data.slice(0, 6));
                                }
                            });
                        },
                        delay: delayTime,
                        minLength: keyPressCount,
                        appendTo: '#search_style_base'
                    })
                    .data('ui-autocomplete')._renderItem = function(ul, item)
                    {
                        var get_url = '';
                        var get_protocol = document.location.protocol + '//';

                        if (document.domain)
                        {
                            get_url = get_protocol + document.domain + '/vg';
                        }
                        else
                        {
                            get_url = get_protocol + document.location.protocol + document.domain;
                        }

                        return $('<li></li>')
                            .data('item.autocomplete', item)
                            .append('<a href="' + item.link + '">' + item.profile_image + item.label + '</a>')
                            .appendTo(ul);
                    };

                // GET LIST OF SEARCH KEY WORDS
                searchForm.autocomplete(
                    {
                        delay: delayTime,
                        minLength: keyPressCount,
                        appendTo: '#search_key_words .key_words--inner',
                        source: function(request, response)
                        {
                            var term = request.term.toLowerCase(),
                                element = this.element
                                /*,
                                                            cache         = this.element.data( 'autocompleteCache' ) || {},
                                                            foundInCache  = false*/
                            ;

                            /*$.each( cache, function( key, data )
                            {
                                if ( term.indexOf( key ) === 0 && data.length > 0 )
                                {
                                    response( data );
                                    foundInCache = true;
                                    return;
                                }
                            });

                            if ( foundInCache ) return;*/

                            $('#search_key_words').find('ul').html('');

                            $.ajax(
                            {
                                url: find_tags,
                                dataType: 'json',
                                data: request,
                                success: function(data)
                                {
                                    /*cache[term] = data;
                                    element.data( 'autocompleteCache', cache );*/
                                    response(data.slice(0, 9));
                                }
                            });

                        },
                        select: function(event, ui)
                        {
                            var item = ui.item;
                            var itemLabel = item.label;
                            var itemValue = item.value;
                            // Update Value in hidden field to search for posts
                            searchFormPosts.val(itemValue);
                            // Update posts panel
                            searchFormPosts.autocomplete('search');

                            if ($(window).outerWidth(true) > 1170)
                            {
                                // Update form stylebase hidden field
                                //searchFormGetStylebase.val( itemValue );
                                // Update form stylebase panel
                                //searchFormGetStylebase.autocomplete( 'search' );
                                // Update form tags hidden field
                                searchFormGetTags.val(itemValue);
                                // Update form tags panel
                                searchFormGetTags.autocomplete('search');
                            }

                            var HOME_URL = $('.view--all').attr('data-home');
                            $('.view--all').attr('href', HOME_URL + '/?s=' + itemValue);

                            $('.post--slider').slick('slickRemove', null, null, true);
                        }

                    })
                    .bind('keyup', function(e)
                    {
                        var pressCount = $(this).val().length;
                        // Bind the esc key to close the search
                        if (e.keyCode === 27)
                        {
                            controlSearch(event);
                            pressCount = 0;
                        }

                        if (e.which == 13)
                        {
                            hideKeyboard($('input'));
                            return false;
                        }


                        $('.search--topics').find('.ui-autocomplete').detach();

                        // Update Keyword Suggestion
                        $(this).autocomplete('search');

                        if (pressCount >= keyPressCount)
                        {
                            // Update Value in hidden field to search for posts
                            searchFormPosts.val($(this).val());
                            // Update posts panel
                            searchFormPosts.autocomplete('search');

                            if ($(window).outerWidth(true) > 1170)
                            {
                                // Update form stylebase hidden field
                                searchFormGetStylebase.val($(this).val());
                                // Update form stylebase panel
                                //searchFormGetStylebase.autocomplete( 'search' );
                                // Update form tags hidden field
                                searchFormGetTags.val($(this).val());
                                // Update form tags panel
                                searchFormGetTags.autocomplete('search');

                            }

                            $('.post--slider').slick('slickRemove', null, null, true);
                        }

                        var HOME_URL = $('.view--all').attr('data-home');
                        $('.view--all').attr('href', HOME_URL + '/?s=' + $(this).val());

                        $('.post--slider').removeClass('stop-load');

                    });


            })(jQuery);

        }
        else
        {
            searchForm.on('keyup', function(e)
            {
                if (e.which == 13 && searchForm.val().length >= 3)
                {
                    var get_url = '';
                    var get_protocol = document.location.protocol + '//';

                    if (document.domain)
                    {
                        get_url = get_protocol + document.domain + '/vg';
                    }
                    else
                    {
                        get_url = get_protocol + document.location.protocol + document.domain;
                    }

                    window.location.replace(get_url + '/?s=' + searchForm.val());
                }
            })
        }

        /* ============================================================================

            - Keypress counter

            - Counts how many keypresses have been made within the search field and
              displays the search results panel.

        ============================================================================ */

        searchForm.keypress(function(event)
        {
            if (event.which == 13)
            {
                event.preventDefault();

                setTimeout(function()
                {

                    var scroll = window.scrollY;

                    if (scroll < 150)
                    {


                        if ($('body').hasClass('is-search'))
                        {
                            $('body').removeClass('is-search');
                        }
                    }

                }, 500);
            }

            xTriggered++;

            if (xTriggered == keyPressCount)
            {
                searchFormResults.addClass(active_class);
                $(autoComplete_class, searchFormResultsID).addClass(active_class);
            }

        });


        /* ============================================================================

            - Keyup

            - Clears search and hides search results panel

        ============================================================================ */

        searchForm.keyup(function()
        {

            if (!this.value)
            {

                xTriggered = 0;

                $(autoComplete_class, searchFormResultsID).removeClass(active_class);

                searchFormResults.removeClass(active_class);

                searchForm.autocomplete('close');

            }

        });

        // $( window ).on('scroll',function()
        // {
        //   var scroll = window.scrollY;
        //   if(scroll > 150){
        //     if ( $('body').hasClass('is-search'))
        //     {
        //       $('body').removeClass('is-search');
        //     }
        //   }
        // });

        var $delay = 600;

        var controlSearch = function(event)
        {

            if (!search_area.hasClass(active_class))
            {

                $('.navigation--search').addClass('toggle--close');

                // if ( $( window ).scrollTop() < 110 )
                // {
                //     body_html.animate({ scrollTop: header.outerHeight( true ) }, 500 );
                // }

                small_logo.addClass(search_active);

                search_area.addClass(active_class);

                searchForm.focus().val('');

                $('body').addClass('is-search');

                $('.site-head').addClass('is-scrolled');

                if (!search_mask.hasClass(active_class))
                {
                    search_mask.addClass(active_class);

                    // body.addClass( fixed_class );

                }

                if (menu_mask.hasClass(active_class))
                {
                    menu_mask.removeClass(active_class);
                }

                if (menu.hasClass(active_class))
                {
                    menu.removeClass(active_class);

                    $('.has--children').removeClass(active_class).removeAttr('style');
                    $('.has--children').find('ul').removeClass(active_class).removeAttr('style');
                }

                if (header.hasClass(in_active_class))
                {
                    header.removeClass(in_active_class);
                }

            }
            else
            {
                xTriggered = 0;

                searchFormResults.removeClass(active_class);

                setTimeout(function()
                {
                    if ($('body').hasClass('is-search'))
                    {
                        $('body').removeClass('is-search');
                        $('.site-head').removeClass('is-scrolled');
                    }
                }, 500);

                search_mask.removeClass(active_class);

                $('.navigation--search').removeClass('toggle--close');

                if (menu_mask.hasClass(active_class))
                {
                    menu_mask.removeClass(active_class);
                }
                body.removeClass(fixed_class);

                searchForm.val('');
                searchFormPosts.val('');
                searchFormGetTags.val('');

                if (!searchFormResults.hasClass(active_class))
                {
                    $delay = 0;
                }
                else
                {
                    $delay = 600;
                }

                setTimeout(function()
                {
                    search_area.removeClass(active_class);
                    $(autoComplete_class).html('');

                }, $delay);

            }

            event.preventDefault();

        }

        control_search.bind('click', controlSearch);


        function mask_interaction(event)
        {
            if (menu.hasClass(active_class))
            {
                controlMenu(event);
            }

            if (search_area.hasClass(active_class))
            {
                controlSearch(event);
            }

        }

        mask.bind('click', mask_interaction);


        /* ============================================================================

            RELATED TAGS EXPANDS

        ============================================================================ */

        function relatedTags(ths)
        {

            var related_tags = $(ths);
            var related_tag = related_tags.find('li');
            var related_tag_count = related_tag.length;
            var related_tag_height = related_tags.first().outerHeight(true);
            var label;
            var alt_label = 'Minimise';

            related_tags.addClass('init');

            if (related_tag_count > 4)
            {

                if (related_tags.hasClass('related--brands'))
                {
                    label = 'Show All brands';
                }
                else if (related_tags.hasClass('related--people'))
                {
                    label = 'Show All people';
                }

                related_tags.append('<li class="toggle--all"> <a href="#"> <span class="related--label">' + label + '</span> <span class="related--more">' + '<span class="more--less">+</span>' + (related_tag_count - 4) + '</span></a> </li>');

                related_tags.find('.toggle--all a').bind('click', function()
                {
                    var related_parent = $(this).parents('.related--tags');

                    if (!related_parent.hasClass('related--minimise'))
                    {
                        related_parent.addClass('related--minimise');
                        $(this).find('.related--label').text(label);
                        $(this).find('.more--less').text('+');
                    }
                    else
                    {
                        related_parent.removeClass('related--minimise');
                        $(this).find('.related--label').text(alt_label);
                        $(this).find('.more--less').text('-');
                    }

                    return false;
                });

            }

        }

        $.each($('.related--tags'), function()
        {
            relatedTags(this);
        });


        /* ============================================================================

            VOGUE SELECTION ( SLICK JS )

        ============================================================================ */

        function vogueSelection(ths)
        {

            var slickSelection = $(ths);
            var slickSelectionContainer = slickSelection.parents('.selection--gallery');

            var image_url = VGAR.tmpl_path + '/assets/images';

            slickSelection.on('init', function(event, slick, currentSlide, nextSlide)
            {
                slickSelection.addClass('init');
                slickSelectionContainer.addClass('init');
            });

            slickSelection.slick(
            {
                lazyload: 'ondemand',
                dots: true,
                infinite: true,
                autoplay: false,
                speed: 1000,
                slidesToShow: 6,
                slidesToScroll: 6,
                adaptiveHeight: true,
                variableWidth: false,
                rtl: set_rtl,
                // draggable: false,
                // touchMove: false,
                cssEase: 'cubic-bezier(0.77, 0, 0.175, 1)',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings:
                        {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            cssEase: 'ease-out',
                            speed: 300
                        },
                  },
                    {
                        breakpoint: 480,
                        settings:
                        {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            cssEase: 'ease-out',
                            speed: 300
                        }
                  }
                ],
                prevArrow: '<div class="gallery--navigation--button gallery--previous"><img src="' + image_url + '/va-gallery-previous-thick.svg' + '" alt="previous"></div>',
                nextArrow: '<div class="gallery--navigation--button gallery--next"><img src="' + image_url + '/va-gallery-next-thick.svg" alt="previous"></div>'
            });

			slickSelection.on('beforeChange', function(event, slick, currentSlide) 
			{
				slick.$slides.each(function(e) {
					$('img', this).removeClass('auto-fade-in').toggleClass('auto-fade-in-gone');
				});
			});

        }



        $.each($('.selection--inner'), function()
        {
            vogueSelection(this);
        });


        /* ============================================================================

            VOGUE SELECTION ( SLICK JS )

        ============================================================================ */
		
        (function($, sr)
        {

            // debouncing function from John Hann
            // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
            var debounce = function(func, threshold, execAsap)
                {
                    var timeout;

                    return function debounced()
                    {
                        var obj = this,
                            args = arguments;

                        function delayed()
                        {
                            if (!execAsap)
                                func.apply(obj, args);
                            timeout = null;
                        };

                        if (timeout)
                            clearTimeout(timeout);
                        else if (execAsap)
                            func.apply(obj, args);

                        timeout = setTimeout(delayed, threshold || 100);
                    };
                }
                // smartresize
            jQuery.fn[sr] = function(fn)
            {
                return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
            };

        })(jQuery, 'smartresize');


        // Set up Runway Ads

        function runwayAdverts()
        {
            setTimeout(function()
            {
                if (window.OX !== undefined)
                {
                    var RERV = new OX_responsive('main_youcantblock_runway');
                    RERV.setup();
                    RERV.addPage();
                    RERV.setAdSlots();
                    RERV.load();
                }
            }, 500);

        }


        function getRunwayGalleryStateFromHash()
        {
            var state =
            {
                runwayGallery: false,
                index: -1,
            };

            if (location.hash.indexOf('/gallery/') == 1)
            {
                state.runwayGallery = true;

                var hashParts = location.hash.split('/');

                if (hashParts.length > 3 && hashParts[2].length)
                {
                    state.index = parseInt(hashParts[2]);
                }
            }

            return state;
        }

        function updateRunwayFromHash()
        {
            var state = getRunwayGalleryStateFromHash();

            if (state.runwayGallery)
            {
                effectivelyShowRunwayGallery();

                if (state.index != -1)
                {
                    galleryContainer.slick.slickGoTo(state.index - 1);
                }
                else
                {
                    galleryContainer.slick.slickGoTo(0);
                }
            }
            else
            {
                effectivelyHideRunwayGallery();
            }
        }
        Runway.updateRunwayFromHash = updateRunwayFromHash;
        VogueHistory.popStateListeners.push(updateRunwayFromHash);

        function updateRunwayGalleryHash(showing, index)
        {
            var url = document.location.protocol +"//"+ document.location.hostname + document.location.pathname;
			var urlWithoutSlash = url.replace(/\/$/, ''); 
			var lastSegment = urlWithoutSlash.split("/").pop();
			console.log('this is the url' + url + 'last segment is: ' + lastSegment );
            if (showing)
            {
			
				url += '#/gallery/';
                index && (url += index + '/');
					

            }

            var state =
            {
                index: VogueHistory.currentContentIndex,
                title: VogueHistory.currentTitle,
                url: VogueHistory.currentURL,
                runwayGallery: showing,
            };

            history.replaceState(state, VogueHistory.currentTitle, url);
        }

        function showRunwayGallery()
        {
            zeroBasedIndex = Runway.index - 1;
			console.log(Runway.index);
            //Runway.index = 1;
            galleryContainer.slick.slickGoTo(zeroBasedIndex);
            updateRunwayGalleryHash(true, Runway.index);
            effectivelyShowRunwayGallery();
        }

        function effectivelyShowRunwayGallery()
        {
            $('.runway--overlay').addClass(active_class);
            $('html').addClass(fixed_class);
            $('.thumbs').removeClass('show');

            echo.render();

            // Set up ads
            runwayAdverts();
        }

        function hideRunwayGallery()
        {
            updateRunwayGalleryHash(false);
            effectivelyHideRunwayGallery();
        }

        function effectivelyHideRunwayGallery()
        {
            $('.runway--overlay').removeClass(active_class);
            $('html').removeClass(fixed_class);
        }


        // Show runway ads if runway overllay is active and some other condition

        if ($('#main_youcantblock_runway').length && $('.runway--overlay').hasClass('is-active'))
        {
            runwayAdverts();
        }


        var galleryContainer;

        function runwayGallery(_galleryContainer)
        {
            galleryContainer = _galleryContainer;

            var $galleryContainer = $(galleryContainer);
            var $thumbs = $galleryContainer.parents('.runway--overlay').find('.thumbs');
            var $thumbItems = $thumbs.find('li');

            $thumbItems.find('a').on('click', function(e)
            {
                e.preventDefault();
            })

            function updateDetails(slideIndex)
            {
                if ($('.js-include').length)
                {
                    var $item = $('.gallery--container').find('li:eq(' + slideIndex + ')');

                    var $model = $item.find('.js-get-model');
                    if ($model.length)
                    {
                        var modelContents = $model.html();
                        $('.js-update-model').html(modelContents);
                    }

                    var $credit = $item.find('.js-get-credit');

                    if ($credit.length)
                    {
                        var creditText = $credit.text();
                        $('.js-update-credits').html(creditText);
                    }
                }
            }


            // If "Start Over" is clicked, go to image 0

            $('.js-start-over').bind('click', function(e)
            {
                galleryContainer.slick.slickGoTo(0);
                e.preventDefault();
            });


            // Set up each thumb
            $thumbItems.each(function(e)
            {
                var $thumb = $(this);

                $thumb.on('click', function(e)
                {
                    zeroBasedIndex = $thumb.index();
                    Runway.index = zeroBasedIndex + 1;

                    if (zeroBasedIndex != $thumbItems.length - 1)
                    {
                        $('.current--slide').text(zeroBasedIndex + 1);
                    }

                    galleryContainer.slick.slickGoTo(zeroBasedIndex);
                    $thumb.addClass('slick-active').siblings().removeClass('slick-active');

                    if ($('.runway--col1').hasClass('show'))
                    {
                        $('.runway--col1').removeClass('show');
                    }

                });

            });

            var $loadingImage;

            $galleryContainer.on('init', function(event, slick)
            {
                $galleryContainer.addClass('init');

                var count = $thumbItems.length - 1;
                $('.js-total-count').text(count);

                echo.init(
                {
                    offset: 400,
                    throttle: 250,
                    unload: false,
                });
                echo.render();

                $('.runway--col1__inner').on('scroll', function()
                {
                    echo.render();
                });

            }).on('beforeChange', function(event, slick, currentSlide)
            {
                $('.gallery--container').addClass('loading');

            }).on('afterChange', function(event, slick, currentSlide)
            {
				//window.NERV[0].load(); // hotfix ad refresh
                zeroBasedIndex = $(slick.$slides.get(currentSlide)).index();
                Runway.index = zeroBasedIndex + 1;

                var $thumb = $thumbItems.eq(zeroBasedIndex);

                $thumb.addClass('slick-active').siblings().removeClass('slick-active');

                updateRunwayGalleryHash(true, Runway.index);

                var $img = $(slick.$slides.get(currentSlide)).find('.loaded--img');
                $loadingImage = $('<img>');
                $loadingImage.one('load error', function()
                {
                    if ($loadingImage.is(this))
                    {
                        $('.gallery--container').removeClass('loading');
                    }
                }).attr('src', $img.attr('src'));

                if (zeroBasedIndex == $thumbItems.length - 1)
                {
                    $('.runway--overlay').addClass('end--slide');

                    echo.render();
                }
                else
                {
                    $('.current--slide').text(Runway.index);
                    $('.runway--overlay').removeClass('end--slide');
                    updateDetails(zeroBasedIndex);
                }
            });

            $('.runway--overlay').find('.js-include').find('img').each(function()
            {

                if (!$(this).parents('.js-include').hasClass('rw-next-slide'))
                {
                    $(this).on('click', function()
                    {
                        zeroBasedIndex = $(this).parents('li').index();
                        Runway.index = zeroBasedIndex + 1;

                        var nextSlide = zeroBasedIndex + 1;

                        galleryContainer.slick.slickGoTo(nextSlide);

                        updateDetails(nextSlide);
                    });
                }

            });

            var zeroBasedIndex = Runway.index - 1;
            $('.current--slide').text(Runway.index);
            updateDetails(zeroBasedIndex);

            $thumbItems.find('li:eq(' + zeroBasedIndex + ')').addClass('slick-active');

            if (zeroBasedIndex == $thumbItems.length - 1)
            {
                $('.runway--overlay').addClass('end--slide');
            }


            $galleryContainer.slick(
            {
                lazyLoad: 'ondemand', // progressive / ondemand
                dots: false,
                infinite: false,
                autoplay: false,
                speed: 200,
                slidesToShow: 1,
                slidesToScroll: 1,
                adaptiveHeight: false,
                // draggable: false,
                // touchMove: false,
                initialSlide: zeroBasedIndex,
                rtl: set_rtl,
                fade: true,
                appendArrows: $('.nav--list'),
                cssEase: 'ease',
                prevArrow: '<li> <a href="#" class="nav--prev"></a> </li>',
                nextArrow: '<li> <a href="#" class="nav--next"></a> </li>',
                responsive: [
                    {
                        breakpoint: 1200,
                        settings:
                        {
                            fade: false,
                            speed: 200
                        }
                }
              ]
            });
            $('.nav--next,.nav--prev').on('click', function(e)
            {
                e.preventDefault();
            })

            $('.js-grid-toggle').on('click', function()
            {
                if ($('.runway--col1').hasClass('show'))
                {
                    $('.runway--col1').removeClass('show');
                }
                else
                {
                    $('.runway--col1').addClass('show');

                    echo.render();
                }
                return false;
            });

            $('.js-runway--col1__close').on('click', function(e)
            {
                e.preventDefault();
                $('.js-runway--col1').removeClass('show');
            });

        }


        // DO set up Runway gallery

        $.each($('.gallery--container'), function()
        {
            runwayGallery(this);
        });

        $('.runway--thumbnail').on('click', function(e)
        {
            Runway.index = 1;
            showRunwayGallery();
            return false;
        });
		
		$('.single-runway-image').on('click', function(e)
        {
            // passing the clicked index number to the lee's global varaible -- this is stupid though 
			var indexnumber = $(this).data('number');
			Runway.index = indexnumber;
            showRunwayGallery();
			e.preventDefault();
            return false;
        });

        $('.overlay--close').add('.gallery-close-button').on('click', function(e)
        {
            hideRunwayGallery();
            e.preventDefault();
        });
        updateRunwayFromHash();



        /* ============================================================================

            ARTICLE GALLERIES ( SLICK JS )

        ============================================================================ */

        function showGridView()
        {
            var target = $(this);
            var target_gallery = target.parents('.article--gallery');

            if (target.hasClass('gallery--view--thumbs__sticky'))
            {
                if (!target_gallery.hasClass(active_class))
                {
                    $('body').css(
                    {
                        overflowY: 'hidden'
                    });

                    target.addClass(active_class);

                    target_gallery.addClass(active_class);

                    var stickyGrid = target_gallery.find('.article--gallery--grid--view').clone();
                    target_gallery.find('.article--gallery--meta__sticky').append(stickyGrid).addClass('has-mask');
                    stickyGrid.addClass(active_class);
                    stickyGrid.addClass('is-sticky');
                    stickyGrid.find('.grid--item').removeClass('is-active');

                    setTimeout(function()
                    {
                        stickyGrid.addClass('loaded');
                    }
                    , 300);

                }
                else
                {
                    $('body').css(
                    {
                        overflowY: ''
                    });

                    var stickyGrid = $('.article--gallery--meta__sticky .article--gallery--grid--view');

                    target_gallery.scrollTop(0);
                    target.removeClass(active_class);

                    target_gallery.removeClass(active_class);
                    stickyGrid.removeClass(active_class + ' loaded is-sticky');
                    target_gallery.find('.article--gallery--meta__sticky').removeClass('has-mask');

                    setTimeout(function()
                    {
                        stickyGrid.remove();
                    }
                    , 300);
                }
            }
            else
            {

                if (!target_gallery.hasClass(active_class))
                {
                    target.addClass(active_class);

                    target_gallery.addClass(active_class);
                    target_gallery.find('.article--gallery--grid--view').addClass(active_class);

                    setTimeout(function()
                    {
                        target_gallery.find('.article--gallery--grid--view').addClass('loaded');
                    }, 300);

                }
                else
                {

                    target_gallery.scrollTop(0);
                    target.removeClass(active_class);

                    target_gallery.removeClass(active_class);
                    target_gallery.find('.article--gallery--grid--view').removeClass(active_class + ' loaded');
                }

            }

        }

        $('.gallery--view--thumbs').on('click', showGridView);



        function slickCarousel(ths)
        {

            var slick_gallery = $(ths);

            var setHeight;

            var image_url = VGAR.tmpl_path + '/assets/images';

            var total_slides = slick_gallery.siblings('.article--gallery--meta').find('.total--slides');
            var current_slide = slick_gallery.siblings('.article--gallery--meta').find('.current--slide');

            slick_gallery.on('init', function(event, slick, currentSlide, nextSlide)
            {

                slick_gallery.addClass('init').parents('.article--gallery').addClass('init');

                var $currentSlide = $(slick_gallery.find('.gallery--item').get(currentSlide));

                var slick_height = slick_gallery.outerHeight(true);
                var content_height = $currentSlide.find('.item--content').outerHeight(true);

                if (!slick_gallery.parents('.article--gallery').hasClass('shoppable'))
                {
                    setHeight = slick_height - content_height;
                }
                else
                {
                    setHeight = slick_height;
                }

                if (slick_gallery.parents('.article--gallery').find('.make--shoppable').length)
                {
                    slick_gallery.parents('.article--gallery').addClass('shoppable');
                    setHeight = slick_height - content_height + 2;
                }

                // slick_gallery.parents( '.article--gallery' ).find( '.article--gallery--meta' ).css({ 'height' : setHeight /*, 'max-height' : setHeight */ });
                // slick_gallery.parents( '.article--gallery' ).find( '.article--gallery--grid--view' ).height( setHeight - 62 );

                $('.article--gallery--grid--view ').find('.grid--item:eq(0)').addClass(active_class);

                var i = (currentSlide ? currentSlide : 0) + 1;
                current_slide.text(i);
                total_slides.text(slick.slideCount);

                $(window).trigger('resize');
            });

            slick_gallery.slick(
                {
                    dots: false,
                    infinite: false,
                    speed: 300,
                    lazyLoad: 'ondemand',
                    slidesToShow: 1,
                    adaptiveHeight: true,
                    draggable: true,
                    touchMove: true,
                    fade: true,
                    rtl: set_rtl,
                    cssEase: 'linear',
                    prevArrow: '<div class="gallery--navigation--button gallery--previous"><img src="' + image_url + '/va-gallery-previous.svg' + '" alt="previous"></div>',
                    nextArrow: '<div class="gallery--navigation--button gallery--next"><img src="' + image_url + '/va-gallery-next.svg" alt="previous"></div>',
                    appendArrows: slick_gallery.siblings('.article--gallery--meta').find('.gallery--navigation'),
                    responsive: [
                        {
                            breakpoint: 1240,
                            settings:
                            {
                                dots: true
                            },
                            breakpoint: 400,
                            settings:
                            {
                                adaptiveHeight: false,
                                // centerMode: true,
                                // variableWidth: true,
                                prevArrow: '<div class="gallery--navigation--button gallery--previous"><img src="' + image_url + '/va-gallery-previous-thick.svg' + '" alt="previous"></div>',
                                nextArrow: '<div class="gallery--navigation--button gallery--next"><img src="' + image_url + '/va-gallery-next-thick.svg" alt="previous"></div>'
                            }
                }
              ]
                })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide)
                {
                    var i = (nextSlide ? nextSlide : 0) + 1;

                    // Pagination animation
                    var tl = new TimelineLite();
                    tl.pause();

                    tl.to(
                        current_slide,
                        0.15,
                        {
                            alpha: 0,
                            x: 10,
                            y: 10,
                            ease: Power3.easeIn
                        }
                    );

                    tl.call(function()
                    {
                        current_slide.text(i);
                    });

                    tl.to(
                        current_slide,
                        0.15,
                        {
                            alpha: 1,
                            x: 0,
                            y: 0,
                            ease: Power3.easeOut
                        }
                    );

                    tl.call(function()
                    {
                        current_slide.css(
                        {
                            opacity: '',
                            transform: ''
                        });
                    });

                    tl.play();
                })
                .on('afterChange', function(event, slick, currentSlide, nextSlide)
                {
					//window.NERV[0].load(); // hot fix ad refresh
                    var $currentSlide = $(slick_gallery.find('.gallery--item').get(currentSlide));

                    var slick_height = slick_gallery.outerHeight(true);
                    var content_height = $currentSlide.find('.item--content').outerHeight(true);

                    if (!slick_gallery.parents('.article--gallery').hasClass('shoppable'))
                    {
                        setHeight = slick_height - content_height - 2;
                    }
                    else
                    {
                        setHeight = slick_height - 2;
                    }

                    // slick_gallery.parents( '.article--gallery' ).find( '.article--gallery--meta' ).height( setHeight );
                    // slick_gallery.parents( '.article--gallery' ).find( '.article--gallery--grid--view' ).height( setHeight - 60 );

                    $currentSlide.addClass(active_class);
                    $currentSlide.siblings().removeClass(active_class);

                    total_slides.text(slick.slideCount);
                });

        }

        $.each($('.article--gallery--inner'), function()
        {
            slickCarousel(this);
        });

        function changeSlide(e)
        {
            var target = $(this);
            var targetItem = target.parents('.grid--item');
            var targetItemPosition = targetItem.index();

            target.parents('.article--gallery').find('.gallery--view--thumbs').removeClass(active_class);

            target.parents('.article--gallery').removeClass(active_class);
            target.parents('.article--gallery--grid--view').removeClass(active_class);

            if (target.parents('.article--gallery--grid--view').hasClass('is-sticky'))
            {
                var stickyGrid = $('.article--gallery--meta__sticky .article--gallery--grid--view');
                stickyGrid.removeClass(active_class + ' loaded is-sticky');
                $('.article--gallery--meta__sticky').removeClass('has-mask');

                setTimeout(function()
                {
                    stickyGrid.remove();
                }, 300);
            }

            if (target.parents('.article--gallery').hasClass('list--view'))
            {
                var image = target.parents('.article--gallery').find('.gallery--item').eq(targetItemPosition);
                var top = image.offset().top - ($('.site-head').outerHeight() * 2);

                $('body').css(
                {
                    overflowY: ''
                });

                $('html,body').animate(
                {
                    scrollTop: top
                },
                {
                    duration: 800,
                    easing: 'easeInOutCubic'
                });
            }
            else
            {
                target.parents('.article--gallery').find('.article--gallery--inner')[0].slick.slickGoTo(parseInt(targetItemPosition));
            }

            e.preventDefault();
            return false;
        }

        $('body').on('click', '.article--gallery--grid--view .grid--item > a', changeSlide);

        function changeView()
        {
            var target = $(this);
            var targetGallery = target.parents('.article--gallery');

            if (!targetGallery.hasClass('list--view'))
            {
                targetGallery.addClass('list--view').removeClass(active_class).find('.article--gallery--grid--view').removeClass(active_class);
                targetGallery.find('.gallery--change').removeClass('to--list').addClass('to--slider');

                $('.gallery--view--thumbs').removeClass(active_class);

                var articles = targetGallery.find('.gallery--item');

                // Scroll to first image
                var firstPost = articles.filter('.slick-current');
                var top = firstPost.offset().top - ($('.site-head').outerHeight() * 2);

                var $fadeInItems = targetGallery.find('.gallery--item').slice(1).find('img');
                FadeIn.add($fadeInItems);
                FadeIn.check();

                $('html,body').animate(
                {
                    scrollTop: top
                },
                {
                    duration: 800,
                    easing: 'easeInOutCubic'
                });
            }
            else
            {
                targetGallery.find('.gallery--change').addClass('to--list').removeClass('to--slider');
                var index = target.closest('.gallery--item').index();
                targetGallery.find('.slick-slider').slick('slickGoTo', index);

                // Scroll to slideshow
                var top = targetGallery.offset().top - ($('.site-head').outerHeight() * 2);

                $('html,body').animate(
                {
                    scrollTop: top
                },
                {
                    duration: 800,
                    easing: 'easeInOutCubic',
                    complete: function()
                    {
                        targetGallery.removeClass('list--view');

                        var $fadeInItems = targetGallery.find('.gallery--item').slice(1).find('img');
                        FadeIn.remove($fadeInItems);
                    }
                });
            }

            $(window).trigger('scroll');
        }

        $('.gallery--change').on('click', changeView);
		if ($('body').hasClass('mobile')){
		$('.gallery--change').each(function() {
			var target = $(this);
            var targetGallery = target.parents('.article--gallery');

            if (!targetGallery.hasClass('list--view'))
            {
                targetGallery.addClass('list--view').removeClass(active_class).find('.article--gallery--grid--view').removeClass(active_class);
                targetGallery.find('.gallery--change').removeClass('to--list').addClass('to--slider');

                $('.gallery--view--thumbs').removeClass(active_class);

                var articles = targetGallery.find('.gallery--item');

                // Scroll to first image
                var firstPost = articles.filter('.slick-current');
                var top = firstPost.offset().top - ($('.site-head').outerHeight() * 2);

                var $fadeInItems = targetGallery.find('.gallery--item').slice(1).find('img');
                FadeIn.add($fadeInItems);
                FadeIn.check();

                /*$('html,body').animate(
                {
                    scrollTop: top
                },
                {
                    duration: 800,
                    easing: 'easeInOutCubic'
                });*/
            }
		});
		}




        /* ============================================================================

          ARTICLE VIDEO PLAYER

        ============================================================================ */


        function rePositionQueue()
        {

            var playerHeight = $('.video-details__video').outerHeight(true);
            var titleHeight = $('.video-details-content__title').outerHeight(true);
            var sidebarHeight = $('.video-details-sidebar').height();
            var playlistOffset = titleHeight + 80;

            if ($(window).outerWidth(true) <= 768)
            {
                setMargin = sidebarHeight;
                if (sidebarHeight > 200)
                {
                    setMargin = 200;
                }
                $('.video-details-sidebar').css(
                {
                    'top': playerHeight + playlistOffset
                });
                $('.video-details-content__text').css(
                {
                    'margin-top': setMargin
                });
            }
            else
            {
                $('.video-details-sidebar').removeAttr('style');
                $('.video-details-content__text').removeAttr('style');
            }

        }

        function scrollToItem($item)
        {
            var $scrollable = $('.video-details-sidebar__inner');

            var scrollableHeight = $scrollable.height();
            var itemHeight = $item.height();

            var difference = $item.position().top - $scrollable.position().top;

            var fit = false;
            var offset = 0;
            if (difference < 0)
            {
                fit = true;
                offset = difference;
            }
            else if (difference + itemHeight > scrollableHeight)
            {
                fit = true;
                offset = difference + itemHeight - scrollableHeight;
            }

            fit && $scrollable.animate(
            {
                scrollTop: $scrollable[0].scrollTop + offset
            });
        }

        if ($('.video-details-sidebar').length)
        {
            rePositionQueue();
            $(window).on('resize', rePositionQueue);
        }

        var video_container = $('.video--container');

        function videoInitiate(ths)
        {

            var target = $(ths);
            var video_player = target.find('.video--player');
            var video = video_player.get(0);
            var play = target.find('.video--play');
            var controls = target.find('.video--controls');
            var full_screen = controls.find('.video--fullscreen');
            var seek_bar = controls.find('.video--seek');
            var volume_bar = controls.find('.video--volume');

            var video_current = target.find('.video--current');
            var video_total = target.find('.video--total');

            var currently_playing_container = $('#currently_playing');
            var video_queue = $('#video_queue');

            var queued = target.parents('.js-in-queue');

            target.addClass('init');

            var form = video_player.find('form');

            queued.on('click', function()
            {

                var current_video = currently_playing_container.find('.video--container');
                var current_video_e = current_video.find('.video--player').get(0);

                if (!$(this).hasClass('js-is-playing'))
                {
                    var currently_playing = video_queue.find('.js-is-playing');
                    var play_next = $(this);
                    var get_video = play_next.find('.video--container').detach();
                    var this_video = get_video.find('.video--player').get(0);

                    // PUSH STATE AND UPDATE FIELDS
                    var next_url = play_next.attr('data-url');
                    var next_title = play_next.attr('data-title');
                    var next_content = play_next.find('.video-details-sidebar__text').html();
                    var next_video_date = play_next.attr('data-date');
                    var put_credits = $('.js-put-credits');

                    if (next_url)
                    {
                        VogueHistory.replaceState(0, next_title, next_url);
                    }

                    if (!play_next.find('.video-details-sidebar__text').length)
                    {
                        $('.video-details-content').addClass('temp-hide');
                    }
                    else
                    {
                        $('.video-details-content').addClass('temp-hide hide-all');
                    }

                    setTimeout(function()
                    {
                        if ($('.js_video_date').length)
                        {
                            $('.js_video_date').text(next_video_date);
                        }

                        if ($('.video-details-content__title').length)
                        {
                            $('.video-details-content__title').text(next_title);
                        }

                        if ($('.video-details-content__text').length)
                        {
                            $('.video-details-content__text').html(next_content);
                        }

                        if ($('.js-get-credits', play_next).length)
                        {
                            $('.js-put-credits').html($('.js-get-credits', play_next).html());
                        }
                        else
                        {
                            $('.js-put-credits').empty();
                        }

                        if ($('.js-get-social', play_next).length)
                        {
                            $('.js-put-social').html($('.js-get-social', play_next).html());
                        }

                        rePositionQueue();
                        scrollToItem(play_next);
                    }, 300);

                    // Pause video
                    current_video_e.pause();
                    // Reset Video to 0
                    current_video_e.currentTime = 0;

                    if (currently_playing.find('.video--container').length)
                    {
                        currently_playing.find('.video--container').detach();
                    }

                    get_video.addClass('is--loading');

                    if (this_video.paused == true)
                    {

                        var total_time = video.duration;
                        var total_seconds = Math.floor(total_time);

                        if (!get_video.hasClass('first--play'))
                        {
                            get_video.find('.video--total').text(formatTime(total_seconds));
                        }
                        else if (isNaN(total_seconds))
                        {
                            total_time = video.duration;
                            total_seconds = Math.floor(total_time);

                            get_video.find('.video--total').text(formatTime(total_seconds));
                        }

                        this_video.play();
                        get_video.addClass('playing currently--playing first--play').removeClass('is--loading currently--paused');
                    }

                    get_video.appendTo(currently_playing_container);
                    current_video.appendTo(currently_playing);

                    $(this).addClass('js-is-playing is-active');
                    currently_playing.removeClass('js-is-playing is-active is-paused');


                    // Commented this because it was causing an exception.
                    // The btnMore variable is undefined in this scope.
                    // I also commented the scrollTo lines because it was scrolling
                    // up to the article whenever you clicked on one of the
                    // video thumbnails next to the player, instead of just letting you
                    // watch the video. And it was pretty weird.
                    //
                    // ~ ces@ingamana.com

                    // var textTop = $(this).offset().top

                    // var tl = new TimelineLite(
                    // {
                    //     paused: true
                    // });

                    // tl
                    //     .to(btnMore, 0.25,
                    //     {
                    //         alpha: 0,
                    //         ease: Expo.easeOut
                    //     })
                    //     .set(btnMore,
                    //     {
                    //         display: "none"
                    //     })
                    //     .set(btnLess,
                    //     {
                    //         display: "inline"
                    //     })
                    //     .from(btnLess, 0.25,
                    //     {
                    //         alpha: 0,
                    //         ease: Expo.easeOut
                    //     })
                    //     .to(btnArrow, 0.5,
                    //     {
                    //         rotation: 180,
                    //         ease: Expo.easeInOut
                    //     }, "-=0.5");


                    // TweenMax.to(window, 0.5,
                    // {
                    //     scrollTo: textTop - 100,
                    //     ease: Expo.easeInOut
                    // });

                    // RESHOW VIDEO DETAILS
                    setTimeout(function()
                    {
                        $('.video-details-content').removeClass('temp-hide hide-all');
                    }, 700);

                }
                else
                {

                    if (video.paused == true)
                    {
                        // Pause video
                        current_video_e.play();

                        $(this).addClass('is-active').removeClass('is-paused');

                        current_video.addClass('playing currently--playing first--play');
                    }
                    else
                    {
                        current_video_e.pause();
                        current_video.removeClass('playing currently--playing currently--paused');
                        $(this).addClass('is-paused').removeClass('is-active');
                    }

                }

            });

            // Play Pause
            play.bind('click', function(event)
            {

                var total_time = video.duration;
                var total_seconds = Math.floor(total_time);

                if (!target.hasClass('first--play'))
                {
                    video_total.text(formatTime(total_seconds));
                }
                else if (isNaN(total_seconds))
                {
                    var total_time = video.duration;
                    var total_seconds = Math.floor(total_time);

                    video_total.text(formatTime(total_seconds));
                }


                if (video.paused == true)
                {
                    video.play();
                    target.addClass('playing currently--playing first--play').removeClass('currently--paused');
                    $('.js-is-playing').addClass('is-active').removeClass('is-paused');
                }
                else
                {
                    video.pause();
                    target.removeClass('playing currently--playing currently--paused');
                    $('.js-is-playing').removeClass('is-active').addClass('is-paused user--paused');
                }

            });

            // Toggle Fullscreen
            full_screen.bind('click', function()
            {
                if (video.requestFullscreen)
                {
                    video.requestFullscreen();
                }
                else if (video.mozRequestFullScreen)
                {
                    video.mozRequestFullScreen(); // Firefox
                }
                else if (video.webkitRequestFullscreen)
                {
                    video.webkitRequestFullscreen(); // Chrome and Safari
                }

            });


            seek_bar.bind('mouseup', function(e)
            {
                var percent = e.offsetX / this.offsetWidth;

                video.currentTime = percent * video.duration;

                seek_bar.find('.video--progress').css(
                {
                    'width': (percent / 100) + '%'
                });

                if (video.paused == true)
                {
                    video.play();
                    target.addClass('playing currently--playing first--play').removeClass('currently--paused');
                }

                $data_view = $('*[data-name=video_view_count]', target).find('input');
                $stored = parseInt($data_view.val());

                if (video.currentTime > 3 && !$data_view.hasClass('gotTime'))
                {
                    if (isNaN($stored) == true)
                    {
                        $data_view.attr('value', 1);
                    }
                    else
                    {
                        $data_view.attr('value', parseInt($stored + 1));
                    }

                    $update = $data_view.val(parseInt($stored + 1));

                    $data_view.addClass('gotTime');

                    event.preventDefault();

                    var form_data = {
                        'action': 'acf/validate_save_post'
                    };

                    $('form :input', target).each(function()
                    {
                        form_data[jQuery(this).attr('name')] = jQuery(this).val();
                    });

                    form_data.action = 'save_my_data';

                    jQuery.post(Globals.ajaxEndPoint, form_data)
                        .done(function(save_data) {});

                }

            });

            video_player.bind('webkitendfullscreen', function()
            {
                // console.log('yep');
            });

            // When video has finished playing
            video_player.on('ended', function()
            {
                // Pause video
                video.pause();
                // Reset Video to 0
                video.currentTime = 0;
                // Remove playing classes to hide controls and show play button again
                target.removeClass('playing currently--playing currently--paused first--play');

                if (target.parents('js-auto-play'))
                {
                    video.play();
                    target.addClass('playing currently--playing first--play');
                }

                $data_view = $('*[data-name=video_view_count]', target).find('input');
                $stored = parseInt($data_view.val());

                if (!$data_view.hasClass('gotTime'))
                {
                    if (isNaN($stored) == true)
                    {
                        $data_view.attr('value', 1);
                    }
                    else
                    {
                        $data_view.attr('value', parseInt($stored + 1));
                    }

                    $update = $data_view.val(parseInt($stored + 1));

                    $data_view.addClass('gotTime');

                    event.preventDefault();

                    var form_data = {
                        'action': 'acf/validate_save_post'
                    };

                    $('form :input', target).each(function()
                    {
                        form_data[jQuery(this).attr('name')] = jQuery(this).val();
                    });

                    form_data.action = 'save_my_data';

                    jQuery.post(Globals.ajaxEndPoint, form_data)
                        .done(function(save_data) {});

                }

                if (video_queue.length)
                {
                    var current_video = currently_playing_container.find('.video--container');
                    var currently_playing = video_queue.find('.js-is-playing');
                    var play_next = currently_playing.next();

                    if (play_next.length)
                    {

                        // PUSH STATE AND DATA UPDATE
                        var next_url = play_next.attr('data-url');
                        var next_title = play_next.attr('data-title');
                        var next_content = play_next.find('.video-details-sidebar__text').html();
                        var next_video_date = play_next.attr('data-date');
                        var put_credits = $('.js-put-credits');

                        if (next_url)
                        {
                            VogueHistory.replaceState(0, next_title, next_url);
                        }

                        if (!play_next.find('.video-details-sidebar__text').length)
                        {
                            $('.video-details-content').addClass('temp-hide');
                        }
                        else
                        {
                            $('.video-details-content').addClass('temp-hide hide-all');
                        }

                        setTimeout(function()
                        {
                            if ($('.js_video_date').length)
                            {
                                $('.js_video_date').text(next_video_date);
                            }

                            if ($('.video-details-content__title').length)
                            {
                                $('.video-details-content__title').text(next_title);
                            }

                            if ($('.video-details-content__text').length)
                            {
                                $('.video-details-content__text').html(next_content);
                            }

                            if ($('.js-get-credits', play_next).length)
                            {
                                $('.js-put-credits').html($('.js-get-credits', play_next).html());
                            }

                            if ($('.js-get-social', play_next).length)
                            {
                                $('.js-put-social').html($('.js-get-social', play_next).html());
                            }

                            rePositionQueue();
                            scrollToItem(play_next);
                        }, 300);


                        var next_video = play_next.find('.video--container').detach();

                        if (next_video)
                        {

                            if (currently_playing.is('#queue_01'))
                            {
                                current_video.detach();
                            }
                            else
                            {
                                current_video.appendTo(currently_playing);
                            }

                            var next_video_player = next_video.find('.video--player');

                            if (currently_playing.next().length)
                            {
                                next_video.appendTo(currently_playing_container);

                                play_next.addClass('js-playing-next');
                                currently_playing.removeClass('js-is-playing is-active is-paused');

                                if (next_video_player)
                                {
                                    var play_next_video = next_video_player.get(0);

                                    var total_time = play_next_video.duration;
                                    var video_total = next_video.find('.video--total');
                                    var total_seconds = Math.floor(total_time);

                                    video_total.text(formatTime(total_seconds));

                                    setTimeout(function()
                                    {
                                        play_next.addClass('js-is-playing is-active').removeClass('js-playing-next');

                                        next_video_player.parents('.video--container').addClass('playing currently--playing first--play');

                                        play_next_video.play();
                                    }, 500);

                                }

                            }

                        }

                        // RESHOW VIDEO DETAILS
                        setTimeout(function()
                        {
                            $('.video-details-content').removeClass('temp-hide hide-all');
                        }, 700);

                    }



                    else
                    {
                        currently_playing.removeClass('is-active');
                    }

                }

            });

            // Bind time update to the video player element
            video_player.on('timeupdate', function()
            {
                var value = (100 / video.duration) * video.currentTime;

                var get_time = video.currentTime;
                var minutes = Math.floor(get_time / 60);
                var seconds = Math.floor(get_time);

                $data_view = $('*[data-name=video_view_count]', target).find('input');
                $stored = parseInt($data_view.val());

                if (seconds === 3 && !$data_view.hasClass('gotTime'))
                {
                    if (isNaN($stored) == true)
                    {
                        $data_view.attr('value', 1);
                    }
                    else
                    {
                        $data_view.attr('value', parseInt($stored + 1));
                    }

                    $update = $data_view.val(parseInt($stored + 1));

                    $data_view.addClass('gotTime');

                    event.preventDefault();

                    var form_data = {
                        'action': 'acf/validate_save_post'
                    };

                    $('form :input', target).each(function()
                    {
                        form_data[jQuery(this).attr('name')] = jQuery(this).val();
                    });

                    form_data.action = 'save_my_data';

                    jQuery.post(Globals.ajaxEndPoint, form_data)
                        .done(function(save_data) {});

                }

                video_current.text(formatTime(seconds));
                video_total.text(formatTime(Math.floor(video.duration)));

                seek_bar.find('.video--progress').css(
                {
                    'width': value + '%'
                });

            });

            video_player.bind('click', function()
            {
                if (video.paused == true)
                {
                    video.play();
                    target.addClass('playing currently--playing first--play').removeClass('currently--paused');
                    $('.js-is-playing').addClass('is-active').removeClass('is-paused');
                }
                else
                {
                    video.pause();
                    target.removeClass('playing currently--playing currently--paused');
                    $('.js-is-playing').removeClass('is-active').addClass('is-paused user--paused');
                }

            });

            // Event listener for the volume bar
            volume_bar.bind('change', function()
            {
                video.volume = volume_bar.attr('value');

                $(this).siblings('.video--volume__progress').css(
                {
                    'width': (volume_bar.attr('value') * 100) + '%'
                });


            });

        }

        $.each(video_container, function(event)
        {
            if (!$(this).hasClass('init'))
            {
                videoInitiate(this);
            }
        });


        /* ============================================================================

          INFINITE SCROLL AUTO PLAY FEATURED

        ============================================================================ */

        function formatTime(seconds)
        {

            minutes = Math.floor(seconds / 60);
            minutes = (minutes >= 10) ? minutes : '0' + minutes;

            seconds = Math.floor(seconds % 60);
            seconds = (seconds >= 10) ? seconds : '0' + seconds;

            return minutes + ':' + seconds;

        }

        function autoPlayFeatured(ths)
        {

            var vid = $(ths);

            var target = vid.find('.video--container');
            var auto_play = vid.find('video').get(0);

            var video_player = vid.find('.video--player');

            video_player.unbind('click');

            var video = auto_play;

            var total_time = video ? video.duration : 0;
            var total_seconds = Math.floor(total_time);

            if (!target.hasClass('first--play'))
            {
                target.find('.video--total').text(formatTime(total_seconds));
            }
            else if (isNaN(total_seconds))
            {
                var total_time = video.duration;
                var total_seconds = Math.floor(total_time);

                target.find('.video--total').text(formatTime(total_seconds));
            }

            if (target.visible(true))
            {

                if (!target.hasClass('user--paused'))
                {
                    video.play();
                    target.addClass('playing currently--playing first--play').removeClass('currently--paused');
                    $('.js-is-playing').addClass('is-active').removeClass('is-paused');
                }

            }
            else
            {

                video && video.pause();
                target.removeClass('playing currently--playing currently--paused');
                $('.js-is-playing').removeClass('is-active').addClass('is-paused');

            }

        }

        // AUTOPLAY ON LOAD
        if ($('.js-auto-play').length)
        {
            $('.js-auto-play').each(function(ths)
            {
                autoPlayFeatured(this);
            });
        }


        function featuredAreaAdjustment(ths)
        {

            var target = $(ths);
            var target_siblings = target.children();

            var target_siblings_count = target_siblings.length;

            if (target_siblings_count == 1)
            {
                target.removeClass('left--anchor').addClass('init');
            }

        }

        $('.featured--area.left--anchor').each(function()
        {
            featuredAreaAdjustment($(this));
        });


        /* ============================================================================

          INFINITE SCROLL

        ============================================================================ */

        var $nextPageLink = $('.get--content:last .jscroll-next-parent:last a');
        var nextPageURL = $nextPageLink.attr('href');
        var nextPageTitle = $nextPageLink.attr('data-title');

        function reInitJS()
        {

            $('.special--container').each(function()
            {
                if ($(this).hasClass('no-js-remove'))
                {
                    $(this).removeClass('no-js-remove');
                }
            });
            $('.get--content').each(function()
            {
                if ($(this).hasClass('no-js-initiate'))
                {
                    $(this).removeClass('no-js-initiate');
                }
            });
            $('.article--gallery').each(function()
            {
                if ($(this).hasClass('no-js-initiate'))
                {
                    $(this).removeClass('no-js-initiate');
                }
            });
            $('.selection--gallery').each(function()
            {
                if ($(this).hasClass('no-js-initiate'))
                {
                    $(this).removeClass('no-js-initiate');
                }
            });

            // if ( $('.promoted--series').length )
            // {
            //   home_series();
            // }

            if ($('.gallery--change').length || $('.article--gallery--inner').length)
            {

                $.each($('.article--gallery--inner'), function()
                {
                    if (!$(this).hasClass('init'))
                    {
                        slickCarousel(this);
                    }
                });

                $('.gallery--view--thumbs').unbind('click');
                $('.gallery--view--thumbs').on('click', showGridView);

                $('.gallery--change').unbind('click');
                $('.gallery--change').on('click', changeView);

                $('.article--gallery--grid--view').find('.grid--item > a').unbind('click');
                $('.article--gallery--grid--view').find('.grid--item > a').on('click', changeSlide);

            }

            if ($('.featured--area.left--anchor').length)
            {
                $('.featured--area.left--anchor').each(function()
                {
                    if (!$(this).hasClass('init'))
                    {
                        featuredAreaAdjustment(this);
                    }

                });

            }

            if ($('.video--container').length)
            {
                $.each($('.video--container'), function(event)
                {
                    if (!$(this).hasClass('init'))
                    {
                        videoInitiate(this);
                    }
                });

            }

            if ($('.selection--inner').length)
            {
                $.each($('.selection--inner'), function()
                {
                    if (!$(this).hasClass('init'))
                    {
                        vogueSelection(this);
                    }
                });

            }

            if ($('.related--tags').length)
            {
                $.each($('.related--tags'), function()
                {
                    if (!$(this).hasClass('init'))
                    {
                        relatedTags(this);
                    }
                });
            }



            if ($('.instagram--feed').length)
            {
                $('.instagram--feed').each(function()
                {
                    if (!$(this).hasClass('init'))
                    {
                        cycleInstagramFeed(this);
                    }
                });
            }

        }

        var animatingToContent = 0;
        var scrollableObject = getScrollableObject(window);
        var $scrollableObject = $(scrollableObject)
        function popStateHandler(e)
        {
            animatingToContent++;
            // console.log('raising', animatingToContent);

            var $contents = $('.scroll').find('.get--content');
            var $historyChangingContents = $contents.eq(0).add($contents.slice(1).filter('.js-change-history'));

            var $targetContent = $historyChangingContents.eq(VogueHistory.currentContentIndex);

            if ($targetContent.attr('data-url') == VogueHistory.currentURL)
            {
                var scrollTop = $targetContent.offset().top - 60;

                TweenMax.to(scrollableObject, 1, {scrollTop: scrollTop, ease: VogueHistory.scrollRestorationEnabled ? Cubic.easeInOut : Cubic.easeOut, onComplete: function()
                {

                    // var freeScrollNavigation = function() 
                    // {
                    //     clearTimeout(freeScrollNavigationTimeout);
                    //     $scrollableObject.off('scroll', freeScrollNavigation);

                    //     animatingToContent--;
                    //     console.log('lowering', animatingToContent);
                    // }

                    // var freeScrollNavigationTimeout = setTimeout(freeScrollNavigation, 400);
                    // $scrollableObject.on('scroll', freeScrollNavigation);

                    // Code above commented because this seems to be a better solution

                    requestAnimationFrame(function()
                    {
                        requestAnimationFrame(function()
                        {
                            animatingToContent--;
                            // console.log('lowering', animatingToContent);
                        });
                    });
                }});
            }
            else
            {
                VogueHistory.skipOne = true;
                location.href = VogueHistory.currentURL;
                // alert('going out!\r\n' + VogueHistory.currentContentIndex + '\r\n' + $targetContent.attr('data-url') + '\r\n' + VogueHistory.currentURL);
            }
        }
        VogueHistory.popStateListeners.push(popStateHandler);
        VogueHistory.init();

        var ticking = false;

        window.addEventListener('scroll', function()
        {
            if (!ticking)
            {
                window.requestAnimationFrame(function()
                {
                    scrollHandler();
                    ticking = false;
                });
            }

            ticking = true;
        });

        function scrollHandler()
        {
            // Bosco
            // if ( $('.js-auto-play').length ) { $('.js-auto-play').each(function( ths ) { autoPlayFeatured( this ); }); }

            loadNextPageIfNecessary();
            updateStuffOnScroll();

            if ($('.sb-gallery-item').length)
            {
                loadVogueCollectionListings();
            }

            echo.render();
        }

        var lastPageReached = false;
        var pageIndex = 0;
        var pageNumber = 1;

        var alreadyLoading = false;

        var $infiniteScrollIndicator = $('<div class="ajax-loading"></div>');

        function loadNextPageIfNecessary()
        {
            if (lastPageReached == false && $('.jscroll-next-parent').length)
            {
                if ($(window).scrollTop() + window.innerHeight > $(document).height() - 3000)
                {
                    loadNextPage();
                }
            }
        }

        function loadNextPage()
        {
            if (!alreadyLoading)
            {
                alreadyLoading = true;

                $.ajax(
                {
                    url: nextPageURL,
                    dataType: 'html',
                    cache: false,
                    success: function(data)
                    {
                        pageLoadedHandler(data);
                    },
                    complete: function()
                    {
                        alreadyLoading = false;
                    }
                });
            }
        }

        function pageLoadedHandler(responseString)
        {
            pageIndex++;
            pageNumber++;

            var $newContent = parseContent(responseString);

            if ($newContent && $newContent.length)
            {
                $('.scroll').append($newContent);

                $newContent.attr('data-url', nextPageURL);
                $newContent.attr('data-title', nextPageTitle);

                var $nextPageLink = $newContent.find('.jscroll-next-parent:last a');
                nextPageURL = $nextPageLink.attr('href');
                nextPageTitle = $nextPageLink.attr('data-title');

                // Check if the last page was reached
                if
                (
                    !$('body').hasClass('category')
                    &&
                    parseInt($newContent.attr('data-page')) >= parseInt($newContent.attr('data-max'))
                )
                {
                    lastPageReached = true;
                    $infiniteScrollIndicator.remove();
                }
                else
                {
                    $('.scroll').append($infiniteScrollIndicator);
                }

                initializeNewContent($newContent);
            }
            else
            {
                lastPageReached = true;
                $infiniteScrollIndicator.remove();
            }
        }

        function parseContent(htmlString)
        {
            var useNewBetterMethod = true;

            var $newContent = null;

            if (useNewBetterMethod)
            {
                var matches = htmlString.match( /<!-- PAGE CONTENT START.*?-->\s*([\s\S]*?)\s*<!-- PAGE CONTENT END.*?-->/)

                if (matches && matches[1])
                {
                    $newContent = $(matches[1]);
                }
            }
            else
            {
                $newContent = $(htmlString).find('.get--content');
            }

            return $newContent;
        }

        function initializeNewContent($newContent)
        {
            // Set unique IDs and other stuff
            $newContent.attr('data-page-num', pageNumber);
            $newContent.attr('id', 'main_youcantblock' + pageNumber);

			if (document.documentElement.lang == 'en-GB') {
            	$newContent.attr('data-desktop-page-id', "537066834");
            	$newContent.attr('data-mobile-page-id', "537066851");
			} else {
				$newContent.attr('data-desktop-page-id', "537066850");
            	$newContent.attr('data-mobile-page-id', "537066852");
			}

            $newContent.find("#my-div").attr('id', 'my-div' + pageNumber);

            $newContent.find("#second-popup").attr('id', 'second-popup' + pageNumber);

            $newContent.find(".openBoxButton").attr('data-mfp-src', '#my-div' + pageNumber);
            $newContent.find(".openBoxButton").attr('href', '#my-div' + pageNumber);

            $newContent.find(".second-popup-link").attr('href', '#second-popup' + pageNumber);
            $newContent.find(".second-popup-link").attr('data-mfp-src', '#second-popup' + pageNumber);

            $newContent.find(".sliderInLightboxw").attr('class', 'sliderInLightboxw' + pageNumber);

            // Init new Videos
            $('.video--container').not('.init').each(function()
            {
                videoInitiate(this);
            });

            // Autoplay videos
            $('.js-auto-play').each(function() 
            { 
                autoPlayFeatured(this); 
            }); 

            // Initialize article content
            initializeArticle();

            // Gather new Fade In elements
            FadeIn.gather();
            FadeIn.check();

            // Initialize newsletter breaks
            Newsletter.parseNewsletterPagebreaks();

            // Add new stuff to the Lazy Loading
            echo.init(
            {
                offset: 400,
                throttle: 250,
                unload: false,
            });
            echo.render();

            // Do this thing
            reInitJS();
        }

        var refreshURLAnimationFrame;

        function updateStuffOnScroll()
        {
            // If there is paging at all
            if ($('.jscroll-next-parent').length)
            {
                // Grab all existing content blocks
                var $contents = $('.scroll').find('.get--content');

                if (window.OX !== undefined)
                {
                    $contents.slice(1).each(function(i)
                    {
                        var $content = $(this);

                        if (!$content.hasClass('ad-loaded') && $content.visible(true))
                        {
                            window.infads = (typeof window.infads == 'undefined') ? [] : window.infads;
							var container = $content.find('[id^="main_youcantblock"]').addBack('[id^="main_youcantblock"]');
                            var IERV = new OX_responsive(jQuery(container).attr('id'));
                            IERV.setup();
                            IERV.addPage();
                            IERV.setAdSlots();
                            IERV.load();
                            $(this).addClass('ad-loaded');
                            window.infads.push(IERV);
                            //var date = new Date();
                            //if (date.getHours() >= 18 || date.getHours() <= 7 || window.mobilecheck()) { setInterval('window.infads.forEach(function(obj){obj.load();}) ', 70000); }
                        }
                    });
                }

                if (!animatingToContent)
                {
                    refreshURLAnimationFrame || cancelAnimationFrame(refreshURLAnimationFrame);

                    refreshURLAnimationFrame = requestAnimationFrame(function()
                    {
                        refreshURLAnimationFrame = null;

                        if (!animatingToContent)
                        {
                            var scrollTop = $(window).scrollTop();

                            var newContentIndex;
                            var newURLIndex;
                            var newTitle;

                            var $historyChangingContents = $contents.eq(0).add($contents.slice(1).filter('.js-change-history'));
                            $historyChangingContents.each(function(i)
                            {
                                var $content = $(this);

                                if
                                (
                                    !i
                                    ||
                                    $content.offset().top - scrollTop < 60
                                )
                                {
                                    newContentIndex = i;
                                    newURL = $content.attr('data-url');
                                    newTitle = $content.attr('data-title');
                                }
                            });

                            VogueHistory.scrolledToPage(newContentIndex, newTitle, newURL);
                        }
                    });
                }
            }
        }

        if ($('.jscroll-next-parent').length)
        $('.scroll').append($infiniteScrollIndicator);

        function loadOnClick(event)
        {
            var $clickedElement = $(this);

            if (!$clickedElement.data('doing'))
            {
                var URL = $clickedElement.attr('data-url');
                var title = $clickedElement.attr('data-title');

                $clickedElement.data('doing', true);

                $.ajax(
                {
                    url: URL,
                    dataType: 'html',
                    cache: false,
                    success: function(data)
                    {
                        loadOnClickSuccessHandler(data, URL);
                    },
                    complete: function()
                    {
                        $clickedElement.remove();
                    }
                });

                $clickedElement.css('pointer-events', 'none');
                $clickedElement.css('opacity', .5);
            }

            event.preventDefault();
            return false;
        }

        function loadOnClickSuccessHandler(responseString, url)
        {
            var $newContent = parseContent(responseString);

            if ($newContent && $newContent.length)
            {
                $('.scroll').append($newContent);

                $newContent.addClass('loaded');
                $newContent.attr('data-url', url);

                var endReached = $newContent.attr('data-end-reached');

                if (typeof endReached !== typeof undefined && endReached !== false)
                {
                    $newContent.find('.js-load-more').remove();
                }
                else
                {
                    addLoadMoreClickEvents();
                }

                FadeIn.gather();
                FadeIn.check();
            }
        }

        function addLoadMoreClickEvents()
        {
            $('.js-load-more').each(function()
            {
                var $element = $(this);
                $element.off('click', loadOnClick);
                $element.on('click', loadOnClick);
            });
        }

        addLoadMoreClickEvents();

        // function sizeSpecialContainer( ths )
        // {

        //   container = $( ths );
        //   ad = container.find('iframe');

        //   $(document).on('DOMNodeInserted', function(e) {

        //     container.each(function()
        //     {
        //       if (container.find('iframe') && !container.find('iframe').hasClass('added')) {

        //         container.find('iframe').addClass('added');
        //         console.log('added');
        //          //element with #someID was inserted.
        //       }
        //     });
        //   });

        //   if ( ad.length )
        //   {
        //     console.log('this');
        //     height = ad.outerHeight(true);
        //     console.log(height);
        //     container.height(height);
        //   }

        // }

        // $( '.special--container' ).each(function()
        // {

        //   sizeSpecialContainer( $( this ) );

        // });






        /* ============================================================================

          STORE COOKIE IDS

        ============================================================================ */

        // function createCookie( name, value, days )
        // {

        //   var expires;

        //   if ( days )
        //   {
        //     var date = new Date();
        //     date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        //     expires = "; expires=" + date.toGMTString();
        //   }
        //   else
        //   {
        //     expires = "";
        //   }

        //   document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/"
        // }

        // function readCookie( name )
        // {

        //   var nameEQ = encodeURIComponent(name) + "=";
        //   var ca = document.cookie.split(';');

        //   for ( var i = 0; i < ca.length; i++ )
        //   {
        //     var c = ca[i];

        //     while ( c.charAt( 0 ) === ' ' ) c = c.substring( 1, c.length );

        //     if ( c.indexOf( nameEQ ) === 0) return decodeURIComponent( c.substring( nameEQ.length, c.length ) );

        //   }

        //   return null;
        // }

        // function eraseCookie( name )
        // {
        //   createCookie( name, "", -1 );
        // }

        // function articleCookieExist (json, modulid) {
        //     var ret = 0;
        //     $(json).each(function(index, data){
        //         if(data.modulId == modulid)
        //             ret++;
        //     })
        //     return ret > 0;
        // }

        // function articleCookies()
        // {

        //   $( '.get--content' ).each( function()
        //   {

        //     if ( $( this ).attr( 'data-set_id' ) && !$( this ).hasClass('checked') )
        //     {

        //       $current = $( this ).attr( 'data-set_id' );

        //       if ( readCookie( 'article_read' ) )
        //       {

        //         $ids = JSON.parse( readCookie( 'article_read' ) );

        //         $found = $ids.indexOf( $current );

        //         console.log($found);

        //         if ( $found < 0 )
        //         {
        //           $ids.push( $current );
        //           document.cookie = 'article_read=' + JSON.stringify( $ids );

        //           $( this ).addClass('checked');
        //         }

        //         // console.log($found);

        //         // if ( $ids.indexOf( $current ) )
        //         // {
        //         //   // found it
        //         //   console.log('found');
        //         // }
        //         // else
        //         // {
        //         //   console.log($ids);
        //         //   $ids.push( $current );
        //         //   console.log($ids);
        //         //   // document.cookie = 'article_read=' + JSON.stringify( $ids ) + '; expires=' + 24;
        //         // }

        //       }
        //       else
        //       {
        //         $ids = [];
        //         $ids.push( $current );

        //         document.cookie = 'article_read=' + JSON.stringify( $ids );
        //       }

        //     }

        //   });

        // }

        // articleCookies();



        /*

                $( '.scroll' ).jscroll(
                {
                    'loadingHtml' : '<small>Loading . . .</small>',
                    'padding' : 100,
                    'contentSelector' : ajax_get_content_class,
                    'nextSelector' : '.jscroll-next-parent > a',
                    // 'autoTrigger': $( this ).attr( 'data-continue' ),

                    callback: function()
                    {

                        currentPage++;

                        var target = $( '.jscroll-added' ).last();
                        var targetTitle = target.find( ajax_get_content_class ).attr( 'data-title' );

                        target.attr( 'data-url', nextPageHREF ).prev().addClass( 'previous loaded' ).prev().removeClass( 'previous' );
        // console.log(nextPageHREF);
                        history.pushState( {'state':nextPageHREF}, $( 'head > title' ).html( targetTitle ), nextPageHREF );

                        if ( $( '.gallery--change' ).length || $( '.article--gallery--inner' ).length )
                        {

                            $.each( $( '.article--gallery--inner' ), function()
                            {
                                if ( !$( this ).hasClass( 'init' ) ) { slickCarousel( this ); }
                            });

                            $( '.gallery--view--thumbs' ).unbind( 'click' );
                            $( '.gallery--view--thumbs' ).on( 'click', showGridView );

                            $( '.gallery--change' ).unbind( 'click' );
                            $( '.gallery--change' ).on( 'click', changeView );

                            $( '.article--gallery--grid--view' ).find( '.grid--item > a' ).unbind( 'click' );
                            $( '.article--gallery--grid--view' ).find( '.grid--item > a' ).on( 'click', changeSlide );

                        }

                        if ( $( '.video--container' ).length )
                        {
                            $.each( $( '.video--container' ), function( event )
                            {
                                if ( !$( this ).hasClass( 'init' ) ) { videoInitiate( this ); }
                            });

                        }

                        if ( $( '.selection--inner' ).length )
                        {
                            $.each( $( '.selection--inner' ), function()
                            {
                                if ( !$( this ).hasClass( 'init' ) ) { vogueSelection( this ); }
                            });

                        }

                        if ( $( '.related--tags' ).length )
                        {
                            $.each( $( '.related--tags' ), function()
                            {
                                if ( !$( this ).hasClass( 'init' ) ) { relatedTags( this ); }
                            });
                        }



                        if ( $( '.instagram--feed' ).length )
                        {
                            $( '.instagram--feed' ).each( function()
                            {
                                if ( !$( this ).hasClass( 'init' ) ) { cycleInstagramFeed( this ); }
                            });
                        }

                        // Animations
                        var elements = target.find('.get--content > .section--break, .container .post--item, .special--container');
                        elements.css({opacity: 0});

                        TweenMax.staggerTo(
                            elements,
                           0.5,
                            {
                                alpha: 1
                            },
                            0.2,
                            function() {
                                elements.css({opacity: ''});
                            }
                        );

                        return false;
                    }

                });*/

    });

    /* ============================================================================

        Swiper

    ============================================================================ */

    $('.video-slider .swiper-container').each(function(index)
    {
        $(this).addClass('s-' + index);
        $(this).parent().find('.swiper-button').addClass('s-btn-' + index);
        var mySwiper = new Swiper('.s-' + index,
        {
            direction: 'horizontal',
            autoPlay: 3000,
            nextButton: '.swiper-button-next.s-btn-' + index,
            prevButton: '.swiper-button-prev.s-btn-' + index,
            pagination: '.swiper-pagination',
            slidesPerView: 3,
            spaceBetween: 30,
            paginationType: 'bullets',
            paginationHide: false,
            paginationClickable: true,
            breakpoints:
            {
                400:
                {
                    slidesPerView: 1,
                },
                770:
                {
                    slidesPerView: 2,
                }
            }
        })
    });

    $('.sb .swiper-container').each(function(index)
    {
        var columns = $(this).data('columns');
        var columnsMobile = $(this).data('columns-mobile');
        $(this).addClass('s-' + index);
        $(this).addClass('s-columns-' + columns);
        $(this).parent().find('.swiper-button').addClass('s-btn-' + index);
        var mySwiper = new Swiper('.s-' + index,
        {
            direction: 'horizontal',
            autoPlay: 3000,
            nextButton: '.swiper-button-next.s-btn-' + index,
            prevButton: '.swiper-button-prev.s-btn-' + index,
            slidesPerView: columns,
            centeredSlides: true,
            loop: true,
            lazyLoading: true,
            lazyLoadingInPrevNextAmount: 1,
            preloadImages: false,
            breakpoints:
            {
                770:
                {
                    slidesPerView: columnsMobile,
                }
            }
        })
    });


    /* ============================================================================

      VOGUE COLLECTION FILTER (STYLEBASE)

    ============================================================================ */

    function loadVogueCollectionListings()
    {
        if ($('.sb-gallery-item').length)
        {

            $('.sb-gallery-item').each(function()
            {
                if ($(this).visible(true))
                {
                    TweenMax.staggerTo(
                        $(this),
                        0.5,
                        {
                            alpha: 1
                        },
                        0.2,
                        function()
                        {
                            $(this).css(
                            {
                                opacity: ''
                            });
                        }
                    );
                }
            });

        }

    }

    loadVogueCollectionListings();

    if ($('.sb-listings-pagination').length && $(window).outerWidth() <= 630)
    {

        var elementPosition = $('.sb-listings').offset();

        $(window).on('scroll', function()
        {
            if ($(window).scrollTop() > elementPosition.top)
            {
                $('.sb-listings-pagination').addClass('is-fixed');
            }
            else
            {
                $('.sb-listings-pagination').removeClass('is-fixed');
            }
        });

    }

    function checkFilterExists()
    {

        var alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        $.each(alphabet, function(index, value)
        {

            if (!$('#' + value).length)
            {
                $('.sb-listings-pagination').find('[data-filter=' + value + ']').parents('li').addClass('not-filterable');
            }


            if ($(window).outerWidth(true) <= 630)
            {
                $('#' + value).remove()
            }

        });

    }

    if ($('.sb-listings-pagination').length)
    {
        checkFilterExists();
    }

    function filterVogueCollection($filter, event)
    {

        if ($filter == 'all')
        {
            $('.sb-listings-pagination').find('li').not('.all').removeClass('filter-active');

            $('.sb-gallery-item').each(function()
            {
                $(this).attr('data-filtered', false).addClass('hidden').detach().appendTo('#temp-storage');
            });

            $('.sb-gallery-item', '#temp-storage').sort(function(a, b)
                {
                    return ($(a).attr('data-to_filter') > $(b).attr('data-to_filter') ? 1 : -1);
                })
                .each(function()
                {

                    if (!$(this).attr('id'))
                    {
                        var value = $(this).attr('data-to_filter');
                        $(this).insertAfter($('#' + String(value)));
                    }

                    $(this).attr('data-filtered', true).removeClass('hidden');

                    $('#temp-storage').find($(this)).detach().appendTo('.sb-gallery--listings');

                    if ($(this).visible(true))
                    {
                        TweenMax.staggerTo(
                            $(this),
                            0.5,
                            {
                                alpha: 1
                            },
                            0.2,
                            function()
                            {
                                $(this).css(
                                {
                                    opacity: ''
                                });
                            }
                        );

                    }

                });

            $('.sb-gallery-item').sort(function(a, b)
            {
                return ($(a).attr('data-to_filter') > $(b).attr('data-to_filter') ? 1 : -1);
            }).each(function()
            {

                if (!$(this).attr('id'))
                {
                    var value = $(this).attr('data-to_filter');
                    $(this).insertAfter($('#' + String(value)));
                }

            });

        }
        else
        {
            $('.sb-listings-pagination').find('[data-filter=' + $filter + ']').parents('li').addClass('filter-active');
            $('.sb-listings-pagination').find('[data-filter=' + $filter + ']').parents('li').siblings().removeClass('filter-active');

            $('#temp-storage').find('[data-to_filter=' + $filter + ']').each(function()
            {

                $(this).detach().appendTo('.sb-gallery--listings').attr('data-filtered', true).removeClass('hidden');

                if (!$(this).attr('id'))
                {
                    $(this).insertAfter('#' + $(this).attr('data-to_filter'));
                }

            });

            $('.sb-gallery--listings').find('.sb-gallery-item').not('[data-to_filter=' + $filter + ']').attr('data-filtered', false).each(function()
            {
                $(this).addClass('hidden').detach().appendTo('#temp-storage');

                if (!$(this).attr('id'))
                {
                    $(this).insertAfter('#' + $(this).attr('data-to_filter'));
                }

                $(this).sort(function(a, b)
                {
                    return ($(a).attr('data-to_filter') > $(b).attr('data-to_filter') ? 1 : -1);
                });
            });

            $('.sb-gallery--listings').find('.sb-gallery-item').each(function()
            {
                if ($(this).visible(true))
                {
                    TweenMax.staggerTo(
                        $(this),
                        0.5,
                        {
                            alpha: 1
                        },
                        0.2,
                        function()
                        {
                            $(this).css(
                            {
                                opacity: ''
                            });
                        }
                    );
                }
            });

        }

        event.preventDefault();

    }

    $('[data-filter]', '.sb-listings').on('click', function(event)
    {
        if ($(this).attr('data-filter') != 'false')
        {
            filterVogueCollection($(this).attr('data-filter'), event);
        }
    });


    /* ============================================================================

        Show more Stylebase profile

    ============================================================================ */

    function stylebaseShowMore()
    {

        var btn = $('.js-sb-info__more');
        var btnLess = btn.find('.js-less');
        var btnMore = btn.find('.js-more');
        var btnArrow = btn.find('.js-icon');

        var text = $('.js-sb-info__text');
        var textTop = text.offset().top

        var tl = new TimelineLite(
        {
            paused: true
        });

        tl
            .to(btnMore, 0.25,
            {
                alpha: 0,
                ease: Expo.easeOut
            })
            .set(btnMore,
            {
                display: "none"
            })
            .set(btnLess,
            {
                display: "inline"
            })
            .from(btnLess, 0.25,
            {
                alpha: 0,
                ease: Expo.easeOut
            })
            .to(btnArrow, 0.5,
            {
                rotation: 180,
                ease: Expo.easeInOut
            }, "-=0.5");

        TweenMax.set(btnLess,
        {
            display: "none"
        });
        TweenMax.set(text,
        {
            height: 0
        });

        function open()
        {

            tl.play();

            TweenMax.set(text,
            {
                height: "auto"
            })
            TweenMax.from(text, 0.5,
            {
                height: 0,
                ease: Expo.easeInOut
            })

            TweenMax.to(window, 0.5,
            {
                scrollTo: textTop - 100,
                ease: Expo.easeInOut
            });
        }

        function close()
        {

            tl.reverse();

            TweenMax.to(window, 0.5,
            {
                scrollTo: textTop - 100,
                ease: Expo.easeInOut
            });

            TweenMax.to(text, 0.5,
            {
                height: 0,
                ease: Expo.easeInOut
            });
        }

        function toggle()
        {

            btn.click(function()
            {
                var el = $(this);
                if (el.hasClass('is-active'))
                {
                    el.removeClass('is-active');

                    close();
                }
                else
                {
                    el.addClass('is-active');

                    open();
                }
            });
        }

        function init()
        {

            toggle();
        }

        init();
    }

    if ($('.js-sb-info__more').length > 0)
    {
        stylebaseShowMore();
    }


    function updatePageViewCount()
    {

        var target = $('.get--content:last');

        $data_view = $('*[data-name=vogue_view_count]', target).find('input');

        if ($data_view.length)
        {

            $stored = parseInt($data_view.val());

            if (isNaN($stored) == true)
            {
                $data_view.attr('value', 1);
            }
            else
            {
                $data_view.attr('value', parseInt($stored + 1));
            }

            $update = $data_view.val(parseInt($stored + 1));

            if (!$data_view.hasClass('gotView'))
            {

                $data_view.addClass('gotView');

                // event.preventDefault();
                setTimeout(function()
                {

                    var form_data = {
                        'action': 'acf/validate_save_post'
                    };

                    $('form :input', target).each(function()
                    {
                        form_data[jQuery(this).attr('name')] = jQuery(this).val();
                    });

                    form_data.action = 'save_my_data';

                    jQuery.post(Globals.ajaxEndPoint, form_data)
                        .done(function(save_data) {});

                }, 100);

            }

        }

    }

    // if ( $('.update_page_view_count').length )
    // {
    updatePageViewCount();

    $(document).ajaxStart(updatePageViewCount);
    // }

    /* ============================================================================

        Side navigation

    ============================================================================ */

    function sideNav()
    {

        $('#menu-side-menu').find('.menu-item-has-children').each(function()
        {
            $(this).addClass('js-has-sub-menu').append('<div class="js-toggle-sub-menu"></div>').bind('click');
            $(this).find('.sub-menu').addClass('js-sub-menu').css(
            {
                'height': 0
            });
        });

        var nav = $('.js-side-nav');
        var open = $('.js-toggle-side-nav');
        var close = $('.js-close-side-nav');
        var mask = $('.js-mask-menu');
        var toggle = $('.js-toggle-sub-menu');
        var submenus = $('.js-sub-menu');
        var search = $('.js-search--area');
        var searchSide = $('.js-toggle-search--side-nav');
        var searchHead = $('.js-toggle-search--site-head');
        var logo = $('.js-top-tranding--home');
        var searchMain = $('.js-search--area');
        var maskSearch = $('.js-mask-search');
        var tl = new TimelineLite(
        {
            paused: true,
            onReverseComplete: setState
        });

        if ($('html').attr('dir') == 'rtl' || window.matchMedia("(max-width: 770px)").matches)
        {
            tl
                .set(nav,
                {
                    autoAlpha: 1
                })
                .from(nav, 0.5,
                {
                    xPercent: 100,
                    ease: Power4.easeInOut
                })
                .to(mask, 0.5,
                {
                    autoAlpha: 1,
                    ease: Power4.easeInOut
                }, "-=0.5");
        }
        else
        {
            tl
                .set(nav,
                {
                    autoAlpha: 1
                })
                .from(nav, 0.5,
                {
                    xPercent: -100,
                    ease: Power4.easeInOut
                })
                .to(mask, 0.5,
                {
                    autoAlpha: 1,
                    ease: Power4.easeInOut
                }, "-=0.5");
        }

        function setState()
        {
            toggle.removeClass('is-active');

            TweenMax.set(submenus,
            {
                height: 0
            });
        }

        function closeActiveSubMenus(el)
        {
            var activeButtons = el.parent().siblings().find('.is-active');
            var activeMenus = activeButtons.parent().find('.js-sub-menu');

            activeButtons.removeClass('is-active');
            TweenMax.to(activeMenus, 0.35,
            {
                height: 0,
                ease: Power3.easeInOut
            });
        }

        function closeSubMenu(el, submenu)
        {
            el.removeClass('is-active');

            TweenMax.to(submenu, 0.35,
            {
                height: 0,
                ease: Power3.easeInOut
            });
        }

        function openSubMenu(el, submenu)
        {
            el.addClass('is-active');

            TweenMax.set(submenu,
            {
                height: "auto"
            });
            TweenMax.from(submenu, 0.35,
            {
                height: 0,
                ease: Power3.easeInOut
            });
        }

        open.click(function()
        {
            $('body').addClass('overflow');
            nav.addClass('is-active');
            tl.play();

            if (search.hasClass('is-active'))
            {
                search.removeClass('is-active');
                maskSearch.removeClass('is-active');
                searchHead.removeClass('is-active');
            }

            //$(".js-toggle-search--site-head").click();

            if (logo.hasClass('is-hidden'))
            {
                //logo.removeClass('is-hidden');
                //TweenMax.to(logo, 0.25, { alpha: 1, ease: Expo.easeOut });
            }
        });

        close.click(function()
        {
            $('body').removeClass('overflow');
            nav.removeClass('is-active');
            tl.reverse();
        });

        mask.click(function()
        {
            $('body').removeClass('overflow');
            tl.reverse();
        });

        searchSide.click(function()
        {

            var el = $(this);

            nav.removeClass('is-active');
            searchHead.addClass('is-active');
            tl.reverse();
            //logo.addClass('is-hidden');
            //TweenMax.to(logo, 0.25, { alpha: 0, ease: Expo.easeOut });
        });

        $(document).on('keyup', function(e)
        {
            if ((e.keyCode === 27) && (search.hasClass('is-active')))
            {
                searchHead.click();
            }
            else if ((e.keyCode === 27) && (nav.hasClass('is-active')))
            {
                close.click();
            }
        });

        searchHead.click(function()
        {
            var el = $(this);

            if ($('body').hasClass('is-search'))
            {
                el.removeClass('is-active');
                $('body').removeClass('overflow');
            }
            else
            {
                el.addClass('is-active');
                $('body').addClass('overflow');
            }
        })

        maskSearch.click(function()
        {
            $('body').removeClass('overflow');
            searchHead.removeClass('is-active');
            if (logo.hasClass('is-hidden'))
            {
                logo.removeClass('is-hidden');
                TweenMax.to(logo, 0.25,
                {
                    alpha: 1,
                    ease: Expo.easeOut
                });
            }
        });

        toggle.each(function()
        {
            var el = $(this);
            var submenu = el.parent().find('.js-sub-menu');

            el.click(function(e)
            {
                e.preventDefault();

                closeActiveSubMenus(el);

                if (el.hasClass('is-active'))
                {
                    closeSubMenu(el, submenu);
                }
                else
                {
                    openSubMenu(el, submenu);
                }
            });
        });

        setState();

    }

    sideNav();

    /* ============================================================================

        Header scroll

    ============================================================================ */

    function headerScroll()
    {

        var head = $('.site-head');

        $(window).on('scroll', function()
        {

            var scroll = window.scrollY;

            if (scroll > 150)
            {
                head.addClass('is-scrolled');
            }
            else
            {
                head.removeClass('is-scrolled');
            }
        });
    }

    headerScroll();


    /* ============================================================================

        Head social icons

    ============================================================================ */

    function headSocialIcons()
    {
        // Selectors
        var container = $('.js-site-head-social');
        var title = $('.js-site-head-social-title');
        var inner = $('.js-site-head-social-inner');
        var lines = $('.js-site-head-social-lines');
        var arrow = $('.js-site-head-social-title-arrow');

        function headSocialIconsToggle(e)
        {
            if (e.type == 'mouseleave' /*container.hasClass('is-active')*/ )
            {
                // Before animation
                inner.css(
                {
                    display: 'none'
                });
                var toHeight = container.height();
                inner.css(
                {
                    display: 'block'
                });

                // Animation
                var tl = new TimelineLite();
                tl.pause();

                tl.to(
                    container,
                    0.4,
                    {
                        height: toHeight,
                        ease: Power3.easeInOut
                    },
                    0
                );

                tl.to(
                    lines,
                    0.4,
                    {
                        height: 0,
                        ease: Power3.easeInOut
                    },
                    0
                );

                tl.to(
                    arrow,
                    0.3,
                    {
                        alpha: 0,
                        y: -3,
                        ease: Power3.easeIn
                    },
                    0
                );

                tl.call(function()
                {
                    container.css(
                    {
                        height: ''
                    });
                    lines.css(
                    {
                        height: ''
                    });
                    inner.css(
                    {
                        display: ''
                    });

                    container.removeClass('is-active');
                });

                tl.to(
                    arrow,
                    0.3,
                    {
                        alpha: 1,
                        y: 0,
                        ease: Power3.easeOut
                    }
                );

                tl.call(function()
                {
                    arrow.css(
                    {
                        transform: '',
                        opacity: ''
                    });
                });

                tl.play();
            }
            else
            {
                // Before animation
                var fromHeight = container.height();
                inner.css(
                {
                    display: 'block'
                });
                var toHeight = container.height();
                container.css(
                {
                    height: fromHeight
                });

                // Animation
                var tl = new TimelineLite();
                tl.pause();

                tl.to(
                    container,
                    0.4,
                    {
                        height: toHeight,
                        ease: Power3.easeInOut
                    },
                    0
                );

                tl.to(
                    lines,
                    0.4,
                    {
                        height: toHeight,
                        ease: Power3.easeInOut
                    },
                    0
                );

                tl.to(
                    arrow,
                    0.3,
                    {
                        alpha: 0,
                        y: 3,
                        ease: Power3.easeIn
                    },
                    0
                );

                tl.call(function()
                {
                    container.css(
                    {
                        height: ''
                    });
                    lines.css(
                    {
                        height: ''
                    });
                    inner.css(
                    {
                        display: ''
                    });

                    container.addClass('is-active');
                });

                tl.to(
                    arrow,
                    0.3,
                    {
                        alpha: 1,
                        y: 0,
                        ease: Power3.easeOut
                    }
                );

                tl.call(function()
                {
                    arrow.css(
                    {
                        transform: '',
                        opacity: ''
                    });
                });

                tl.play();
            }
        }

        container.on('mouseenter mouseleave', headSocialIconsToggle);
    }

    headSocialIcons();


    /* ============================================================================

        Article inline images share links

    ============================================================================ */

    function initializeArticle()
    {
        $('.js-the-article-content:not(.initialized)').each(function()
        {
            // Selectors
            var $article = $(this);
            $article.addClass('initialized');

            var $images = $article.find('img');

            // Add the auto-fade-in class to those who don't belong in a gallery
            var $galleryImages = $article.find('.article--gallery .article--gallery--inner .gallery--item:not(:first-of-type) img');
            var $galleryThumbnails = $article.find('.article--gallery .article--gallery--grid--view img');
            var $nonGalleryImages = $images.not($galleryImages).not($galleryThumbnails);
            $nonGalleryImages.addClass('auto-fade-in');

            $images.each(function()
            {
                // Get image or parent link
                var $image = $(this);

                var parent = $image.parent();
                if ($image.closest('a').length != 0)
                    $image = $image.closest('a');

                if (!$image.parents('.grid--item').length)
                {
                    var src;
                    var srcfull;

                    // Get image source
                    if ($image.is('img'))
                    {
                        src = ($image.attr('src') || $image.attr('srcset'));
                        srcfull = ($image.attr('data-src') || $image.attr('srcset'));
                    }
                    else if ($image.is('a'))
                    {
                        src = $image.attr('href');
                        srcfull = $image.attr('data-src');
                    }
                    src = encodeURI(src);

                    // Set content
                    var wrapper = $(window.vgar_share_image);

                    wrapper.find('a').each(function()
                    {
                        var link = $(this);
                        var href = link.attr('href').replace('IMGSRC', src);
                        link.attr('href', href);
                        if (link.attr('data-src'))
                            link.attr('data-src', link.attr('data-src').replace('IMGSRCFULL', srcfull));
                    });

                    if (parent.hasClass('item--image'))
                    {
                        parent.addClass('post-share-image');
                        parent.append(wrapper.find('ul'));
                    }
                    else
                    {
                        // Set classes
                        if ($image.hasClass('alignnone'))
                            wrapper.addClass('alignnone');
                        if ($image.hasClass('aligncenter'))
                            wrapper.addClass('aligncenter');
                        if ($image.hasClass('alignleft'))
                            wrapper.addClass('alignleft');
                        if ($image.hasClass('alignright'))
                            wrapper.addClass('alignright');

                        // Prepend image inside wrapper
                        wrapper.prepend($image.clone());

                        // Replace image by wrapper
                        $image.replaceWith(wrapper);
                    }

                }

            });

        });

        FadeIn.gather();
    }

    initializeArticle();


    /* ============================================================================

        Zoom

    ============================================================================ */

    function zoom()
    {

        var item = $('.js-has-zoom');
        var container = $('.js-zoom');
        var inner = container.find('.js-zoom__inner');
        var close = container.find('.js-zoom__close');

        var tl = new TimelineLite(
        {
            paused: true,
            onReverseComplete: killZoom
        })

        tl
            .set('body',
            {
                overflow: "hidden"
            })
            .to(container, 0.25,
            {
                autoAlpha: 1,
                ease: Expo.easeOut
            })

        function createImage(el, e)
        {

            var src = el.data('src');

            var img = new Image();

            img.onload = function()
            {
                inner.append(img);
                moveImage(e);
            }

            img.src = src;
        }

        function killZoom()
        {

            inner.empty();
        }

        function moveImage(e)
        {

            var img = inner.find('img');

            var imgHeight = img.height();
            var imgWidth = img.width();

            var winHeight = $(window).height();
            var winWidth = $(window).width();

            var height = imgHeight - winHeight;
            var width = imgWidth - winWidth;

            var current = {
                x: 0,
                y: 0
            };

            if ((e.clientX) && (e.clientY))
            {

                current.x = e.clientX;
                current.y = e.clientY;

            }
            else if (e.targetTouches)
            {

                current.x = e.targetTouches[0].clientX;
                current.y = e.targetTouches[0].clientY;

                e.preventDefault();
            }

            var posY = (height / winHeight) * current.y;
            var posX = (width / winWidth) * current.x;

            TweenMax.set(img,
            {
                y: -posY,
                x: -posX
            });

            function event(e)
            {

                if ((e.clientX) && (e.clientY))
                {

                    current.x = e.clientX;
                    current.y = e.clientY;

                }
                else if (e.targetTouches)
                {

                    current.x = e.targetTouches[0].clientX;
                    current.y = e.targetTouches[0].clientY;

                    e.preventDefault();
                }

                posY = (height / winHeight) * current.y;
                posX = (width / winWidth) * current.x;

                TweenMax.to(img, 1,
                {
                    y: -posY,
                    x: -posX,
                    ease: Power4.easeOut
                });
            }

            inner.on('mousemove', function(e)
            {
                event(e);
            });

            inner.on('touchmove', function(e)
            {
                event(e);
            });

            inner.on('touchstart', function(e)
            {
                event(e);
            });
        }

        function init()
        {

            item.each(function()
            {

                var el = $(this);

                el.click(function(e)
                {
                    tl.play();

                    createImage(el, e);
                });
            });

            close.click(function()
            {

                tl.reverse();
            });

            inner.click(function()
            {

                tl.reverse();
            });
        }

        init();
    }

    if ($('.js-has-zoom').length > 0)
    {
        zoom();
    }


    /* ============================================================================

        Lightbox

    ============================================================================ */

    $('body').on('click', '.js-has-lightbox', function(e)
    {
        e.preventDefault();

        var button = $(this);
        var src = button.attr('data-src');
        if (!src)
            return;

        // Set close
        var close = $('<div class="va-lightbox-close"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="-268.678 380.822 16 16"><path d="M-252.678 382.277l-1.454-1.455-6.546 6.546-6.545-6.546-1.455 1.455 6.546 6.545-6.546 6.546 1.455 1.454 6.545-6.545 6.546 6.545 1.454-1.454-6.545-6.546"/></svg></div>');

        // Set caption
        /*var galleryItem = button.closest('.gallery--item');
        var caption = $('<div class="va-lightbox-caption"></div>');
        if (galleryItem.length == 1)
        {
            caption.html(galleryItem.find('.item--content__header').html());
            caption.find('*').not('.item--caption, .item--credit').remove();
        }*/

        // Set links
        var links = button.closest('.post-share-image__links').clone();
        links.find('li').not('.post-share-image__link').remove();
        links.removeClass('post-share-image__links').addClass('va-lightbox-links');
        links.find('li').removeClass('post-image__link-image__links').addClass('va-lightbox-link');

        // Set image
        var image = $('<div class="va-lightbox-image"><img src="' + src + '" alt="" /></div>');

        // Set layout
        var lightbox = $('<div class="va-lightbox"></div>');
        lightbox.append(close);
        lightbox.append(image);
        //lightbox.append(caption);
        lightbox.append(links);
        lightbox.css(
        {
            opacity: 0
        });

        $('body').append(lightbox);

        TweenMax.set('body',
        {
            overflow: "hidden"
        });

        // Animation
        TweenMax.to(
            lightbox,
            0.5,
            {
                alpha: 1
            }
        );

        // Close function
        close.add(image).on('click', function()
        {
            TweenMax.to(
                lightbox,
                0.5,
                {
                    alpha: 0,
                    onComplete: function()
                    {
                        lightbox.remove();
                        TweenMax.set('body',
                        {
                            clearProps: "overflow"
                        });
                    }
                }
            );
        });
    });



    /* ============================================================================

        Same height

    ============================================================================ */

    function sameHeight()
    {

        $('.js-video-post').matchHeight();

    }

    if ($('.js-video-post').length > 0)
    {
        sameHeight();
    }

    /* ============================================================================

        Mobile toggle

    ============================================================================ */

    function filterToggle()
    {
        $('.js-toggle-filter-dropdown').click(
		function(){

            var el = $(this);
            var filterList = el.next('.js-filter-dropdown');

            if (el.hasClass('is-active'))
            {
                el.removeClass('is-active');
                TweenMax.to(filterList, 0.25,
                {
                    height: 0,
                    ease: Power1.easeInOut
                });
            }
            else
            {
                el.addClass('is-active');
                TweenMax.to(filterList, 0.25,
                {
                    height: 260,
                    ease: Power1.easeInOut
                });
            }
        });
    }

    function imgShareToggle()
    {
        $('body').on('click', '.js-post-share-image-toggle', function()
        {

            var el = $(this);
            var siblings = el.parent().siblings();

            if (el.hasClass('is-active'))
            {
                el.removeClass('is-active');
                TweenMax.to(siblings, 0.15,
                {
                    alpha: 0,
                    ease: Power1.easeInOut
                });
            }
            else
            {
                el.addClass('is-active');
                TweenMax.to(siblings, 0.15,
                {
                    alpha: 1,
                    ease: Power1.easeInOut
                });
            }
        });
    }

    if (!$('html').hasClass('no-touchevents'))
    {
        filterToggle();
        imgShareToggle();
    }

});