/* ============================================================================

  JQUERY EASING

============================================================================ */

!function(){var a={};jQuery.each(["Quad","Cubic","Quart","Quint","Expo"],function(b,c){a[c]=function(a){return Math.pow(a,b+2)}}),jQuery.extend(a,{Sine:function(a){return 1-Math.cos(a*Math.PI/2)},Circ:function(a){return 1-Math.sqrt(1-a*a)},Elastic:function(a){return 0===a||1===a?a:-Math.pow(2,8*(a-1))*Math.sin((80*(a-1)-7.5)*Math.PI/15)},Back:function(a){return a*a*(3*a-2)},Bounce:function(a){for(var b,c=4;a<((b=Math.pow(2,--c))-1)/11;);return 1/Math.pow(4,3-c)-7.5625*Math.pow((3*b-2)/22-a,2)}}),jQuery.each(a,function(a,b){jQuery.easing["easeIn"+a]=b,jQuery.easing["easeOut"+a]=function(a){return 1-b(1-a)},jQuery.easing["easeInOut"+a]=function(a){return a<.5?b(2*a)/2:1-b(a*-2+2)/2}})}();