var scaleAd = function(adContainer)
{
    var ad_container = jQuery(adContainer);
    var ad = jQuery('iframe[id^=ox_]', ad_container);
    var cont_width = ad_container.width();
    var ad_width = ad.attr('width');
    var scale = Math.min(1, cont_width / ad_width);
    var lmargin = Math.min((ad_width - cont_width) / -2, 0);
    ad.css("cssText", '-ms-transform : scale(' + scale + ',' + scale + '); -webkit-transform : scale(' + scale + ',' + scale + '); transform : scale(' + scale + ',' + scale + '); margin-left : ' + lmargin + 'px !important');
};

window.mobilecheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

matchMedia("screen and (max-width:1025px)").addListener(function(mql)
{
    if (mql.matches)
    {
        try { NERV.load(); } catch (e) {}
    }
});
matchMedia("screen and (min-width:1025px)").addListener(function(mql)
{
    if (mql.matches)
    {
        try { NERV.load(); } catch (e) {}
    }
});

function OX_responsive(name)
{
    this.name = name;
	this.id = '#' + this.name;
    this.root = jQuery('#' + this.name);
    this.ox =
    {
        mobile: false,
        desktop: false
    }
    this.locked = false;
}

OX_responsive.prototype.mq = function()
{
    if (document.documentElement.clientWidth <= 960)
    {
        return true;
    }
    else
    {
        return false;
    }
};

OX_responsive.prototype.setup = function()
{
    this.ox.mobile = this.ox.mobile || OX();
    this.ox.desktop = this.ox.desktop || OX();
	if (this.root.data('url')) {
		this.ox.desktop.setPageURL(this.root.data('url'));
		this.ox.mobile.setPageURL(this.root.data('url'));
	}
};

OX_responsive.prototype.addPage = function()
{
    this.setup();
    this.ox.desktop.addPage(jQuery(this.id+'[data-desktop-page-id]').data('desktop-page-id'), this.root);
    this.ox.mobile.addPage(jQuery(this.id+'[data-mobile-page-id]').data('mobile-page-id'), this.root);
};

OX_responsive.prototype.setAdSlots = function()
{
    // console.log('setadslots');
    if (this.ox.desktop.getQueryArgs().pgid)
    {
        var that = this;
        jQuery('[data-ad-group^=desktop]', this.root).each(
            function(a, b)
            {
                var ad = jQuery(this);
                that.ox.desktop.setAdUnitSlotId(ad.data('ad-id'), ad.attr('id'));
            });
        // console.log(this.ox.desktop.get('ad_units').length + ' units set');
    }
    else
    {
        // console.log('Ad Slots Not Loaded: No desktop page id provided to ad server.');
    }
    if (this.ox.mobile.getQueryArgs().pgid)
    {
        var that = this;
        jQuery('[data-ad-group^=mobile]', this.root).each(
            function()
            {
                var ad = jQuery(this);
                that.ox.mobile.setAdUnitSlotId(ad.data('ad-id'), ad.attr('id'));
            });
        // console.log(this.ox.mobile.get('ad_units').length + ' units set');
    }
    else
    {
        // console.log('Ad Slots Not Loaded: No mobile page id provided to ad server.');
    }

    jQuery('.ad_970.mobile_ads', this.root).each(
        function(i, e)
        {
            jQuery(e).bind('DOMNodeInserted', function(e)
            {
                scaleAd(jQuery(e.target).parent());
            });
        }
    );
};

OX_responsive.prototype.load = function()
{
    if (this.mq())
    {
        if (this.ox.mobile.getProperties().ad_units.length > 0 && !this.locked)
        {
            this.clear();
            this.locked = true;
            // console.log('load');
            this.ox.mobile.load();
            //scaleAd(jQuery('.ad_970.desktop_ads, .ad_970.mobile_ads'));
            //setTimeout(function(){ scaleAd(jQuery('.ad_970.desktop_ads, .ad_970.mobile_ads')); }, 2000);
            var self = this;
            setTimeout(function()
            {
                self.locked = false;
            }, 4000);
        }
        else
        {
            // console.log('no ads to load or locked');
        }
    }
    else
    {
        if (this.ox.desktop.getProperties().ad_units.length > 0 && !this.locked)
        {
            this.clear();
            this.locked = true;
            // console.log('load');
            this.ox.desktop.load();
            var self = this;
            setTimeout(function()
            {
                self.locked = false;
            }, 4000);
        }
        else
        {
            // console.log('no ads to load or locked');
        }
    }
};

OX_responsive.prototype.clear = function()
{
    jQuery('iframe[id^=ox_]', this.root).each(function()
    { // note: cleared scope to account for scoping issue in overlay galleries
        frameDoc = this.contentDocument || this.contentWindow.document;
        frameDoc.documentElement.innerHTML = '';
    });
};