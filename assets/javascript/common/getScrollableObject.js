var getScrollableObject;
(function($)
{
    getScrollableObject = function (target)
    {
        var isWindow = !target.nodeName || $.inArray( target.nodeName.toLowerCase(), ['iframe','#document','html','body'] ) != -1;

        if( !isWindow ) return target;

        var documentObject = (target.contentWindow || target).document || target.ownerDocument || target;
        
        var isSafariOrChrome = navigator.userAgent.indexOf("Safari") > -1;
        if (isSafariOrChrome || documentObject.compatMode == 'BackCompat')
        return documentObject.body;
        else
        return documentObject.documentElement;
    };
}
)(jQuery);