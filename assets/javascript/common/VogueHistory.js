var VogueHistory = {popStateListeners: [], skipOne: false};

(function($)
{
    var $window = $(window);

    var popStateListeners = VogueHistory.popStateListeners;

    var stackedStateIndexes = [0];

    var scrollRestorationEnabled = false;

    function cleanTitle(title)
    {
        return title.replace(/<\/?[^>]+(>|$)/g, "");
    }

    VogueHistory.scrolledToPage = function(index, title, url)
    {
        if (index != VogueHistory.currentContentIndex)
        {
            // console.log('scrolledToPage', index);
            if (scrollRestorationEnabled)
            {
                var newContentIndexIndex = stackedStateIndexes.indexOf(index);
                // console.log('newContentIndexIndex', newContentIndexIndex);

                if (newContentIndexIndex == -1)
                {
                    VogueHistory.addState(index, title, url);
                }
                else
                {
                    VogueHistory.goBackToIndex(index, title, url);
                }
            }
            else
            {
                VogueHistory.addState(index, title, url);
            }
        }
    }

    VogueHistory.addState = function (index, title, url)
    {
        // console.log('addState');
        history.pushState
        (
            {
                index: index,
                title: title,
                url: url
            },
            title,
            url
        );

        document.title = cleanTitle(title);

        // console.log('pushing', index);
        stackedStateIndexes.push(index);
        // console.log('new length', stackedStateIndexes.length);

        VogueHistory.currentContentIndex = index;
        VogueHistory.currentTitle = title;
        VogueHistory.currentURL = url;
    }

    VogueHistory.replaceState = function (index, title, url)
    {
        // console.log('replaceState');
        history.replaceState
        (
            {
                index: index,
                title: title,
                url: url
            },
            title,
            url
        );

        document.title = cleanTitle(title);


        // console.log('popping-pushing', index);
        stackedStateIndexes.pop();
        stackedStateIndexes.push(index);
        // console.log('new length', stackedStateIndexes.length);

        VogueHistory.currentContentIndex = index;
        VogueHistory.currentTitle = title;
        VogueHistory.currentURL = url;
    }

    VogueHistory.navigateToNewState = function (index, title, url)
    {
        VogueHistory.addState(index, title, url);
        popStateHandler();
    }

    var movingBackwards = false;

    VogueHistory.moveBackwards = function(steps, newIndex, newTitle, newURL)
    {
        // console.log('moveBackwards');
        VogueHistory.currentContentIndex = newIndex;
        VogueHistory.currentTitle = newTitle;
        VogueHistory.currentURL = newURL;

        document.title = cleanTitle(newTitle);

        // console.log(stackedStateIndexes.length, steps);
        stackedStateIndexes = stackedStateIndexes.slice(0, stackedStateIndexes.length - steps);

        movingBackwards = true;

        history.go(-steps);
    }

    VogueHistory.goBackToIndex = function(index, title, url)
    {
        // console.log('goBackToIndex');
        var newContentIndexIndex = stackedStateIndexes.indexOf(index);
        var steps = stackedStateIndexes.length - 1 - newContentIndexIndex;

        VogueHistory.moveBackwards(steps, index, title, url);
    }

    function popStateHandler(e)
    {
        if (!VogueHistory.skipOne)
        {   
            if (!movingBackwards)
            {
                if (e)
                {
                    var state = e.originalEvent.state;

                    var newContentIndexIndex = state ? stackedStateIndexes.indexOf(state.index) : 0;
                    var steps = stackedStateIndexes.length - 1 - newContentIndexIndex;
                    stackedStateIndexes = stackedStateIndexes.slice(0, stackedStateIndexes.length - steps);

                    if (state == null)
                    {
                        VogueHistory.currentContentIndex = 0;
                        VogueHistory.currentTitle = VogueHistory.originalTitle;
                        VogueHistory.currentURL = VogueHistory.originalURL;
                    }
                    else
                    {
                        VogueHistory.currentContentIndex = state.index;
                        VogueHistory.currentTitle = state.title;
                        VogueHistory.currentURL = state.url;
                    }
                }

                document.title = cleanTitle(VogueHistory.currentTitle);

                for (var i = 0; i < popStateListeners.length; i++)
                {
                    popStateListeners[i](e);
                }
            }
            else
            {
                movingBackwards = false;
            }
        }
        else
        {
            VogueHistory.skipOne = false;
        }

        return false;
    }

    VogueHistory.init = function()
    {
        if ('scrollRestoration' in history)
        {
            scrollRestorationEnabled = true;
            history.scrollRestoration = 'manual';
        }

        VogueHistory.scrollRestorationEnabled = scrollRestorationEnabled;

        VogueHistory.currentContentIndex = 0;
        VogueHistory.originalTitle = $('head > title').text();
        VogueHistory.currentTitle = $('head > title').text();
        VogueHistory.originalURL = window.location.href;
        VogueHistory.currentURL = window.location.href;

        $window.on('popstate', popStateHandler);
    }
}
)(jQuery);