var FadeIn = {};
(function($)
{
    var THROTTLE_TIME = 250;

    var THRESHOLD = 150;
    var INCREMENTAL_DELAY = 90;

    var MAX_ATTEMPTS_TO_FIND_SOMETHING_OTHER_THAN_THE_LOADING_IMAGE = 40;

    var $window = $(window);

    FadeIn.$allElements = $();
    FadeIn.$hiddenElements = $();

    FadeIn.gather = function()
    {
        FadeIn.add($('.auto-fade-in'));
    }

    FadeIn.add = function($elements)
    {
        $elements.addClass('auto-fade-in');

        var $newElements = $elements.not(FadeIn.$allElements);

        FadeIn.$allElements = FadeIn.$allElements.add($newElements);
        FadeIn.$hiddenElements = FadeIn.$hiddenElements.add($newElements);
    }

    var lastTime = null;
    var checkTimeout = null;

    FadeIn.check = function()
    {
        if (checkTimeout == null)
        {
            var currentTime = (new Date()).getTime();

            if (lastTime == null)
            {
                FadeIn.doCheck();
                lastTime = currentTime;
            }
            else
            {
                if (currentTime >= lastTime + THROTTLE_TIME)
                {
                    FadeIn.doCheck();
                    lastTime = currentTime;
                }
                else
                {
                    checkTimeout = setTimeout(function()
                    {
                        FadeIn.doCheck();
                        checkTimeout = null;
                    },
                    lastTime + THROTTLE_TIME - currentTime)

                    lastTime += THROTTLE_TIME;
                }
            }

        }
    }

    FadeIn.doCheck = function()
    {
        var scrollTop = $window.scrollTop();
        var viewportHeight = window.innerHeight;

        var $shownElements = $();
        var immediatelyShownCount = 0;

        var attempts = 0;

        FadeIn.$hiddenElements.each(function()
        {
            var $element = $(this);

            function attemptShow()
            {
                var isPicture = $element.is('picture');

                function showImmediately()
                {
                    setTimeout(function()
                    {
                        $element.addClass('auto-fade-in-show');
                    },
                    immediatelyShownCount * INCREMENTAL_DELAY);

                    setTimeout(function()
                    {
                        $element.addClass('auto-fade-in-gone');
                        $element.removeClass('auto-fade-in');
                        $element.removeClass('auto-fade-in-show');
                    },
                    immediatelyShownCount * INCREMENTAL_DELAY + 1250);

                    immediatelyShownCount++;
                }

                if ($element.is('img') || isPicture)
                {
                    var img = isPicture ? $element.find('img')[0] : $element[0];

                    var src = img && (img.src || img.srcset);

                    if // If...
                    (
                        !img // ... there isn't an image really, ...
                        ||
                        (
                            src // ... the image elements has a src value, ....
                            &&
                            !(/V-loading/i.test(src)) // ... the source doesn't point to the "loading image", ...
                            &&
                            img.complete // ... the image was loaded successfully, ...
                        )
                    )
                    {
                        showImmediately(); // ... then show the element immediately.
                    }
                    else // Otherwise...
                    {
                        if (src && /V-loading/i.test(src)) // If it's the "loading image"
                        {
                            if (attempts < MAX_ATTEMPTS_TO_FIND_SOMETHING_OTHER_THAN_THE_LOADING_IMAGE)
                            {
                                setTimeout(function() // Wait a bit and try again
                                {
                                    attemptShow();
                                },
                                1000);
                            }

                            attempts++;
                        }
                        else // Otherwise
                        {
                            $(img).one('load', function() // Wait for it to load
                            {
                                $element.addClass('auto-fade-in-show');

                                setTimeout(function()
                                {
                                    $element.addClass('auto-fade-in-gone');
                                    $element.removeClass('auto-fade-in');
                                    $element.removeClass('auto-fade-in-show');
                                },
                                1250);
                            });
                        }
                    }
                }
                else
                showImmediately();
            }

            if (scrollTop + viewportHeight - THRESHOLD > $element.offset().top)
            {
                $shownElements = $shownElements.add($element);
                attemptShow();
            }
        });

        FadeIn.$hiddenElements = FadeIn.$hiddenElements.not($shownElements);
    }

    FadeIn.bypassNow = function()
    {
        var scrollTop = $window.scrollTop();
        var viewportHeight = window.innerHeight;

        var $removedElements = $();

        FadeIn.$hiddenElements.each(function()
        {
            var $element = $(this);

            if (scrollTop + viewportHeight - THRESHOLD > $element.offset().top)
            {
                $element.addClass('auto-fade-in-cleared');
                $element.removeClass('auto-fade-in');
                $removedElements = $removedElements.add($element);
            }
        });

        FadeIn.$allElements = FadeIn.$allElements.not($removedElements);
        FadeIn.$hiddenElements = FadeIn.$hiddenElements.not($removedElements);
    }

    FadeIn.remove = function($elements)
    {
        $elements.removeClass('auto-fade-in');
        $elements.removeClass('auto-fade-in-cleared');
        $elements.removeClass('auto-fade-in-show');
        $elements.removeClass('auto-fade-in-gone');

        FadeIn.$allElements = FadeIn.$allElements.not($elements);
        FadeIn.$hiddenElements = FadeIn.$hiddenElements.not($elements);
    }
}
)(jQuery);