(function($)
{
    var EMAIL_REGEX = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,8})?$/;

    var lang;

    function init()
    {
        lang = $('html').is('[lang="en-GB"]') ? 'en' : 'ar';
        parseMainNewsletterForm();
    }
    $(init);

    function parseMainNewsletterForm()
    {
        var $newsletterForm = $('#newsletter-signup');
        if ($newsletterForm.length)
        {
            var $nameInput = $newsletterForm.find('#newsletter-name');
            var $emailInput = $newsletterForm.find('#newsletter-email');

            var $message = $newsletterForm.find('.message');

            new NewsletterForm($newsletterForm, $nameInput, $emailInput, $message);
        }
    }

    Newsletter.parseNewsletterPagebreaks = function()
    {
        var $newsletterForms = $('.newsletter-signup-break-form:not(.ready)');

        $newsletterForms.each(function()
        {
            var $newsletterForm = $(this);

            var $nameInput = $newsletterForm.find('.newsletter--field.name');
            var $emailInput = $newsletterForm.find('.newsletter--field.email');

            var $message = $newsletterForm.find('.message');

            new NewsletterForm($newsletterForm, $nameInput, $emailInput, $message);

            $newsletterForm.addClass('ready');
        });
    }

    function NewsletterForm($newsletterForm, $nameInput, $emailInput, $message)
    {
        $newsletterForm.on('submit', function(e)
        {
            e.preventDefault();
            submitNewsletterSubscription();
        });

        function submitNewsletterSubscription()
        {
            var name = $nameInput.val();
            var email = $emailInput.val();

            if (!name.length)
            {
                showMessage(Newsletter.BAD_NAME_MESSAGE, 'error');
            }
            else if (email == '')
            {
                showMessage(Newsletter.NO_EMAIL_MESSAGE, 'error');
            }
            else if (!EMAIL_REGEX.test(email))
            {
                showMessage(Newsletter.BAD_EMAIL_MESSAGE, 'error');
            }
            else
            {
                showMessage(Newsletter.SUBMITTING_MESSAGE, 'info');
                
                $.ajax(
                {
                    url: Globals.ajaxEndPoint,
                    method: 'POST',
                    data: 
                    {
                        action: 'storeAddress',
                        name: name,
                        email: email,
                        lang: lang,
                    },
                    success: function(data)
                    {
                        if (data == "success")
                        {
                            showMessage(Newsletter.SUCCESS_MESSAGE, 'glamour');
                            $nameInput.val('');
                            $emailInput.val('');
                        }
                        else
                        {
                            showMessage(Newsletter.ERROR_MESSAGE, 'error');
                        }   
                    },
                    error: function()
                    {
                        showMessage(Newsletter.ERROR_MESSAGE, 'error');
                    }
                });
            }
        }

        function showMessage(text, type)
        {
            $message.html(text);

            $message.removeClass('info');
            $message.removeClass('error');
            $message.removeClass('glamour');

            $message.addClass(type);
        }
    }
}
)(jQuery);