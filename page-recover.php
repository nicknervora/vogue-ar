<?php if ( is_user_logged_in() ) : if ( current_user_can( 'publish_posts' ) ) : header( 'location:' . get_bloginfo( 'url' ) . '/wp-admin' ); else : header( 'location:' . get_bloginfo( 'url' ) . '/account' ); endif; endif;

  /* Template Name: Account - Recovery */

  get_header();

?>

<section class="hero-banner">
  <div class="background" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/herobanner-runway.jpg');"></div>
</section>

<div class="scroll" data-ui="jscroll-default">

  <div class="get--content">

    <div id="account-header">
      <div id="ah-inner">
        <h1><?php echo __( 'Password Recovery', 'vogue.me' ); ?></h1>
      </div>
    </div>

    <div id="account-details">

      <span class="welcome"><?php echo __( 'Enter your email', 'vogue.me' ); ?></span>

      <?php if ( !empty( $_GET['failed'] ) ) : ?>
        <?php if ( $_GET['failed'] == 'wrongkey' ) : ?><p style="margin-bottom:30px;"><?php echo __( 'Wrong key, please try resending the recovery email.', 'vogue.me' ); ?></p><?php endif; ?>
      <?php endif; ?>

      <form name="lostpasswordform" action="<?php echo site_url( 'wp-login.php?action=lostpassword', 'login_post' ) ?>" method="post">

        <?php if ( function_exists( 'wp_nonce_field' ) ) wp_nonce_field( 'rs_user_lost_password_action', 'rs_user_lost_password_nonce' ); ?>

        <div class="acc-hold">
          <label for="user_login"><?php echo __( 'Email', 'vogue.me' ); ?></label>
          <input type="email" name="user_login" id="user_login" class="acc-text" value="">
        </div>

        <input type="hidden" name="redirect_to" value="<?php echo site_url( '/account/reset/?action=forgot&success=1' ); ?>">
        <button type="submit" name="wp-submit" id="wp-submit" class="acc-submit"><span class="acc-arrow"><?php echo __( 'Resent Password', 'vogue.me' ); ?></span></button>

      </form>

    </div>

  </div>

</div>

<?php get_footer(); ?>