<?php 

// Template name: Stylebase profile videos

	get_header();

	if ( isset( $_GET[ 'profile' ] ) ) :

	$term = $_GET[ 'profile' ];

	$term = get_term_by( 'slug', $term, COLLECTION );

	$term_id = $term->term_id;
	$term_slug = $term->slug;
	$term_name = $term->name;

	$tag_object = $term->taxonomy;

	$get_taxonomy_term = $tag_object . '_' . $term_id;

	$header_bg = getArchiveHeaderBG( $get_taxonomy_term );

	$video_cat_id = get_category_by_slug( 'video' )->term_id;

?>

	<div class="hero hero--sb">
		<div class="hero__bg" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/assets/images/vogue-temp.jpg');"></div>
	</div>

	<section class="pc pc--sb sb">

		<div class="pc__header pc-header">
			<div class="container container--mid--reduced">
				<div class="pc-header__inner">
					<div class="pc-header__top">
						<h1 class="pc-header__title"><?php echo $term_name; ?></h1>
					<?php if ( get_field( 'social_profiles', $get_taxonomy_term ) ) : $social = get_field( 'social_profiles', $get_taxonomy_term ); ?>
            <ul class="pc-social">
          <?php foreach ( $social as $profile ) : $network = $profile['network']; $account = $profile['tag']; ?>
              <li><a class="fa fa-<?php echo $network; ?>" href="<?php echo get_social_url( $network, $account ); ?>" target="_blank"></a></li>
          <?php endforeach; ?>
            </ul>
          <?php endif; ?>
					</div>

					<div class="pc-header__search">
						<?php VogueCollectionSearch(); ?>
					</div>

				</div>
			</div>
		</div>

<?php

	$args = array( 'post_type' => array( 'legacy', 'post' ), 'category__in' => array( $video_cat_id ), 'posts_per_page' => -1, 'tax_query' => array( array( 'taxonomy' => 'vogue_collection', 'field' => 'slug', 'terms' => $term_slug ) ) );
	$style_query = new WP_Query( $args );

?>

<?php if ( $style_query->have_posts() ) : ?>
		<section class="sb-gallery sb-gallery--profile-videos">
			<div class="container container--mid">
				<header class="sb-gallery__head">
					<a href="<?php echo get_term_link( $term_slug, COLLECTION ); ?>" class="sb-gallery__link"><?php echo __( 'Go Back','vogue.me' ); ?></a>
					<h1 class="sb-gallery__title"><?php echo __( 'Videos','vogue.me' ); ?></h1>
				</header>
				<div class="sb-gallery__inner">

				<?php $i = 1; while ( $style_query->have_posts() ) : $style_query->the_post(); ?>

				<?php if ( $i % 2 == 1 ) : ?> <div class="row"> <?php endif; ?>

          <?php if ( get_field( 'mp4_source_remote' ) ) : $src = get_field( 'mp4_source_remote' ); ?>
            <article class="sb-gallery-video"> <?php getVideoPlayer( $src ); ?> </article>
          <?php endif; ?>

        <?php if ( $i % 2 == 0 ) : ?> </div> <?php endif; ?>

        <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

        <?php if ( $i % 2 != 1 ) : ?> </div> <?php endif; ?>

			</div>
		</section>
<?php endif; ?>

	</section>

<?php endif; get_footer(); ?>