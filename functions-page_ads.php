<?php




function getAdvert( $type, $pos = 'DESK' )
{

  $asset_url = get_stylesheet_directory_uri() . '/assets/images/';

  $allowed = array( 'vert', 'strip' );

  if ( in_array( $type, $allowed ) )
  {
    // echo '<div class="special--container ' . $type . '">';
    //echo '<div class="special--container ' . $type . ' ad_300 ad_label desktop_ads no-js-remove">';

    if ( is_front_page() )
    {
      $ad = 'home';
    }
    elseif ( is_page() )
    {
      $ad = 'home';
    }
    elseif ( is_single() )
    {
      $ad = 'single';
    }
    elseif ( is_category() )
    {
      $ad = 'category';
    }

    if ( $type == 'strip' ) // billboard
    {
			if ($pos == 'DESK') {
			
			echo '<div class="special--container ' . $type . ' ad_label desktop_ads no-js-remove">';
      
			echo '<div class="horizontalAdSection gallerylockead"> <div class="horizontalAdspaceGallery">';
      
			echo '<div class="ad_970 desktop_ads"> <div id="BBD' . rand(100,999) . '" data-ad-id="' . page_ads('DESK','BBD') . '" data-ad-group="desktop"></div> </div>';
			echo '<div class="ad_970 mobile_ads"> <div id="BBD_m' . rand(100,999)  . '" data-ad-id="' . page_ads('MOB','BBD') . '" data-ad-group="mobile"></div> </div>';
     
			echo '</div> </div>';
			echo '</div>';
			} else if ($pos == 'TOP') {
				echo '<div class="special--container ' . $type . ' ad_label desktop_ads no-js-remove">';

				echo '<div class="horizontalAdSection gallerylockead"> <div class="horizontalAdspaceGallery">';

				echo '<div class="ad_970 desktop_ads"> <div id="BBD' . rand(100,999) . '" data-ad-id="' . page_ads('TOPDESK','BBD') . '" data-ad-group="desktop"></div> </div>';
				echo '<div class="ad_970 mobile_ads"> <div id="BBD_m' . rand(100,999)  . '" data-ad-id="' . page_ads('TOPMOB','BBD') . '" data-ad-group="mobile"></div> </div>';

				echo '</div> </div>';
				echo '</div>';
			}

    }
    else // half page
    {
	
			if ($pos == 'DESK') {
      	echo '<div class="special--container ' . $type . ' ad_300 ad_label desktop_ads"><div class="vertable ad_300 ad_label desktop_ads"><div id="HLF' . rand(100,999) . '" data-ad-id="' . page_ads( 'DESK','HLF' ) . '" data-ad-group="desktop"></div></div></div>';
				echo '<div class="special--container ' . $type . ' ad_300 ad_label mobile_ads"><div class="vertable ad_300 ad_label mobile_ads"><div id="HLF_m' . rand(100,999) . '" data-ad-id="' . page_ads( 'MOB','HLF' ) . '" data-ad-group="mobile"></div></div></div>';
			} else if ($pos == 'TOP') {
				echo '<div class="special--container ' . $type . ' ad_300 ad_label desktop_ads"><div class="vertable ad_300 ad_label desktop_ads"><div id="HLF' . rand(100,999) . '" data-ad-id="' . page_ads( 'TOPDESK','HLF' ) . '" data-ad-group="desktop"></div></div></div>';
				echo '<div class="special--container ' . $type . ' ad_300 ad_label mobile_ads"><div class="vertable ad_300 ad_label mobile_ads"><div id="HLF_m' . rand(100,999) . '" data-ad-id="' . page_ads( 'TOPMOB','HLF' ) . '" data-ad-group="mobile"></div></div></div>';
			}
				
    }

    //echo '</div>';

  }

}


function page_ads($section, $type) {

 $english_ads = array(
	'DESK' => array(
	'pageid' => 537066834,
	'BBD' => 538615912,
	'HLF' => 538615929
	),
	'MOB' => array(
	'pageid' => 537066851,
	'BBD' => 538629165,
	'HLF' => 538629166
	),
	'TOPDESK' => array(
	'pageid' => 537066860,
	'BBD' => 538643348,
	'HLF' => 538643352
	),
	'TOPMOB' => array(
	'pageid' => 537066861,
	'BBD' => 538643353,
	'HLF' => 538643351
	),
  'home' => array(
   'pageid' => 537065050,
   'z1lb' => 537256069,
   'z1mpu' => 537256073,
   'z2lb' => 537256075,
   'z2mpu' => 537256076,
   'z3lb' => 537256078,
   'z3mpu' => 537256079,
   'z4lb' => 537256080,
   'z4mpu' => 537256081
   ),
  'single' => array(
   'pageid' => 537065055,
   'z1lb' => 537256069,
   'z1mpu' => 537256073,
   'z2mpu' => 537256076,
   'z3mpu' => 537256079,
   'z4lb' => 537256080
  ),
  'category' => array(
   'pageid' => 537065087,
   'z1lb' => 537256069,
   'z1mpu' => 537256073,
   'z2lb' => 537256075,
   'z2mpu' => 537256076,
   'z4lb' => 537256080
  ),
  'gallery' => array(
   'pageid' => 537065088,
   'z1lb' => 537483487,
   'z1mpu' => 537598388,
   'z2lb' => 537598384,
   'z3lb' => 537598385,
   'z2mpu' => 537483488,
   'z3mpu' => 537598386,
   'z4mpu' => 537598387,
  ),
  'overlay' => array(
   'pageid' => 537065152,
   'z1lb' => 537256069,
   'z1mpu' => 537256073,
   'z2mpu' => 537256076
  ),
  'fparchive' => array(
   'pageid' => 537065727,
   'z1lb' => 537256069,
   'z2lb' => 537256075,
   'z3lb' => 537256078,
   'z1mpu' => 537256073,
   'z2mpu' => 537256076
  ),
  'fpsinglepost' => array(
   'pageid' => 537065728,
   'z1lb' => 537256069,
   'z2lb' => 537256075,
   'z1mpu' => 537256073
  ),
  'fpsinglepage' => array(
   'pageid' => 537065729,
   'z1lb' => 537256069
  ),
  'fpprofile' => array(
   'pageid' => 537065730,
   'z1mpu' => 537256073
  ),
  'fpvideo' => array(
   'pageid' => 537065731,
   'z1lb' => 537256069,
   'z2lb' => 537256075,
   'z2mpu' => 537256076
  ),
  'single_first_lock' => array(
    'pageid'=> 537066442,
    'z1lb' => 538479282,
    'z1mpu' => 538479281
   ),
  'single_second_lock' => array(
    'pageid'=> 537066442,
    'z2lb' => 538479282,
    'z2mpu' => 538479281
   ),
  'single_third_lock' => array(
    'pageid'=> 537066442,
    'z3lb' => 538479282,
    'z3mpu' => 538479281
   ),  
  'single_incase_lock' => array(
    'pageid'=> 537066443,
    'z2lb' => 538479280,
    'z2mpu' => 538479281
  )
  
 );

 $arabic_ads = array(
	'DESK' => array(
	'pageid' => 537066850,
	'BBD' => 538628846,
	'HLF' => 538628847
	),
	'MOB' => array(
	'pageid' => 537066852,
	'BBD' => 538629174,
	'HLF' => 538629175
	),
	'TOPDESK' => array(
	'pageid' => 537066859,
	'BBD' => 538643354,
	'HLF' => 538643356
	),
	'TOPMOB' => array(
	'pageid' => 537066862,
	'BBD' => 538643355,
	'HLF' => 538643357
	),
  'home' => array(
   'pageid' => 537065091,
   'z1lb' => 537271376,
   'z1mpu' => 537271378,
   'z2lb' => 537271379,
   'z2mpu' => 537271386,
   'z3lb' => 537271387,
   'z3mpu' => 537271388,
   'z4lb' => 537271389,
   'z4mpu' => 537271390
   ),
  'single' => array(
   'pageid' => 537065110,
   'z1lb' => 537271376,
   'z1mpu' => 537271378,
   'z2mpu' => 537271386,
   'z3mpu' => 537271388,
   'z4lb' => 537271389
  ),
  'category' => array(
   'pageid' => 537065089,
   'z1lb' => 537271376,
   'z1mpu' => 537271378,
   'z2lb' => 537271379,
   'z2mpu' => 537271386,
   'z4lb' => 537271389
  ),
  'gallery' => array(
   'pageid' => 537065090,
   'z1lb' => 537483485,
   'z1mpu' => 537483486,
   'z2lb' => 537598339,
   'z3lb' => 537598340,
   'z2mpu' => 537598341,
   'z3mpu' => 537598342,
   'z4mpu' => 537598343,
  ),
  'overlay' => array(
   'pageid' => 537065151,
   'z1lb' => 537271376,
   'z1mpu' => 537271378,
   'z2mpu' => 537271386
  ),
  'fparchive' => array(
   'pageid' => 537065732,
   'z1lb' => 537271376,
   'z2lb' => 537271379,
   'z3lb' => 537271387,
   'z1mpu' => 537271378,
   'z2mpu' => 537271386
  ),
  'fpsinglepost' => array(
   'pageid' => 537065733,
   'z1lb' => 537271376,
   'z2lb' => 537271379,
   'z1mpu' => 537271378
  ),
  'fpsinglepage' => array(
   'pageid' => 537065734,
   'z1lb' => 537271376
  ),
  'fpprofile' => array(
   'pageid' => 537065735,
   'z1mpu' => 537271378
  ),
  'fpvideo' => array(
   'pageid' => 537065736,
   'z1lb' => 537271376,
   'z2lb' => 537271379,
   'z2mpu' => 537271386
  ),
  'single_first_lock' => array(
    'pageid'=> 537066442,
    'z1lb' => 538479282,
    'z1mpu' => 538479281
   ),
  'single_second_lock' => array(
    'pageid'=> 537066442,
    'z2lb' => 538479282,
    'z2mpu' => 538479281
   ),
  'single_third_lock' => array(
    'pageid'=> 537066442,
    'z3lb' => 538479282,
    'z3mpu' => 538479281
   ),  
  'single_incase_lock' => array(
    'pageid'=> 537066443,
    'z2lb' => 538479280,
    'z2mpu' => 538479281
  )
 );

 $resp_ads = ( check_site( false, 'ar' ) ) ? $arabic_ads : $english_ads;

 return $resp_ads[$section][$type];
}