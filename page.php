<?php get_header(); the_post(); ?>

<div class="scroll" data-ui="jscroll-default">

  <?php

    $featured_id = get_post_thumbnail_id();
    $featured = wp_get_attachment_image_src( $featured_id, 'full' );
    $featured = imageProvider( $featured[0], 1600, 300 );

  ?>

  <?php if ( $featured ) : ?><div class="archive--header no--margin"<?php echo ' style="background-image: url(' . $featured . ');"'; ?>></div><?php endif; ?>

  <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
  <div class="get--content" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

    <div class="container container--mid--reduced">

      <div class="content--column is-page">

        <h2 class="page--title"><?php the_title(); ?></h2>

        <?php the_content(); ?>

      </div>

    </div>

  </div>
  <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

</div>

<?php get_footer(); ?>

