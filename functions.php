<?php


$blog_id = get_current_blog_id();
if($blog_id == 6){
  wp_redirect( 'http://en.vogue.me', 301 );
  exit;
}


if (function_exists('add_image_size')) {
    add_image_size('vrthumb', 270, 83, true);
}

/*
if( $_SERVER['REMOTE_ADDR'] != "181.167.52.31" && $_SERVER['REMOTE_ADDR'] != "190.188.179.71" ){
  if ( !in_array($GLOBALS['pagenow'], array('wp-login.php')) && !is_user_logged_in()) {
    wp_redirect( site_url() . '/landing', 301);
    exit;
  }
}*/


function country_geo_redirect() {
    $blog_id = get_current_blog_id();
     if($blog_id == 6){
         //check if the superglobal exist basically check if wpengine plugin is enabled
        if (getenv('HTTP_GEOIP_COUNTRY_CODE') !== false){
                $country = getenv('HTTP_GEOIP_COUNTRY_CODE');
                ?>
                <script>console.log("<?php echo $country; ?>"); </script>
                <?php
                // bahrain, oman, qater, saudi, eygpt, Lebanon, iraq, Libya, algeria, jordan, kewait, Morocco, Somalia, sudan, yeman, tuisia, syria

                if ( $country == "BH" || $country == "OM" || $country == "SA" || $country == "EG" || $country == "LB" || $country == "IQ" || $country == "LY" || $country == "DZ" || $country == "JO" || $country == "KW" || $country == "MA" || $country == "SO" || $country == "SD" || $country == "YE" || $country == "TN" || $country == "SY") {

                    // after going live
                    wp_redirect( 'http://ar.vogue.me', 301 );

                     exit;

                } else {
                    // after going live
                    wp_redirect( 'http://en.vogue.me', 301 );
                     exit;
                }
        }
     }
}
add_action('init', 'country_geo_redirect');

session_start();

// DEFINE BLOG IDS FOR GENDER / LANGUAGE SPLITS
define( 'WOMEN_EN', 1 );
define( 'MEN_EN', 2 );
define( 'WOMEN_AR', 3 );
define( 'MEN_AR', 4 );

define( 'HOME_ID', get_home_ID() );
define( 'COLLECTION', 'vogue_collection' );

define( 'FEATURED_VIDEO', get_field( 'promoted_video', HOME_ID )[0] );

// function my_function_admin_bar(){ return false; }
// add_filter( 'show_admin_bar' , 'my_function_admin_bar');

include_once( 'functions-queries.php' );


function setup_theme(){

  if ( check_site(false,'ar') )
  {
    load_textdomain('vogue.me', TEMPLATEPATH . '/languages/ar_SY.mo');
  }

}
add_action('after_setup_theme', 'setup_theme');

function check_site($gender=false,$lang=false)
{

  if ( $lang == 'english' ) : $lang = str_replace( 'english', 'en', $lang ); endif;
  if ( $lang == 'arabic' ) : $lang = str_replace( 'arabic', 'ar', $lang ); endif;

  $a_g = array( 'women', 'men' );
  $a_l = array( 'en', 'ar' );

// ENGLISH
  $women_en = WOMEN_EN;
  $men_en = MEN_EN;
// ARABIC
  $women_ar = WOMEN_AR;
  $men_ar = MEN_AR;

  $women = array( $women_en, $women_ar );
  $men = array( $men_en, $men_ar );

  $en = array( $women_en, $men_en );
  $ar = array( $women_ar, $men_ar );

  $current = get_current_blog_id();

  if ( in_array( $lang, $a_l ) && $gender == false )
  {

    $check = $lang;

    if ( in_array( $current, $$check ) )
    {
      return true;
    }
    else
    {
      return false;
    }

  }
  elseif ( in_array( $gender, $a_g ) && $lang == false )
  {

    $check = $gender;

    if ( in_array( $current, $$check ) )
    {
      return true;
    }
    else
    {
      return false;
    }

  }
  elseif ( in_array( $gender, $a_g ) && in_array( $lang, $a_l ) )
  {
    $check = $gender . '_' . $lang;

    if ( $current == $$check )
    {
      return true;
    }
    else
    {
      return false;
    }

  }
  else
  {
    return false;
  }

}


/**
 * Force all network uploads to reside in "wp-content/uploads", and by-pass
 * "files" URL rewrite for site-specific directories.
 *
 * @link    http://wordpress.stackexchange.com/q/147750/1685
 *
 * @param   array   $dirs
 * @return  array
 */
function wpse_147750_upload_dir( $dirs ) {
    $dirs['baseurl'] = network_site_url( '/wp-content/uploads' );
    $dirs['basedir'] = ABSPATH . 'wp-content/uploads';
    $dirs['path'] = $dirs['basedir'] . $dirs['subdir'];
    $dirs['url'] = $dirs['baseurl'] . $dirs['subdir'];

    return $dirs;
}

add_filter( 'upload_dir', 'wpse_147750_upload_dir' );




/* ============================================================================

  GOOGLE ANALYTICS

============================================================================ */



function manual_google_analytics_code(){
    if (is_archive()){
        ?>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-80823014-1', 'auto');
     ga('send', 'pageview');

    </script><?php
    }else{ ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

        __gaTracker('create', 'UA-80823014-1', 'auto');
        __gaTracker('set', 'forceSSL', true);
        __gaTracker('require', 'displayfeatures');
        __gaTracker('set', 'dimension1', '<?php echo get_the_author(); ?>');
        <?php if(has_category()){ ?>
        __gaTracker('set', 'dimension2', '<?php  $numItems = count(get_the_category()); $i = 0; foreach(get_the_category() as $category){ if(++$i === $numItems){ echo $category->slug; } else { echo $category->slug . ", "; } } ?>');
        <?php } ?>
        __gaTracker('set', 'dimension3', '<?php echo get_post_type(); ?>');
        __gaTracker('set', 'dimension4', '<?php echo get_the_time(DateTime::ATOM); ?>');
        <?php $blog_id = get_current_blog_id();
                if ($blog_id == 1){ ?>
                __gaTracker('set', 'dimension5', 'EN');
                <?php }else if ($blog_id == 3){ ?>
                __gaTracker('set', 'dimension5', 'AR');
                <?php } ?>

        __gaTracker('send','pageview');
    </script>
    <?php }

}

add_action('wp_footer', 'manual_google_analytics_code');




/* ============================================================================

  LEGACY POST TYPE

============================================================================ */

function register_legacy_post_type()
{
    $labels = array(
        'name'               => _x('Legacy Posts', 'vogue_'),
        'singular_name'      => _x('Legacy Post', 'vogue_'),
        'all_items'          => __('All Legacy Posts'),
        'add_new'            => _x('Add New Legacy Post', 'tutorials'),
        'add_new_item'       => __('Add New Legacy Post'),
        'edit_item'          => __('Edit Legacy Post'),
        'new_item'           => __('New Legacy Post'),
        'view_item'          => __('View Legacy Post'),
        'search_items'       => __('Search Legacy Posts'),
        'not_found'          => __('No Legacy Posts found'),
        'not_found_in_trash' => __('No Legacy Posts found in Trash'),
        'parent_item_colon'  => ''
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_nav_menus'  => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'archive', 'with_front' => true ),
        'hierarchical'       => false,
        'menu_position'      => 5,
        'has_archive'        => false,
        'taxonomies'         => array('category', 'post_tag'),
        'supports'           => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'excerpt',
            'custom-fields',
            // 'comments',
            'revisions',
        ),
    );
    register_post_type('legacy', $args);

    $set = get_option('post_type_rules_flased_legacy');

    if ($set !== true){

    flush_rewrite_rules(true);
    update_option('post_type_rules_flased_legacy',true);

    }
}
add_action('init', 'register_legacy_post_type');


/* ============================================================================

  GALLERY POST TYPE

============================================================================ */

add_action("init", "style_gallery_init");
function style_gallery_init(){
        $gallery_labels = array(
        'name' => _x('Style Galleries', 'vogue_'),
        'singular_name' => _x('Gallery', 'vogue_'),
        'all_items' => __('All Gallery'),
        'add_new' => _x('Add new Gallery', 'Gallery'),
        'add_new_item' => __('Add new Gallery '),
        'edit_item' => __('Edit Gallery'),
        'new_item' => __('New Gallery'),
        'view_item' => __('View Gallery'),
        'search_items' => __('Search in Gallery'),
        'not_found' =>  __('No Gallery Found'),
        'not_found_in_trash' => __('No Gallery found in trash'),
        'parent_item_colon' => ''
    );

        $args = array(
        'labels' => $gallery_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'gallery','with_front' => FALSE ),
        // 'capability_type' => 'page',
        'hierarchical' => false,
        'menu_position' => 20,
        'taxonomies'         => array('category', 'post_tag'),
        'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
        'has_archive' => 'gallery'
    );
    register_post_type('gallery',$args);
}


/* ============================================================================

  GALLERY POST TYPE

============================================================================ */

add_action("init", "style_biography_init");
function style_biography_init(){
    $biography_labels = array(
    'name' => _x('Profiles', 'vogue_'),
    'singular_name' => _x('Profile', 'vogue_'),
    'all_items' => __('All Profiles'),
    'add_new' => _x('Add new Profile', 'Biography'),
    'add_new_item' => __('Add new Profile '),
    'edit_item' => __('Edit Profile'),
    'new_item' => __('New Profile'),
    'view_item' => __('View Profile'),
    'search_items' => __('Search in Profiles'),
    'not_found' =>  __('No Profile Found'),
    'not_found_in_trash' => __('No Profile found in trash'),
    'parent_item_colon' => ''
  );

    $args = array(
    'labels' => $biography_labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'profile','with_front' => FALSE ),
    // 'capability_type' => 'page',
    'hierarchical' => false,
    'menu_position' => 21,
    'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
    'has_archive' => 'fashionprize/profiles'
  );
  register_post_type('biography',$args);
}

add_action( 'init', 'fashionprize_year_category_taxonomies', 0 );
function fashionprize_year_category_taxonomies()
{

  $fashionprize_category_tax_labels = array(
    'name' => _x( 'Fashion Prize Year', 'vogue_' ),
    'singular_name' => _x( 'Fashion Prize Year', 'vogue_' ),
    'search_items' =>  __( 'Search in Fashion Prize Years' ),
    'all_items' => __( 'All Fashion Prize Years' ),
    'most_used_items' => null,
    'parent_item' => null,
    'parent_item_colon' => null,
    'exclude_from_search' =>false,
    'edit_item' => __( 'Edit Fashion Prize Year' ),
    'update_item' => __( 'Update Fashion Prize Year' ),
    'add_new_item' => __( 'Add New Fashion Prize Year' ),
    'new_item_name' => __( 'New Fashion Prize Year' ),
    'menu_name' => __( 'Fashion Prize Year' ),
  );
  register_taxonomy('fashionprizeyear', array( 'fashionprize','fpvideos', 'biography'), array(
    'hierarchical' => true,
    'labels' => $fashionprize_category_tax_labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'fashionprizeyear', 'with_front' => true, 'hierarchical' => true )
  ));


}


/* ============================================================================

  CONVERT NEW ROYAL SLIDER GALLERIES TO STANDARD WORDPRESS GALLERIES

============================================================================ */

function additional_gallery_settings() {
  ?>

    <script type="text/html" id="tmpl-custom-gallery-setting">
        <span>Slider</span>
        <select data-setting="slider">
            <option value="none">None</option>
            <option value="new_royalslider">New RoyalSlider</option>
        </select>
    </script>

    <script type="text/javascript">
        jQuery( document ).ready( function() {
            _.extend( wp.media.gallery.defaults, {
                slider: 'none'
            } );

            wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend( {
                template: function( view ) {
                    return wp.media.template( 'gallery-settings' )( view )
                         + wp.media.template( 'custom-gallery-setting' )( view );
                }
            } );
        } );
    </script>

  <?php
}
add_action('print_media_templates', 'additional_gallery_settings');


/* ============================================================================

  CONVERT ROYAL SLIDER GALLERIES TO STANDARD WORDPRESS GALLERIES

============================================================================ */

function customize_gallery_abit( $html, $attr, $instance ) {

    if( isset( $attr['slider'] ) && $slider = $attr['slider'] ) {
        // Unset attribute to avoid infinite recursive loops
        unset( $attr['slider'] );

        // Our custom HTML wrapper
        $html = sprintf(
            '<div class="wpse-gallery-wrapper-%s">%s</div>',
            esc_attr( $slider ),
            gallery_shortcode( $attr )
        );
    }

    return $html;
}
add_filter('post_gallery', 'customize_gallery_abit', 10, 3);



/* ============================================================================

  GET HOME ID

===============================================================================

  - Simple function to get the ID for the homepage

============================================================================ */

function get_home_ID()
{

  $home = get_page_by_title( 'Home' );

  $home_ID = $home->ID;

  return $home_ID;

}

/* ============================================================================

  CLEAN WP HEADER

  - Removes new emoji settings

  - Remove Feed links

  - Remove Generator tags, hiding version number

  - Remove WooCommerce Stylesheets

============================================================================ */

function clean_wp_header() {

  remove_action( 'wp_head', 'feed_links_extra', 3 );
  remove_action( 'wp_head', 'feed_links', 2 );
  remove_action( 'wp_head', 'rsd_link' );
  remove_action( 'wp_head', 'wlwmanifest_link' );
  remove_action( 'wp_head', 'index_rel_link' );
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
  remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
  remove_action( 'wp_head', 'wp_generator' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );

  remove_action( 'wp_print_styles', 'print_emoji_styles' );

}

add_action( 'after_setup_theme', 'clean_wp_header' );

/* ============================================================================

  Further WordPress Cleanup

===============================================================================

  SOIL

  - Cleans up the head section in WordPress, linked with Soil plugin

  - Sanitize is now packaged with Soil, will now sanitize front end when soil is activated.

  - Sanitize strips out all unneeded white space.

============================================================================ */

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( is_plugin_active( 'soil-master/soil.php' ) ) :

  add_theme_support( 'soil-clean-up' );

  add_theme_support( 'soil-nice-search' );

  add_theme_support( 'soil-relative-urls' );

  add_theme_support( 'soil-disable-trackbacks' );

  add_theme_support( 'soil-disable-asset-versioning' );

  if ( !is_admin() ) :

    function sanitize_output( $buffer ) {

      $search = array(
        '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
        '/[^\S ]+\</s',  // strip whitespaces before tags, except space
        '/(\s)+/s'       // shorten multiple whitespace sequences
      );

      $replace = array(
          '>',
          '<',
          '\\1'
      );

      $buffer = preg_replace( $search, $replace, $buffer );

      return $buffer;

    }

    ob_start( 'sanitize_output' );

  endif;

endif;


/* ============================================================================

  MAKE POST TAGS HIERARCHIAL

============================================================================ */

// function wd_hierarchical_tags_register() {

//   // Maintain the built-in rewrite functionality of WordPress tags

//   global $wp_rewrite;

//   $rewrite =  array(
//     'hierarchical'              => false, // Maintains tag permalink structure
//     'slug'                      => get_option('tag_base') ? get_option('tag_base') : 'tag',
//     'with_front'                => ! get_option('tag_base') || $wp_rewrite->using_index_permalinks(),
//     'ep_mask'                   => EP_TAGS,
//   );

//   // Redefine tag labels (or leave them the same)

//   $labels = array(
//     'name'                       => _x( 'Tags', 'Taxonomy General Name', 'hierarchical_tags' ),
//     'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'hierarchical_tags' ),
//     'menu_name'                  => __( 'Taxonomy', 'hierarchical_tags' ),
//     'all_items'                  => __( 'All Tags', 'hierarchical_tags' ),
//     'parent_item'                => __( 'Parent Tag', 'hierarchical_tags' ),
//     'parent_item_colon'          => __( 'Parent Tag:', 'hierarchical_tags' ),
//     'new_item_name'              => __( 'New Tag Name', 'hierarchical_tags' ),
//     'add_new_item'               => __( 'Add New Tag', 'hierarchical_tags' ),
//     'edit_item'                  => __( 'Edit Tag', 'hierarchical_tags' ),
//     'update_item'                => __( 'Update Tag', 'hierarchical_tags' ),
//     'view_item'                  => __( 'View Tag', 'hierarchical_tags' ),
//     'separate_items_with_commas' => __( 'Separate tags with commas', 'hierarchical_tags' ),
//     'add_or_remove_items'        => __( 'Add or remove tags', 'hierarchical_tags' ),
//     'choose_from_most_used'      => __( 'Choose from the most used', 'hierarchical_tags' ),
//     'popular_items'              => __( 'Popular Tags', 'hierarchical_tags' ),
//     'search_items'               => __( 'Search Tags', 'hierarchical_tags' ),
//     'not_found'                  => __( 'Not Found', 'hierarchical_tags' ),
//   );

//   // Override structure of built-in WordPress tags

//   register_taxonomy( 'post_tag', array( 'post', 'legacy' ), array(
//     // 'hierarchical'              => true, // Was false, now set to true
//     'query_var'                 => 'tag',
//     'labels'                    => $labels,
//     'rewrite'                   => $rewrite,
//     'public'                    => true,
//     'show_ui'                   => true,
//     'show_admin_column'         => true,
//   ) );

// }

// add_action( 'init', 'wd_hierarchical_tags_register' );



/* ============================================================================

  REGISTER STYLEBASE / VOGUE COLLECTION TAXONOMY

============================================================================ */

add_action( 'init', 'register_stylebase_taxonomy', 0 );

function register_stylebase_taxonomy() {

  $label = ucwords( str_replace( array( '_', '-' ), ' ', COLLECTION ) );

  $labels = array(
    'name'              => _x( $label, COLLECTION, 'vogue_' ),
    'singular_name'     => _x( $label, COLLECTION, 'vogue_' ),
    'search_items'      => __( 'Search ' . $label, 'vogue_' ),
    'all_items'         => __( 'All Tags', 'vogue_' ),
    'parent_item'       => __( 'Parent Tag', 'vogue_' ),
    'parent_item_colon' => __( 'Parent Tag:', 'vogue_' ),
    'edit_item'         => __( 'Edit Tag', 'vogue_' ),
    'update_item'       => __( 'Update Tag', 'vogue_' ),
    'add_new_item'      => __( 'Add New Tag', 'vogue_' ),
    'new_item_name'     => __( 'New Tag', 'vogue_' ),
    'menu_name'         => __( $label, 'vogue_' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => str_replace( '_', '-', COLLECTION ), 'hierarchical' => true )
  );

  register_taxonomy( 'vogue_collection', array( 'post', 'legacy', 'attachment' ), $args );
}


/* ============================================================================

  REGISTER SERIES TAXONOMY

============================================================================ */

add_action( 'init', 'register_series_taxonomy', 0 );

function register_series_taxonomy() {

  $labels = array(
    'name'              => _x( '#Series', 'vogue_collection', 'vogue_' ),
    'singular_name'     => _x( '#Series', 'vogue_collection', 'vogue_' ),
    'search_items'      => __( 'Search #Series', 'vogue_' ),
    'all_items'         => __( 'All Tags', 'vogue_' ),
    'parent_item'       => __( 'Parent Tag', 'vogue_' ),
    'parent_item_colon' => __( 'Parent Tag:', 'vogue_' ),
    'edit_item'         => __( 'Edit Tag', 'vogue_' ),
    'update_item'       => __( 'Update Tag', 'vogue_' ),
    'add_new_item'      => __( 'Add New Tag', 'vogue_' ),
    'new_item_name'     => __( 'New Tag', 'vogue_' ),
    'menu_name'         => __( '#Series', 'vogue_' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => false,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'series' )
  );

  register_taxonomy( 'series', array( 'post', 'legacy' ), $args );
}


/* ============================================================================

  REGISTER SERIES TAXONOMY

============================================================================ */

add_action( 'init', 'register_hide_post_taxonomy', 0 );

function register_hide_post_taxonomy() {

  $labels = array(
    'name'              => _x( 'Hide', 'hide', 'vogue_' ),
    'singular_name'     => _x( 'Hide', 'hide', 'vogue_' ),
    'search_items'      => __( 'Search Tags', 'vogue_' ),
    'all_items'         => __( 'All Tags', 'vogue_' ),
    'parent_item'       => __( 'Parent Tag', 'vogue_' ),
    'parent_item_colon' => __( 'Parent Tag:', 'vogue_' ),
    'edit_item'         => __( 'Edit Tag', 'vogue_' ),
    'update_item'       => __( 'Update Tag', 'vogue_' ),
    'add_new_item'      => __( 'Add New Tag', 'vogue_' ),
    'new_item_name'     => __( 'New Tag', 'vogue_' ),
    'menu_name'         => __( 'Hide', 'vogue_' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => false,
    'query_var'         => true,
    'rewrite'           => false,
    'public'            => true
  );

  register_taxonomy( 'hide_post', array( 'post', 'legacy' ), $args );
}


/* ============================================================================

  REGISTER FASHION PRIZE TAXONOMY

===============================================================================

  - This will be removed once we start using pages for fashion prize

============================================================================ */

// add_action( 'init', 'register_fashionprizecategory_taxonomy', 0 );

function register_fashionprizecategory_taxonomy() {

  $labels = array(
    'name'              => _x( 'Fashion Prize', 'vogue_collection', 'vogue_' ),
    'singular_name'     => _x( 'Vogue Collection', 'vogue_collection', 'vogue_' ),
    'search_items'      => __( 'Search Vogue Collection', 'vogue_' ),
    'all_items'         => __( 'All Tags', 'vogue_' ),
    'parent_item'       => __( 'Parent Tag', 'vogue_' ),
    'parent_item_colon' => __( 'Parent Tag:', 'vogue_' ),
    'edit_item'         => __( 'Edit Tag', 'vogue_' ),
    'update_item'       => __( 'Update Tag', 'vogue_' ),
    'add_new_item'      => __( 'Add New Tag', 'vogue_' ),
    'new_item_name'     => __( 'New Tag', 'vogue_' ),
    'menu_name'         => __( 'FP TO GO', 'vogue_' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => false,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'fashionprizecategory' )
  );

  register_taxonomy( 'fashionprizecategory', array( 'post', 'legacy' ), $args );
}


/* ============================================================================

  REGISTER VOICES TAXONOMY

============================================================================ */

// add_action( 'init', 'register_voices_taxonomy', 0 );

function register_voices_taxonomy() {
  $labels = array(
    'name'              => _x( 'Voices', 'vogue_collection', 'vogue_' ),
    'singular_name'     => _x( 'Voice', 'vogue_collection', 'vogue_' ),
    'search_items'      => __( 'Search Voices', 'vogue_' ),
    'all_items'         => __( 'All Tags', 'vogue_' ),
    'parent_item'       => __( 'Parent Tags', 'vogue_' ),
    'parent_item_colon' => __( 'Parent Tags:', 'vogue_' ),
    'edit_item'         => __( 'Edit Tag', 'vogue_' ),
    'update_item'       => __( 'Update Tag', 'vogue_' ),
    'add_new_item'      => __( 'Add New Tag', 'vogue_' ),
    'new_item_name'     => __( 'New Tag', 'vogue_' ),
    'menu_name'         => __( 'Voices', 'vogue_' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => false,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'voices_category' )
  );

  register_taxonomy( 'voices_category', array( 'post', 'legacy' ), $args );
}


function new_subcategory_hierarchy() {
    $category = get_queried_object();

    $parent_id = $category->category_parent;

    $templates = array();

    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );

        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";

        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php';
    }
    return locate_template( $templates );
}

add_filter( 'category_template', 'new_subcategory_hierarchy' );


/* ============================================================================

  REMOVE CATEGORIES AND TAGS FROM ADMIN DASHBOARD TO HELP IMPROVE PERFORMANCE

============================================================================ */

function my_manage_columns( $columns )
{

  unset( $columns[ 'categories' ], $columns[ 'tags' ] );
  return $columns;

}

function my_column_init()
{
  add_filter( 'manage_posts_columns' , 'my_manage_columns' );
}

add_action( 'admin_init' , 'my_column_init' );



/* ============================================================================

  ENQUEUE SCRIPTS AND STYLESHEETS

============================================================================ */

function wae_enqueue_scripts() {

// Import Stylesheet
  wp_enqueue_style( 'base_styles', get_stylesheet_directory_uri() . '/assets/css/main.css?v='.rand(0,100000) );


// Include jQuery
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'jquery-ui-autocomplete' );
  // wp_register_style( 'jquery-ui-styles','http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' );
  // wp_enqueue_style( 'jquery-ui-styles' );

// Include Modernizr
  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/javascript/lib/modernizr.js', array( 'jquery' ), false, false );

// Include Infinite scroll

  // wp_enqueue_script( 'infinite', get_template_directory_uri() . '/assets/javascript/lib/jquery.jscroll.min.js', array( 'jquery' ), false, false );

// Include theme javascript
  wp_register_script( 'gsap_js', '//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js', array( 'jquery' ), false, true );

  wp_register_script( 'jquery-visible', get_stylesheet_directory_uri() . '/assets/javascript/lib/jquery-visible.js', array( 'jquery' ), "1", true );

  wp_register_script( 'slick', get_stylesheet_directory_uri() . '/assets/javascript/lib/slick.js', array( 'jquery' ), "1", true );

  wp_register_script( 'jquery-easing', get_stylesheet_directory_uri() . '/assets/javascript/lib/jquery-easing.js', array( 'jquery' ), "1", true );

  wp_register_script( 'gsap-scrollTo', get_stylesheet_directory_uri() . '/assets/javascript/lib/gsap-scrollTo.js', array( 'jquery' ), "1", true );

  wp_register_script( 'echo', get_stylesheet_directory_uri() . '/assets/javascript/lib/echo.js', array( 'jquery' ), "1", true );

  wp_register_script( 'MatchHeight', get_stylesheet_directory_uri() . '/assets/javascript/lib/MatchHeight.js', array( 'jquery' ), "1", true );

  wp_register_script( 'Swiper', get_stylesheet_directory_uri() . '/assets/javascript/lib/Swiper.js', array( 'jquery' ), "1", true );

  wp_register_script( 'getScrollableObject', get_stylesheet_directory_uri() . '/assets/javascript/common/getScrollableObject.js', array( 'jquery' ), "1", true );

  wp_register_script( 'Globals', get_stylesheet_directory_uri() . '/assets/javascript/common/Globals.js', array( 'jquery' ), "1", true );

  wp_localize_script( 'Globals', 'Globals', array
  (
      'ajaxEndPoint' => admin_url( 'admin-ajax.php' )
  ) );

  wp_register_script( 'ox', get_stylesheet_directory_uri() . '/assets/javascript/common/ox.js', array( 'jquery' ), "1", true );

  wp_register_script( 'FadeIn', get_stylesheet_directory_uri() . '/assets/javascript/common/FadeIn.js', array( 'jquery' ), "1", true );

  wp_register_script( 'VogueHistory', get_stylesheet_directory_uri() . '/assets/javascript/common/VogueHistory.js', array( 'jquery' ), "1", true );

  wp_register_script( 'Newsletter', get_stylesheet_directory_uri() . '/assets/javascript/common/Newsletter.js', array( 'jquery' ), "3", true );

  wp_localize_script( 'Newsletter', 'Newsletter', array
  (
      'BAD_NAME_MESSAGE' => __('Please enter a valid name', 'vogue.me'),
      'NO_EMAIL_MESSAGE' => __('Please enter an email address', 'vogue.me'),
      'BAD_EMAIL_MESSAGE' => __('Please enter a valid email address', 'vogue.me'),
      'SUBMITTING_MESSAGE' => __('Sending<i class="ldot">.</i><i class="ldot">.</i><i class="ldot">.</i>', 'vogue.me'),
      'SUCCESS_MESSAGE' => __('Thank you!', 'vogue.me'),
      'ERROR_MESSAGE' => __('Sorry, an error has occurred.', 'vogue.me'),
  ) );

  wp_register_script( 'base_js', get_stylesheet_directory_uri() . '/assets/javascript/javascript.js?v='.rand(0,100000), array( 'jquery', 'modernizr' ), false, true );

  wp_localize_script( 'base_js', 'VGAR', array( 'tmpl_path' => trim( get_stylesheet_directory_uri(), '/' ) ) );


  wp_register_script( 'html5_shiv', get_stylesheet_directory_uri() . '/assets/javascript/lib/html5shiv-printshiv.js', array( 'jquery', 'modernizr' ), false, true );
  // wp_register_script( 'polyfills', get_stylesheet_directory_uri() . '/assets/javascript/lib/polyfills.js', array( 'jquery', 'modernizr' ), false, true );

  wp_register_script( 'EventHelpers', get_stylesheet_directory_uri() . '/assets/javascript/lib/EventHelpers.js', array( 'jquery', 'modernizr' ), false, true );
  wp_register_script( 'cssQuery', get_stylesheet_directory_uri() . '/assets/javascript/lib/cssQuery-p.js', array( 'jquery', 'modernizr' ), false, true );
  wp_register_script( 'sylvester', get_stylesheet_directory_uri() . '/assets/javascript/lib/sylvester.js', array( 'jquery', 'modernizr' ), false, true );
  wp_register_script( 'cssSandpaper', get_stylesheet_directory_uri() . '/assets/javascript/lib/cssSandpaper.js', array( 'jquery', 'modernizr' ), false, true );

  wp_script_add_data( 'html5_shiv', 'conditional', 'if gte IE 8' );
  // wp_script_add_data( 'polyfills', 'conditional', 'if gte IE 8' );

  wp_script_add_data( 'EventHelpers', 'conditional', 'if gte IE 8' );
  wp_script_add_data( 'cssQuery', 'conditional', 'if gte IE 8' );
  wp_script_add_data( 'sylvester', 'conditional', 'if gte IE 8' );
  wp_script_add_data( 'cssSandpaper', 'conditional', 'if gte IE 8' );

  wp_enqueue_script( 'gsap_js' );

  wp_enqueue_script( 'jquery-visible' );
  wp_enqueue_script( 'slick' );
  wp_enqueue_script( 'jquery-easing' );
  wp_enqueue_script( 'gsap-scrollTo' );
  wp_enqueue_script( 'echo' );
  wp_enqueue_script( 'MatchHeight' );
  wp_enqueue_script( 'Swiper' );
  wp_enqueue_script( 'getScrollableObject' );
  wp_enqueue_script( 'Globals' );
  wp_enqueue_script( 'ox' );
  wp_enqueue_script( 'FadeIn' );
  wp_enqueue_script( 'VogueHistory' );
  wp_enqueue_script( 'Newsletter' );

  wp_enqueue_script( 'base_js' );
  wp_enqueue_script( 'html5_shiv' );
  wp_enqueue_script( 'EventHelpers' );
  wp_enqueue_script( 'cssQuery' );
  wp_enqueue_script( 'sylvester' );
  wp_enqueue_script( 'cssSandpaper' );
  // wp_enqueue_script( 'polyfills' );

  wp_deregister_script( 'wp-embed.min.js' );
  wp_deregister_script( 'wp-embed' );
  wp_deregister_script( 'embed' );


}

add_action( 'wp_enqueue_scripts', 'wae_enqueue_scripts', 100 );


/* ============================================================================

  WORDPRESS MENUS

===============================================================================

  - Register custom menus

  - Create Custom Walker, strips out unnecessary classes

============================================================================ */


function register_my_menus() {
  register_nav_menus(
    array(
      'vogue_head' => __( 'Primary', 'vogue.me' ),
      'secondary'  => __( 'Secondary', 'vogue.me' ),
      'footer'  => __( 'Footer', 'vogue.me' )
    )
  );
}
add_action( 'init', 'register_my_menus' );



/* ============================================================================

  Custom Nav Walker

============================================================================ */

// class custom_walker extends Walker_Nav_Menu
// {

//   function start_lvl( &$output, $depth )
//   {

//     $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' );
//     $display_depth = ( $depth + 1);
//     $classes = array(
//       ( $display_depth ==1 ? 'sub-nav' : '' ),
//       ( $display_depth >=2 ? 'low-menu' : '' )
//     );

//     $class_names = implode( ' ', $classes );

//     $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";

//   }

//   function start_el( &$output, $item, $depth, $args )
//   {

//     global $wp_query;

//     $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' );

//     $classes = empty( $item->classes ) ? array() : (array) $item->classes;
//     if(in_array('current-menu-item',$classes) || in_array('current-menu-parent',$classes))
//     {
//       $class_names = 'active';
//     }

//     if(in_array('category-seating',$classes))
//     {
//       $class_names = 'category-seating';
//     }

//     $output .= $indent . '<li>';

//     $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
//     $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
//     $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
//     $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
//     $attributes .= ! empty( $class_names )        ? ' class="'   . esc_attr(  $class_names        ) .'"' : '';
//     $item_output = sprintf('%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',

//       $args->before,
//       $attributes,
//       $args->link_before,
//       apply_filters( 'the_title', $item->title, $item->ID ),
//       $args->link_after,
//       $args->after

//     );

//     $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

//   }

// }


// Register Custom Taxonomy
function register_country_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Countries', 'Countries', 'vogue_' ),
    'singular_name'              => _x( 'Country', 'country', 'vogue_' ),
    'menu_name'                  => __( 'Countries', 'vogue_' ),
    'all_items'                  => __( 'All Countries', 'vogue_' ),
    'parent_item'                => __( 'Parent Tag', 'vogue_' ),
    'parent_item_colon'          => __( 'Parent Tag:', 'vogue_' ),
    'new_item_name'              => __( 'New Tag', 'vogue_' ),
    'add_new_item'               => __( 'Add Tag', 'vogue_' ),
    'edit_item'                  => __( 'Edit Tag', 'vogue_' ),
    'update_item'                => __( 'Update Tag', 'vogue_' ),
    'view_item'                  => __( 'View Tag', 'vogue_' ),
    'separate_items_with_commas' => __( 'Separate tags with commas', 'vogue_' ),
    'add_or_remove_items'        => __( 'Add or remove tags', 'vogue_' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'vogue_' ),
    'popular_items'              => __( 'Popular Tags', 'vogue_' ),
    'search_items'               => __( 'Search Tags', 'vogue_' ),
    'not_found'                  => __( 'Not Found', 'vogue_' ),
  );

  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => false,
    'show_in_nav_menus'          => false,
    'show_tagcloud'              => false,
  );

  register_taxonomy( 'countries', array( 'legacy', 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'register_country_taxonomy', 0 );


if ( ! function_exists( 'in_category_child' ) ) {
    function in_category_child( $cats, $_post = null ) {
        foreach ( (array) $cats as $cat ) {
            // get_term_children() accepts integer ID only
            $descendants = get_term_children( (int) $cat, 'category' );
            if ( $descendants && in_category( $descendants, $_post ) )
                return true;
        }
        return false;
    }
}



function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_admin_head() {
  $css = '';

  $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';

  echo '<style type="text/css">'.$css.'</style>';
}
add_action('admin_head', 'custom_admin_head');


add_theme_support( 'post-thumbnails' );

add_image_size( 'single-post-thumbnail', 590, 180 );



function wpse64933_add_posttype_note()
{
    global $post, $pagenow;

    // Abort in certain conditions, based on the global $pagenow
    if ( ! in_array(
         $pagenow
        ,array(
             'post-new.php'
            ,'post.php'
         )
    ) )
        return;

    // Abort in certain conditions, based on the global $post
    if ( ! in_array(
         $post->post_type
        ,array(
             'post'
            ,'page'
         )
    ) )
        return;

    // You can use the global $post here
    echo '<div style="background-color: #cccccc;color: #000;font-size: 12px;line-height: 18px;width:75%;padding:10px;box-sizing:border-box;">
            <strong>Inline Galleries: </strong>To embed a gallery inline with the content use shortcode [embedgallery number="POST_ID"] with POST_ID being the id of the gallery post.<br />
            <strong>Inline Videos: </strong>To embed a video inline with the content use shortcode [embedvideo number="POST_ID"] with POST_ID being the id of the video post.<br /><br />
            <strong>Categories & Tags: Please Note: </strong>Any categories or tags; Vogue Collection, Standard Tags, Countries; that are added within a post will not have a compelete profile, make sure to update the profile by going here: <a href="' . get_bloginfo( 'url' ) . '/wp-admin/edit-tags.php?taxonomy=post_tag">Standard Tags</a>, <a href="' . get_bloginfo( 'url' ) . '/wp-admin/edit-tags.php?taxonomy=vogue_collection">Vogue Collection</a>, <a href="' . get_bloginfo( 'url' ) . '/wp-admin/edit-tags.php?taxonomy=countries">Countries</a>, <a href="' . get_bloginfo( 'url' ) . '/wp-admin/edit-tags.php?taxonomy=category">Categories</a></div>';
}
add_action( 'all_admin_notices', 'wpse64933_add_posttype_note' );


/* ============================================================================

  FUNCTION TO CHECK IF VALUES EXIST IN ARRAYS

============================================================================ */

function search_array( $needle, $haystack )
{
  if ( in_array( $needle, $haystack ) )
  {
    return true;
  }

  foreach ( $haystack as $element )
  {
    if ( is_array ( $element ) && search_array( $needle, $element ) ) return true;
  }

  return false;

}

$total_tags = 10;

/**
 * Extend get terms with post type parameter.
 *
 * @global $wpdb
 * @param string $clauses
 * @param string $taxonomy
 * @param array $args
 * @return string
 */
function df_terms_clauses( $clauses, $taxonomy, $args ) {
  if ( isset( $args['post_type'] ) && ! empty( $args['post_type'] ) && $args['fields'] !== 'count' ) {
    global $wpdb;

    $post_types = array();

    if ( is_array( $args['post_type'] ) ) {
      foreach ( $args['post_type'] as $cpt ) {
        $post_types[] = "'" . $cpt . "'";
      }
    } else {
      $post_types[] = "'" . $args['post_type'] . "'";
    }

    if ( ! empty( $post_types ) ) {
      $clauses['fields'] = 'DISTINCT ' . str_replace( 'tt.*', 'tt.term_taxonomy_id, tt.taxonomy, tt.description, tt.parent', $clauses['fields'] ) . ', COUNT(p.post_type) AS count';
      $clauses['join'] .= ' LEFT JOIN ' . $wpdb->term_relationships . ' AS r ON r.term_taxonomy_id = tt.term_taxonomy_id LEFT JOIN ' . $wpdb->posts . ' AS p ON p.ID = r.object_id';
      $clauses['where'] .= ' AND (p.post_type IN (' . implode( ',', $post_types ) . ') OR p.post_type IS NULL)';
      $clauses['orderby'] = 'GROUP BY t.term_id ' . $clauses['orderby'];
    }
  }
  return $clauses;
}

add_filter( 'terms_clauses', 'df_terms_clauses', 10, 3 );


function search_by_tags() {

  $term = strtolower( $_GET[ 'term' ] );

  $slug = str_replace( ' ', '-', $term );

  $tags = get_terms( array( 'post_type' => array( 'post' ), 'taxonomy' => 'post_tag', 'search' => $slug, 'hide_empty' => true, 'number' => 200, 'fields' => 'all'
 ) );

  $suggestions = array();

  $i = 0; foreach ( $tags as $tag ) {

    if ( $tag->count > 3 && strlen( $tag->name ) <= 20 /* TEMPORARY UNTIL TAGS HAVE BEEN CONSOLIDATED */ )
    {

      $suggestions[] = $tag->name;
      $i++; if ( $i >= 10 ) break;
    }

  }

  wp_reset_query();

  $response = json_encode( $suggestions );
  echo $response;
  exit();

}

function search_posts() {

  $term = strtolower( $_GET[ 'term' ] );
  $suggestions = array();

  $args = array( 'post_type' => array( 'post' ), 's' => $term, 'posts_per_page' => 20, 'date_query' => array(
      array(
          'column' => 'post_date_gmt',
          'after' => '6 months ago'
      )
  ) );

  $loop = new WP_Query( $args );

  $i = 0; while( $loop->have_posts() )
  {
    $loop->the_post();

    $default = get_bloginfo('template_url') . '/assets/images/vogue-thumb.png';

    if ( has_post_thumbnail( get_the_ID() ) ) {
      $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' );
      $thumbnail = $thumbnail[0];
    }
    else
    {
      $thumbnail = $default;
    }

    $handle = curl_init( $thumbnail );
    curl_setopt( $handle, CURLOPT_RETURNTRANSFER, TRUE );

    $response = curl_exec( $handle );
    $httpCode = curl_getinfo( $handle, CURLINFO_HTTP_CODE );

    if ( $httpCode == 404 || $httpCode == 500 )
    {
      $thumbnail = $default;
    }
    else
    {
      if ( $thumbnail == false || $thumbnail == '' )
      {
        $thumbnail = $default;
      }
      else
      {
        $thumbnail = $thumbnail;
      }

    }

    if ( is_null( $thumbnail ) )
    {
      $thumbnail = $default;
    }

    curl_close( $handle );

    $suggestion = array();
    $suggestion['label'] = get_the_title();
    $suggestion['link'] = get_permalink();
    $suggestion['thumbnail'] = $thumbnail;

    $key_topic = get_field( 'key_topic', get_the_ID() );
    $key_category = get_field( 'key_category', get_the_ID() );

    $posttags = wp_get_post_terms( get_the_ID() , 'post_tag' );
    $postcats = wp_get_post_terms( get_the_ID() , 'category' );

    if ( $key_category )
    {
      $key = $key_category->name;
      $slug = get_term_link( $key_topic->slug, 'category' );
    }
    elseif ( $key_topic )
    {
      $key = $key_topic->name;
      $slug = get_term_link( $key_topic->slug, 'post_tag' );
    }
    elseif ( $postcats )
    {

      $i = 0; foreach ( $postcats as $cat )
      {
        if ( strpos( $cat->slug, 'legacy' ) === false )
        {
          $key = $cat->name;
          $slug = get_term_link( $cat->slug, 'category' );
          break;
        }

        $i++;
      }

    }
    else
    {
      $key = $posttags[0]->name;
      $slug = get_term_link( $posttags[0]->slug, 'post_tag' );
    }

    $suggestion['key_topic_label'] = $key;
    $suggestion['key_topic_link'] = $slug;

    $suggestions[] = $suggestion;

  // LIMIT POSTS THAT APPEAR IN SEARCH
    $i++; if ( $i >= 25 ) break;
  }

  wp_reset_query();

  $response = json_encode( $suggestions );
  echo $response;
  exit();

}

function display_relevant_tags()
{

  $term = strtolower( $_GET[ 'term' ] );
  $suggestions = array();
  $tags = array();

  $find_tags = array();

  $args = array(

    's' => $term,
    'posts_per_page' => 6,

    'post_type' => array( 'post' )

  );

  $loop = new WP_Query( $args );

  while( $loop->have_posts() )
  {
    $loop->the_post();

    $suggestions[] = get_the_ID();
  }

  foreach ( $suggestions as $suggestion )
  {

    $tags[] = wp_get_post_tags( $suggestion );

  }

  $i = 1; foreach ( $tags as $tag )
  {

    foreach ( $tag as $a => $key )
    {

      if ( !search_array( $key->name, $find_tags ) )
      {
        $get_tags = array();

        $get_tags['label'] = $key->name;
        $get_tags['link'] = get_term_link( $key, 'post_tag' );

        $find_tags[] = $get_tags;
      }

    }

    $i++; if ( $i >= 10 ) break;

  }

  wp_reset_query();

  $response = json_encode( $find_tags );
  echo $response;
  exit();

}

function display_stylebase_tags()
{

  $term = strtolower( $_GET[ 'term' ] );
  $suggestions = array();
  $tags = array();

  $find_tags = array();

  $args = array(

    's' => $term,
    'posts_per_page' => 6,

    'post_type' => array( 'post' )

  );

  $loop = new WP_Query( $args );

  while( $loop->have_posts() )
  {
    $loop->the_post();

    $suggestions[] = get_the_ID();
  }

  foreach ( $suggestions as $suggestion )
  {

    $tags[] = wp_get_post_terms( $suggestion, 'vogue_collection' );

  }

  $i = 1; foreach ( $tags as $tag )
  {

    foreach ( $tag as $a => $key )
    {

      if ( !search_array( $key->name, $find_tags ) )
      {
        $get_tags = array();

        $get_tags['label'] = $key->name;
        $get_tags['link'] = get_term_link( $key, 'vogue_collection' );

        $profile_image = get_field( 'profile_image', COLLECTION . '_' . $key->term_id );

        if ( $profile_image )
        {
          $get_tags['profile_image'] = '<img src="' . $profile_image['url'] . '" alt="' . $key->name . '">';
        }
        else
        {
          $get_tags['profile_image'] = '';
        }

        $find_tags[] = $get_tags;
      }

    }

    $i++; if ( $i >= 6 ) break;

  }

  wp_reset_query();

  $response = json_encode( $find_tags );
  echo $response;
  exit();

}


function VogueCollectionSearch()
{ ?>

  <form class="search-form" action="#" id="search_vogue_collection">

    <div class="vogue_collection__input"> <input type="text" placeholder="<?php echo __('Search here','vogue.me'); ?>" id="searching_vogue"> </div>

    <button type="submit"> <?php inline_svg( 'va-search-white' ); ?> </button>

    <div id="vogue_collection__results" class="vogue-collection__results"></div>

  </form>

<?php }

function search_vogue_collection() {

  $term = strtolower( $_GET[ 'term' ] );

  $b = get_term_by( 'slug', 'brand', COLLECTION );
  $c = get_term_by( 'slug', 'celebrity', COLLECTION );
  $m = get_term_by( 'slug', 'model', COLLECTION );
  $d = get_term_by( 'slug', 'fashion-designer', COLLECTION );
  $i = get_term_by( 'slug', 'fashion-influencer', COLLECTION );

// BRAND TERM ID
  $b_id = $b->term_id;
// CELEBRITY TERM ID
  $c_id = $c->term_id;
// MODEL TERM ID
  $m_id = $m->term_id;
// DESIGNER TERM ID
  $d_id = $d->term_id;
// INFLUENCER TERM ID
  $i_id = $i->term_id;

  $exclude = array( $b_id, $c_id, $m_id, $d_id, $i_id );

  $suggestions = array();

  $terms = get_terms( array(

    'taxonomy' => COLLECTION,
    'hide_empty' => false,
    'number' => 10,
    'search' => $term,
    'exclude' => $exclude,
    'childless' => true

  ));

  $i = 1; foreach ( $terms as $term )
  {
    $suggestion = array();
    $suggestion['label'] = $term->name;
    $suggestion['link'] = get_term_link( $term->slug, COLLECTION );
    if ( isset( get_field( 'profile_image', COLLECTION . '_' . $term->term_id )['url'] ) )
    {
      $suggestion['thumbnail'] = '<img class="vogue-seach-item__image" src="' . get_field( 'profile_image', COLLECTION . '_' . $term->term_id )['url'] . '" alt="' . $term->name . '">';
    }
    else
    {
      $suggestion['thumbnail'] = '';
    }

    $suggestions[] = $suggestion;

    $i++; if ( $i > 10 ) break;
  }

  wp_reset_query();

  $response = json_encode( $suggestions );
  echo $response;
  exit();

}



add_action( 'init', 'my_ajax_init' );

function my_ajax_init() {
  add_action( 'wp_ajax_search_posts', 'search_posts' );
  add_action( 'wp_ajax_nopriv_search_posts', 'search_posts' );
}

add_action( 'init', 'my_ajax_no_geo_init' );
function my_ajax_no_geo_init() {
  add_action( 'wp_ajax_search_by_tags', 'search_by_tags' );
  add_action( 'wp_ajax_nopriv_search_by_tags', 'search_by_tags' );
}

add_action( 'init', 'my_ajax_relevant_tags' );
function my_ajax_relevant_tags() {
  add_action( 'wp_ajax_display_relevant_tags', 'display_relevant_tags' );
  add_action( 'wp_ajax_nopriv_display_relevant_tags', 'display_relevant_tags' );
}

add_action( 'init', 'my_ajax_stylebase_tags' );
function my_ajax_stylebase_tags() {
  add_action( 'wp_ajax_display_stylebase_tags', 'display_stylebase_tags' );
  add_action( 'wp_ajax_nopriv_display_stylebase_tags', 'display_stylebase_tags' );
}

add_action( 'init', 'my_ajax_search_vogue_collection' );
function my_ajax_search_vogue_collection() {
  add_action( 'wp_ajax_search_vogue_collection', 'search_vogue_collection' );
  add_action( 'wp_ajax_nopriv_search_vogue_collection', 'search_vogue_collection' );
}

function time_since($older_date, $newer_date = false){
  $chunks = array(
    'year'   => 60 * 60 * 24 * 365,
    'month'  => 60 * 60 * 24 * 30,
    'week'   => 60 * 60 * 24 * 7,
    // 'day'    => 60 * 60 * 24,
    'hour'   => 60 * 60,
    'minute' => 60,
    'second' => 1
  );

  $newer_date = ($newer_date == false) ? (time()+(60*60*get_option("gmt_offset"))) : $newer_date;
  $since = $newer_date - $older_date;

  foreach ($chunks as $key => $seconds)
    if (($count = floor($since / $seconds)) != 0) break;


        if ( $key == 'second' || $key == 'minute' || $key == 'hour' )
        {

          if ( $key == 'hour' && $count <= 23 ) // 48 original
          {
            $messages = array(
              'hour'   => _n('%s hour ago', '%sh ago', $count, 'vogue.me'),
              'minute' => _n('%s minute ago', '%sm ago', $count, 'vogue.me'),
              'second' => _n('%s second ago', '%ss ago', $count, 'vogue.me'),
            );
            return sprintf($messages[$key],$count);
          }
          elseif ( $key == 'second' || $key == 'minute' )
          {
            $messages = array(
              'hour'   => _n('%s hour ago', '%sh ago', $count, 'vogue.me'),
              'minute' => _n('%s minute ago', '%sm ago', $count, 'vogue.me'),
              'second' => _n('%s second ago', '%ss ago', $count, 'vogue.me'),
            );
            return sprintf($messages[$key],$count);
          }
          else
          {
            return get_the_date( 'F j, Y' );
          }

    }
    else
    {
      return get_the_date( 'F j, Y' );
    }

}


function getInstagramFeed( $username, $count = 20 )
{

  if ( $username )
  {

    $url = 'https://www.instagram.com/' . $username . '/media/';
    $ch = curl_init();
    $timeout = 1;

    $postion = '';

    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );

    $data = curl_exec( $ch );

    curl_close( $ch );

    $decode = json_decode( $data, true );

    $instaImagecount = count( $decode[ 'items' ] );

    if ( $instaImagecount > 1 )
    {
      echo '<div class="instagram--feed">';
      echo '<div class="instagram--label">' . __( 'Follow', 'vogue.me' ) . '<a href="https://instagram.com/' . $username . '" target="_blank">@' . $username . '</a>' . __( 'on Instagram', 'vogue.me' ) . '</div>';

      echo '<div class="instagram--items">';

      if ( $instaImagecount < $count )
      {
        $count = $instaImagecount;
      }

      for ( $i = 0; $i < $count; $i++ )
      {
        $thumbnail = $decode[ 'items' ][$i][ 'images' ][ 'low_resolution' ][ 'url' ];
        $linkcode = $decode[ 'items' ][$i][ 'code' ];
        $imagelink = 'https://www.instagram.com/p/' . $linkcode . '/';

        if ( $i < 5 )
        {
          $position = ' instagram--0' . $i . ' is-active';
        }
        else
        {
          $position = ' in-active';
        }

        echo '<div class="instagram--item' . $position . '"> <a href="' . $imagelink . '" target="_blank"> <img src="' . $thumbnail . '" alt="" /> </a> </div>';
      }
      echo '</div>';
      echo '</div>';

    }

  }

}

add_action( 'parse_query','changept' );
function changept() {
    if( is_category() && !is_admin() )
        set_query_var( 'post_type', array( 'post', 'legacy' ) );
    return;
}


// CHANGE DEFAULT WORDPRESS GALLERY TO USE SLICK AND NEW STYLES

add_filter( 'post_gallery', 'my_post_gallery', 10, 2 );

function my_post_gallery( $output, $attr )
{
    global $post;

    if ( isset( $attr[ 'orderby' ] ) )
    {
      $attr[ 'orderby' ] = sanitize_sql_orderby( $attr[ 'orderby' ] );

      if ( !$attr[ 'orderby' ] ) unset( $attr[ 'orderby' ] );
    }

    extract( shortcode_atts( array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval( $id );

    if ( 'RAND' == $order ) $orderby = 'none';

    if ( !empty( $include ) )
    {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if ( empty( $attachments ) ) return '';

    $output = '<div class="article--gallery">';
    $output .= '<div class="article--gallery--inner">';

    // Now you loop through each attachment
    $i = 1;

    foreach ( $attachments as $id => $attachment )
    {
        // Fetch the thumbnail (or full image, it's up to you)
        $thumb = wp_get_attachment_image_src($id, 'medium');
//      $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
        $img = wp_get_attachment_image_src( $id, 'full' );

        $attr = get_post_meta( $id ); // Get post meta by ID
        $get_post = get_post($id);
        $image_alt = get_post_meta( $id, '_wp_attachment_image_alt', true);
        $image_title = $get_post->post_title;
        $caption = $attachment->post_excerpt;

        $legacy = false;

        if ( isset( $attr['nrs_description'] ) ) :
          $legacy = true;
          $description = $attr['nrs_description'][0];
          $alt = $attr[ '_wp_attachment_image_alt' ][ '0' ];
          $thumb = $img[0];
          $image = $img[0];
        else :
          if ( $get_post->post_content )
          {
            $description = $get_post->post_content;
          }
          else
          {
            $description = $caption;
          }
          $alt = $image_alt;
          $thumb = $img[0];
          $image = $img[0];
        endif;

        $output .= '<div class="gallery--item">';
        $output .= '<div class="item--image"> <img data-lazy="' . $image . '" alt="' . $alt . '"> </div>';
        $output .= '<div class="item--content">';

        if ( $description != '' && ( strpos( $description, 'Buy it now' ) !== false || strpos( $description, 'US $' ) !== false || strpos( $description, 'amazon' ) !== false || strpos( $description, 'Shop here' ) !== false || strpos( $description, 'Shop now' ) !== false ) )
        {
          $output .= '<div class="make--shoppable"></div>';
        }
        else
        {
          $output .= '<header class="item--content__header">';
          $output .= '<div class="item--caption"> ' . $image_title . ' </div>';
          // $output .= '<div class="item--credit"> <strong>Photo:</strong><br /> Ygol Sharifi </div>';
          $output .= '<div class="gallery--change to--list"></div>';
          $output .= '</header>';
        }

        if ( $description )
        {
          $output .= '<section class="item--content__copy">';
          $output .= '<p>' . $description . '</p>';
          $output .= '<div class="clear"></div>';
          $output .= '</section>';
        }

        $output .= '</div>';
        $output .= '</div>';

        $i++;

    }

    $output .= '</div>';

    $output .= '<div class="article--gallery--meta">';

    if ( $description == '' && ( strpos( $description, 'Buy it now' ) !== true || strpos( $description, 'US $' ) !== true || strpos( $description, 'amazon' ) !== true || strpos( $description, 'Shop here' ) !== true || strpos( $description, 'Shop now' ) !== true ) )
    {
      $output .= '<div class="gallery--view--thumbs"></div>';
    }

    $output .= '<div class="gallery--position"> <span class="current--slide">1</span> <span class="position--seperator"></span> <span class="total--slides"></span> </div> <div class="gallery--navigation"></div> </div>';

    if ( $description == '' && ( strpos( $description, 'Buy it now' ) !== true || strpos( $description, 'US $' ) !== true || strpos( $description, 'amazon' ) !== true || strpos( $description, 'Shop here' ) !== true || strpos( $description, 'Shop now' ) !== true ) )
    {

      $output .= '<div class="article--gallery--grid--view">';

      foreach ( $attachments as $id => $attachment )
      {
          // Fetch the thumbnail (or full image, it's up to you)
          $img = wp_get_attachment_image_src($id, 'medium');
          $thumb = wp_get_attachment_image_src($id, 'thumbnail');
          $attr = get_post_meta( $id ); // Get post meta by ID

          if ( isset( $attr['nrs_description'] ) ) :
            $alt = $attr[ '_wp_attachment_image_alt' ][ '0' ];
            $thumb_image = $thumb[0];
            $image = $img[0];
          else :
            $alt = $image_alt;
            $thumb_image = $thumb[0];
            $image = $img[0];
          endif;

          $output .= '<div class="grid--item"> <a href="#"> <img data-echo="' . $image . '" src="' . get_bloginfo('template_url') . '/assets/images/V-Loading-Small.gif" alt="' . $alt . '" sizes="640px" srcset="' . $thumb_image . ' 150w"> </a> </div>';

      }

      $output .= '</div>';

    }

    $output .= '</div>';

  return $output;

}

function isSponsored( $id )
{

  $id = (int) $id;

  $isSponsored = get_field( 'sponsored', $id );

  if ( $isSponsored ) :

    $sponsored = get_post_meta( $id, 'sponsor_details' );

    $i = 0; foreach ( $sponsored as $detail )
    {
      $height = false;

      $name = get_post_meta( $id, 'sponsor_details_' . $i . '_sponsor_name', true );
      $logo = get_post_meta( $id, 'sponsor_details_' . $i . '_sponsor_logo', true );
      $maxh = get_post_meta( $id, 'sponsor_details_' . $i . '_max_logo_height', true );
      $link = get_post_meta( $id, 'sponsor_details_' . $i . '_slink', true );

      if ( $maxh > 0 )
      {
        $height = 'style="max-height: ' . $maxh . 'px;"';
      }

      $image = wp_get_attachment_url( $logo );
	  $blog_id = get_current_blog_id();
	  
	  
	  if($blog_id == 1){
      echo '<div class="post-sponsored" style="vertical-align: middle;"><span class="presented-by" >' . __('Presented by','vogue.me') . '</span>';
	  }else{
		  if(is_single()){
		  $margin_right = '37px';
		  }else{
			$margin_right = '0px';
		  }
		  
		 echo '<div class="post-sponsored"><span class="presented-by" style="padding-top: 1px; margin-right: '.$margin_right.'; margin-left: 5px;"> ' . __('تقدمة','vogue.me') . '</span>';
	  }
        echo '<a href="' . $link . '" target="_blank">';

        echo '<img ' . $height . ' src="' . $image . '" alt="' . $name . '">';

        echo '<span class="post-sponsored__name">' . $name . '</span>';

        echo '</a>';

      echo '</div>';

      $i++; if ( $i > 0 ) break;

    }

  endif;

}

function getPrefix( $get = false, $w = false, $h = false )
{
  if ( $get != false )
  {
    if ( $w && $h )
    {
      $prefix = '';
    }
    else
    {
      $prefix = $get . '/';
    }

    return $prefix;

  }

}


function imageProvider( $thumbnail = false, $w = false, $h = false, $c = '1' )
{
  $domain = $_SERVER['HTTP_HOST'];
  $width = $w;
  $height = $h;

  $prefix = '';

  $p_w = getPrefix('w', $w, $h );
  $p_h = getPrefix('h', $w, $h );

  if ( $w == false && $w != 0 )
  {
    $w = '/' . $p_w . '600';
  }
  else
  {
    if ( $w != 0)
    {
      $w = '/' . $p_w . $w;
    }
    else
    {
      $w = '';
    }
  }

  if ( $h == false )
  {
    $h = '';
  }
  else
  {
    $h = '/' . $p_h . $h;
  }

  $new_domain = false;

  if ( is_array($thumbnail) )
  {
    $new_domain = $thumbnail[0];
  }
  else
  {
    $new_domain = $thumbnail;
  }

  if ( strpos( $new_domain, 'image_provider' ) === false )
  {
    if ( $domain == 'localhost' )
    {
      $domain = $domain . '/vg';
    }
    else
    {
      $domain = $domain;
    }

    //if ( function_exists('exif_imagetype') )
       //{

            $ch = curl_init ($thumbnail);


            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERPWD, "voguearabia:b9dba1dc");
            curl_exec ($ch);

            $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
            if($content_type != 'image/gif'){
                $new_image = str_replace( $domain, $domain . '/image_provider'. $w . $h . '/' . $c . '/100/ffffff', $thumbnail );
            }else{
                 //$img_array = style_image_resize( $thumbnail, $width, $height);
                 //$new_image = $img_array['url'];
                 $new_image = $thumbnail;
            }
            //$new_image = str_replace( $domain, $domain . '/image_provider'. $w . $h . '/' . $c . '/100/ffffff', $thumbnail );

        /* if ( exif_imagetype($thumbnail) != IMAGETYPE_GIF )
         {
           $new_image = str_replace( $domain, $domain . '/image_provider'. $w . $h . '/' . $c . '/100/ffffff', $thumbnail );
         }
         else
         {
           $new_image = $thumbnail;
         }
       }
       else
       {
         $new_image = str_replace( $domain, $domain . '/image_provider'. $w . $h . '/' . $c . '/100/ffffff', $thumbnail );
       }
    */

  }
  else
  {
    $new_image = $thumbnail;
  }

  $thumbnail = $new_image;

  return $thumbnail;

}

function getFeedItem( $i, $post, $type = false )
{ setup_postdata($post);

  $post_time = time_since( abs( strtotime( get_the_date('Y-m-d H:i:s', get_the_ID()) . " GMT" ) ) );

  $key_topic = get_field( 'key_topic', get_the_ID() );
  $key_category = get_field( 'key_category', get_the_ID() );

  $posttags = wp_get_post_terms( $post , 'post_tag' );
  $postcats = wp_get_post_terms( get_the_ID() , 'category' );

  if ( get_field( 'feat_mobile_image' ) )
  {
    $mobile_image = wp_get_attachment_image_src( get_field('feat_mobile_image'), 'medium' );
    $mobile_image_mid = wp_get_attachment_image_src( get_field('feat_mobile_image'), 'full' );

    $mobile_image = $mobile_image[0];
    $mobile_image_mid = $mobile_image_mid[0];
  }

  if ( get_field( 'gif_hosted' ) ) :

      if ( is_array( get_field('gif_hosted') ) )
      {
        $image = get_field('gif_hosted');

        if ( isset( $image['sizes']['large'] ) )
        {
          $thumbnail = $image['sizes']['large'];
        }
        else
        {
          $thumbnail = $image['url'];
          $mobile_image = $thumbnail;
        }

      }
      else
      {
        $thumbnail = (int) get_field('gif_hosted');
        $thumbnail = wp_get_attachment_url($thumbnail);
      }

  // *** Commented *** We don't want poster images as item thumbnails

  // elseif ( get_field( 'poster_image' ) ) :

  //   if ( is_array( get_field('poster_image') ) )
  //   {
  //     $image = get_field('poster_image');

  //     if ( isset( $image['sizes']['large'] ) )
  //     {
  //       $thumbnail = $image['sizes']['large'];
  //     }
  //     else
  //     {
  //       $thumbnail = $image['url'];
  //     }

  //   }
  //   else
  //   {
  //     $thumbnail = (int) get_field('poster_image');
  //     $thumbnail = wp_get_attachment_url($thumbnail, 'large' );
  //   }

  else :
      $thumbnail = getThumbnail( $post )[0];
			//echo '<script>console.log("' .$thumbnail . '");</script>';
  endif;

  if ( is_numeric( $thumbnail ) ) :
    $img = wp_get_attachment_image_src($thumbnail, 'large');
    $thumbnail = $img[0];
  else :
    $thumbnail = $thumbnail;
  endif;

  if ( $key_category )
  {
    $key = $key_category->name;
    $slug = get_term_link( $key_topic->slug, 'category' );
  }
  elseif ( $key_topic )
  {
    $key = $key_topic->name;
    $slug = get_term_link( $key_topic->slug, 'post_tag' );
  }
  elseif ( $posttags )
  {
    $key = $posttags[0]->name;
    $slug = get_term_link( $posttags[0]->slug, 'post_tag' );
  }
  else
  {
    $key = $postcats[0]->name;
    $slug = get_term_link( $postcats[0]->slug, 'category' );
  }

  if ( !isset($mobile_image) )
  {
    $mobile_image = $thumbnail;
  }

  $x_375 = imageProvider( $thumbnail, 375 );
  $x_400 = imageProvider( $thumbnail, 400 );
  $x2_400 = imageProvider( $thumbnail, 800, 600 );
  $x_600 = imageProvider( $thumbnail, 600 );
  $x_800 = imageProvider( $thumbnail, 800 );
  $x_870 = imageProvider( $thumbnail, 870 );

?>

<?php if ( $type != 'video' ) : ?>
<li class="post--item <?php if ( $type == 'videos' ) : ?>video--item<?php else : ?><?php if ( $type == 'simple' ) : echo ' post--standard'; else : if ( get_field( 'large_feed_item' ) == true && $type != 'recommends' ) : echo ' post--full'; else : echo ' post--standard'; endif; endif; ?><?php endif; ?>">

    <div class="post--image">
      <?php

      // TO DO : FOR POST TYPE TAG : VIDEO / GALLERY - NOT SURE WHAT CLASS IF ANY IS RELEVANT FOR THE CORRECT ICON
          $class = '';
          if ( in_category( get_field( 'media_type' ) == 'gallery' || get_field( 'media_type' ) == 'video_plus' ) ) : $class = ' fa-video'; // VIDEO
          elseif ( get_field( 'media_type' ) == 'gallery' ) : $class = 'fa-photo'; // GALLERY
          endif;
      ?>

      <?php if ( $class != '' ) : ?><div class="post--type--tag fa<?php echo $class; ?>"></div><?php endif; ?> <a href="<?php echo get_permalink(); ?>">

      <picture class="auto-fade-in">
        <source srcset="<?php echo $x_870; ?>" media="(min-width: 801px)">
        <source srcset="<?php echo $x_600; ?>" media="(min-width: 300px)">
        <img data-echo="<?php echo $x_375; ?>" src="<?php bloginfo( 'template_url' ); ?>/assets/images/feed-placeholder.png" alt="">
      </picture>
    </a>
    </div>

<?php endif; ?>

    <div class="post--snippet">
      <div class="snippet--inner">
        <div class="post--meta">
          <?php if ( check_site( 'men', false ) ) : ?>
            <small class="tag--men men--full"><?php echo __('Men'); ?></small>
          <?php endif; ?>
					<?php if ($type != 'recommends') : ?>
          	<?php getKeyTopic( true ); ?>
          	<span class="post--time<?php if (strpos($post_time,'ago') === false ) : ?> no--time<?php endif; ?>">
              	<?php echo $post_time; ?> 
          	</span>
					<?php else : ?>
						<?php getKeyTopic( false ); ?>
					<?php endif; ?>
        </div>
        <h1 class="post--title"> <a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo get_the_title(get_the_ID()); ?></a> </h1>
        <?php if ( check_site( 'men', false ) ) : ?><small class="tag--men men--standard"><?php echo __('Men'); ?></small><?php endif; ?>

        <?php isSponsored( get_the_ID() ); ?>

      </div>
    </div>

<?php if ( $type != 'video' ) : ?>
</li>
<?php endif; ?>

<?php }


// ARCHIVE HEADER BG

function getArchiveHeaderBG( $id )
{

  if ( get_field( 'header_image', $id ) ) :

    if ( wp_is_mobile() ) :
      $header_image_id = get_field( 'header_image_mobile', $id );
      if ( $header_image_id == NULL ) :
        $header_image_id = get_field( 'header_image', $id );
      endif;
    else:
      $header_image_id = get_field( 'header_image', $id );
    endif;

    $header_bg = wp_get_attachment_image_src( $header_image_id, 'full' )[0];
    return $header_bg;

  else :

    return false;

  endif;

}


// ARCHIVE HEADER COLOR

function getArchiveHeaderCOLOR( $id )
{

  if ( get_field( 'header_text_colour', $id ) ) :

    $header_text_color = get_field( 'header_text_colour', $id );

    return $header_text_color;

  else :

    return false;

  endif;

}


if ( !is_admin() && !is_single() )
{
  function set_query_post_types( $wp_query ) {

    if ( is_author() || is_tag() || is_front_page() )
    {
      if ( $wp_query->is_main_query() && ! $wp_query->get( 'cat' ) )
      {
        $wp_query->set( 'post_type', array( 'legacy', 'post' ) );
      }

    }

  }

  add_action( 'pre_get_posts', 'set_query_post_types' );
}


function getNextPageLink( $paged )
{

  global $paged;

  $next_link = get_next_posts_link(); if ( $next_link ) : ?> <div class="next jscroll-next-parent"> <a href="<?php echo get_next_posts_page_link(); ?>"></a> </div> <?php endif;

}



function getNextPostLink( $id = false )
{

  $current_id = $id;
  /*
	if(get_current_blog_id() == '3' ) {
		$read = $_SESSION['read_ar'];
	} else {
		$read = $_SESSION['read'];
	}

  //$read = $_SESSION['read'];
  */
  //$dont_read[] = $current_id;
  /*
  if ( !in_array( $current_id, $read ) )
  {
    // var_dump('not in');
    $read[] = ( int ) $current_id;
  }
  
  
  $postcats = wp_get_post_terms( $current_id , 'category' );

  //$terms = get_the_terms( $current_id, 'series' );

  //$last_series = false;

  $today = getdate();

  
  $post_args =

    array( 'post_type' => array( 'post' ),
           'tax_query' => array(
              'taxonomy' => 'category',
              'field' => 'slug',
              'terms' => 'gallery'
            ),
           'date_query' => array( array( 'column' => 'post_date_gmt', 'after' => '1 year ago' ) ),
           'category__not_in' => getIgnore_Cats( array( 'runway', 'runway_ar', 'video', 'video_ar', 'uncategorized' ) ),
           'post__not_in' => $dont_read,
           //'orderby' => 'rand',
		   'order' => 'ASC',
           'posts_per_page' => 1
    );

  $post_query = new WP_Query( $post_args );

  $not_in = $dont_read;

  array_push($not_in, get_the_ID() );

  $i = 0; foreach ( $postcats as $cat )
  {
    if ( strpos( $cat->slug, 'legacy' ) === false )
    {
      $key = $cat->term_id;
      break;
    }

    $i++;
  }

  $get_next_post = array( 'post_type' => array( 'legacy', 'post' ),'date_query' => array(
        array(
            'column' => 'post_date_gmt',
            'after' => '1 year ago'
        )
    ), 'category__not_in' => getIgnore_Cats( array( 'runway', 'runway_ar', 'video', 'video_ar', 'uncategorized' ) ), 'category__in' => array( $key ), 'post__not_in' => $not_in, 'orderby' => 'rand', 'posts_per_page' => 1 );

  $get_posts = new WP_Query( $get_next_post );

  /*if ( $terms )
  {

    $_series_ids = array();

    $args = array(
      'post_type' => 'legacy',
      'tax_query' => array(
        array(
          'taxonomy' => 'series',
          'field' => 'id',
          'terms' => $terms[0]->term_id,
        )
      ),
      'category__not_in' => getIgnore_Cats( array( 'runway','video', 'video_ar' ) ),
      'post__not_in' => $dont_read
    );

    $series = new WP_Query( $args );

    while ( $series->have_posts() ) : $series->the_post(); setup_postdata( $post );

    $_series_ids[] = get_the_ID();

    endwhile; wp_reset_postdata(); wp_reset_query();

    $last = count($_series_ids);

    $position = array_search( $current_id, $_series_ids );

    $next = ( int ) $position + 1;

    if ( $next == $last )
    {
      $last_series = true;
    }

  }*/

  /*if ( $id != ( $count - 1 ) && $terms && $last_series == false )
  {

    $id = $_series_ids[$next];

  }*/
  if ( get_field( 'next_article' , $current_id ) )
  {
    $id = get_field( 'next_article', $current_id );
  }
  /* else if ( $postcats )
  {

    if ( !is_null( $get_post->post_count ) )
    {

      $i = 1; while ( $get_posts->have_posts() ) : $get_posts->the_post(); setup_postdata( $post );

        $id = get_the_ID();

        if ( $i > 1 ) break; $i++;

      endwhile; wp_reset_postdata(); wp_reset_query();

    }
    else
    {
      $i = 1; while ( $post_query->have_posts() ) : $post_query->the_post(); setup_postdata($post); $id = get_the_ID(); endwhile; /*if ( $i > 1 ) break; $i++;*/ wp_reset_query(); wp_reset_postdata();
    //}

  //}

 /* else
  {
    $i = 1; while ( $post_query->have_posts() ) : $post_query->the_post(); setup_postdata($post); $id = get_the_ID(); endwhile; /*if ( $i > 1 ) break; $i++;*/ wp_reset_query(); wp_reset_postdata();

  //}
  

  /*if ( !is_null($id) )
  {
    $id = $id;
  }
  else if ( !is_null( $get_post->post_count ) )
  {
    $i = 1; while ( $get_posts->have_posts() ) : $get_posts->the_post(); setup_postdata( $post );

      $id = get_the_ID();

      if ( $i > 1 ) break; $i++;

    endwhile; wp_reset_postdata(); wp_reset_query();

  }
  else
  {
    $i = 1; while ( $post_query->have_posts() ) : $post_query->the_post(); setup_postdata($post); $id = get_the_ID(); endwhile; /*if ( $i > 1 ) break; $i++;*/ wp_reset_query(); wp_reset_postdata();
  //}
  
  
  $catIds = getIgnore_Cats( array( 'runway_ar','runway','video','video_ar', 'uncategorized' ) );
	$prev_post = get_previous_post(false, $catIds);
	if (!empty( $prev_post )): ?>
		<?php echo '<div class="next jscroll-next-parent" style="display: none;"> <a data-title="' . $prev_post->post_title . '" href="' . get_permalink($prev_post->ID) . '"></a> </div>'; ?>
	<?php endif;

 /*$prev_post = get_adjacent_post( true,  $catIds, true, 'category' ); ?>
  <?php if ( is_a( $prev_post, 'WP_Post' ) ) { 
	echo '<div class="next jscroll-next-parent" style="display: none;"> <a data-title="' . get_the_title( $prev_post->ID ) . '" href="' . get_the_permalink( $prev_post->ID ) . '"></a> </div>';
  } */
  

 
}

function getVogueRecommendsEditorPosts( $IDS = false )
{
  $posts = get_field( 'vogue_recommends', HOME_ID );

  if ( $IDS == true )
  {
    $results = array();

    foreach ( $posts as $key => $value )
    {
      $results[] = $value->ID;
    }

  }
  else
  {
    $results = $posts;
  }

  return $results;
}


function getVideoCats()
{

  $cats = array( 'video', 'video_ar' );

  return $cats;

}


function getRunwayCats( $array = false )
{

if ( check_site( false, 'ar' ) )
{
  $runway = get_term_by( 'slug', 'runway_ar', 'category' );
}
else
{
  $runway = get_term_by( 'slug', 'runway', 'category' );
}

  $runway_sub = get_term_children( $runway->term_id, 'category' );

  $runway_cats = array();

  $runway_cats[] = $runway->term_id;

  foreach ( $runway_sub as $cat ) :
    $runway_cats[] = $cat;
  endforeach;

  return $runway_cats;

}


function get_social_url( $network, $account )
{

  $url = '';

  switch ( $network ) {
    case 'instagram':
        $url = 'https://instagram.com/';
        break;
    case 'twitter':
        $url = 'https://twitter.com/';
        break;
    case 'facebook':
        $url = 'https://facebook.com/';
        break;
    case 'pinterest':
        $url = 'https://pinterest.com/';
        break;
    case 'google':
        $url = 'https://plus.google.com/';
        break;
    case 'vimeo':
        $url = 'https://vimeo.com/';
        break;
  }

  $url .= $account;

  return $url;

}


function get_vogue_social( $extra = false, $class = false )
{

  $facebook = 'voguearabia';
  $instagram = 'voguearabia';
  $twitter = 'voguearabia';
  $google = '114089235630690160471';
  $vimeo = 'stylearabia';
  $pinterest = 'voguearabia';

  if ( check_site( false, 'ar' ) )
  {
    $facebook = 'voguealarabiya';
    $twitter = 'voguealarabiya';
  }

?>

  <ul<?php if ( $class !== false ) : echo ' class="' . $class . '"'; endif; ?>>
    <li><a href="<?php echo get_social_url('facebook',$facebook); ?>" class="fa fa-facebook" target="_blank"></a></li>
    <li><a href="<?php echo get_social_url('instagram',$instagram); ?>" class="fa fa-instagram" target="_blank"></a></li>
    <li><a href="<?php echo get_social_url('twitter',$twitter); ?>" class="fa fa-twitter" target="_blank"></a></li>
  <?php if ( $extra !== false ) : ?>
    <li><a href="<?php echo get_social_url('google',$google); ?>" class="fa fa-google-plus" target="_blank"></a></li>
    <li><a href="<?php echo get_social_url('vimeo',$vimeo); ?>" class="fa fa-vimeo" target="_blank"></a></li>
    <li><a href="<?php echo get_social_url('vimeo',$pinterest); ?>" class="fa fa-pinterest" target="_blank"></a></li>
  <?php endif; ?>
  </ul>

<?php

}


function get_video_categories()
{

  $video_cats = array();

  if ( check_site( false, 'ar' ) )
  {
    $video_cat = get_term_by( 'slug', 'video_ar', 'category' );
  }
  else
  {
    $video_cat = get_term_by( 'slug', 'video', 'category' );
  }

  if ( $video_cat )
  {
    $video_id = (int) $video_cat->term_id; // INTEGER / PUSH

    array_push( $video_cats, $video_id );

    $video_cat_children = get_term_children( $video_id, 'category' ); // ARRAY / MERGE

    if ( $video_cat_children )
    {
        $video_cats = array_merge( $video_cats, $video_cat_children);
    }

    $fashion_prize_videos = get_term_by( 'name', 'fpvideos', 'category' );

    if ( $fashion_prize_videos )
    {
      $fashion_prize_id = (int) $fashion_prize_videos->term_id; // INTERGET / PUSH

      array_push( $video_cats, $fashion_prize_id );
    }

  }

  return $video_cats;

}



if ( function_exists( 'acf_add_options_page' ) ) {

  acf_add_options_page( array(

    'page_title'  => 'Options',
    'menu_title'  => 'Options',
    'menu_slug'   => 'general-options',
    'capability'  => 'edit_posts',
    'redirect'    => false

  ));

}


function randomString($length = 6) {
  $str = "";
  $characters = array_merge(range('A','Z'), range('a','z') );
  $max = count($characters) - 1;
  for ($i = 0; $i < $length; $i++) {
    $rand = mt_rand(0, $max);
    $str .= $characters[$rand];
  }
  return $str;
}



function getFallbackMeta( $id, $key ) {

  $meta = get_post_meta( $id, $key );

  if ( count( $meta )== 0 ) {
    $post_id_opp = icl_object_id( $id, "post", true, otherLanguage() );
    $meta = get_post_meta( $post_id_opp, $key );
  }

  if ( count($meta)==0 ) return false;

  return $meta[0];
}

function getNewGalleryDataRunway( $galleryid ) {

    $res = new stdClass;
  $res->id = $galleryid;
  $res->title = get_the_title( $galleryid );
  $res->link = get_the_permalink( $galleryid );
  $previewpic = get_sub_field('gallery_cover_image', $galleryid);
  $previewpicA = wp_get_attachment_image_src($previewpic['id'], "full");
  $previewpicURL = $previewpicA[0];
  $res->preview = $previewpicURL;
  //$res->collection_title = get_field('collection_title' , $galleryid);
  //$res->credits = get_field('credits' , $galleryid);


  $res->pictures = array();
  $total_count = count(get_field('gallery_image_uploader', $galleryid));
    //while(has_sub_field("gallery_image_uploader" , $galleryid)):
    //$ids = $attachment_id['id'];
    //endwhile;
    // caching it so php does not have to make multiple calls
    //$cache = get_posts(array('post_type' => 'attachment', 'numberposts' => -1, 'post__in' => $ids));

    $counter = 1;
    while(has_sub_field("gallery_image_uploader" , $galleryid)):

    $pict = new stdClass;

    $slideTitle = get_sub_field('image_title_runway', $galleryid);
    if (($slideTitle == "look number") || (empty($slideTitle))){
      $slideTitle = "Look " . $counter . " of " . $total_count;
    }
    $pict->title = $slideTitle;
    //$pict->description = get_sub_field('image_html_description', $galleryid);
    $attachment_id = get_sub_field('image', $galleryid);
    $image = wp_get_attachment_image_src($attachment_id['id'], "full");
    $imageUrl = $image[0];
    $pict->imagepath = $imageUrl;

    array_push( $res->pictures, $pict );
    $counter++;
      endwhile;

  return $res;

}



/* ============================================================================

  INLINE SVG FUNCTIONS

===============================================================================


  /**
   * Include inline SVG
   */
  function inline_svg( $filename, $alt = null, $key = null, $return = false ) {
    $file = STYLESHEETPATH . '/assets/images/' . $filename . '.svg';

    if ( file_exists( $file ) ) {
      $svg = file_get_contents( $file );

      // Clean SVG
      $cleaned_svg = aw_clean_svg( $svg, $key );
      if ( $cleaned_svg ) {
        $output = '<span class="svg svg-' . $filename . ($key?' svg-' . $key:'') .'">';
        $output .= $cleaned_svg;
        $output .= '<span class="alt sr-only">' . $alt . '</span>';
        $output .= '</span>';

        if ( $return )
          return $output;
        else
          echo $output;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   * Clean SVG file
   */
  $svg_key = 1;
  function aw_clean_svg( $svg, $key = null ) {
    global $svg_key;

    // Keep only <svg> tag
    preg_match( '~<svg(.*?)</svg>~si', $svg, $matches );

    if ( $matches ) {
      $cleaned_svg = $matches[0];
      if ( $key == null )
        $key = 'svg'.$svg_key.'-';
      else
        $key .= '-';

      // Unique ID
      preg_match_all( '~id="(.*?)"~i', $cleaned_svg, $matches );
      foreach ( $matches[1] as $old_id ) {
        $new_id = $key.$old_id;

        $cleaned_svg = str_replace( '"'.$old_id.'"', '"'.$new_id.'"', $cleaned_svg );
      }

      // Add <defs>
      $cleaned_svg = str_replace( '<radialGradient', '<defs><radialGradient', $cleaned_svg );
      $cleaned_svg = str_replace( '</radialGradient>', '</radialGradient></defs>', $cleaned_svg );

      $cleaned_svg = preg_replace('/<g>\r\n<\/g>/', '', $cleaned_svg);
      $cleaned_svg = preg_replace('/\r\n\r\n/', '', $cleaned_svg);

      $svg_key++;
      return $cleaned_svg;
    }

    return false;
  }


function _get_shortcodes( $the_content, $sc ) {

    $shortcode = "";
    $pattern = get_shortcode_regex();
    preg_match_all('/'.$pattern.'/uis', $the_content, $matches);

    for ( $i=0; $i < 40; $i++ ) {

        if ( isset( $matches[0][$i] ) ) {

            if ( strpos( $matches[0][$i], 'embedgallery' ) || strpos( $matches[0][$i], 'gallery' ) )
            {
              $shortcode .= $matches[0][$i];
            }

        }

    }

    return $shortcode;

}

function getNewGalleryDataRunwayneededinfo( $galleryid ) {

  $res = new stdClass;
  $res->id = $galleryid;
  $previewpic = get_sub_field('gallery_cover_image', $galleryid);

  //$previewpicA = wp_get_attachment_image_src($previewpic['id'], "full");
  $previewpicURL = $previewpic['url'];
  if(empty($previewpicURL)){
    /*$url = wp_get_attachment_url( get_post_thumbnail_id($galleryid), 'thumbnail' );
    var_dump($url);exit;
    $post_th = the_post_thumbnail('thumbnail');
    $temp = get_sub_field("gallery_image_uploader" , $galleryid);
    $attachment_id = get_sub_field('image', $galleryid);
    $previewpicURL = $attachment_id['url'];
    var_dump($post_th);
    //var_dump($attachment_id);
    //var_dump($previewpicURL);
    exit;*/
    while(has_sub_field("gallery_image_uploader" , $galleryid)):
    $attachment_id = get_sub_field('image', $galleryid);
    //print_r($attachment_id);
    //$previewpicA = wp_get_attachment_image_src($attachment_id['id'], "full");
    $previewpicURL = $attachment_id['url'];
    break;
    endwhile;
  }
  $res->preview = $previewpicURL;
  return $res;
}

function fjarrett_get_attachment_id_by_url( $url ) {
  // Split the $url into two parts with the wp-content directory as the separator
  $parsed_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );
  // Get the host of the current site and the host of the $url, ignoring www
  $this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
  $file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );
  // Return nothing if there aren't any $url parts or if the current host and $url host do not match
  if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
    return;
  }
  // Now we're going to quickly search the DB for any attachment GUID with a partial path match
  // Example: /uploads/2013/05/test-image.jpg
  global $wpdb;
  $attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parsed_url[1] ) );
  // Returns null if no attachment is found
  return $attachment[0];
}

function get_runway_legacy_thumbnail( $new_gallery )
{
  $new_gallery_id = $new_gallery->ID;
  $gallery_data = getNewGalleryDataRunwayneededinfo( $new_gallery_id );
  $hasgallery = true;
  $galleryType = "newtype";
  $previewImage = $gallery_data->preview;
  $previewURL =  parse_url($previewImage);
  $filepath = $previewURL['path'];
  $posterImage = "/image_provider/w/215/1/90/ffffff/" . $filepath;

  $image_id = fjarrett_get_attachment_id_by_url($previewImage );
  return wp_get_attachment_image_src($image_id,'large')[0];
}



// STRIP INLINE GALLERY SHORTCODES
function strip_gallery_shortcode( $content, $gallery )
{
  $replaces = '';

  $text = $content;

  $output = preg_replace( '/\[[^\[]*' . $gallery . '[^\]]*\]/i', '', $text );

  return $output;

}

// USED TO MOVE INLINE GALLERIES TO FEATURED AREA
function get_stripped_content( $content, $remove = false )
{

  if ( $remove !== false )
  {
    $content = strip_gallery_shortcode($content, $remove);
  }

  else
  {
    $content = $content;
  }

  $content = apply_filters('the_content', $content );

  return $content;
}


// SORTS ACF FIELDS BACKEND
if ( is_admin() )
{

  function acf_relationship_result_query( $args, $field, $post )

   {

    $args['post_status'] = array('publish');

    $args['orderby'] = 'date';

    $args['order'] = 'DESC';

    return $args;

   }

   add_filter('acf/fields/relationship/query', 'acf_relationship_result_query', 10, 3);
}


// USED TO SAVE VIEW COUNT
if ( !is_admin() )
{
  add_action( 'wp_ajax_save_my_data', 'acf_form_head' );
  add_action( 'wp_ajax_nopriv_save_my_data', 'acf_form_head' );
}


if ( !is_admin() )
{


// Add data-src to inline-images
function va_image_data_src( $content ) {
  if ( ! preg_match_all( '/<img [^>]+>/', $content, $matches ) ) {
    return $content;
  }

  $selected_images = $attachment_ids = array();

  foreach( $matches[0] as $image ) {
    if ( preg_match( '/wp-image-([0-9]+)/i', $image, $class_id ) &&
      ( $attachment_id = absint( $class_id[1] ) ) ) {

      /*
       * If exactly the same image tag is used more than once, overwrite it.
       * All identical tags will be replaced later with 'str_replace()'.
       */
      $selected_images[ $image ] = $attachment_id;
      // Overwrite the ID when the same image is included more than once.
      $attachment_ids[ $attachment_id ] = true;
    }
  }

  foreach ( $selected_images as $image => $attachment_id ) {
    $image_meta = get_post_meta( $attachment_id, '_wp_attachment_metadata', true );

    $full_image = wp_get_attachment_image_src( $attachment_id, 'full' );
    $attr = 'data-src="'.$full_image[0].'"';
    $new_image = preg_replace( '/<img ([^>]+?)[\/ ]*>/', '<img $1' . $attr . ' />', $image );

    $content = str_replace( $image, $new_image, $content );
  }

  return $content;
}
add_filter( 'the_content', 'va_image_data_src' );

}


// Store address submitted via Newsletter form

add_action( 'init', 'storeAddressAjaxInit' );
function storeAddressAjaxInit()
{
  add_action( 'wp_ajax_storeAddress', 'storeAddress' );
  add_action( 'wp_ajax_nopriv_storeAddress', 'storeAddress' );
}

function storeAddress()
{
    // Validation
    if (!isset($_POST['email'])) exit("No email");

    if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $_POST['email']))
    exit("Bad email");

    // Import MailChimp library
    require_once('newsletter-MCAPI.class.php');

    // API Key
    $api = new MCAPI('9e1d52c282cd73ed62f0b6d1686919bf-us5');
    
    // List's Unique Id
    $list_id = "8b3c5cb32e";
    
    if ( $_POST['lang'] == "ar" )
    $merge_vars = array( 'GROUPINGS'=>array( array('name'=>'Language', 'groups'=>'Arabic') ) );
    else
    $merge_vars = array( 'GROUPINGS'=>array( array('name'=>'Language', 'groups'=>'English') ) );

    $merge_vars['FNAME'] = $_POST['name'];

    // Require library and make call
    $apiCallResult = $api->listSubscribe($list_id, $_POST['email'], $merge_vars, "html", false, true, false, false);
    
    //var_dump( $api->listInterestGroupings($list_id) );

    // Respond
    if ($apiCallResult === true)
    {
        // It worked! 
        exit("success");
    }
    else
    {
        error_log($api->errorMessage);

        exit("error");
        exit("error<br><br>" . $api->errorMessage);
    }
  
}


// INCLUDE FUNCTIONS

include_once( 'custom-shortcode.php' );

include_once( 'functions-page_ads.php' );

include_once( 'functions-article-components.php' );

include_once( 'functions-section-breaks.php' );

include_once( 'functions-account-area.php' );

include_once( 'functions-post-page.php' );

include_once( 'functions-resize.php' );

include_once( 'functions-post-count.php' );
