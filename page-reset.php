<?php if ( is_user_logged_in() ) : if ( current_user_can( 'publish_posts' ) ) : header( 'location:' . get_bloginfo( 'url' ) . '/wp-admin' ); else : header( 'location:' . get_bloginfo( 'url' ) . '/account' ); endif; endif;

  /* Template Name: Account - Reset */

  get_header();

  // TO DO: Fix password reset issue

?>

<section class="hero-banner">
  <div class="background" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/herobanner-runway.jpg');"></div>
</section>

<div class="scroll" data-ui="jscroll-default">

  <div class="get--content">

    <div id="account-header">
      <div id="ah-inner">
        <h1><?php echo __( 'Reset Password', 'vogue.me' ); ?></h1>
      </div>
    </div>

    <div id="account-details">
    
    <?php /* if ( !empty($_GET['action'] ) && !empty($_GET['success'] ) ) : ?>

      <span class="welcome">Check email</span>
      <span class="welcome">

</span>

    <?php else : ?>

      <span class="welcome">Create new password</span>

      <form name="resetpasswordform" action="<?php echo site_url('wp-login.php?action=resetpass', 'login_post') ?>" method="post">
          
        <?php
          $rp_login = '';
          $rp_key = '';
          $rp_cookie = 'rp-'.COOKIEHASH;
          if ( isset( $_COOKIE[ $rp_cookie ] ) && 0 < strpos( $_COOKIE[ $rp_cookie ], ':' ) ) {
            list( $rp_login, $rp_key ) = explode( ':', wp_unslash( $_COOKIE[ $rp_cookie ] ), 2 );
          } ?>
          <input type="hidden" name="rp_key" value="<?php echo esc_attr( $rp_key ); ?>">
          <input type="hidden" name="rp_login" value="<?php echo esc_attr( $rp_login ); ?>">

          <div class="acc-hold left">
              <label for="pass1">New Password</label>
              <input class="text-input acc-text" name="pass1" type="password" id="pass1">
          </div>
          <div class="acc-hold right">
              <label for="pass2">Confirm Password</label>
              <input class="text-input acc-text" name="pass2" type="password" id="pass2">
          </div>

          <input type="hidden" name="redirect_to" value="<?php echo site_url( '/account/login/' ) . '?action=resetpass&success=1'; ?>">
          <button type="submit" name="wp-submit" id="wp-submit" class="acc-submit"><span class="acc-arrow">Reset Password</span></button>
      </form>

    <?php endif; */ ?>

    </div>

  </div>

</div>

<?php get_footer(); ?>