<?php if ( is_single() || is_tax('vogue-collection') ) : acf_form_head(); endif; ?>
<!DOCTYPE html>
<html<?php if ( check_site( false, 'ar' ) ) : echo ' dir="rtl" lang="ar-SY"'; else : echo ' lang="en-GB"'; endif; ?>>
  <head>

    <meta http-equiv="Cache-control" content="public">
		<meta http-equiv="refresh" content="180">
    <meta charset="UTF-8">

    <title><?php wp_title(); ?></title>

    <?php
        if(is_single() && get_post_type() == 'legacy')
        {
            $runwaycat = getRunwayCats();

            $categories    = get_the_category( get_the_ID());

            $catagoryids = array();
            foreach($categories as $catagory){
                $catagoryids[] = $catagory->cat_ID; 
            }

            if ((bool)array_intersect($catagoryids, $runwaycat) == false){
                ?>
                <!-- it is false -->
                <meta name="robots" content="noindex">
                <?php 
            }
        }
    ?>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Vogue Arabia">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width = device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, minimal-ui">
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="apple-mobile-web-app-capable" content="yes" />


    <meta name="apple-mobile-web-app-title" content="Vogue Arabia">
    <meta name="application-name" content="Vogue Arabia">
    <?php $touch_icon_dir = get_bloginfo( 'template_url' ) . '/assets/images/touch-icons/'; ?>
    <link rel="apple-touch-icon" href="<?php echo $touch_icon_dir; ?>apple-touch-icon-precomposed.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo $touch_icon_dir; ?>android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $touch_icon_dir; ?>favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $touch_icon_dir; ?>favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $touch_icon_dir; ?>favicon-16x16.png">
    <link rel="shortcut icon" href="<?php echo $touch_icon_dir; ?>favicon.ico" type="image/x-icon">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $touch_icon_dir; ?>ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>


    <?php if ( check_site(false,'ar') ) : ?>
      <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/fonts-ar.css">
    <?php endif; ?>

    <script src="https://use.typekit.net/obs0tze.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

  </head>
  <?
    $extra_class = '';

    if (wp_is_mobile()) { $extra_class .= 'mobile '; }

    $extra_class .= check_site('men', false ) ? 'vogue-men' : 'vogue-women';

  ?>

  <body <?php body_class( $extra_class ); ?> >

    <div class="site-head <?php if (is_front_page() ) : ?>site-head--home<?php endif; ?>">
      <div class="site-head__inner">
        <div class="top-branding top-branding--header">
          <a href="<?php bloginfo( 'url' ); ?>"> <?php inline_svg( 'vogue_g_b' ); ?> </a>
        </div>
        <button type="button" class="site-head__toggle site-head__toggle--nav js-toggle-side-nav">
          <?php inline_svg( 'va-menu' ); ?>
        </button>

        <?php if ( has_nav_menu('vogue_head') ) : ?>

        <?php $menu_args = array(
              'theme_location'=>'vogue_head',
              'container'     => 'nav',
              'container_class' => 'site-head__nav site-nav',
              'menu_class' => false,
              'menu_id'=>false,
              'echo'        => true,
              // 'walker' => new custom_walker(),
              'items_wrap'    => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          ); ?>

          <?php wp_nav_menu( $menu_args ); ?>
        <?php endif; ?>

        <?php if ( is_front_page() ): ?>
        <div class="site-head-social--small">
          <?php get_vogue_social(); ?>
        </div>

        <div class="site-head-language">
          <?php var_dump(WOMEN_AR); ?>
        <?php if ( check_site( 'women', 'en' ) ) : ?>
          <a href="<?php echo get_site_url( WOMEN_AR ); ?>?l">العربية</a>
        <?php elseif ( check_site( 'men', 'en' ) ) : ?>
          <a href="<?php echo get_site_url( MEN_AR ); ?>?l">العربية</a>
        <?php elseif ( check_site( 'women', 'ar' ) ) : ?>
          <a href="<?php echo get_site_url( WOMEN_EN ); ?>?l">English</a>
        <?php else : ?>
          <a href="<?php echo get_site_url( MEN_EN ); ?>?l">English</a>
        <?php endif; ?>
        </div>
        <?php endif; ?>

        <div class="site-head-social js-site-head-social">
          <div class="site-head-social__lines js-site-head-social-lines"></div>
          <div class="site-head-social__title js-site-head-social-title">
            <?php echo __( 'Follow Vogue Arabia', 'vogue.me' ); ?>
            <span class="js-site-head-social-title-arrow"></span>
          </div>
          <div class="site-head-social__inner js-site-head-social-inner">
            <?php get_vogue_social(); ?>

            <a href="/subscribe/" class="button black"><?php echo __( 'Newsletter', 'vogue.me' ); ?></a>
          </div>
        </div>

        <button type="button" class="site-head__toggle site-head__toggle--search js-toggle-search js-search js-toggle-search--site-head">
            <span class="search-closed"><?php inline_svg( 'va-search' ); ?></span>
            <span class="search-open"><?php inline_svg( 'va-close' ); ?></span>
         </button>
      </div>
    </div>

    <?php if(is_front_page()) { ?>
      <div class="top-branding top-branding--home<?php //if ( get_field( 'no_overlay', HOME_ID ) != true ) : no-overlay php endif; ?> js-top-tranding--home">
        <a href="<?php bloginfo( 'url' ); ?>"> <?php inline_svg( 'vogue_g_b' ); ?> </a>

      <?php /* Nicks 'Fixed' Code
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 896.9 256.3"><style></style><defs class="undefined"><rect width="896.9" height="256.3" class="undefined"/></defs><clipPath class="undefined"><use xlink:href="#SVGID_1_" class="undefined"/></clipPath><path class="vogue_w" d="M7.51 20.81h77.32c0.45 0 0.31 2.28 0 2.28H68.87c0 0-4.46-0.19-3.13 2.28 0.27 0.5 54.62 149.14 54.62 149.14s0.95 2.56 1.71 2.85c0 0 48.3-149.24 48.35-149.38 0.49-1.48 2.21-4.8-0.76-4.8h-17.48c-0.31 0-0.31-2.28 0-2.28h41.61c0.31 0 0.31 2.28 0 2.28h-15.48c-4.25-0.08-4.28 1.23-5.03 3.42 -0.38 1.11-67.26 209.56-67.26 209.56s-0.3-0.91-0.57-1.71C77.18 152.65 33.29 38.84 29.83 26.7c-1.04-3.64-1.57-3.52-3.7-3.51 -0.76 0-18.07 0-18.62 0C6.96 23.18 7.03 20.81 7.51 20.81" /><path class="vogue_w" d="M208.77 125.79c0-59.65 11.45-108.41 53.75-108.39 45.48 0.02 54.42 50.78 54.42 108.4 0 59.65-8.68 111.11-54.04 111.06C214.21 236.8 208.77 188.49 208.77 125.79M262.06 238.57c45.12 0.16 93.95-49.81 94.07-111.63 0.12-64.75-46.81-110.87-93.3-111.25 -47.57-0.39-93.21 51.85-93.21 111.25C169.61 188.38 212.74 238.4 262.06 238.57" /><path class="vogue_w" d="M483.84 146.54h17.66c0 0 3.28-0.35 3.25 1.24 0 0.1 0 50.51 0 50.51 0 38.85-38.72 39.29-49.67 38.05 -40.48-4.58-41.33-88.16-41.53-110.11C413 66.58 422.37 14.62 463.1 16.69c48.37 2.45 61.02 71.75 61.79 74.86 1.61 0.57 1.63-0.76 1.63-0.76l0.1-75.61c0 0-0.33-0.64-1.29 0.25 -0.34 0.32-0.74 0.72-1.19 1.19 -21.4 21.87-32.11-1.01-63.21-2.01C422.6 13.36 375 66.47 374.39 127.76c-0.61 61.66 39.88 109.18 84.14 110.87 24 0.91 23.48-5.77 53.1-14.94 18.1-5.61 26.64 6.59 27.3 11.65 1.15 0.77 1.24-0.57 1.24-0.57v-86.69c0-1.72 2.39-1.43 2.39-1.43h16.32c0.41 0 0.41-2.39 0-2.39h-75.14C483.19 144.25 483.19 146.54 483.84 146.54" /><path class="vogue_w" d="M880.64 20.78v77.67c0 0.38-1.52 0.47-1.52 0 0-0.48 1.43-75.47-68.19-75.47h-26.1c-1.46 0-2.34 0.26-2.34 1.38v97.03c0 0.91 0.91 1.4 2.3 1.4h8.73c41.43 0 38.09-41.57 38.09-42.05 0-0.48 1.62-0.38 1.62 0 0 0.38 0.09 87.28 0.09 87.66 0 0.38-1.23 0.48-1.23 0 0-0.47-0.95-43.33-39.52-43.33h-7.77c-1.34 0-2.3 0.5-2.3 1.42v104.41c0 0.73 0.89 1.41 2.34 1.41 5.84 0.02 20.88 0.06 25.05 0.07 74.82 0.19 73.81-80.12 73.81-80.51 0-0.38 1.9-0.38 1.9 0v83.19H727.71c-0.47 0-0.43-2.29 0-2.29h15.78c1.4 0 2.31-0.48 2.31-1.4 0-18.1 0.03-205.36 0-206.92 -0.02-1.34-1.75-1.43-2.31-1.43s-34.94 0-34.94 0 -2.28 0.02-2.28 1.33c0 14.72-0.01 138.97 0 156.65v0.08c0.09 43.81-33.59 58.1-57.81 58.5l-0.04-0.07c-28.5 0.15-79.7-4.66-79.7-67.82 0 0-0.13-143.73-0.19-145.72 -0.09-3.13-3.98-2.97-3.98-2.97s-16.2 0.03-16.72 0.03c-0.52 0-0.43-2.28 0-2.28h78.84c0.43 0 0.28 2.28 0 2.28s-16.77 0-16.77 0 -5.18-0.3-5.18 3.14c0 4.84 0.1 160.35 0.1 165.86 0 24.25 13.72 45.72 43.6 45.22 23.29-0.39 55.66-14.12 55.57-56.23 -0.02-2.06 0-141.02 0-156.65 -0.09-1.22-2.47-1.33-2.47-1.33h-21.89c-0.47 0-0.39-2.19 0.09-2.19C680.19 20.83 880.64 20.78 880.64 20.78" /> <path class="vogue_b" d="M7.51 20.81h77.32c0.45 0 0.31 2.28 0 2.28H68.87c0 0-4.46-0.19-3.13 2.28 0.27 0.5 54.62 149.14 54.62 149.14s0.95 2.56 1.71 2.85c0 0 48.3-149.24 48.35-149.38 0.49-1.48 2.21-4.8-0.76-4.8h-17.48c-0.31 0-0.31-2.28 0-2.28h41.61c0.31 0 0.31 2.28 0 2.28h-15.48c-4.25-0.08-4.28 1.23-5.03 3.42 -0.38 1.11-67.26 209.56-67.26 209.56s-0.3-0.91-0.57-1.71C77.18 152.65 33.29 38.84 29.83 26.7c-1.04-3.64-1.57-3.52-3.7-3.51 -0.76 0-18.07 0-18.62 0C6.96 23.18 7.03 20.81 7.51 20.81" /><path class="vogue_b" d="M208.77 125.79c0-59.65 11.45-108.41 53.75-108.39 45.48 0.02 54.42 50.78 54.42 108.4 0 59.65-8.68 111.11-54.04 111.06C214.21 236.8 208.77 188.49 208.77 125.79M262.06 238.57c45.12 0.16 93.95-49.81 94.07-111.63 0.12-64.75-46.81-110.87-93.3-111.25 -47.57-0.39-93.21 51.85-93.21 111.25C169.61 188.38 212.74 238.4 262.06 238.57" /><path class="vogue_b" d="M483.84 146.54h17.66c0 0 3.28-0.35 3.25 1.24 0 0.1 0 50.51 0 50.51 0 38.85-38.72 39.29-49.67 38.05 -40.48-4.58-41.33-88.16-41.53-110.11C413 66.58 422.37 14.62 463.1 16.69c48.37 2.45 61.02 71.75 61.79 74.86 1.61 0.57 1.63-0.76 1.63-0.76l0.1-75.61c0 0-0.33-0.64-1.29 0.25 -0.34 0.32-0.74 0.72-1.19 1.19 -21.4 21.87-32.11-1.01-63.21-2.01C422.6 13.36 375 66.47 374.39 127.76c-0.61 61.66 39.88 109.18 84.14 110.87 24 0.91 23.48-5.77 53.1-14.94 18.1-5.61 26.64 6.59 27.3 11.65 1.15 0.77 1.24-0.57 1.24-0.57v-86.69c0-1.72 2.39-1.43 2.39-1.43h16.32c0.41 0 0.41-2.39 0-2.39h-75.14C483.19 144.25 483.19 146.54 483.84 146.54" /><path class="vogue_b" d="M880.64 20.78v77.67c0 0.38-1.52 0.47-1.52 0 0-0.48 1.43-75.47-68.19-75.47h-26.1c-1.46 0-2.34 0.26-2.34 1.38v97.03c0 0.91 0.91 1.4 2.3 1.4h8.73c41.43 0 38.09-41.57 38.09-42.05 0-0.48 1.62-0.38 1.62 0 0 0.38 0.09 87.28 0.09 87.66 0 0.38-1.23 0.48-1.23 0 0-0.47-0.95-43.33-39.52-43.33h-7.77c-1.34 0-2.3 0.5-2.3 1.42v104.41c0 0.73 0.89 1.41 2.34 1.41 5.84 0.02 20.88 0.06 25.05 0.07 74.82 0.19 73.81-80.12 73.81-80.51 0-0.38 1.9-0.38 1.9 0v83.19H727.71c-0.47 0-0.43-2.29 0-2.29h15.78c1.4 0 2.31-0.48 2.31-1.4 0-18.1 0.03-205.36 0-206.92 -0.02-1.34-1.75-1.43-2.31-1.43s-34.94 0-34.94 0 -2.28 0.02-2.28 1.33c0 14.72-0.01 138.97 0 156.65v0.08c0.09 43.81-33.59 58.1-57.81 58.5l-0.04-0.07c-28.5 0.15-79.7-4.66-79.7-67.82 0 0-0.13-143.73-0.19-145.72 -0.09-3.13-3.98-2.97-3.98-2.97s-16.2 0.03-16.72 0.03c-0.52 0-0.43-2.28 0-2.28h78.84c0.43 0 0.28 2.28 0 2.28s-16.77 0-16.77 0 -5.18-0.3-5.18 3.14c0 4.84 0.1 160.35 0.1 165.86 0 24.25 13.72 45.72 43.6 45.22 23.29-0.39 55.66-14.12 55.57-56.23 -0.02-2.06 0-141.02 0-156.65 -0.09-1.22-2.47-1.33-2.47-1.33h-21.89c-0.47 0-0.39-2.19 0.09-2.19C680.19 20.83 880.64 20.78 880.64 20.78" /> <path class="arabia" d="M219.53 130.95l-2.17 6.57h-2.79l7.13-20.9h3.23l7.13 20.9h-2.88l-2.23-6.57H219.53zM226.41 128.84l-2.08-6.01c-0.46-1.36-0.77-2.6-1.08-3.81h-0.06c-0.31 1.21-0.62 2.51-1.05 3.78l-2.05 6.05H226.41z" /><path class="arabia" d="M235.12 116.9c1.36-0.25 3.35-0.43 5.18-0.43 2.88 0 4.77 0.56 6.05 1.71 1.02 0.93 1.64 2.36 1.64 4 0 2.73-1.74 4.56-3.91 5.3v0.09c1.58 0.56 2.54 2.05 3.04 4.22 0.68 2.92 1.18 4.93 1.61 5.74h-2.79c-0.34-0.62-0.81-2.39-1.36-4.99 -0.62-2.88-1.77-3.97-4.22-4.06h-2.54v9.06h-2.7V116.9zM237.82 126.42h2.76c2.88 0 4.71-1.58 4.71-3.97 0-2.7-1.95-3.87-4.81-3.87 -1.3 0-2.2 0.09-2.67 0.22V126.42z" /><path class="arabia" d="M255.18 130.95l-2.17 6.57h-2.79l7.13-20.9h3.23l7.13 20.9h-2.88l-2.23-6.57H255.18zM262.06 128.84l-2.08-6.01c-0.46-1.36-0.77-2.6-1.08-3.81h-0.06c-0.31 1.21-0.62 2.51-1.05 3.78l-2.05 6.05H262.06z" /><path class="arabia" d="M270.77 116.94c1.18-0.28 3.07-0.46 4.93-0.46 2.7 0 4.43 0.47 5.71 1.52 1.09 0.81 1.77 2.05 1.77 3.69 0 2.05-1.36 3.81-3.53 4.59v0.09c1.98 0.47 4.31 2.11 4.31 5.21 0 1.8-0.71 3.19-1.8 4.19 -1.43 1.33-3.78 1.95-7.19 1.95 -1.86 0-3.29-0.12-4.19-0.25V116.94L270.77 116.94zM273.46 125.49h2.45c2.82 0 4.5-1.52 4.5-3.53 0-2.42-1.83-3.41-4.56-3.41 -1.24 0-1.95 0.09-2.39 0.19V125.49zM273.46 135.48c0.56 0.09 1.3 0.12 2.26 0.12 2.79 0 5.36-1.02 5.36-4.06 0-2.82-2.45-4.03-5.4-4.03h-2.23V135.48L273.46 135.48z" /><path class="arabia" d="M290.27 116.63v20.9h-2.73v-20.9H290.27z" /><path class="arabia" d="M298.36 130.95l-2.17 6.57h-2.79l7.13-20.9h3.23l7.13 20.9h-2.88l-2.23-6.57H298.36zM305.24 128.84l-2.08-6.01c-0.46-1.36-0.77-2.6-1.08-3.81h-0.06c-0.31 1.21-0.62 2.51-1.05 3.78l-2.05 6.05H305.24z" /> </svg>
      */ ?>

      </div>
    <?php } ?>
