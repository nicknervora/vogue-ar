<?php

define( 'ACCOUNT_PAGE', '/account/' );
define( 'ACCOUNT_LOGIN', '/account/login/' );
define( 'ACCOUNT_RECOVER', '/account/recover/' );
define( 'ACCOUNT_RESET', '/account/reset/' );
define( 'ACCOUNT_REGISTER', '/account/register/' );

// add_action( 'admin_init', 'cubiq_admin_init' );

// function cubiq_admin_init()
// {
//   if ( current_user_can( 'subscriber' ) && !defined( 'DOING_AJAX' ) )
//   {
//     wp_redirect( home_url( ACCOUNT_PAGE ) );
//     exit;
//   }

// }



add_action( 'wp_login_failed', 'pu_login_failed' ); // hook failed login

function pu_login_failed( $user )
{
  if ( isset($_SERVER[ 'HTTP_REFERER' ]))
  {
// check what page the login attempt is coming from
    $referrer = $_SERVER[ 'HTTP_REFERER' ];
// check that were not on the default login page
    if ( !empty( $referrer ) && !strstr( $referrer, 'wp-login' ) && !strstr( $referrer, 'wp-admin' ) && $user!=null )
    {
// make sure we don't already have a failed login attempt
      if ( !strstr( $referrer, '?login=failed' ) )
      {
// Redirect to the login page and append a querystring of login failed
        wp_redirect( $referrer . '?login=failed' );
      }
      else
      {
        wp_redirect( $referrer );
      }

      exit;

    }

  }

}

add_action( 'authenticate', 'pu_blank_login');

function pu_blank_login( $user )
{
  if ( isset( $_POST['log'] ) || isset( $_POST['pwd'] ) )
  {
// check what page the login attempt is coming from
    $referrer = $_SERVER['HTTP_REFERER'];

    $error = false;

    if ( $_POST['log'] == '' || $_POST['pwd'] == '' )
    {
      $error = true;
    }

// check that were not on the default login page
    if ( !empty( $referrer ) && !strstr( $referrer, 'wp-login' ) && !strstr( $referrer, 'wp-admin' ) && $error )
    {
// make sure we don't already have a failed login attempt
      if ( !strstr( $referrer, '?login=failed' ) )
      {
// Redirect to the login page and append a querystring of login failed
        wp_redirect( $referrer . '?login=failed' );
      }
      else
      {
        wp_redirect( $referrer );
      }

      exit;

    }

  }

}

add_action('template_redirect', 'register_user');
 
function register_user()
{
  if ( isset( $_GET['do'] ) && $_GET['do'] == 'register' )
  {
    $errors = array();

    if ( empty( $_POST['email'] ) )
    {
      $errors[] = 'Please enter a email.<br>';
    }

    if ( ( !empty( $_POST['cemail'] ) && !empty( $_POST['email'] ) ) && ( $_POST['email'] != $_POST['cemail'] ) )
    {
      $errors[] = 'Email addresses did not match.';
    }

    if ( empty( $_POST['pass'] ) )
    {
      $errors[] = 'Please enter a password.<br>';
    }

    if ( empty( $_POST['cpass'] ) )
    {
      $errors[] = 'Please enter a confirm password.<br>';
    }

    if ( ( !empty( $_POST['cpass'] ) && !empty( $_POST['pass'] ) ) && ( $_POST['pass'] != $_POST['cpass'] ) )
    {
      $errors[] = 'Entered password did not match.';
    }

    $user_email = esc_attr( $_POST['email'] );
    $user_confirm_email = esc_attr( $_POST['cemail'] );

  // Great string before @ to use as username prefix
    $explode = explode( '@', $_POST['email'] );
    array_pop( $explode );
    $gen_user_name = join('@', $explode);
  // Generate a random 10 character string to append to username
    $rand_string = randomString(10);

    $gen_user_name = $gen_user_name.$rand_string;

    $user_pass = esc_attr( $_POST['pass'] );
    $user_confirm_pass = esc_attr( $_POST['cpass'] );

    $sanitized_user_login = sanitize_user($gen_user_name);

    $user_email = apply_filters('user_registration_email', $user_email);
  
    if ( !is_email( $user_email ) )
    {
      $errors[] = 'Invalid e-mail.<br>';
    }
    elseif ( email_exists( $user_email ) )
    {
      $errors[] = 'This email is already registered.<br>';
    }

    if ( empty( $sanitized_user_login ) || !validate_username( $sanitized_user_login ) )
    {
      $errors[] = 'Invalid user name.<br>';
    }
    elseif ( username_exists( $sanitized_user_login ) )
    {
      $errors[] = 'User name already exists.<br>';
    }

    if ( empty( $errors ) )
    {
      $user_id = wp_create_user($sanitized_user_login, $user_pass, $user_email);

      if ( !$user_id )
      {
        $errors[] = 'Registration failed';
      }
      else
      {
        update_user_option( $user_id, 'default_password_nag', true, true );
        wp_new_user_notification( $user_id, $user_pass );
        wp_cache_delete ( $user_id, 'users' );
        wp_cache_delete ( $user_login, 'userlogins' );
        do_action ( 'user_register', $user_id );
        $user_data = get_userdata ( $user_id );

        if ( $user_data !== false )
        {
           wp_clear_auth_cookie();
           wp_set_auth_cookie ( $user_data->ID, true );
           do_action ( 'wp_login', $user_data->user_login, $user_data );
          // Redirect user.
           wp_redirect ( home_url( ACCOUNT_PAGE ) );
           exit();
         }
      
      }

    }

    if( !empty( $errors ) )
    {
      define( 'REGISTRATION_ERROR', serialize( $errors ) );
    }

  }

}

function cubiq_login_init () {

  $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'login';


  if ( isset( $_POST['wp-submit'] ) ) {
    $action = 'post-data';
  } else if ( isset( $_GET['reauth'] ) ) {
    $action = 'reauth';
  }
  // redirect to change password form
  if ( $action == 'rp' || $action == 'resetpass' || $action == 'resetpass-key' ) {
    if( isset($_GET['key']) && isset($_GET['login']) ) {

      $rp_path = wp_unslash( ACCOUNT_RESET );
      $rp_cookie  = 'wp-resetpass-' . COOKIEHASH;
      $rp_key = $_GET['key'];
      $rp_login = $_GET['login'];

      $wp_login_rp_path = wp_unslash('/wp-login.php');
      $wp_login_value = sprintf( '%s:%s', wp_unslash( $rp_login ), wp_unslash( $rp_key ) );
      $wp_login_rp_cookie = $rp_cookie;
      setcookie( $wp_login_rp_cookie, $wp_login_value, 0, $wp_login_rp_path, COOKIE_DOMAIN, is_ssl(), true );


      setcookie( 'rp-'.COOKIEHASH, wp_unslash($rp_login).':'.wp_unslash($rp_key), 0, home_url( ACCOUNT_RESET ) .'/', COOKIE_DOMAIN, is_ssl(), true );

      $action = 'resetpass-key';

          wp_redirect( home_url( ACCOUNT_RESET ) . '?action=resetpass&key=' . $rp_key . '&login=' . $rp_login );
          exit;
    }
    
    // wp_redirect( home_url( ACCOUNT_RESET ) . '?action=resetpass&key='.$_GET['key'].'&login='.$_GET['login'] );
    // exit;
  }
  // redirect from wrong key when resetting password
  if ( $action == 'lostpassword' && isset($_GET['error']) && ( $_GET['error'] == 'expiredkey' || $_GET['error'] == 'invalidkey' ) ) {
    wp_redirect( home_url( ACCOUNT_RECOVER ) . '?action=forgot&failed=wrongkey' );
    exit;
  }
  if (
    $action == 'post-data'    ||      // don't mess with POST requests
    $action == 'reauth'     ||      // need to reauthorize
    $action == 'resetpass-key'    ||
    $action == 'logout'           // user is logging out
  ) {
    return;
  }
  wp_redirect( home_url( ACCOUNT_LOGIN ) );
  exit;
}
add_action('login_init', 'cubiq_login_init');


add_action( 'lostpassword_post', 'cubiq_reset_password');

function cubiq_reset_password()
{
  $user_data = '';

  if ( !empty( $_POST['user_login'] ) )
  {
    if ( strpos( $_POST['user_login'], '@' ) )
    {
      $user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
    }
    else
    {
      $user_data = get_user_by( 'login', trim( $_POST['user_login'] ) );
    }
  
  }

  if ( empty( $user_data ) )
  {
    wp_redirect( home_url( ACCOUNT_RECOVER ) . '?action=forgot&failed=1' );
    exit;
  }

}

add_action( 'validate_password_reset', 'cubiq_validate_password_reset', 10, 2 );

function cubiq_validate_password_reset( $errors, $user )
{
// passwords don't match
  if ( $errors->get_error_code() )
  {
    wp_redirect( home_url( ACCOUNT_RESET ).'?action=resetpass&failed=nomatch' );
    exit;
  }

// wp-login already checked if the password is valid, so no further check is needed
  if ( !empty( $_POST['pass1'] ) )
  {
    reset_password( $user, $_POST['pass1'] );

    wp_redirect( home_url( ACCOUNT_LOGIN ).'?action=resetpass&success=1' );
    exit;
  }

  // redirect to change password form
  wp_redirect( home_url( ACCOUNT_RESET ).'?action=resetpass' );
  exit;
}
