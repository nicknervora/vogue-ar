<?php

/* ============================================================================

  SECTION BREAKS

============================================================================ */

function getBreak_Recommends( $get_user = false )
{

  global $post;

  $posts = getVogueRecommendsEditorPosts();

  $editor_count = count( $posts );

  $promoted_choice = get_field( 'promoted_user', HOME_ID );

  $promoted_username = get_field( 'instagram_user', HOME_ID );

  if ( $promoted_choice && ( $promoted_username != '' ) )
  {
    $get_user = $promoted_username;
  }

  if ( $posts ) : ?>

  <div class="container container--mid">

    <h3 class="section--header auto-fade-in"><?php echo __( 'Top Stories', 'vogue.me' ); ?></h3>

    <ul class="list post--list vogue--recommends">

    <?php $i = 0; foreach ( $posts as $post ) : setup_postdata( $post ); ?>

      <?php /* if ( $i == 5 ) : ?>

      <?php /* Close list, insert vertical advert, close containing div ?>
        </ul> <?php getAdvert( 'vert' ); ?> </div>
        <?php getBreak_Social(); ?>
      <?php /* Open new container, open new list ?>
        <div class="container container--mid"> <ul class="list post--list vogue--recommends">
      <?php endif; */ ?>

      <?php getFeedItem( $i, $post, 'recommends' ); ?>

    <?php $i++; if ( $i > 5 ) break; endforeach; wp_reset_postdata(); ?>

    </ul>

    <?php getAdvert( 'vert', 'TOP' ); ?>

  </div>

<?php endif; }



function getBreak_Newsletter()
{
    ?>
        <section class="section--break break--newsletter">

            <form class="newsletter-signup-break-form newsletter--form mc-embedded-subscribe-form">
                <h1 class="newsletter--heading"><?php echo __( 'The Vogue Newsletter', 'vogue.me' ); ?></h1>
                <h2 class="newsletter--heading__sub"><?php echo __( 'The only newsletter you won\'t unsubscribe from', 'vogue.me' ); ?></h2>

                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/va-newsletter.png" alt="<?php echo __( 'Newsletter sign up', 'vogue.me' ); ?>">

                <fieldset class="newsletter--fields">
                    <input type="text" value="" name="NAME" class="newsletter--field required name" placeholder="<?php echo __('Your name', 'vogue.me'); ?>">
                    <input type="text" value="" name="EMAIL" class="newsletter--field required email" placeholder="<?php echo __('Your email', 'vogue.me'); ?>">

                    <div class="message mobile"></div>
                    <input type="submit" value="<?php echo __('Sign up now', 'vogue.me'); ?>" name="subscribe" class="mc-embedded-subscribe newsletter--submit button brand">

                </fieldset>

                <div class="message"></div>

                <small><?php echo __( '* you consent to receiving communications from Vogue Arabia', 'vogue.me' ); ?></small>
            </form>

        </section>
    <?php
}

/* ============================================================================

  EXPLORE VOGUE

============================================================================ */

function getBreak_Explore($the_post_id)
{ ?>

<?php if ( is_single() ) :  ?>

  <?php $post_id[] = $the_post_id;
		$post_id_array[] = $the_post_id;
  $current_user = get_the_author_meta( 'ID' ); $ignore_id = false; ?>

  <section class="section--break break--explore">

    <div class="container container--mid">

      <h3 class="section--header auto-fade-in"> <?php echo __('Vogue Recommends','vogue.me'); ?></h3>

      <ul class="post--list post--explore post--small">

  <?php
  // i assume this is done this way so the first post in vogue recommend is from the same author but again the query is not specfic enough adding catagory to the mix 
	$cat_ids = array(); 
	$cats = get_the_category( $the_post_id, 'category' ); 
	if(empty($cat_ids)){
		foreach ( $cats as $term ) : $cat_ids[] = (int) $term->cat_ID; endforeach; 
	}
	
    $args = array
    (
      'post_type' => array( 'post' ),
      'date_query' => array
      (
        array
        (
          'column' => 'post_date_gmt',
          'after' => '2 months ago'
        )
      ),
      'category__not_in' => getIgnore_Cats( array( 'runway', 'video', 'runway_ar','video_ar','uncategorized' ) ),
	  'category__in' => $cat_ids,
      'post__not_in' => $post_id_array,
      'author' => $current_user,
	  'posts_per_page' => 20,
	  'orderby' => 'date', 'order' => 'DESC'
      //'orderby' => 'rand',
      //'order'=> 'ASC'
    );

    /*
    $args = array
    (
      'name' => 'vogue-arabia-editors-letter'
    );
    */
	
    $explore_query = new WP_Query( $args );

		//var_dump($explore_query);

    $listed_IDs = array(); // used to prevent duplicates

    if ( $explore_query->have_posts() ) :

      $i = 1; while ( $explore_query->have_posts() ) : $explore_query->the_post();

      $thumbnail = getThumbnail();

      if ( is_array($thumbnail) ):
        $thumbnail = $thumbnail[0];
      else:
        $thumbnail = $thumbnail;
      endif;

      $ignore_id = true; $ignore_id = get_the_ID();
	  
	   // to remove possibilty of duplicate of first and seconf one 
      $listed_IDs[] = get_the_ID();
	  $post_not_in = get_the_ID();
			$blog_id = get_current_blog_id();
			//if ($blog_id == 3){
			//    $thumbnail =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
			//} else {

      	$thumbnail = imageProvider( $thumbnail, 270, 202 );

			//}

      $key_category = getKeyTopicName();

      if ( $key_category != "uncategorized" ) : ?>

        <li class="post--item author">

          <div class="post--image"> <a href="<?php echo get_permalink(); ?>"> <img class="auto-fade-in" src="<?php echo $thumbnail; ?>" alt=""> </a> </div>
          <div class="post--snippet">
            <div class="snippet--inner">
              <div class="post--meta"> <?php getKeyTopic( false ); ?> </div>
              <h3 class="post--title"> <a href="<?php echo get_permalink(); ?>"><?php echo the_title(); ?></a> </h3>

              <?php isSponsored(get_the_ID()); ?>
            </div>
          </div>

        </li>

    <?php endif; ?>

  <?php $i++; if ( $i > 1 ) break; endwhile; wp_reset_query(); wp_reset_postdata(); ?>

<?php endif; ?>

<?php $post_limit = 2; ?>

<?php if ( !$ignore_id ) : $post_limit = 3; endif; ?>

<?php $post_count = 10; ?>

<?php $tags = get_the_tags( $the_post_id, 'post_tag' ); ?>

<?php //$collection = wp_get_post_terms( $the_post_id, COLLECTION ); ?>

<?php 
$cats = get_the_category( $the_post_id, 'category' ); 
?>

<?php $the_term_ids = array(); ?>

<?php $the_cat_ids = array(); ?>

<?php foreach ( $tags as $term ) : $the_term_ids[] = (int) $term->term_id; endforeach; ?>



<?php //foreach ( $collection as $term ) : $term_ids[] = (int) $term->term_id; endforeach; ?>
<?php
// this is not specific enough and as a result it comes up with the same list // since the parent category is included
// foreach ( $cats as $term ) : $the_cat_ids[] = (int) $term->cat_ID; endforeach; 
 foreach ($cats as $term){
	 if (cat_is_ancestor_of(10, $term)) {
		$the_cat_ids[] = (int) $term->cat_ID; 
	 }	 
}
// only if there is no subcategory
if(empty($the_cat_ids)){
	foreach ( $cats as $term ) : $the_cat_ids[] = (int) $term->cat_ID; endforeach; 
}
 
 
?>

<?php //$term_ids = implode( ',', $term_ids ); ?>
<?php // $cat_ids = implode( ',', $cat_ids ); ?>

<?php
session_start();
if(get_current_blog_id() == '3' ) {
	$_SESSION['read_ar'][] = ( int ) get_the_ID();
	$post_not_in = $_SESSION['read_ar'];
} else {
	$_SESSION['read'][] = ( int ) get_the_ID();
	$post_not_in = $_SESSION['read'];
}

$post_not_in = $post_id_array;
	?>
<script>
  //console.log("<?php echo json_encode($_SESSION['read']); ?>");
  console.log("<?php echo json_encode($the_cat_ids); ?>");
  console.log("<?php echo json_encode($the_term_ids); ?>");
</script>
<?php 
// no need for expensive tax query with OR and it was wrong (needed condition in) since we are not using vogue collection just use tag__in and catagory__in
$term_args = array( 'post_type' => array( 'post' ),  'tag__in' => $tag_ids, 'category__in' => $the_cat_ids, 'date_query' => array(
          array(
              'column' => 'post_date_gmt',
              'after' => '6 months ago'
          )
      ),'category__not_in' => getIgnore_Cats( array( 'runway','video','uncategorized' ) ), 'post__not_in' => $post_not_in, 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => $post_count ); ?>


<?php $term_query = new WP_Query( $term_args ); ?>

<?php if ( $term_query->have_posts() ) : ?>

<?php $temp_i = 0; ?>

  <?php while ( $term_query->have_posts() ) : $term_query->the_post(); ?>

    <?php $key_category = getKeyTopicName(); ?>

    <?php if ( !in_array(get_the_ID(), $listed_IDs) && $temp_i<=$post_limit && $key_category != "uncategorized" ) : ?>
    <?php //if ( 1==1 ) : ?>

      <?php $thumbnail = getThumbnail(); ?>

      <?php if ( is_array($thumbnail) ) : ?>
        <?php $thumbnail = $thumbnail[0]; ?>
      <?php else : ?>
        <?php $thumbnail = $thumbnail; ?>
      <?php endif; ?>

      <?php $thumbnail = imageProvider( $thumbnail, 270, 202 ); ?>

            <li class="post--item tag">

              <div class="post--image"> <a href="<?php echo get_permalink(); ?>"> <img class="auto-fade-in" src="<?php echo $thumbnail; ?>" alt=""> </a> </div>
              <div class="post--snippet">
                <div class="snippet--inner">
                  <div class="post--meta"> <?php getKeyTopic( false ); ?> </div>
                  <h3 class="post--title"> <a href="<?php echo get_permalink(); ?>"><?php echo the_title(); ?></a> </h3>
                </div>
              </div>

            </li>

      <?php $listed_IDs[] = get_the_ID(); ?>
      <?php $temp_i++; ?>

    <?php endif; ?>

  <?php endwhile; wp_reset_postdata(); wp_reset_query(); ?>

<?php endif; ?>

      </ul>

    </div>

  </section>

<?php endif; ?>

<?php }

/* ============================================================================

  INSTAGRAM

============================================================================ */

function getBreak_Instagram( $get_user = false )
{

  if ( $get_user == false ) $get_user = 'voguearabia'; ?>

  <section class="section--break break--instagram">

    <div class="post--item post--full">

      <?php getInstagramFeed( $get_user ); ?>

    </div>

  </section>

<?php }

/* ============================================================================

  SOCIAL

============================================================================ */

function getBreak_Social()
{ ?>

  <section class="section--break break--social">
    <div class="bg auto-fade-in"></div>
    <h1 class="section--heading"><?php echo __( 'Follow Vogue Arabia', 'vogue.me' ); ?></h1>

    <?php get_vogue_social(false,'section--social--list'); ?>

  </section>

<?php }

/* ============================================================================

  VOGUE SELECTION

============================================================================ */

function getBreak_Selection()
{

if ( check_site( false, 'ar' ) )
{
  $video_cat_id = get_category_by_slug( 'video_ar' )->term_id;
}
else
{
  $video_cat_id = get_category_by_slug( 'video' )->term_id;
}

  $ignore = array();

  $ignore[] = $video_cat_id;
  foreach( getRunwayCats() as $cat )
  {
    $ignore[] = $cat;
  }

  $uncatObj = get_category_by_slug('uncategorized');
  $uncategorizedID = $uncatObj->term_id;
  $ignore[] = $uncategorizedID;

  $args = array( 'posts_per_page' => 40,  'date_query' => array(
          array(
              'column' => 'post_date_gmt',
              'after' => '2 months ago'
          )
      ),'post_type' => array( 'post'), 'category__not_in' => $ignore, 'meta_query'  => array( array( 'key' => 'shoppable', 'value' => true ) ) );

  $selection_query = new WP_Query( $args );

  if ( $selection_query->have_posts() ) : ?>

  <section class="section--break break--selection">

    <div class="container container--mid">

      <h3 class="section--header auto-fade-in"> <?php echo __('Vogue Selection','vogue.me'); ?> </h3>

    <?php // GALLERY OUTER CONTAINER ?>
      <div class="selection--gallery no-js-initiate">

      <?php // GALLERY INNER CONTAINER, APPLY SLICK TO INNER ?>
        <div class="selection--inner">

        <?php $i = 1; while ( $selection_query->have_posts() ) : $selection_query->the_post(); ?>
        <?php if ( get_field( 'shoppable_thumbnail' ) ) : ?>
          <?php $thumbnail = get_field( 'shoppable_thumbnail' ); ?>
        <?php else : ?>
          <?php $thumbnail = getThumbnail()[0]; ?>
        <?php endif; ?>
        <?php if ( is_numeric( $thumbnail ) ) :
              $img = wp_get_attachment_image_src($thumbnail, 'medium');
              $thumbnail = $img[0];
            else :
              $thumbnail = $thumbnail;
            endif; ?>

        <?php $key_category = getKeyTopicName(); ?>

          <?php if ( $key_category != "uncategorized" ) : ?>

            <div class="selection--item">

              <a href="<?php echo get_permalink(); ?>">
                <span class="selection--image" data-title="<?php echo __('View Gallery','vogue.me'); ?>">
                  <img class="auto-fade-in" data-lazy="<?php echo imageProvider($thumbnail,189); ?>" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/V-loading.gif" alt="">
                </span>

                <h3 class="selection--title"> <?php the_title(); ?></h3>
              </a>

            </div>

          <?php endif; ?>

        <?php $i++; endwhile; wp_reset_postdata(); wp_reset_query(); ?>

        </div>
      <?php // END INNER ?>

      </div>
    <?php // END OUTER ?>

    </div>

  </section>

<?php endif; }




function getBreak_Videos( $promoted = false )
{

  global $post;

  $home_ID = get_home_ID();

  $video = get_field( 'promoted_video', $home_ID );

  if ( $video ) :

?>

  <section class="section--break break--video<?php if ( $promoted == true ) : ?> promoted--video<?php endif; ?>">

  <?php if ( $promoted != true ) : ?> <h3 class="section--header auto-fade-in"> <?php echo __( 'Videos', 'vogue.me' ); ?> </h3> <?php endif; ?>

  <?php $post = $video[0]; setup_postdata( $post ); $src = $post; ?>

  <?php if ( $src ) : ?>

    <div class="container container--mid container--mid--reduced">

      <div class="post--item post--full">

        <?php getVideoPlayer( $src ); ?>

        <?php getFeedItem( $i = 0, $post, 'video' ); ?>

      </div>

    </div>

  <?php endif; ?>

  <?php wp_reset_postdata(); ?>

  <?php $video_args = array( 'post_type' => array( 'post' ), 'date_query' => array(
          array(
              'column' => 'post_date_gmt',
              'after' => '1 year ago'
          )
      ),'posts_per_page' => 4, 'category__in' => get_video_categories(), 'post__not_in' => array( $video[0] ) ); $video_query = new WP_Query( $video_args ); if ( $video_query->have_posts() ) : ?>

    <div class="container<?php if ( $promoted == true ) : ?> container--full black--bg<?php else : ?> container--mid--reduced<?php endif; ?>">

      <?php if ( $promoted == true ) : ?><h3 class="section--header auto-fade-in archive--colour__white"><?php echo __( 'Trending Videos', 'vogue.me' ); ?></h3><?php endif; ?>

      <ul class="post--list post--videos post--small<?php if ( $promoted == true ) : ?> trending--videos<?php endif; ?>">

      <?php while( $video_query->have_posts() ) : $video_query->the_post(); setup_postdata($post); ?>

      <?php if ( get_field( 'gif_hosted', $post ) ) : ?>
        <?php $thumbnail = get_field( 'gif_hosted', $post ); ?>
      <?php elseif ( get_field( 'poster_image', $post ) ) : ?>
        <?php $thumbnail = get_field( 'poster_image', $post ); ?>
      <?php else : ?>

        <?php $thumbnail = getThumbnail(); ?>

        <?php if ( is_array( $thumbnail ) ) : ?>

          <?php $thumbnail = $thumbnail[0]; ?>

        <?php else : ?>

          <?php $thumbnail = $thumbnail; ?>

        <?php endif; ?>

      <?php endif; ?>

            <?php if ( is_numeric( $thumbnail ) ) :

              $img = wp_get_attachment_image_src($thumbnail, 'medium');
              $thumbnail = $img[0];

            else :
              $thumbnail = $thumbnail;
            endif;

            if ( is_array( $thumbnail ) )
            {
              if ( isset($thumbnail['sizes']['medium']))
              {
                $thumbnail = $thumbnail['sizes']['medium'];
              }
              else
              {
                $thumbnail = $thumbnail[0];
              }
            }
            else
            {
              $thumbnail = $thumbnail;
            }

            $thumbnail = imageProvider( $thumbnail, 255, 145 );

            ?>
        <li class="post--item">

          <div class="post--image"> <a href="<?php echo get_permalink(); ?>"> <img class="auto-fade-in" src="<?php echo $thumbnail; ?>" alt="" width="255" height="145" > </a> </div>

          <div class="post--snippet">
            <div class="snippet--inner">
              <h3 class="post--title"> <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a> </h3>
            </div>
          </div>

        </li>
      <?php endwhile; wp_reset_postdata(); wp_reset_query(); ?>

      </ul>

    <?php if ( $promoted != true ) : ?> <a href="<?php if ( check_site( false, 'ar' ) ) : echo get_term_link( 'video_ar', 'category' ); else : echo get_term_link( 'video', 'category' ); endif; ?>" class="button black inline--image inline--right"><?php echo __('All Videos','vogue.me'); ?> <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/va-gallery-next-white.svg" alt=""></a> <?php endif; ?>

      <div class="clear"></div>

    </div>

  <?php endif; ?>

  </section>


<?php endif; }

$promoted_i = 1;

function getBreak_Promoted( $item = false, $promoted_id = false, $is_home = false )
{

  $home_ID = get_home_ID();

  $promoted_post = get_field( 'promoted_article', $home_ID );

  if ( $promoted_post ) :

    if ( $item != false )
    {
      $post_ID = $promoted_post[0];
    }
    elseif ( $promoted_id != false )
    {
      $post_ID = $promoted_post[$promoted_id];
    }
    else
    {
      $count = count( $promoted_post ) - 1;
      $select = rand( 1, $count );
      $post_ID = $promoted_post[$select];
    }

    if ( is_object( $post_ID ) )
    {
      $post_ID = $post_ID->ID;
    }

?>

  <div class="container">

    <div class="post--item post--full featured">

<?php

        $width = '';
        $height = '';

        $img = false;
        $set_max = false;
        $mobile_image = false;



        if ( get_field( 'feat_mobile_image', $post_ID ) ) :

          //$mobile_image = wp_get_attachment_image_src( get_field( 'feat_mobile_image', $post_ID ), 'large' );
          //$mobile_image = $mobile_image[0];

          $mobile_image = get_field( 'feat_mobile_image', $post_ID );

        endif;

        //var_dump($mobile_image);exit;

        if ( get_field( 'feat_large_image', $post_ID ) ) :

          $image = get_field( 'feat_large_image', $post_ID );

          if ( is_numeric( $image ) ) :

            $img = wp_get_attachment_image_src( $image, 'full' );

            $image = $img[0];
            $width = $img[1];

            if ( $width >= 1400 ) :

              if ( $width > 1920 ) :

                $width = 1920;
                $height = 800;

              elseif ( $width >= 1600 ) :

                $width = 1600;
                $height = 700;

              else :

                $width = 1400;
                $height = 700;

              endif;

            elseif ( $width >= 1260 && $width < 1400 ) :

              $width = 1260;
              $height = 600;

            endif;

            $set_max = $width;

          else :

            $image = $image;

          endif;

        else :

          $img = getThumbnail( $post_ID );

          $image = $img[0];
          $width = $img[1];

          if ( $width >= 1400 ) :

            if ( $width >= 1920 ) :

              $width = 1920;
              $height = 800;

            elseif ( $width >= 1600 ) :

              $width = 1600;
              $height = 700;

            else :

              $width = 1400;
              $height = 700;

            endif;

          elseif ( $width >= 1260 && $width < 1400 ) :

            $width = 1260;
            $height = 600;

          endif;

          $set_max = $width;

        endif;

        if ( $mobile_image )
        {
          $img_small = $mobile_image;
        }
        else
        {
          $img_small = $image;
        }

        $x_375 = imageProvider( $img_small, 375, 281 );
        $x_400 = imageProvider( $img_small, 400, 300 );
        $x2_400 = imageProvider( $img_small, 800, 600 );
        $x_600 = imageProvider( $img_small, 600, 400 );
        $x_800 = imageProvider( $img_small, 800 );
        $x_870 = imageProvider( $image, 870 );
        $x_1260 = imageProvider( $image, 1260, 600 );
        $x_1400 = false;
        $x_1600 = false;
        $x_1920 = false;

        if ( $set_max >= 1400 )
        {
          $x_1400 = imageProvider( $image, 1400, 700 );
        }
        if ( $set_max >= 1600 )
        {
          $x_1600 = imageProvider( $image, 1600, 700 );
        }
        if ( $set_max >= 1920 )
        {
          $x_1920 = imageProvider( $image, 1920, 800 );
        }

        // VIDEO PLUS / WITH VIDEO
        if ( get_field( 'media_type', $post_ID ) == 'video_plus'  &&
						(get_field( 'mp4_source_remote_homepage', $post_ID ) ||
						 get_field( 'mp4_source_hosted_homepage', $post_ID ) ||
						 get_field( 'ogv_source_hosted_homepage', $post_ID ) ||
						 get_field( 'wemb_source_hosted_homepage', $post_ID ) ||
						 get_field( 'gif_hosted_homepage', $post_ID ))
					 ) : // if the article is video + article and has a video select)


          $src = $post_ID;

          if ( get_field( 'external_video', $post_ID ) ) :

            $src = get_field( 'video_select', $post_ID )[0];

          endif;

          if( $is_home ){
            if ( get_field( 'external_video_homepage', $post_ID ) ) :
              $src = get_field( 'video_select_homepage', $post_ID )[0];
            endif;
          }



      ?>

                          <?php if ( $mobile_image && 1==2 ) : ?>
                            <picture class="featured--image with--video">
                              <source srcset="<?php echo $x_1260; ?>" media="(min-width: 640px)">
                              <source srcset="<?php echo $x_800; ?>" media="(min-width: 400px)">
                              <source srcset="<?php echo $x_600; ?>" media="(min-width: 300px)">
                              <img data-echo="<?php echo $x_1260; ?>" src="" alt="">
                            </picture>
                          <?php endif; ?>


      <a href="<?php echo get_permalink( $post_ID ); ?>">

        <?php if ( wp_is_mobile() ) : ?>

            <?php

                  // Get Mobile thumbnail 4:3
                  $mobile_thumbnail_4_3 = get_field( 'mobile_thumbnail_4_3', $post_ID )["url"];

             ?>

            <picture class="featured--image--home-mobile" >
              <img src="<?php echo $mobile_thumbnail_4_3; ?>" alt="" >
            </picture>

        <?php else : ?>

          <div class="js-auto-play featured--video">
            <?php
              if( $is_home ){
                getVideoPlayerHomepage( $src, 'no', false, 700, true, true );
              }else{
                getVideoPlayer( $src, 'no', false, 700 );
              }
            ?>
          </div>

        <?php endif; ?>

      </a>

      <?php else : ?>

        <div class="post--image post--image--bg<?php if ( $set_max <= 1600 ) : ?> no_scale <?php if ( $set_max < 1600 ) : ?>no_scale--medium<?php endif; ?> <?php if ( $set_max < 1400 ) : ?>no_scale--small<?php endif; ?><?php endif; ?> <?php if ( $mobile_image ) : ?>has-mobile<?php endif; ?>">
          <a href="<?php echo get_permalink( $post_ID ); ?>">

            <?php if ( 1==2 ) : ?>
            <!--
            <picture class="featured--image--home">
            <?php if ( $x_1600 != false ) : ?>
              <source srcset="<?php echo $x_1600; ?>" media="(min-width: 1401px)">
            <?php endif; ?>
            <?php if ( $x_1400 != false ) : ?>
              <source srcset="<?php echo $x_1400; ?>" media="(min-width: 1261px)">
            <?php endif; ?>
              <source srcset="<?php echo $x_1260; ?>" media="(min-width: 801px)">
              <source srcset="<?php echo $x_800; ?>" media="(min-width: 400px)">
              <source srcset="<?php echo $x_600; ?>" media="(min-width: 300px)">
              <img data-echo="<?php echo $x_1260; ?>" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/feed-placeholder.png" alt="">
            <?php if ( $x_1920 != false ) : ?>
              <div style="background-image: url('<?php echo $x_1920; ?>');"></div>
            <?php endif; ?>
            </picture>
            -->
            <?php endif; ?>

            <?php

                  // Get Mobile thumbnail 4:3
                  $mobile_thumbnail_4_3 = get_field( 'mobile_thumbnail_4_3', $post_ID )["url"];

                  // Get Featured article image 16:9
                  $featured_article_image_16_9 = get_field( 'featured_article_image_16_9', $post_ID )["url"];

             ?>

            <picture class="featured--image--home-mobile auto-fade-in" >
              <img src="<?php echo $mobile_thumbnail_4_3; ?>" alt="" >
            </picture>

            <picture class="featured--image--home auto-fade-in" style="background-image:url(<?php echo $featured_article_image_16_9; ?>);" >
            </picture>

          </a>
        </div>

      <?php endif; ?>

      <div class="post--snippet">
        <div class="snippet--inner">
          <h1 class="post--title"> <a href="<?php echo get_permalink( $post_ID); ?>"><?php echo get_the_title( $post_ID ); ?></a> </h1>

          <?php isSponsored($post_ID); ?>
        </div>
      </div>

    </div>

  </div>

<?php endif; wp_reset_postdata(); }

function getBreak_Series()
{

  $series = get_field( 'promoted_series', HOME_ID );

  if ( $series )
  {

    echo '<section class="section--break break--series">';

    echo '<div class="container container--mid">';

    echo '<h3 class="section--header auto-fade-in">' . __( '#Series', 'vogue.me' ) . '</h3>';

    echo '<ul class="list post--list promoted--series">';

    $i = 1; foreach ( $series as $get => $value )
    {
      $series_id = $value->term_id;

      $_series = get_term_by( 'id', $series_id, 'series' );

      $term_id = 'series_' . $value->term_id;

      $size_01 = false;

      if ( get_field( 'series_image', $term_id ) ) :
        $image = get_field( 'series_image', $term_id );

        $size_01 = $image['sizes']['medium'];
        $size_02 = $image['sizes']['medium_large'];
        $size_03 = $image['sizes']['large'];
        $size_04 = $image['url'];

      endif;

      echo '<li> <a href="' . get_term_link( $_series->slug, 'series' ) . '">';

      echo '<div class="series--image">';

      if ( $size_01 )
      {
        echo '<img class="auto-fade-in" src="' . $size_01 . '" srcset="' . $size_02 . ' 600w, ' . $size_03 . ' 900w, ' . $size_03 . ' 1200w" alt="' . $_series->name . '">';
      }
      else
      {
        echo '<img class="auto-fade-in" src="' . get_bloginfo('template_url') . '/assets/images/feed-placeholder.png" alt="">';
      }

        echo '<div class="series--content">';
        echo '<div class="series--inner"> <span class="series--count">' . $_series->count . ' ' . __('Posts','vogue.me') . '</span>' . $_series->name . '</div>';
        echo '</div>';

        echo '<div class="clear"></div>';

      echo '</div>';

      echo '</a> </li>';

      $i++; if ( $i > 4 ) break;
    }

    echo '</ul>';

    // echo getAdvert('vert');

    echo '</div>';

    echo '</section>';

    echo getAdvert('strip');

  }

}


function getSectionBreaks( $paged = false )
{

  $home_ID = get_home_ID();

  $section_breaks = get_field( 'section_splits', $home_ID );

  $count = count( $section_breaks );

  if ( $paged <= 4 )
  {

    if ( $paged % 4 == 2 )
    {
      getBreak_Promoted( false, 1, true );
      getBreak_Selection();
    }
    else if ( $paged % 4 == 3 )
    {
      getBreak_Promoted( false, 2, true );
      //getBreak_Newsletter();
      getBreak_Series();
    }
    else if ( $paged % 4 == 0 )
    {
      getBreak_Videos();
    }

  }
  else if ( $paged > 4 )
  {
    if ( $paged % 3 == 2 )
    {
      getBreak_Promoted(false,false,true);
      getBreak_Selection();
    }
    else if ( $paged % 3 == 1 )
    {
      getBreak_Videos();
    }
    else if ( $paged % 3 == 0 )
    {
      getBreak_Promoted(false,false,true);

      if ( $paged % 6 == 0 )
      {
        getBreak_Social();
      }
      else
      {
        getBreak_Social(); // getBreak_Newsletter();
      }
      getBreak_Series();
    }

  }

}


function getBreak_TrendingVideos()
{

  $trending = getQuery__TrendingVideos();

  if ( $trending ) : ?>

  <section class="video-slider">
    <div class="container container--mid--reduced">
      <h1 class="section--header section--header--light auto-fade-in"><?php echo __('Trending videos','vogue.me'); ?></h1>
      <div class="video-slider__inner">
        <div class="swiper-container"<?php if ( check_site( false, 'ar' ) ) : echo ' dir="rtl"'; endif; ?>>
          <div class="swiper-wrapper">

<?php foreach ( $trending as $post ) : setup_postdata( $post );

    if ( get_field( 'gif_hosted' ) ) : $thumbnail = get_field( 'gif_hosted', $post ); elseif ( get_field( 'poster_image', $post ) ) : $thumbnail = get_field( 'poster_image', $post ); ?>
  <?php else : ?>
    <?php $thumbnail = getThumbnail( $post )[0]; ?>
  <?php endif;
    if ( is_numeric( $thumbnail ) ) :
      $img = wp_get_attachment_image_src($thumbnail, 'medium');
      $thumbnail = $img[0];
    elseif ( is_array($thumbnail) ) :
      $img = wp_get_attachment_image_src($thumbnail['ID'], 'medium');;
      $thumbnail = $img[0];
    else :
      $thumbnail = $thumbnail;
    endif;
            ?>
              <article class="swiper-slide">
                <a href="<?php echo get_permalink($post); ?>" class="video-post video-post--dark video-post--m">
                  <div class="video-post__thumb">
                     <img class="auto-fade-in" src="<?php echo $thumbnail; ?>" alt="">
                  </div>
                  <div class="video-post__content">
                    <h2><?php echo get_the_title($post); ?></h2>
                    <?php getKeyTopic( true ); ?>
                  </div>
                </a>
              </article>

<?php endforeach; wp_reset_postdata(); ?>

          </div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev swiper-button swiper-button--light"></div>
        <div class="swiper-button-next swiper-button swiper-button--light"></div>
      </div>
    </div>
  </section>

<?php endif;

}
