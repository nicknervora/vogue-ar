<?php

add_action( 'admin_head', 'change_meta_box_title' );
function change_meta_box_title() {
    remove_meta_box( 'postimagediv', 'post', 'side' );
    add_meta_box('postimagediv', __('Promo Landscape / Portrait 4:3 or 2:3'), 'post_thumbnail_meta_box', 'post', 'side');
}

