<?php

  $post_ID = get_the_ID(); $post_id = $post_ID;

  $content = get_the_content();

  
  $section = get_query_var('section'); 
  switch ( $section ) {
	case 'review':
		$gallery = get_field( 'linked_gallery' );
		$gallery_id = $gallery->ID;
		break;
	case 'collection':
		$gallery = get_field( 'linked_gallery' );
		$gallery_id = $gallery->ID;
		break;
	case 'details':
		$gallery = get_field( 'linked_gallery_detail' );
		$gallery_id = $gallery->ID;
		break;
	case 'beauty':
		$gallery = get_field( 'linked_gallery_beauty' );
		$gallery_id = $gallery->ID;
		break;
	case 'front-row':
		$gallery = get_field( 'linked_gallery_front_row' );
		$gallery_id = $gallery->ID;
		break;
	default :
		$gallery = get_field( 'linked_gallery' );
		$gallery_id = $gallery->ID;
	}
  
  
  
  /*if ( get_field( 'linked_gallery' ) )
  {
    $gallery = get_field( 'linked_gallery' );
    $gallery_id = $gallery->ID;
  }
  else
  {
    $gallery_id = $post_ID;
  }*/

  if ( get_field( 'gallery_image_uploader', $gallery_id ) )
  {
    $runway_gallery = get_field( 'gallery_image_uploader', $gallery_id );
    $count = count($runway_gallery);
  }
  elseif ( get_field( 'gallery_image_uploader_normal', $gallery_id ) )
  {
    $runway_gallery = get_field( 'gallery_image_uploader', $gallery_id );
    $count = count($runway_gallery);
  }
  elseif ( has_shortcode( $content, 'gallery' ) ) {

  }
  elseif ( get_field( 'gallery', $gallery_id ) || get_field( 'gallery_select', $gallery_id ) )
  {
    if ( get_field( 'gallery_select', $gallery_id ) == false )
    {
      $gallery_id = get_field( 'gallery_select', $gallery_id )[0];
      $runway_gallery = get_field( 'gallery', $gallery_id );
    }
    else
    {
      $runway_gallery = get_field( 'gallery', $gallery_id );
    }

  }
  else
  {
    $legacy_gallery = array();

    $g_collection = getFallbackMeta( $post_id , "style_fashionshow_collection" );
    $g_details = getFallbackMeta( $post_id , "style_fashionshow_details" );
    $g_backstage = getFallbackMeta( $post_id , "style_fashionshow_backstage" );
    $g_frontrow = getFallbackMeta( $post_id , "style_fashionshow_frontrow" );
    $g_beauty = getFallbackMeta( $post_id , "style_fashionshow_beauty" );
    $g_atmosphere = getFallbackMeta( $post_id , "style_fashionshow_atmosphere" );

    if ( $g_collection ) $legacy_gallery[] = getGalleryData( $g_collection );
    if ( $g_details ) $legacy_gallery[] = getGalleryData( $g_details );
    if ( $g_backstage ) $legacy_gallery[] = getGalleryData( $g_backstage );
    if ( $g_frontrow ) $legacy_gallery[] = getGalleryData( $g_frontrow );
    if ( $g_beauty ) $legacy_gallery[] = getGalleryData( $g_beauty );
    if ( $g_atmosphere ) $legacy_gallery[] = getGalleryData( $g_atmosphere );

    $runway_gallery = $legacy_gallery;

  }

  $linked_gallery = get_field('linked_gallery');
  $linked_gallery_detail = get_field('linked_gallery_detail');
  $linked_gallery_beauty = get_field('linked_gallery_beauty');
  $linked_gallery_front_row = get_field('linked_gallery_front_row');
  $linked_gallery_atmosphere = get_field('linked_gallery_atmosphere');

  $combo_linked_gallery = array();

  if ($linked_gallery)
    $combo_linked_gallery[$linked_gallery->ID] = 'Collection';

  if ($linked_gallery_detail)
    $combo_linked_gallery[$linked_gallery_detail->ID] = 'Detail';

  if ($linked_gallery_beauty)
    $combo_linked_gallery[$linked_gallery_beauty->ID] = 'Beauty';

  if ($linked_gallery_front_row)
    $combo_linked_gallery[$linked_gallery_front_row->ID] = 'Front Row';

  if ($linked_gallery_atmosphere)
    $combo_linked_gallery[$linked_gallery_atmosphere->ID] = 'Atmosphere';

  $asset_url = get_stylesheet_directory_uri() . '/assets/images/';

  $set = 0;

  $cats = wp_get_post_terms( $post_id, 'category' );
  if ( check_site( false, 'ar' ) )
  {
    $runway = get_term_by( 'slug', 'runway_ar', 'category' );
  }
  else
  {
    $runway = get_term_by( 'slug', 'runway', 'category' );
  }
  $runway_sub = get_term_children( $runway->term_id, 'category' );
  $the_cat = false;
  $cat_slug = 'runway';
  foreach ( $cats as $cat ) : if ( $cat->parent == $runway->term_id ) : $the_cat = $cat->name; $cat_id = $cat->term_id; $cat_slug = $cat->slug; break; endif; endforeach;

?>

<section class="runway--overlay">

  <div class="overlay--inner container">

    <div class="overlay--close" tabindex="0">
      <span><?php echo __('Back to runway', 'vogue.me'); ?></span>
      <img src="<?php echo $asset_url; ?>va-close-white.svg">
    </div>
    <div class="runway--logo">
      <a href="<?php echo get_bloginfo( 'url' ); ?>">
        <img src="<?php echo $asset_url; ?>logo-white.svg">
      </a>
    </div>

    <div class="gallery-details">
      <div class="gallery-details__content gallery-details__content--titles">
        <?php if ( get_field( 'collection_title', $gallery_id ) || get_field( 'designer', $gallery_id ) ) : ?>
          <span class="gallery-details__sub-title"><?php echo get_field( 'collection_title', $gallery_id ); ?></span>
          <h2 class="gallery-details__title"><?php echo get_field( 'designer', $gallery_id ); ?></h2>
        <?php else : ?>
          <span class="gallery-details__sub-title"><?php echo $the_cat; ?></span>
          <h2 class="gallery-details__title"><?php echo the_title(); ?></h2>
        <?php endif; ?>
      </div>
      <div class="gallery-details__content">
        <p class="gallery-details__count"><span><?php echo __('Look','vogue.me'); ?> <span class="current--slide">1</span></span> / <span class="js-total-count"></span></p>
      </div>
      <div class="gallery-details__content">
        <p class="gallery-details__model"><?php echo __('Model','vogue.me'); ?>: <span class="js-update-model"></span></p>
        <p class="nav--author"><?php echo __( 'Credits', 'vogue.me' ); ?>: <span class="js-update-credits"></span></p>
        <div class="item--navigation" data-position="1">
          <ul class="nav--list"></ul>
        </div>
        <div class="toggle-thumbs">
        <button type="button" class="js-grid-toggle"><?php echo __('All images', 'vogue.me'); ?></button>
        </div>
        <!--<div style="margin-top: 15px;">
          <select name="selectGallery">
            <?php foreach ($combo_linked_gallery as $key => $value) { ?>
              <option value="<?= $key ?>"><?= $value ?></option>
            <?php } ?>
          </select>
        </div> -->
      </div>
    </div>

    <div class="runway--col1 js-runway--col1">
      <a href="#" class="runway--col1__close js-runway--col1__close">
        <img src="<?php echo $asset_url; ?>va-close-white.svg">
      </a>
      <div class="runway--col1__inner">
        <ul class="thumbs js-thumbs">

<?php $i = 1; foreach ( $runway_gallery as $item ) : ?>

  <?php if ( isset( $item->pictures ) ) : $pictures = $item->pictures; $path = $item->path; ?>

    <?php $count = array();
          $count[] = count( $pictures );
          $p = 1;

          foreach ( $pictures as $picture ) : $image = get_bloginfo( 'url' ) . $path . '/' . $picture->thumb; ?>
              <li>
                <a href="#image-<?php echo $p; ?>">
                  <div class="overlay"><span><?php echo sprintf( "%02d", $p ); ?></span></div>
                  <img src="<?php echo $image; ?>" alt="">
                </a>
              </li>
          <?php $p++; endforeach; ?>
            <?php foreach ( $count as $s=>$value ) : $set += $value; endforeach; ?>
          <?php else : ?>
              <li>
                <a href="#image-<?php echo $i; ?>">
                  <div class="overlay"><span><?php echo sprintf( "%02d", $i ); ?></span></div>
                  <img data-echo="<?php echo $item[ 'image' ]['sizes']['medium']; ?>" src="<?php bloginfo('template_url'); ?>/assets/images/runway-placeholder.png" alt="">
                </a>
              </li>
            <?php endif; ?>
          <?php $i++; endforeach; ?>
          <?php if ( $set > 0 ) : $count = $set; else : $count = count( $runway_gallery ); endif; ?>

            <li class="rw-next-slide rw-next-slide__small"> <a href="#"> <div class="overlay"><span><?php echo __('Next','vogue.me'); ?></span></div> <img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/runway-placeholder.png" alt=""> </a> </li>
          </ul>

        </div>
      </div>

        <div class="runway--col2">

          <header class="mobile-head">
            <a class="overlay--thumbtoggle js-toggle-search js-search" href="#">
              <img src="<?php echo $asset_url; ?>va-search-white.svg">
            </a>
            <a href="#">
              <img src="<?php echo $asset_url; ?>logo-white.svg">
            </a>
            <a class="overlay--thumbtoggle js-toggle-side-nav" href="#">
              <img src="<?php echo $asset_url; ?>va-menu-white.svg">
            </a>
          </header>

          <div class="gallery--main">
            <div class="mobile-details mobile-details--top">
              <p class="gallery-details__count"><span class="current--slide">1</span> / <span class="js-total-count"></span></p>
              <p class="gallery-details__model"><?php echo __('Model','vogue.me'); ?>: <span class="js-update-model"></span></p>
              <div>
                <a class="overlay--thumbtoggle js-grid-toggle" href="#">
                  <img src="<?php echo $asset_url; ?>thumb-grid.svg">
                </a>
                <a class="overlay--thumbtoggle gallery-close-button" href="<?php if ( get_the_content() ) : echo '#'; else : echo get_term_link($cat_slug,'category'); endif; ?>">
                  <img src="<?php echo $asset_url; ?>va-close-white.svg">
                </a>
              </div>
            </div>

            <ul class="gallery--container">
            <?php $i = 1; foreach ( $runway_gallery as $item ) : ?>

            <?php if ( isset( $item->pictures ) ) : $pictures = $item->pictures; $path = $item->path; ?>

              <?php $count = array();
                    $count[] = count( $pictures );
                    $p = 1;

                    foreach ( $pictures as $picture ) :

                      $image = get_bloginfo( 'url' ) . $path . '/' . $picture->thumb;
                    ?>
                        <li> <img class="loaded--img" src="<?php echo $image; ?>" alt=""> </li>
                    <?php $p++; endforeach; ?>

                        <?php foreach ( $count as $s=>$value ) : $set += $value; endforeach; ?>
                    <?php else : ?>

                        <?php $image = $item[ 'image' ]['sizes']['medium_large']; ?>
                        <?php $medium = $item[ 'image' ]['sizes']['medium']; ?>
                        <?php $large = $item[ 'image' ]['sizes']['large']; ?>

                        <?php //if ( isset($item[ 'image' ]['sizes']['full']) ) : ?>
                          <?php //$zoom = $item[ 'image' ]['sizes']['full']; ?>
                        <?php //elseif ( isset($item[ 'image' ]['sizes']['large']) ) : ?>
                          <?php //$zoom = $item[ 'image' ]['sizes']['large']; ?>
                        <?php //else : ?>
                          <?php $zoom = $item['image']['url']; ?>
                        <?php //endif; ?>

                        <?php //var_dump($item); ?>

                        <li class="js-include">
                            <span class="loaded--img--wrapper">
                              <button class="runway-zoom-btn js-has-zoom" data-src="<?php echo $zoom; ?>"></button>

                              <picture>
                                <!--<source media="(min-width:1024px)" srcset="<?php echo $image; ?>">
                                <source media="(min-width:600px)" srcset="<?php echo $medium; ?>">-->
                                <img class="loaded--img" data-lazy="<?php echo $image; ?>" alt="">
                              </picture>

                            </span>
                            <div style="display: none;">
                              <?php if ( $item['model_vogue_collection'] ) : ?>
                                <span class="js-get-model"><?php $model = get_term( $item['model_vogue_collection'], 'vogue_collection' ); ?>
                                  <a href="<?php echo get_term_link($model->term_id); ?>"><?php echo $model->name; ?></a>
                                </span>
                              <?php endif; ?>
                              <?php if ( $item['photo_credit'] ) : ?>
                                <span class="js-get-credit"><?php echo $item['photo_credit']; ?></span>
                              <?php endif; ?>
                            </div>
                          </li>
                      <?php endif; ?>
                    <?php $i++; endforeach; ?>
                    <?php if ( $set > 0 ) : $count = $set; else : $count = count( $runway_gallery ); endif; ?>

                    <li class="rw-next-slide">

                      <?php

                        $cat_args = array( 'post_type' => array( 'post','legacy' ), 'fields' => 'ids', 'category' => $cat_id, 'exclude' => array(get_the_ID()), 'orderby' => 'rand', 'posts_per_page' => 3 );

                        $runway_categories = get_posts( $cat_args );

                      ?>

                      <div class="rw-next-slide__inner">
                        <div class="rw-next-slide__top">
                          <h2 class="rw-next-slide__heading__main"> <?php echo the_title(); ?> </h2>

                          <div class="rw-next-slide__social">
                            <ul class="rw-next-slide__social">
                              <li> <a href="http://twitter.com" class="fa fa-twitter"></a> </li>
                              <li> <a href="http://twitter.com" class="fa fa-facebook"></a> </li>
                              <li> <a href="http://twitter.com" class="fa fa-pinterest"></a> </li>
                            </ul>
                          </div>

                          <div class="rw-next-slide__actions">
                            <a href="#image-1" class="button border white js-start-over"><?php echo __('Start Over', 'vogue.me'); ?></a> <a href="<?php echo get_permalink( $runway_categories[0] ); ?>#/gallery/" class="button border white"><?php echo __( 'Next Show', 'vogue.me' ); ?></a>
                          </div>
                        </div>
                        <div class="rw-next-slide__listings">

                          <h2 class="rw-next-slide__heading"> <span><?php echo __( 'Latest shows' ); ?></span> </h2>
                          <h3 class="rw-next-slide__category"><?php echo $the_cat; ?></h3>
                          <div class="rw-next-slide__list-wrapper">
                            <ul class="rw-next-slide__list">
                            <?php foreach ( $runway_categories as $post ) : setup_postdata($post); $gallery_id = $post ?>

                            <?php if ( get_field( 'linked_gallery' ) && get_post_type() !== 'legacy' ) : $gallery = get_field( 'linked_gallery' ); $gallery_id = $gallery->ID; endif; $thumbnail = getThumbnail( $gallery_id )[0]; ?>

                              <li class="rw-next-item">
                                <div class="rw-next-item__inner">
                                  <a class="rw-next-item__bg" href="<?php echo get_permalink(); ?>#/gallery/" style="background-image: url('<?php echo $thumbnail; ?>'); ?>">
                                  </a>
                                  <div class="rw-next-item__content">
                                    <a href="<?php echo get_permalink(); ?>#/gallery/">
                                      <?php the_title(); ?>
                                    </a>
                                  </div>
                                </div>
                              </li>
                            <?php endforeach; wp_reset_query(); wp_reset_postdata(); ?>
                            </ul>
                          </div>

                        </div>


                      </div>

                    </li>

                  </ul>

            <!-- //MOBILE CONTENT -->

            <!-- MOBILE CONTENT -->
            <div class="mobile-details mobile-details--bottom">
             <div class="gallery-details__titles">
                 <span class="gallery-details__sub-title"><?php echo $the_cat; ?></span>
                 <h2 class="gallery-details__title"><?php echo the_title(); ?></h2>
             </div>
             <div class="gallery-details__social">
               <ul>
                 <li><a href="#" class="fa fa-twitter"></a></li>
                 <li><a href="#" class="fa fa-facebook"></a></li>
                 <li><a href="#" class="fa fa-pinterest"></a></li>
               </ul>
             </div>
           </div>

          </div>
        </div>

        <div class="runway-vert" id="main_youcantblock_runway" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">
          <?php getAdvert( 'vert' ); ?>
        </div>

      </div>
    </section>

