<?php 

  get_header();

  while ( have_posts() ) : the_post();

?>

<div class="scroll" data-ui="jscroll-default" data-continue="true">

  <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
  <div class="get--content single-outer js-change-history" data-title="<?php echo the_title(); ?>" data-url="<?php echo get_permalink(); ?>" data-set_id="<?php echo get_the_ID(); ?>" id="main_youcantblock" data-desktop-page-id="<?php echo page_ads('DESK','pageid'); ?>" data-mobile-page-id="<?php echo page_ads('MOB','pageid'); ?>">

  <?php vogue_increment_view_count(get_the_ID()); ?> 
  
  <?php //if ( !is_null(get_field( 'vogue_view_count', get_the_ID() )) ) : ?>

    <?php //$stored = (int) get_field( 'vogue_view_count', get_the_ID() ); ?>

<!--    <form class="form" method="post" class="update_page_view_count" style="display: none !important;"> -->
      <?php // $args = array( 'post_id' => get_the_ID(), 'form' => false, 'fields' => array('field_57f2004b3cd98'), 'submit_value' => '' ); acf_form( $args );  ?>
<!--    </form> -->
  <?php //endif; ?>

  <?php if ( in_category( array( 'video_ar', 'video', 'fpvideos' ) ) ) : ?>

  <span class="single--video">

    <?php echo get_template_part( 'in-category-video' ); ?>

  </span>

  <?php else : ?>

    <?php echo get_template_part( 'current' ); ?>

    <?php if ( in_category( getRunwayCats() ) || get_field( 'gallery_type' ) == 'Runway' ) : echo get_template_part( 'in-category-runway' ); endif; ?>

  <?php endif; ?>

  </div>
  <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

</div>

<?php if ( in_category( array( 'video_ar', 'video', 'fpvideos' ) ) ) : $paged = getPaged(); $video_posts = getQuery__VideoPosts(); ?>


<?php //if ( CLEAR_QUERY_CACHE || CLEAR_QUERY_CACHE !== false ) : delete_transient( 'video_postlist-'.$paged ); endif; if ( false === ( $video_postlist = get_transient( 'video_postlist-'.$paged ) ) ) {  
	$video_postlist = get_posts($video_posts); //set_transient( 'video_postlist-'.$paged, $video_postlist, 12 * 60 * 60 ); } ?>

<?php if ( $video_postlist ) : ?>

  <section class="video-post-feed video-post-feed--single">

    <div class="container container--mid--reduced">

      <h3 class="section--header auto-fade-in"> <?php echo __( 'Recent Videos', 'vogue.me' ); ?> </h3>

      <div class="video-posts">

        <div class="scroll" data-ui="jscroll-default" data-continue="false">

          <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
          <div class="get--content" data-url="<?php echo get_permalink(); ?>" data-title="Video Archive">

<?php $i = 1; foreach ( $video_postlist as $post ) : setup_postdata( $post ); ?>

  <?php if ( $i % 4 == 1 ) : ?> <div class="row"> <?php endif; ?>

          <?php $thumbnail = getThumbnail()[0]; ?>

            <article class="video-post js-video-post">
              <a href="<?php echo get_permalink(); ?>">
                <div class="video-post__thumb auto-fade-in" style="background-image: url('<?php echo $thumbnail; ?>');"></div>
                <div class="video-post__content">
                  <h2><?php the_title(); ?></h2>
                  <?php getKeyTopic(); ?>
                </div>
              </a> 
            </article>

  <?php if ( $i % 4 == 0 ) : ?> </div> <?php endif; ?>

<?php $i++; endforeach; wp_reset_postdata(); ?>

<?php if ( $i % 4 != 1 ) : ?> </div> <?php endif; ?>

          </div>
          <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

        </div>

      </div>

    </div>

  </section>

<?php endif; ?>

<?php endif; ?>

<?php endwhile; ?>

<?php get_footer(); ?>
