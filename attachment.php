<?php get_header(); the_post(); ?>

<style>
.div
{
  height: calc( 100vh - 80px );margin-top:20px;margin-right: auto;margin-left:auto;width:100%;max-width:1024px;
}
img
{
  display: block;
  height: auto;
  width: auto;
  margin-right: auto;
  margin-left: auto;
  max-height: 100vh;
  max-width: calc(100% - 20px );
}
</style>

<div class="scroll" data-ui="jscroll-default">

  <!-- PAGE CONTENT START [Important! Do NOT remove this comment] -->
  <div class="get--content">
<div class="div">
<?php echo wp_get_attachment_image( get_the_ID(), 'thumbnail' ); ?>
</div>
  </div>
  <!-- PAGE CONTENT END [Important! Do NOT remove this comment] -->

</div>

<?php get_footer(); ?>

