'use strict';

var gulp = require('gulp');

gulp.task('serve', ['sass', 'js'], function() {
	
	gulp.watch('assets/sass/**/*.scss', ['sass']);
	gulp.watch('assets/javascript/**/*.js', ['js']);
});