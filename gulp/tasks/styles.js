'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var compass = require('gulp-compass');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var rename = require("gulp-rename");

gulp.task('sass', function() {
    gulp.src('assets/sass/main.scss')
        .pipe(compass({
            css: 'build',
            sass: 'assets/sass',
            comments: false,
        }).on('error', gutil.log)) 
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))  
        .pipe(minifycss())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('build/'))
});